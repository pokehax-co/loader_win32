/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */
 
/*
 * This "DLL" is used to generate an import library for functions exported by
 * NTDLL. Compiling a DLL and creating an import library during the linking
 * process is the only reliable way in MSVC to generate import libraries.
 *
 * Function definitions for NTDLL functions can be extracted from the ReactOS
 * project (https://www.reactos.org/).
 */


#define WIN32_LEAN_AND_MEAN
#include <windows.h>


/*
 * Dummy startup code so we don't need to link against the C runtime
 */

BOOL WINAPI _DllMainCRTStartup(HINSTANCE a, DWORD b, LPVOID c)
{
	return TRUE;
}

/*
 * NT heap functions
 */

typedef struct _RTL_HEAP_PARAMETERS RTL_HEAP_PARAMETERS,
    *PRTL_HEAP_PARAMETERS;

__declspec(dllexport) HANDLE NTAPI RtlCreateHeap(ULONG Flags, PVOID Addr,
	SIZE_T TotalSize, SIZE_T CommitSize, PVOID Lock,
	PRTL_HEAP_PARAMETERS Parameters)
{
	return 0;
}

__declspec(dllexport) PVOID NTAPI RtlAllocateHeap(PVOID HeapPtr, ULONG Flags,
	SIZE_T Size)
{
	return 0;
}

__declspec(dllexport) PVOID NTAPI RtlReAllocateHeap(PVOID HeapPtr, ULONG Flags,
	PVOID Ptr, SIZE_T Size)
{
	return 0;
}

__declspec(dllexport) BOOLEAN NTAPI RtlFreeHeap(HANDLE HeapPtr, LONG Flags,
	PVOID Ptr)
{
	return 0;
}

__declspec(dllexport) HANDLE NTAPI RtlDestroyHeap(HANDLE HeapPtr)
{
	return 0;
}

__declspec(dllexport) SIZE_T NTAPI RtlSizeHeap(HANDLE HeapPtr, ULONG Flags,
	PVOID Ptr)
{
	return 0;
}
