# loader_win32

This application allows to run NT 3.51's PowerPC compiler (internally called "C8.5") and linker (2.60.5512) on 32-bit x86 systems. They can be found in early releases of the Win32 SDK for Windows NT 3.51 and Windows 95 under the `mstools/bin/winnt/ppc/` directory.

The following components are present in the loader:

 - PowerPC emulator with no privileged instructions and mediocre FPU support derived from the [Dolphin emulator](https://dolphin-emu.org/)
 - PE executable parsing and loading (no support for relocations)
 - Address space simulation
 - Simulated ADVAPI32, CRTDLL, KERNEL32, and NTDLL functions through system calls
 - Heaps with fixed size of 32 MiB (relying on the host OS' heap implementation)

The integrated CRTDLL simulation has quality issues and does not provide all required functions. Copy the PowerPC version of CRTDLL.dll from the Windows NT 3.51 CD-ROM into the same directory as the PowerPC executables you want to run.

The following features of the PowerPC NT linker (it supports all four NT architectures!) have been tested:

 - Help output (`link /?`)
 - Linking simple x86 and MIPS applications
 - Dumpbin (`link /dump`) entire x86 and MIPS applications
 - Lib (`link /lib`) for creating x86 import libraries
 - Lib.exe wrapper which spawns a new loader process

The following features of the PowerPC NT compiler have been tested:

 - Help output (`cl /?`)
 - Compiling a very simple C file with `/O2` and without optimization

Operating system compatibility:

 - Wine (3.16 tested)
 - Windows XP
 - Windows 2000
 - Windows 95, 98, ME will **NOT** be supported

## License

MIT except for the supported FPU instructions, which are GPL2 (so the resulting application is GPL2).

## Building

loader_win32 can be compiled using MinGW-w64 and Visual Studio.

### Via Visual Studio

Requirements:

- Visual Studio 2008 or .NET 2003 (in either case with Service Pack 1)
- Static build of the [Capstone](https://github.com/aquynh/capstone) disassembler, using the patches in the `capstone/` directory

Release build:

```sh
nmake
```

Builds using Visual Studio will require the corresponding C runtime (`msvcrXX.dll`).

### Using MinGW-w64

Requirements:

- MinGW-w64 targeting 32-bit Windows (`i686-w64-mingw32`)
- [Capstone](https://github.com/aquynh/capstone) disassembler built for MinGW-w64 (for debug builds only)

Release build:

```sh
make
```

Debug build:

```sh
make DEBUG=1
```

Use the `-j&lt;cores&gt;` switch to compile it faster -- `windows.h` is a huge beast.

Builds using MinGW-w64 will require the Visual C++ 6 runtime (`msvcrt.dll`), which is a OS component since Windows 2000.

## Legal issues

The ADVAPI32 simulation contains registration information to bypass the timebomb. I'm not going to call Motorola to register software which nobody has run for over 20 years now. Disable the `CONFIG_ACTIVATE_C8_5` option in `src/config.h` if you can't stand this.

## This project sucks!

I agree with this, because I would like to run the actual PowerPC port of Windows NT 3.51 instead.

While QEMU supports a PowerPC NT-compatible machine (IBM RS/6000 40p), it cannot perform endianness switching, as Windows NT can only run in little-endian mode (see [https://lists.gnu.org/archive/html/qemu-devel/2015-06/msg02915.html]). eBay (in Europe) has PowerPC machines, but they're too new to run Windows (released in 1997 or later) and too expensive from my perspective.
