#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

.SUFFIXES: .cpp .c

CFLAGS_base=/MD /W3
CXXFLAGS_base=/MD /W3 /GR- /GX-
LDFLAGS_base=/machine:ix86 /subsystem:console /opt:nowin98
LIBS=advapi32.lib ntdll\ntdll.lib

!if "$(DEBUG)" == "1"
CFLAGS=$(CFLAGS_base) /Od /D_DEBUG
CXXFLAGS=$(CXXFLAGS_base) /Od /D_DEBUG
LDFLAGS=$(LDFLAGS_base) /debug
!else
CFLAGS=$(CFLAGS_base) /Og /Oy /GF /DNDEBUG
CXXFLAGS=$(CXXFLAGS_base) /Og /Oy /GF /DNDEBUG
LDFLAGS=$(LDFLAGS_base) /opt:icf /opt:ref /incremental:no
!endif

OBJS=\
    src\exefmt\pe.obj \
    src\exefmt\pestruct.obj \
    src\hw\cpu.obj \
    src\hw\powerpc.obj \
    src\hw\ppc_fpu.obj \
    src\hw\ppc_impl.obj \
    src\misc\log.obj \
    src\misc\tools.obj \
    src\os\advapi32.obj \
    src\os\crtdll.obj \
    src\os\kernel32.obj \
    src\os\ntdll.obj \
    src\os\ntppc.obj \
    src\os\printf.obj \
    src\os\win32.obj \
    src\os\win32mod.obj \
    src\portab\cxxsup.obj \
    src\portab\getopt.obj \
    src\portab\getopt_long.obj \
    src\portab\swprintf.obj \
    src\process\virtmem.obj \
    src\loader.obj


all: loader_win32.exe
clean:
	-del loader_win32.exe
	-del $(OBJS)
	-del ntdll\*.obj ntdll\*.dll ntdll\*.lib ntdll\*.exp

loader_win32.exe: $(OBJS)
	link /nologo $(LDFLAGS) /out:$@ $** $(LIBS)

loader_win32.exe: ntdll\ntdll.lib

ntdll\ntdll.dll ntdll\ntdll.lib: ntdll\ntdll.obj
	link /nologo $(LDFLAGS) /dll /def:ntdll\ntdll.def /implib:ntdll\ntdll.lib /out:ntdll\ntdll.dll $**

.cpp.obj:
	cl /nologo /TP /c $(CXXFLAGS) /Fo$@ $<

.c.obj:
	cl /nologo /TC /c $(CXXFLAGS) /Fo$@ $<
