/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef PORTAB_PORTAB_H
#define PORTAB_PORTAB_H

/*!
 * \file src/portab/portab.h
 * \brief Macros for compiler-specific features.
 */

/*
 * Visual C++ (exclude clang-cl)
 */
#if defined(_MSC_VER) && !defined(__clang__)

/* These GCC-spefific functions are not supported */
#define GNU_UNREACHABLE()
#define GNU_PRINTF_ATTRIBUTE(fmt, args)
#define LIKELY(x) (x)
#define UNLIKELY(x) (x)


/*
 * GCC (exclude clang)
 */
#elif defined(__GNUC__) && !defined(__clang__)

/* Replace MSVC-specific keywords with their standardized counterpart */
#define __inline inline
#define __int64 long long

/* Macro to mark a code path as unreachable */
#define GNU_UNREACHABLE() __builtin_unreachable()

/* Attribute to allow printf() format checking */
#define GNU_PRINTF_ATTRIBUTE(fmt, args) \
	__attribute__ ((format(printf, fmt, args)))

/* Branch prediction */
#define LIKELY(x) __builtin_expect((x), 1)
#define UNLIKELY(x) __builtin_expect((x), 0)


/*
 * No other compilers supported
 */
#else
#error Unsupported compiler
#endif

#endif
