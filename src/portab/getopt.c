/*
 * Copyright (c) 2011-2018 Rich Felker <dalias@aerifal.cx>
 * Copyright (c) 2013 Isaac Dunham <idunham@lavabit.com>
 * Copyright (c) 2014 Gianluca Anzolin <gianluca@sottospazio.it>
 * Copyright (c) 2014 Felix Fietkau <nbd@openwrt.org>
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

/*
 * This is the getopt() implementation of the musl C library, intended
 * for compatibility with Visual C++. GCC builds use the getopt()
 * implementation of the operating system's C library.
 *
 * The following changes have been made:
 *  - Removed call to musl's own __lctrans_cur
 *  - Removed <unistd.h> and added <stdio.h>
 *  - Removed weak aliases
 *  - Use _lock_file() and _unlock_file() on Windows
 */

#ifndef __GNUC__
#include <wchar.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

char *optarg;
int optind=1, opterr=1, optopt, __optpos, __optreset=0;

#define optpos __optpos

#ifdef _WIN32
#ifdef _MSC_VER
#if _MSC_VER > 1400
#define flockfile(f) _lock_file(f)
#define funlockfile(f) _unlock_file(f)
#else
/* File locking not supported before VS 2005 */
#define flockfile(f)
#define funlockfile(f)
#endif
#else
/* File locking not supported on MinGW with msvcrt.dll before Vista */
#define flockfile(f)
#define funlockfile(f)
#endif
#endif

void __getopt_msg(const char *a, const char *b, const char *c, size_t l)
{
	FILE *f = stderr;
	flockfile(f);
	fputs(a, f)>=0
	&& fwrite(b, strlen(b), 1, f)
	&& fwrite(c, 1, l, f)==l
	&& putc('\n', f);
	funlockfile(f);
}

int getopt(int argc, char * const argv[], const char *optstring)
{
	int i;
	wchar_t c, d;
	int k, l;
	char *optchar;

	if (!optind || __optreset) {
		__optreset = 0;
		__optpos = 0;
		optind = 1;
	}

	if (optind >= argc || !argv[optind])
		return -1;

	if (argv[optind][0] != '-') {
		if (optstring[0] == '-') {
			optarg = argv[optind++];
			return 1;
		}
		return -1;
	}

	if (!argv[optind][1])
		return -1;

	if (argv[optind][1] == '-' && !argv[optind][2])
		return optind++, -1;

	if (!optpos) optpos++;
	if ((k = mbtowc(&c, argv[optind]+optpos, MB_LEN_MAX)) < 0) {
		k = 1;
		c = 0xfffd; /* replacement char */
	}
	optchar = argv[optind]+optpos;
	optpos += k;

	if (!argv[optind][optpos]) {
		optind++;
		optpos = 0;
	}

	if (optstring[0] == '-' || optstring[0] == '+')
		optstring++;

	i = 0;
	d = 0;
	do {
		l = mbtowc(&d, optstring+i, MB_LEN_MAX);
		if (l>0) i+=l; else i++;
	} while (l && d != c);

	if (d != c || c == ':') {
		optopt = c;
		if (optstring[0] != ':' && opterr)
			__getopt_msg(argv[0], ": unrecognized option: ", optchar, k);
		return '?';
	}
	if (optstring[i] == ':') {
		optarg = 0;
		if (optstring[i+1] != ':' || optpos) {
			optarg = argv[optind++] + optpos;
			optpos = 0;
		}
		if (optind > argc) {
			optopt = c;
			if (optstring[0] == ':') return ':';
			if (opterr) __getopt_msg(argv[0],
				": option requires an argument: ",
				optchar, k);
			return '?';
		}
	}
	return c;
}
#endif
