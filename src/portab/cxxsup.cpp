/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

/*
 * Provides the 'new' and 'delete' operators and dummy functions for GCC's
 * thread-safe static variable initialization (making it unsafe).
 */

#include "../config.h"
#include "../portab/portab.h"

#include <stdlib.h>


void *operator new(size_t count)
{
    void *const ret = ::malloc(count);
    if (UNLIKELY(ret == NULL))
        abort();
    return ret;
}

void *operator new[](size_t count)
{
    void *const ret = ::malloc(count);
    if (UNLIKELY(ret == NULL))
        abort();
    return ret;
}

void operator delete(void *ptr)
{
    if (LIKELY(ptr != NULL))
        free(ptr);
}

void operator delete[](void *ptr)
{
    if (LIKELY(ptr != NULL))
        free(ptr);
}


#ifdef __GNUC__
namespace __cxxabiv1 {
extern "C" {

/* effectively int64_t */
__extension__ typedef int __guard __attribute__((mode (__DI__)));

int __cxa_guard_acquire(__guard *g)
{
    /* no thread safety */
    (void)g;
    return 1;
}

void __cxa_guard_release(__guard *g)
{
    /* no thread safety */
    (void)g;
}

}
}
#endif
