/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef PORTAB_STDINT_H
#define PORTAB_STDINT_H

/*!
 * \file src/portab/stdint.h
 * \brief stdint.h replacement for older compilers
 */

/*
 * Determine which compilers have it natively
 */
#if defined(__GNUC__) || defined(__clang__)

#define PORTAB_HAVE_STDINT_H__

#elif defined(_MSC_VER) && !defined(__clang__)

/* from Visual C++ 2010 */
#if _MSC_VER >= 1600
#define PORTAB_HAVE_STDINT_H__
#endif

#else

#warning Unknown compiler

#endif

/*
 * Add it natively
 */
#ifdef PORTAB_HAVE_STDINT_H__

#include <stdint.h>

/*
 * Provide our own types (MSVC)
 */
#elif defined(_MSC_VER)

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned __int8 uint8_t;
typedef signed __int8 int8_t;
typedef unsigned __int16 uint16_t;
typedef signed __int16 int16_t;
typedef unsigned __int32 uint32_t;
typedef signed __int32 int32_t;
typedef unsigned __int64 uint64_t;
typedef signed __int64 int64_t;

/* Limits */
#define INT8_MIN (-128)
#define INT16_MIN (-32768)
#define INT32_MIN (-2147483647 - 1)
#define INT64_MIN (-9223372036854775807LL - 1)

#define INT8_MAX 127
#define INT16_MAX 32767
#define INT32_MAX 2147483647
#define INT64_MAX 9223372036854775807i64

#define UINT8_MAX 255
#define UINT16_MAX 65535
#define UINT32_MAX 0xffffffffu  /* 4294967295u */
#define UINT64_MAX 0xffffffffffffffffui64 /* 18446744073709551615ui64 */

#ifdef __cplusplus
}
#endif

#else

#error Unknown compiler

#endif

#endif
