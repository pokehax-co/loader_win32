/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

/*!
 * \file src/portab/swprintf.c
 * \brief Harmonizes swprintf() across C libraries by exposing the C99 version
 *        with an additional "maxlen" parameter.
 */

#include "../config.h"
#include "swprintf.h"

#include <wchar.h>


/*
 * Public functions
 */
int tools_vswprintf(wchar_t *wcs, size_t maxlen, const wchar_t *format,
    va_list args)
{
#ifdef _MSC_VER
#if _MSC_VER < 1400
    (void)maxlen;
    return vswprintf(wcs, format, args);
#else
    (void)maxlen;
    return _vswprintf(wcs, format, args);
#endif
#elif defined(__GNUC__)
    /* MinGW is standards-compliant */
    return vswprintf(wcs, maxlen, format, args);
#else
#error unsupported compiler
#endif
}
