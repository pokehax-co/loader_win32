/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef MISC_TOOLS_SWPRINTF_H
#define MISC_TOOLS_SWPRINTF_H

/*!
 * \file src/portab/swprintf.h
 * \brief Harmonizes swprintf() across C libraries by exposing the C99 version
 *        with an additional "maxlen" parameter.
 */

#include <stdarg.h>
#include <wchar.h>


#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \fn tools_vswprintf
 * \brief
 *    Formats a wide character string.
 * \param wcs
 *    Wide character buffer to write string into
 * \param maxlen
 *    Size of the wcs buffer
 * \param format
 *    String to format
 * \param args
 *    Variadic arguments according to format
 * \returns
 *    Number of characters written into wcs. Visual C++ runtime may return -1
 *    if it overflows.
 */
int tools_vswprintf(wchar_t *wcs, size_t maxlen, const wchar_t *format,
    va_list args);

/*!
 * \fn tools_swprintf
 * \brief
 *    Formats a wide character string.
 * \param wcs
 *    Wide character buffer to write string into
 * \param maxlen
 *    Size of the wcs buffer
 * \param format
 *    String to format
 * \param ...
 *    Variadic arguments according to format
 * \returns
 *    Number of characters written into wcs. Visual C++ runtime may return -1
 *    if it overflows.
 */
static __inline int tools_swprintf(wchar_t *wcs, size_t maxlen,
	const wchar_t *format, ...)
{
	int ret;
	va_list args;
	va_start(args, format);
	ret = tools_vswprintf(wcs, maxlen, format, args);
	va_end(args);
	return ret;
}

#ifdef __cplusplus
}
#endif

#endif
