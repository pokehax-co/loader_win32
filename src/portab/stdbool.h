/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef PORTAB_STDBOOL_H
#define PORTAB_STDBOOL_H

/*!
 * \file src/portab/stdbool.h
 * \brief stdbool.h replacement for older compilers
 */

/*
 * Determine which compilers have it natively
 */
#if defined(__cplusplus)

/* older Visual C++ don't have bool */
#if __cplusplus >= 199711L
#define PORTAB_HAVE_NATIVE_BOOL__
#endif

#elif defined(__GNUC__) || defined(__clang__)

#define PORTAB_HAVE_STDBOOL_H__

#elif defined(_MSC_VER) && !defined(__clang__)

/* from Visual C++ 2013 */
#if _MSC_VER >= 1800
#define PORTAB_HAVE_STDBOOL_H__
#endif

#else

#warning Unknown compiler

#endif


/*
 * Add it natively
 */
#ifdef PORTAB_HAVE_NATIVE_BOOL__
#elif defined(PORTAB_HAVE_STDBOOL_H__)

#include <stdbool.h>

/*
 * Provide our own types (MSVC)
 */
#elif defined(_MSC_VER)

#ifdef __cplusplus
extern "C" {
#endif

/* bool was an int before Visual C++ 5 */
#if _MSC_VER < 1100
typedef int bool;
#else
typedef char bool;
#endif

#define false 0
#define true 1

#ifdef __cplusplus
}
#endif

#else

#error Unknown compiler

#endif

#endif
