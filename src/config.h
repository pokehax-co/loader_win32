/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef CONFIG_H
#define CONFIG_H

/*!
 * \file src/config.h
 * \brief Compile-time configuration and compatibility options.
 */

/*
 * Debugging options (only for debug builds)
 */
#ifndef NDEBUG

/* process/virtmem: check whether the memory allocations are kept sorted */
//#define DEBUG_VIRTUAL_MEMORY_CHECK_CORRUPTION

/* process/virtmem: enable listing of all memory allocations */
#define DEBUG_VIRTUAL_MEMORY_SHOW_ALLOCATIONS

/* process/virtmem: enable dumping of memory */
#define DEBUG_VIRTUAL_MEMORY_DUMP

/* loader: show headers of PE executable */
//#define DEBUG_SHOW_EXECUTABLE_HEADERS

/* exefmt/pestruct: print structures */
//#define DEBUG_PE_PRINT_STRUCTURES

/* exefmt/pe: print imports while walking import table */
//#define DEBUG_PE_IMPORTS

/* exefmt/pe: print exports while walking export table */
//#define DEBUG_PE_EXPORTS

/* hw/cpu: disassemble instructions as they are execute */
#define DEBUG_DISASSEMBLE

/* hw/cpu: disassembly instructions only if the CPU faults */
#define DEBUG_DISASSEMBLE_ONLY_ON_ERROR

/* hw/powerpc: print which TOC address is read from */
//#define DEBUG_PPC_SHOW_R2_LOADS

/* hw/cpu: print registers if the CPU faults */
#define DEBUG_SHOW_REGISTERS_ON_ERROR

/* os/kernel32: print trace messages from kernel32 simulation */
#define DEBUG_KERNEL32

/* os/ntdll: print trace messages from ntdll simulation */
#define DEBUG_NTDLL

/* os/advapi32: print trace messages from advapi32 simulation */
#define DEBUG_ADVAPI32

/* os/win32: print when native DllMain() routines are called */
//#define DEBUG_DLLMAIN

#endif

/*
 * Program options
 */

/* Use pointer aliasing instead of bitshift operations to convert between
   integers and byte arrays. This might be expensive on architectures without
   unaligned memory accesses. */
#define CONFIG_USE_POINTER_ALIASING

/* Support for floating-point instructions in CPU simulations. */
#define CONFIG_ENABLE_FPU

/* Compile the loader to run 64-bit applications by setting the word size for
   CPU registers and memory address to 64 bits. If this macro is not defined,
   the loader is compiled to run 32-bit applications.
   For best efficiency, the loader itself should be compiled for the same
   word size as the host system. */
//#define CONFIG_LOADER_64BIT

/* Virtualize selected registry accesses to pass a serial number to the
   C8.5 compiler. */
#define CONFIG_ACTIVATE_C8_5

#endif
