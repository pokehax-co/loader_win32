/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef PROCESS_VIRTMEM_H
#define PROCESS_VIRTMEM_H

/*!
 * \file src/process/virtmem.h
 * \brief Simulation of virtual memory.
 */

#include "../iface/memory.h"


#ifdef __cplusplus
extern "C" {
#endif

enum MemoryProtection
{
	MEMORY_PROT_READ = (1u << 0),
	MEMORY_PROT_WRITE = (1u << 1),
	MEMORY_PROT_EXECUTE = (1u << 2),

	/* Memory is allocated by us, not supplied by the caller */
	MEMORY_INTERNAL_ALLOCATION = (1u << 30),
	/* Performing memset (lower bits 8-15 are the value) */
	MEMORY_INTERNAL_MEMSET = (1u << 29),
};

/* Size of one page */
#define MEMORY_PAGE_SIZE ((memory_address_t)0x1000u)

struct VirtualMemoryContext;

struct VirtualMemoryInformation
{
	memory_address_t base_address;
	void *host_pointer;
	size_t allocation_size;
	enum MemoryProtection protection;
};

typedef void (*VirtualMemoryFaultCallback)(struct VirtualMemoryContext *ctx,
   memory_address_t fault_address, size_t fault_size,
   enum MemoryStatus failure_status, enum MemoryProtection failure_op,
   void *callback_user);


/* Creates a new virtual memory allocator. */
int virtual_memory_new(struct VirtualMemoryContext **ctx);

/* Destroys a virtual memory allocator. This will also deallocate all
   anonymous memory allocations. */
int virtual_memory_destroy(struct VirtualMemoryContext **ctx);

/* Sets a callback to be invoked when a memory fault happens. */
// XXX: callback should allow retries of memory accesses
int virtual_memory_set_fault_callback(struct VirtualMemoryContext *ctx,
	VirtualMemoryFaultCallback callback, void *user,
	VirtualMemoryFaultCallback *old_callback, void **old_user);

/* Shows the internal allocation list. */
int virtual_memory_show_allocations(struct VirtualMemoryContext *ctx);

/*!
 * \fn virtual_memory_allocate
 * \brief
 *     Allocates virtual memory.
 * \param ctx
 *     Virtual memory context.
 * \param host_pointer
 *     Pointer to host memory to use as backing memory for the allocation. If
 *     this parameter is NULL, host memory will be allocated internally.
 * \param base_address
 *     Pointer to variable holding address at which memory is to be allocated.
 *     If this variable is set to NULL, a free memory region will be selected
 *     and the allocation address is written back.
 * \param allocation_size
 *     Size of the desired allocation. It will be rounded up to the next page
 *     size.
 * \param protection
 *     Access control for the new allocation.
 * \return
 *     0 on success, negative value on failure
 */
int virtual_memory_allocate(struct VirtualMemoryContext *ctx,
	void *host_pointer, memory_address_t *base_address,
	size_t allocation_size, const enum MemoryProtection protection);

/*!
 * \fn virtual_memory_allocate2
 * \brief
 *     Allocates virtual memory.
 * \param ctx
 *     Virtual memory context.
 * \param host_pointer
 *     Pointer to host memory to use as backing memory for the allocation. If
 *     this parameter is NULL, host memory will be allocated internally.
 * \param base_address
 *     Pointer to variable holding address at which memory is to be allocated.
 *     If this variable is set to NULL, a free memory region will be selected
 *     and the allocation address is written back.
 * \param granularity
 *     If the variable behind base_address is set to NULL, the free memory
 *     region will be rounded up to the specified granularity. The default
 *     granularity of virtual_memory_allocate() is one page.
 * \param allocation_size
 *     Size of the desired allocation. It will be rounded up to the next page
 *     size.
 * \param protection
 *     Access control for the new allocation.
 * \return
 *     0 on success, negative value on failure
 */
int virtual_memory_allocate2(struct VirtualMemoryContext *ctx,
	void *host_pointer, memory_address_t *base_address,
	memory_address_t granularity, size_t allocation_size,
	const enum MemoryProtection protection);

/* Queries information on virtual memory. */
int virtual_memory_query(struct VirtualMemoryContext *ctx,
	const memory_address_t address, struct VirtualMemoryInformation *info);

/* Changes protection of virtual memory. */
int virtual_memory_protect(struct VirtualMemoryContext *ctx,
	const memory_address_t address, const enum MemoryProtection protection);

/* Frees virtual memory. */
int virtual_memory_free(struct VirtualMemoryContext *ctx,
	memory_address_t base_address);


/*
 * Read/Write/Execute functions. These are the same ones as exposed in the
 * virtual memory interface and are used by the CPU emulation and the API
 * function simulations.
 */

enum MemoryStatus virtual_memory_read(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	void *buffer, size_t size);

enum MemoryStatus virtual_memory_read_string(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	char *buffer, size_t *size);
enum MemoryStatus virtual_memory_read_string_alloc(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	char **buffer);

enum MemoryStatus virtual_memory_execute(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	void *buffer, size_t size);

enum MemoryStatus virtual_memory_write(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	const void *buffer, size_t size);

enum MemoryStatus virtual_memory_write_string(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	const char *buffer, size_t size);

/* Copies data internally (like memcpy()/memmove()). */
enum MemoryStatus virtual_memory_copy(struct VirtualMemoryContext *ctx,
	memory_address_t dest, memory_address_t source, size_t size);

/* Sets data internally (like memset()). */
enum MemoryStatus virtual_memory_set(struct VirtualMemoryContext *ctx,
	memory_address_t address, unsigned char value, size_t size);

/* Prints the contents of memory to standard output. */
void virtual_memory_dump(struct VirtualMemoryContext *ctx,
	memory_address_t address, size_t size);


/*
 * Host read/write functions. These bypass memory protections so that for
 * example anonymous allocations can be written to even if they are marked as
 * read-only.
 */

enum MemoryStatus virtual_memory_host_read(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	void *buffer, size_t size);

enum MemoryStatus virtual_memory_host_read_string(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	char *buffer, size_t *size);
enum MemoryStatus virtual_memory_host_read_string_alloc(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	char **buffer);

enum MemoryStatus virtual_memory_host_write(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	const void *buffer, size_t size);


/* Virtual memory interface for the CPU emulation. Use a VirtualMemoryContext
   as context for these functions. */
extern const struct MemoryInterface virtual_memory_interface;


#ifdef __cplusplus
}
#endif

#endif
