/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "virtmem.h"

#include "../portab/stdbool.h"

#if defined(DEBUG_VIRTUAL_MEMORY_SHOW_ALLOCATIONS) || \
	defined(DEBUG_VIRTUAL_MEMORY_CHECK_CORRUPTION) || \
	defined(DEBUG_VIRTUAL_MEMORY_DUMP)
#include <stdio.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <assert.h>


#define _VIRTUAL_MEMORY_MAX_ALLOCS_START 16
#define _VIRTUAL_MEMORY_MAX_ALLOCS_INCREMENT 16


/* Similar to 'struct VirtualMemoryInformation', but saves
   the allocation size as (start + size - 1) to simplify
   internal comparisons. */
struct _VirtualMemoryAlloc
{
	memory_address_t start_address;
	memory_address_t end_address;
	void *host_pointer;
	enum MemoryProtection protection;
};

struct VirtualMemoryContext
{
	size_t num_allocs;
	size_t max_allocs;
	struct _VirtualMemoryAlloc *allocs;
	VirtualMemoryFaultCallback fault_callback;
	void *fault_callback_user;
};


/*
 * Internal functions
 */

/* Platform-specific allocation and free functions */
static void *_host_rw_mmap(void *preferred_addr, size_t size);
static void _host_rw_munmap(void *addr);

static __inline bool _allocations_overlap(
	const struct _VirtualMemoryAlloc *const a,
	const struct _VirtualMemoryAlloc *const b)
{
	return a->start_address <= b->end_address &&
		b->start_address <= a->end_address;
}

static void _free_alloc(struct _VirtualMemoryAlloc *alloc)
{
	if (alloc->protection & MEMORY_INTERNAL_ALLOCATION) {
		_host_rw_munmap(alloc->host_pointer);
		alloc->host_pointer = NULL;
	}
}

static int _query_internal(struct VirtualMemoryContext *ctx,
	const memory_address_t address, size_t *index)
{
	/* One allocation: check if address is inside range */
	if (ctx->num_allocs == 1) {
		if (address >= ctx->allocs[0].start_address &&
				address <= ctx->allocs[0].end_address) {
			*index = 0;
			return 0;
		}
	}

	/* More allocations: find via binary search */
	else if (ctx->num_allocs > 1) {
		size_t left = 0;
		size_t right = ctx->num_allocs - 1;

		while (left <= right) {
			const size_t mid = (left + right) / 2;
			const memory_address_t sa = ctx->allocs[mid].start_address;
			const memory_address_t ea = ctx->allocs[mid].end_address;

			if (address < sa) {
				/* We went to the leftmost entry and didn't find it */
				if (mid == 0)
					break;
				right = mid - 1;
			}
			else if (address > ea) {
				/* We went to the rightmost entry and didn't find it */
				if (mid >= ctx->num_allocs - 1)
					break;
				left = mid + 1;
			}
			else if (address >= sa && address <= ea) {
				*index = mid;
				return 0;
			}
		}
	}

	/* No allocations or address not found */
	return -1;
}

static bool _find_free_address(struct VirtualMemoryContext *ctx,
	size_t requested_size, memory_address_t *found_address,
	const memory_address_t granularity)
{
#define SIZE_FITS_BETWEEN(lower, upper) \
	((lower) + requested_size <= (upper) && \
	 (lower) <= (upper) - requested_size)

	const memory_address_t fixed_lower = 0x10000u;
	const memory_address_t fixed_upper = 0x7FFFFFFFu;
	memory_address_t chosen_address = fixed_lower;

	/* Go through each allocation */
	size_t i;
	for (i = 0; i < ctx->num_allocs; ++i) {
		const struct _VirtualMemoryAlloc *const alloc = &ctx->allocs[i];
		memory_address_t rounded_lower;

		/* First element. Check space before it. */
		if (i == 0) {
			rounded_lower = (fixed_lower + granularity - 1u) &
				~(granularity - 1u);

			if (SIZE_FITS_BETWEEN(rounded_lower, alloc->start_address)) {
				chosen_address = rounded_lower;
				break;
			}
		}

		/* Any other element. Check space between it and predecessor */
		else {
			const struct _VirtualMemoryAlloc *const prev_alloc =
				&ctx->allocs[i - 1];

			rounded_lower = (prev_alloc->end_address + 1u +
				granularity - 1u) & ~(granularity - 1u);

			if (SIZE_FITS_BETWEEN(rounded_lower, alloc->start_address)) {
				chosen_address = rounded_lower;
				break;
			}

			/* Last element. Check space after it. */
			if (i == ctx->num_allocs - 1) {
				rounded_lower = (alloc->end_address + 1u +
					granularity - 1u) & ~(granularity - 1u);

				if (SIZE_FITS_BETWEEN(rounded_lower, fixed_upper + 1u)) {
					chosen_address = rounded_lower;
					break;
				}
			}
		}
	}

	/* Allocation doesn't fit? */
	if (ctx->num_allocs && i >= ctx->num_allocs)
		return false;

	/* We have our address */
	*found_address = chosen_address;
	return true;

#undef SIZE_FITS_BETWEEN
}

static enum MemoryStatus _memory_operation(struct VirtualMemoryContext *ctx,
	memory_address_t address, unsigned char *buffer, size_t size,
	enum MemoryProtection op, bool privileged)
{
	enum MemoryStatus status = MEMORY_STATUS_OK;
	unsigned char value = (op >> 8) & 0xFFu;

	while (size > 0) {
		unsigned char *alloc_address;
		struct VirtualMemoryInformation info;
		memory_address_t offset_in_alloc;
		size_t max_copy;

		/* Query allocation for the current address */
		if (virtual_memory_query(ctx, address, &info) < 0) {
			status = MEMORY_STATUS_NOT_PRESENT;
			goto error;
		}

		/* Page protection adhered to (except for host RW)? */
		if (!(info.protection & op) && !privileged) {
			status = MEMORY_STATUS_PROTECTION;
			goto error;
		}

		/* Determine how much data to copy */
		offset_in_alloc = address - info.base_address;
		max_copy = info.allocation_size - (size_t)offset_in_alloc;
		if (max_copy > size)
			max_copy = size;

		alloc_address = (unsigned char *)(info.host_pointer) +
			offset_in_alloc;

		/* Perform copy operation */
		if (op & MEMORY_INTERNAL_MEMSET)
			memset(alloc_address, value, max_copy);
		else {
			if (op == MEMORY_PROT_WRITE)
				memcpy(alloc_address, buffer, max_copy);
			else
				memcpy(buffer, alloc_address, max_copy);
		}

		/* Update addresses */
		address += max_copy;
		buffer += max_copy;
		size -= max_copy;
	}

error:
	/* Execute fault callback on error */
	if (status != MEMORY_STATUS_OK && !privileged && ctx->fault_callback) {
		ctx->fault_callback(ctx, address, size, status, op,
			ctx->fault_callback_user);
	}
	return status;
}

static enum MemoryStatus _virtual_memory_copy_unchecked(
	struct VirtualMemoryContext *ctx, memory_address_t dest,
	memory_address_t source, size_t size)
{
	enum MemoryStatus status = MEMORY_STATUS_OK;
	struct VirtualMemoryInformation source_info = {0};
	struct VirtualMemoryInformation dest_info = {0};

	while (size > 0) {
		memory_address_t source_offset_in_alloc;
		memory_address_t dest_offset_in_alloc;
		size_t max_copy, source_max_copy, dest_max_copy;
		unsigned char *source_alloc_address;
		unsigned char *dest_alloc_address;

		/* Reload source address info */
		if (!source_info.allocation_size) {
			/* Query allocation for the current source address */
			if (virtual_memory_query(ctx, source, &source_info) < 0) {
				status = MEMORY_STATUS_NOT_PRESENT;
				goto error;
			}

			/* Readable? */
			if (!(source_info.protection & MEMORY_PROT_READ)) {
				status = MEMORY_STATUS_PROTECTION;
				goto error;
			}
		}

		/* Reload destination address info */
		if (!dest_info.allocation_size) {
			/* Query allocation for the current source address */
			if (virtual_memory_query(ctx, dest, &dest_info) < 0) {
				status = MEMORY_STATUS_NOT_PRESENT;
				goto error;
			}

			/* Readable? */
			if (!(dest_info.protection & MEMORY_PROT_WRITE)) {
				status = MEMORY_STATUS_PROTECTION;
				goto error;
			}
		}

		/*
		 * Determine how many bytes we can copy until a reload is needed
		 * (or the copy process is finished).
		 */
		source_offset_in_alloc = source - source_info.base_address;
		source_max_copy = source_info.allocation_size -
			(size_t)source_offset_in_alloc;

		dest_offset_in_alloc = dest - dest_info.base_address;
		dest_max_copy = dest_info.allocation_size -
			(size_t)dest_offset_in_alloc;

		max_copy = size;
		if (max_copy > source_max_copy)
			max_copy = source_max_copy;
		if (max_copy > dest_max_copy)
			max_copy = dest_max_copy;

		/* Construct pointers to the data segment we're copying */
		source_alloc_address = (unsigned char *)(source_info.host_pointer) +
			source_offset_in_alloc;
		dest_alloc_address = (unsigned char *)(dest_info.host_pointer) +
			dest_offset_in_alloc;

		memcpy(dest_alloc_address, source_alloc_address, max_copy);

		/* Update addresses */
		source += max_copy;
		dest += max_copy;
		size -= max_copy;
	}

error:
	// FIXME: call callback on error
	return status;
}

#ifdef DEBUG_VIRTUAL_MEMORY_CHECK_CORRUPTION
static void _validate_entries(struct VirtualMemoryContext *ctx)
{
	size_t i;
	for (i = 0; i < ctx->num_allocs; ++i) {
		const struct _VirtualMemoryAlloc *const alloc = &ctx->allocs[i];

		if (alloc->start_address & (MEMORY_PAGE_SIZE - 1))
			goto fail;
		if ((alloc->end_address - alloc->start_address + 1) & (MEMORY_PAGE_SIZE - 1))
			goto fail;
		// we only have 32 bit allocations
		if (alloc->start_address >> 32)
			goto fail;
		if (alloc->end_address >> 32)
			goto fail;
		if (i > 0 && _allocations_overlap(alloc, alloc - 1))
			goto fail;
	}

	return;
fail:
	fprintf(stderr, "Corruption detected\n");
	virtual_memory_show_allocations(ctx);
	abort();
}
#endif


/*
 * Public functions
 */

int virtual_memory_new(struct VirtualMemoryContext **ctx)
{
	if (!ctx)
		return -1;

	*ctx = malloc(sizeof(**ctx));
	if (!*ctx)
		return -1;

	(*ctx)->num_allocs = 0;
	(*ctx)->max_allocs = _VIRTUAL_MEMORY_MAX_ALLOCS_START;
	(*ctx)->allocs = malloc(_VIRTUAL_MEMORY_MAX_ALLOCS_START *
		sizeof(*(*ctx)->allocs));
	if (!(*ctx)->allocs) {
		free(*ctx);
		*ctx = NULL;
		return -1;
	}
	return 0;
}

int virtual_memory_destroy(struct VirtualMemoryContext **ctx)
{
	size_t i;

	if (!ctx || !*ctx)
		return -1;

	/* Free internal allocations */
	for (i = 0; i < (*ctx)->num_allocs; ++i)
		_free_alloc(&(*ctx)->allocs[i]);
	free((*ctx)->allocs);
	(*ctx)->allocs = NULL;

	free(*ctx);
	*ctx = NULL;
	return 0;
}

int virtual_memory_set_fault_callback(struct VirtualMemoryContext *ctx,
	VirtualMemoryFaultCallback callback, void *user,
	VirtualMemoryFaultCallback *old_callback, void **old_user)
{
	if (!ctx)
		return -1;

	if (old_callback)
		*old_callback = ctx->fault_callback;
	if (old_user)
		*old_user = ctx->fault_callback_user;

	ctx->fault_callback = callback;
	ctx->fault_callback_user = user;
	return 0;
}

int virtual_memory_show_allocations(struct VirtualMemoryContext *ctx)
{
#ifdef DEBUG_VIRTUAL_MEMORY_SHOW_ALLOCATIONS
	size_t i;
#endif

	if (!ctx)
		return -1;

#ifdef DEBUG_VIRTUAL_MEMORY_SHOW_ALLOCATIONS
	for (i = 0; i < ctx->num_allocs; ++i) {
		const struct _VirtualMemoryAlloc *const p = &ctx->allocs[i];
		char r = (p->protection & MEMORY_PROT_READ) ? 'R' : ' ';
		char w = (p->protection & MEMORY_PROT_WRITE) ? 'W' : ' ';
		char x = (p->protection & MEMORY_PROT_EXECUTE) ? 'X' : ' ';

		fprintf(stderr, "%3u (0x%03X): 0x%" PRIXMA "...0x%" PRIXMA ": "
			"host %p, perms %c%c%c\n", i, i, p->start_address, p->end_address,
			p->host_pointer, r, w, x);
	}
#endif
	return 0;
}

int virtual_memory_allocate(struct VirtualMemoryContext *ctx,
	void *host_pointer, memory_address_t *base_address,
	size_t allocation_size, const enum MemoryProtection protection)
{
	return virtual_memory_allocate2(ctx, host_pointer, base_address,
		MEMORY_PAGE_SIZE, allocation_size, protection);
}

int virtual_memory_allocate2(struct VirtualMemoryContext *ctx,
	void *host_pointer, memory_address_t *base_address,
	memory_address_t granularity, size_t allocation_size,
	const enum MemoryProtection protection)
{
	size_t insertion_point;
	struct _VirtualMemoryAlloc new_alloc;

	if (!ctx || !base_address || !allocation_size)
		return -1;
	if (!granularity || (granularity & (granularity - 1u)))
		return -1;

	/* Only allow RWX bits as protection bits */
	if (protection & ~(MEMORY_PROT_READ | MEMORY_PROT_WRITE |
			MEMORY_PROT_EXECUTE))
		return -1;

	/* Reject allocations of size 0. Round them up to the next page */
	if (!allocation_size)
		return -1;
	allocation_size = (allocation_size + MEMORY_PAGE_SIZE - 1) &
		~(MEMORY_PAGE_SIZE - 1);

	/*
	 * If no base address is specified, search for a memory region which can
	 * fit the requested allocation size
	 */
	if (!*base_address) {
		if (!_find_free_address(ctx, allocation_size, base_address,
				granularity))
			return -1;
	}

	insertion_point = UINT_MAX;
	new_alloc.start_address = *base_address;
	new_alloc.end_address = *base_address + allocation_size - 1u;
	if (host_pointer)
		new_alloc.host_pointer = host_pointer;
	else {
		new_alloc.host_pointer = _host_rw_mmap(NULL, allocation_size);
		if (!new_alloc.host_pointer)
			return -1;
	}
	new_alloc.protection = protection | (host_pointer ? 0 :
		MEMORY_INTERNAL_ALLOCATION);

	/*
	 * If there are no allocations or the allocation is above the last
	 * allocation, perform an append operation without moves.
	 */
	if (ctx->num_allocs == 0 ||
			ctx->allocs[ctx->num_allocs - 1].end_address <
			new_alloc.start_address) {
		insertion_point = ctx->num_allocs;
	}

	/*
	 * If the allocation is below the first one, this will be the new first
	 * allocation and all others must be moved.
	 */
	else if (ctx->allocs[0].start_address > new_alloc.end_address)
		insertion_point = 0;

	/*
	 * If the allocation spans all previous allocations, reject it.
	 */
	else if (new_alloc.start_address <= ctx->allocs[0].start_address &&
			new_alloc.end_address >=
			ctx->allocs[ctx->num_allocs - 1].end_address)
		goto error;

	/*
	 * In the case of only one allocation, check if the new allocation
	 * overlaps.
	 */
	else if (ctx->num_allocs == 1 &&
			_allocations_overlap(&ctx->allocs[0], &new_alloc))
		goto error;

	/*
	 * Find an insertion point.
	 * XXX: This search algorithm requires linear time, can we do it better?
	 */
	else {
		size_t i;

		for (i = 0; i < ctx->num_allocs; ++i) {
			const struct _VirtualMemoryAlloc *const alloc = &ctx->allocs[i];

			/* Must not overlap with existing allocation */
			if (_allocations_overlap(alloc, &new_alloc))
				goto error;

			/* If the current allocation and the previous allocation are not
			   overlapping and provide sufficient space in between, place it
			   here. */
			if (i > 0 && (alloc - 1)->end_address < new_alloc.start_address &&
					alloc->start_address > new_alloc.end_address) {
				insertion_point = i;
				break;
			}
		}
	}

	if (insertion_point == UINT_MAX) {
		assert(0 && "No insertion point found");
		goto error;
	}

	/* If we do not have enough space, expand the allocation array */
	if (ctx->num_allocs >= ctx->max_allocs) {
		struct _VirtualMemoryAlloc *new_allocs;
		const size_t new_max_allocs = ctx->max_allocs +
			_VIRTUAL_MEMORY_MAX_ALLOCS_INCREMENT;

		new_allocs = realloc(ctx->allocs, new_max_allocs *
			sizeof(*new_allocs));
		if (!new_allocs)
			goto error;
		ctx->allocs = new_allocs;
		ctx->max_allocs = new_max_allocs;
	}

	/* Move entries if not appending to the last one */
	if (insertion_point < ctx->num_allocs) {
		memmove(&ctx->allocs[insertion_point + 1],
			&ctx->allocs[insertion_point],
			(ctx->num_allocs - insertion_point) * sizeof(*ctx->allocs));
	}

	/* Fill the allocation in */
	// XXX: maybe individual assignments for older compilers
	ctx->allocs[insertion_point] = new_alloc;
	++ctx->num_allocs;
#ifdef DEBUG_VIRTUAL_MEMORY_CHECK_CORRUPTION
	_validate_entries(ctx);
#endif
	return 0;

error:
	if (!host_pointer)
		_host_rw_munmap(new_alloc.host_pointer);
	return -1;
}

int virtual_memory_query(struct VirtualMemoryContext *ctx,
	const memory_address_t address, struct VirtualMemoryInformation *info)
{
	size_t alloc_index;
	const struct _VirtualMemoryAlloc *alloc;

	if (!ctx || !info)
		return -1;

	/* Find internally */
	if (_query_internal(ctx, address, &alloc_index) < 0)
		return -1;
	alloc = &ctx->allocs[alloc_index];

	/* Use allocation index to access information */
	info->base_address = alloc->start_address;
	info->allocation_size = (size_t)(
		alloc->end_address - alloc->start_address + 1u
	);
	info->host_pointer = alloc->host_pointer;

	/* Don't expose the "internal allocation" bit */
	info->protection = alloc->protection &
		(MEMORY_PROT_READ | MEMORY_PROT_WRITE | MEMORY_PROT_EXECUTE);
	return 0;
}

int virtual_memory_protect(struct VirtualMemoryContext *ctx,
	const memory_address_t address, const enum MemoryProtection protection)
{
	size_t alloc_index;
	struct _VirtualMemoryAlloc *alloc;

	if (!ctx)
		return -1;

	/* Only allow RWX bits as protection bits */
	if (protection & ~(MEMORY_PROT_READ | MEMORY_PROT_WRITE |
			MEMORY_PROT_EXECUTE))
		return -1;

	/* Find internally */
	if (_query_internal(ctx, address, &alloc_index) < 0)
		return -1;
	alloc = &ctx->allocs[alloc_index];

	/* Change protection bits */
	alloc->protection &= ~(MEMORY_PROT_READ | MEMORY_PROT_WRITE |
		MEMORY_PROT_EXECUTE);
	alloc->protection |= protection;
	return 0;
}

int virtual_memory_free(struct VirtualMemoryContext *ctx,
	memory_address_t base_address)
{
	size_t alloc_index;
	struct _VirtualMemoryAlloc *alloc;

	if (!ctx)
		return -1;

	/* Find internally */
	if (_query_internal(ctx, base_address, &alloc_index) < 0)
		return -1;
	alloc = &ctx->allocs[alloc_index];

	/* Base address must match before freeing */
	if (base_address != alloc->start_address)
		return -1;

	/* Free internally */
	_free_alloc(alloc);

	/* Overwrite it with entries after it */
	if (ctx->num_allocs > 1 && alloc_index < ctx->num_allocs - 1) {
		memmove(&ctx->allocs[alloc_index],
			&ctx->allocs[alloc_index + 1],
			(ctx->num_allocs - alloc_index - 1) * sizeof(*ctx->allocs));
	}
	--ctx->num_allocs;
#ifdef DEBUG_VIRTUAL_MEMORY_CHECK_CORRUPTION
	_validate_entries(ctx);
#endif
	return 0;
}

enum MemoryStatus virtual_memory_read(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	void *buffer, size_t size)
{
	if (!ctx || !buffer)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	if (size == 0)
		return MEMORY_STATUS_OK;

	return _memory_operation(ctx, address, buffer, size,
		MEMORY_PROT_READ, 0);
}

enum MemoryStatus virtual_memory_read_string(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	char *buffer, size_t *size)
{
	uint8_t c;

	if (!ctx || !size)
		return MEMORY_STATUS_INVALID_ARGUMENT;

	/* Read it character by character */
	*size = 0;
	do {
		enum MemoryStatus status = _memory_operation(ctx, address, &c,
			sizeof(c), MEMORY_PROT_READ, 0);
		if (!MEMOP_OK(status))
			return status;
		if (buffer)
			*buffer++ = c;

		++(*size);
		++address;
	} while (c);

	return MEMORY_STATUS_OK;
}

enum MemoryStatus virtual_memory_read_string_alloc(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	char **buffer)
{
	size_t n;
	enum MemoryStatus status;

	/* Determine length */
	if (MEMOP_OK(status = virtual_memory_read_string(ctx, address, NULL,
			&n))) {
		/* Allocate memory */
		*buffer = malloc(n + 1);
		if (!*buffer)
			return MEMORY_STATUS_NO_MEMORY;

		/* Read string */
		if (!MEMOP_OK(status = virtual_memory_read_string(ctx, address,
				*buffer, &n))) {
			free(*buffer);
			*buffer = NULL;
		}
	}
	return status;
}

enum MemoryStatus virtual_memory_write_string(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	const char *buffer, size_t size)
{
	enum MemoryStatus status = MEMORY_STATUS_OK;

	if (!ctx || !buffer)
		return MEMORY_STATUS_INVALID_ARGUMENT;

	if (size == (size_t)-1)
		size = strlen(buffer) + 1;

	if (size > 0) {
		const char null = '\0';
		const char *p = buffer;

		while (*p && (size_t)(p - buffer) < size - 1) {
			// FIXME: cast to char *, not to unsigned char *
			status = _memory_operation(ctx, address, (unsigned char *)p, 1,
				MEMORY_PROT_WRITE, 0);
			if (!MEMOP_OK(status))
				return status;
			++p;
			++address;
		}

		/* Null terminator */
		// FIXME: cast to char *, not to unsigned char *
		status = _memory_operation(ctx, address, (unsigned char *)&null, 1,
			MEMORY_PROT_WRITE, 0);
	}
	return status;
}

enum MemoryStatus virtual_memory_execute(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	void *buffer, size_t size)
{
	if (!ctx || !buffer)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	if (size == 0)
		return MEMORY_STATUS_OK;

	return _memory_operation(ctx, address, buffer, size,
		MEMORY_PROT_EXECUTE, 0);
}

enum MemoryStatus virtual_memory_write(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	const void *buffer, size_t size)
{
	if (!ctx || !buffer)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	if (size == 0)
		return MEMORY_STATUS_OK;

	return _memory_operation(ctx, address, (void *)buffer, size,
		MEMORY_PROT_WRITE, 0);
}

enum MemoryStatus virtual_memory_copy(struct VirtualMemoryContext *ctx,
	memory_address_t dest, memory_address_t source, size_t size)
{
	enum MemoryStatus status;
	unsigned char move_buffer[4];
	const size_t WS = sizeof(move_buffer);

	if (!ctx)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	if (size == 0 || dest == source)
		return MEMORY_STATUS_OK;

	/* Check if we can perform a simple (and fast) memcpy() in case the
	   memory ranges do not overlap. */
	if (source + size <= dest || dest + size <= source)
		return _virtual_memory_copy_unchecked(ctx, dest, source, size);

	/*
	 * We cannot. If destination < source, copy in forward direction.
	 */
	if (dest < source) {
		while (size > WS) {
			if (!MEMOP_OK(status = virtual_memory_read(ctx, source,
					move_buffer, WS)))
				return status;
			if (!MEMOP_OK(status = virtual_memory_write(ctx, dest,
					move_buffer, WS)))
				return status;
			source += WS;
			dest += WS;
			size -= WS;
		}

		/* Final move (size < WS) */
		if (!MEMOP_OK(status = virtual_memory_read(ctx, source,
				move_buffer, size)))
			return status;
		if (!MEMOP_OK(status = virtual_memory_write(ctx, dest,
				move_buffer, size)))
			return status;
	}

	/*
	 * Otherwise, copy in backward direction.
	 */
	else {
		source += size;
		dest += size;
		while (size > WS) {
			if (!MEMOP_OK(status = virtual_memory_read(ctx, source - WS,
					move_buffer, WS)))
				return status;
			if (!MEMOP_OK(status = virtual_memory_write(ctx, dest - WS,
					move_buffer, WS)))
				return status;
			source -= WS;
			dest -= WS;
			size -= WS;
		}

		/* Final move (size < WS) */
		if (!MEMOP_OK(status = virtual_memory_read(ctx, source - size,
				move_buffer, size)))
			return status;
		if (!MEMOP_OK(status = virtual_memory_write(ctx, dest - size,
				move_buffer, size)))
			return status;
	}
	return MEMORY_STATUS_OK;
}

enum MemoryStatus virtual_memory_set(struct VirtualMemoryContext *ctx,
	memory_address_t address, unsigned char value, size_t size)
{
	if (!ctx)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	if (size == 0)
		return MEMORY_STATUS_OK;

	return _memory_operation(ctx, address, NULL, size,
		MEMORY_PROT_WRITE | MEMORY_INTERNAL_MEMSET | (value << 8), 0);
}

void virtual_memory_dump(struct VirtualMemoryContext *ctx,
	memory_address_t p, size_t size)
{
#ifdef DEBUG_VIRTUAL_MEMORY_DUMP
	while (size) {
		unsigned int i;
		int line_values[16];
		const unsigned int line_max = size > 16 ? 16 : size;

		/* Precompute values to display for each of the 16 bytes */
		for (i = 0; i < 16; ++i) {
			if (i >= line_max)
				line_values[i] = -1; /* whitespace */
			else {
				uint8_t byte;
				if (MEMOP_OK(virtual_memory_read(ctx, p + i, &byte, 1)))
					line_values[i] = byte;
				else
					line_values[i] = -2; /* question marks */
			}
		}

		/* Print the text */
		printf("%" PRIXMA " | ", p);
		for (i = 0; i < 16; ++i) {
			if (line_values[i] == -1)
				printf("   ");
			else
				printf("%02X ", line_values[i]);
		}
		printf("| ");
		for (i = 0; i < 16; ++i) {
			int c;
			if (line_values[i] == -1)
				c = ' ';
			else if (line_values[i] >= 0x20 && line_values[i] <= 0x7E)
				c = line_values[i];
			else if (line_values[i] >= 0xA0 && line_values[i] <= 0xFF) {
#ifdef _WIN32
				/* Windows can't UTF-8 on console for compat reasons */
				c = line_values[i];
#else
				unsigned char v1 = 0xC0u | ((line_values[i] >> 6u) & 0x3u);
				unsigned char v2 = 0x80u | (line_values[i] & 0x3Fu);
				putc(v1, stdout);
				c = v2;
#endif
			} else
				c = '.';
			putc(c, stdout);
		}
		putc('\n', stdout);

		p += line_max;
		size -= line_max;
	}
#else
	(void)ctx;
	(void)p;
	(void)size;
#endif
}


/*
 * Host read/write functions
 */

enum MemoryStatus virtual_memory_host_read(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	void *buffer, size_t size)
{
	if (!ctx || !buffer)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	if (size == 0)
		return MEMORY_STATUS_OK;

	return _memory_operation(ctx, address, buffer, size,
		MEMORY_PROT_READ, 1);
}

enum MemoryStatus virtual_memory_host_read_string(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	char *buffer, size_t *size)
{
	uint8_t c;

	if (!ctx || !size)
		return MEMORY_STATUS_INVALID_ARGUMENT;

	/* Read it character by character */
	*size = 0;
	do {
		enum MemoryStatus status = _memory_operation(ctx, address, &c,
			sizeof(c), MEMORY_PROT_READ, 1);
		if (!MEMOP_OK(status))
			return status;
		if (buffer)
			*buffer++ = c;

		++(*size);
		++address;
	} while (c);

	return MEMORY_STATUS_OK;
}

enum MemoryStatus virtual_memory_host_read_string_alloc(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	char **buffer)
{
	size_t n;
	enum MemoryStatus status;

	/* Determine length */
	if (MEMOP_OK(status = virtual_memory_host_read_string(ctx, address, NULL,
			&n))) {
		/* Allocate memory */
		*buffer = malloc(n + 1);
		if (!*buffer)
			return MEMORY_STATUS_NO_MEMORY;

		/* Read string */
		if (!MEMOP_OK(status = virtual_memory_host_read_string(ctx, address,
				*buffer, &n))) {
			free(*buffer);
			*buffer = NULL;
		}
	}
	return status;
}

enum MemoryStatus virtual_memory_host_write(
	struct VirtualMemoryContext *ctx, memory_address_t address,
	const void *buffer, size_t size)
{
	if (!ctx || !buffer)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	if (size == 0)
		return MEMORY_STATUS_OK;

	return _memory_operation(ctx, address, (void *)buffer, size,
		MEMORY_PROT_WRITE, 1);
}


/*
 * Interface
 */

enum MemoryStatus virtual_memory_read_c(void *memory_ctx,
	memory_address_t address, void *buffer, size_t size)
{
	return virtual_memory_read(memory_ctx, address, buffer, size);
}

enum MemoryStatus virtual_memory_execute_c(void *memory_ctx,
	memory_address_t address, void *buffer, size_t size)
{
	return virtual_memory_execute(memory_ctx, address, buffer, size);
}

enum MemoryStatus virtual_memory_write_c(void *memory_ctx,
	memory_address_t address, const void *buffer, size_t size)
{
	return virtual_memory_write(memory_ctx, address, buffer, size);
}

const struct MemoryInterface virtual_memory_interface = {
	virtual_memory_read_c,
	virtual_memory_execute_c,
	virtual_memory_write_c
};


/*
 * internal functions, platform specific
 */

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

static void *_host_rw_mmap(void *preferred_addr, size_t size)
{
	return VirtualAlloc(
		preferred_addr, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE
	);
}

static void _host_rw_munmap(void *addr)
{
	VirtualFree(addr, 0, MEM_RELEASE);
}
#endif
