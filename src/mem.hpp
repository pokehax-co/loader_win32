#pragma once

/*!
 * \file src/mem.hpp
 * \brief C++ wrapper of the virtmem implementation
 */

#include "iface/memory.h"
#include "process/virtmem.h"

#include <stdlib.h>


class AddressSpace
{
public:
	AddressSpace(void)
	{
		if (virtual_memory_new(&_virtmem) < 0)
			abort();
	}

	~AddressSpace(void)
	{
		virtual_memory_destroy(&_virtmem);
	}

	enum MemoryStatus read_string(memory_address_t address, char *&buf)
	{
		enum MemoryStatus status;

		/* Determine length */
		size_t n;
		status = virtual_memory_read_string(_virtmem, address, NULL, &n);
		if (status == MEMORY_STATUS_OK) {
			/* Allocate and read */
			buf = new char[n + 1];
			status = virtual_memory_read_string(_virtmem, address, buf, &n);
			if (status != MEMORY_STATUS_OK) {
				delete[] buf;
				buf = NULL;
			}
		}
		return status;
	}

	struct VirtualMemoryContext *get_virtmem(void)
	{
		return _virtmem;
	}

private:
	struct VirtualMemoryContext *_virtmem;
};
