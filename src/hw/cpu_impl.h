/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef HW_CPU_IMPL_H
#define HW_CPU_IMPL_H

/*!
 * \file src/hw/cpu_impl.h
 * \brief Implementation details for CPUs.
 */

#include "cpu.h"


#ifdef __cplusplus
extern "C" {
#endif

/* Initializes the implementation-specific part of the CPU. */
typedef void (*CPUInterfaceInitialize)(void *cpu_ctx);

/* Destroys the implementation-specific part of the CPU. */
typedef void (*CPUInterfaceDestroy)(void *cpu_ctx);

/* Resets all CPU registers to their default state. */
typedef void (*CPUInterfaceReset)(void *cpu_ctx);

/* Performs additional operations after the CPU has been stopped. */
typedef void (*CPUInterfaceOnStop)(void *cpu_ctx);

/* Prints the CPU's registers. */
typedef void (*CPUInterfacePrintRegisters)(void *cpu_ctx);

/* Returns the value of a CPU register. */
typedef cpu_register_t (*CPUInterfaceGetRegister)(void *cpu_ctx,
	unsigned int regnum);

/* Sets the value of a CPU register. */
typedef void (*CPUInterfaceSetRegister)(void *cpu_ctx, unsigned int regnum,
	cpu_register_t value);

/* Saves the current register state into `buffer`. */
typedef void (*CPUInterfaceSaveRegisterState)(void *cpu_ctx, void *buffer);

/* Loads the current register state from `buffer`. */
typedef void (*CPUInterfaceLoadRegisterState)(void *cpu_ctx,
	const void *buffer);

/* Runs a single step in the CPU. */
typedef int (*CPUInterfaceRunStep)(void *cpu_ctx);

struct CPUInterface
{
	/* CPU architecture */
	enum CPUArchitecture architecture;

	/* Size of the context (including the CPUContext structure). */
	unsigned int allocation_size;

	/* Size of the register state. */
	unsigned int register_state_size;

	/* Last valid register number. */
	unsigned int max_regnum;

	/* Functions */
	CPUInterfaceInitialize initialize;
	CPUInterfaceDestroy destroy;
	CPUInterfaceReset reset;
	CPUInterfaceOnStop on_stop;
	CPUInterfacePrintRegisters print_registers;
	CPUInterfaceGetRegister get_register;
	CPUInterfaceSetRegister set_register;
	CPUInterfaceSaveRegisterState save_register_state;
	CPUInterfaceLoadRegisterState load_register_state;
	CPUInterfaceRunStep run_step;
};

/* Must be inherited by implementations */
struct CPUContext
{
	const struct CPUInterface *cpu_iface;
	unsigned char stopped;
	unsigned char stop_intended;

	const struct MemoryInterface *memory_iface;
	void *memory_ctx;
};

#ifdef __cplusplus
}
#endif

#endif
