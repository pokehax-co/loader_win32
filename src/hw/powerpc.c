/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "ppc_impl.h"

#include "../misc/tools.h"

#include <stdio.h>
#include <assert.h>

#ifdef DEBUG_DISASSEMBLE
#include <capstone/capstone.h>
#endif


/*
 * PowerPC-specific CPU context
 */

#ifdef DEBUG_DISASSEMBLE
/* Capstone disassembler */
static csh _dis;
#endif


/*
 * Macros to make memory accesses more readable
 */

#define mem_execute_ok(address, buffer, size) \
	(ppc_ctx->common.memory_iface->execute(ppc_ctx->common.memory_ctx, \
		(address), (buffer), (size)) == MEMORY_STATUS_OK)


/*
 * Public C functions
 */

int cpu_powerpc_set_big_endian(struct CPUContext *ctx,
	unsigned int big_endian)
{
	struct _PPCContext *ppc_ctx;

	if (!ctx)
		return -1;
	ppc_ctx = (struct _PPCContext *)ctx;

#ifdef DEBUG_DISASSEMBLE
	/* Re-initialize Capstone disassembler becuase endianness changes */
	if (big_endian != ppc_ctx->state.big_endian) {
		cs_close(&_dis);
		cs_open(CS_ARCH_PPC, (big_endian ? CS_MODE_BIG_ENDIAN :
			CS_MODE_LITTLE_ENDIAN) | CS_MODE_32, &_dis);
	}
#endif

	ppc_ctx->state.big_endian = big_endian;
	return 0;
}

int cpu_powerpc_set_syscall_handler(struct CPUContext *ctx,
	PowerPCSyscallHandler handler, void *user)
{
	struct _PPCContext *ppc_ctx;

	if (!ctx)
		return -1;
	ppc_ctx = (struct _PPCContext *)ctx;

	ppc_ctx->syscall_handler = handler;
	ppc_ctx->syscall_user = user;
	return 0;
}

enum MemoryStatus cpu_powerpc_read_16(struct CPUContext *ctx,
	memory_address_t address, unsigned short *value)
{
	enum MemoryStatus status;
	struct _PPCContext *ppc_ctx;
	unsigned char buffer[sizeof(*value)];

	if (!ctx || !value)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	ppc_ctx = (struct _PPCContext *)ctx;

	status = ppc_ctx->common.memory_iface->read(ppc_ctx->common.memory_ctx,
		address, buffer, sizeof(buffer));
	if (status == MEMORY_STATUS_OK) {
		if (ppc_ctx->state.big_endian)
			*value = read_u16be(buffer);
		else
			*value = read_u16le(buffer);
	}

	return status;
}

enum MemoryStatus cpu_powerpc_read_32(struct CPUContext *ctx,
	memory_address_t address, unsigned int *value)
{
	enum MemoryStatus status;
	struct _PPCContext *ppc_ctx;
	unsigned char buffer[sizeof(*value)];

	if (!ctx || !value)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	ppc_ctx = (struct _PPCContext *)ctx;

	status = ppc_ctx->common.memory_iface->read(ppc_ctx->common.memory_ctx,
		address, buffer, sizeof(buffer));
	if (status == MEMORY_STATUS_OK) {
		if (ppc_ctx->state.big_endian)
			*value = read_u32be(buffer);
		else
			*value = read_u32le(buffer);
	}

	return status;
}

enum MemoryStatus cpu_powerpc_read_64(struct CPUContext *ctx,
	memory_address_t address, uint64_t *value)
{
	enum MemoryStatus status;
	struct _PPCContext *ppc_ctx;
	unsigned char buffer[sizeof(*value)];

	if (!ctx || !value)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	ppc_ctx = (struct _PPCContext *)ctx;

	status = ppc_ctx->common.memory_iface->read(ppc_ctx->common.memory_ctx,
		address, buffer, sizeof(buffer));
	if (status == MEMORY_STATUS_OK) {
		if (ppc_ctx->state.big_endian)
			*value = read_u64be(buffer);
		else
			*value = read_u64le(buffer);
	}

	return status;
}

enum MemoryStatus cpu_powerpc_write_16(struct CPUContext *ctx,
	memory_address_t address, unsigned short value)
{
	struct _PPCContext *ppc_ctx;
	unsigned char buffer[sizeof(value)];

	if (!ctx)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	ppc_ctx = (struct _PPCContext *)ctx;

	if (ppc_ctx->state.big_endian)
		write_u16be(buffer, value);
	else
		write_u16le(buffer, value);

	return ppc_ctx->common.memory_iface->write(ppc_ctx->common.memory_ctx,
		address, buffer, sizeof(buffer));
}

enum MemoryStatus cpu_powerpc_write_32(struct CPUContext *ctx,
	memory_address_t address, unsigned int value)
{
	struct _PPCContext *ppc_ctx;
	unsigned char buffer[sizeof(value)];

	if (!ctx)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	ppc_ctx = (struct _PPCContext *)ctx;

	if (ppc_ctx->state.big_endian)
		write_u32be(buffer, value);
	else
		write_u32le(buffer, value);

	return ppc_ctx->common.memory_iface->write(ppc_ctx->common.memory_ctx,
		address, buffer, sizeof(buffer));
}

enum MemoryStatus cpu_powerpc_write_64(struct CPUContext *ctx,
	memory_address_t address, uint64_t value)
{
	struct _PPCContext *ppc_ctx;
	unsigned char buffer[sizeof(value)];

	if (!ctx)
		return MEMORY_STATUS_INVALID_ARGUMENT;
	ppc_ctx = (struct _PPCContext *)ctx;

	if (ppc_ctx->state.big_endian)
		write_u64be(buffer, value);
	else
		write_u64le(buffer, value);

	return ppc_ctx->common.memory_iface->write(ppc_ctx->common.memory_ctx,
		address, buffer, sizeof(buffer));
}


/*
 * C interface functions
 */

static void _cpu_powerpc_reset(void *cpu_ctx)
{
	unsigned int i;
	struct _PPCContext *const ppc_ctx = cpu_ctx;

	/* Reset all CPU registers to 0 */
	for (i = 0; i < 32; ++i)
		ppc_ctx->state.gpr[i] = 0u;

	ppc_ctx->state.cia = 0u;

	for (i = 0; i < 8; ++i)
		ppc_ctx->state.cr[i] = 0u;

	ppc_ctx->state.xer = 0u;
	ppc_ctx->state.lr = 0u;
	ppc_ctx->state.ctr = 0u;

#ifdef CONFIG_ENABLE_FPU
	/* Reset all FPU registers to 0 */
	for (i = 0; i < 32; ++i)
		ppc_ctx->state.fpr[i] = 0u;
	ppc_ctx->state.fpscr = 0u;
#endif
}

static void _cpu_powerpc_initialize(void *cpu_ctx)
{
	struct _PPCContext *const ppc_ctx = cpu_ctx;

	/* Reset registers */
	_cpu_powerpc_reset(cpu_ctx);

	/* Set little-endian mode explicitly for initialization, this won't
	   be reset when cpu_reset() is called. */
	ppc_ctx->state.big_endian = 0;

#ifdef DEBUG_DISASSEMBLE
	/* Initialize Capstone disassembler in this mode */
	cs_open(CS_ARCH_PPC, CS_MODE_LITTLE_ENDIAN | CS_MODE_32, &_dis);
#endif
}

static void _cpu_powerpc_destroy(void *cpu_ctx)
{
	(void)cpu_ctx;

#ifdef DEBUG_DISASSEMBLE
	/* Delete disassembler */
	cs_close(&_dis);
#endif
}

static void _cpu_powerpc_print_registers(void *cpu_ctx)
{
	size_t i;
	struct _PPCContext *const ppc_ctx = cpu_ctx;

	printf("[CPUPowerPC] ------ Register dump starts here ------\n");


	printf("    cia = 0x%08X\n", ppc_ctx->state.cia);

	/* General-purpose registers (two per line) */
	for (i = 0; i < 32; i += 2) {
		printf(&"     r%d = 0x%08X\t\t  "[i >= 10],
			i, ppc_ctx->state.gpr[i]);
		printf(&" r%d = 0x%08X\n"[i >= 9],
			i + 1, ppc_ctx->state.gpr[i + 1]);
	}

	/* CR and XER on the same line */
	printf("     cr = 0x%1X%1X%1X%1X%1X%1X%1X%1X\t\t  xer = 0x%08X\n",
		ppc_ctx->state.cr[0], ppc_ctx->state.cr[1], ppc_ctx->state.cr[2],
		ppc_ctx->state.cr[3], ppc_ctx->state.cr[4], ppc_ctx->state.cr[5],
		ppc_ctx->state.cr[6], ppc_ctx->state.cr[7], ppc_ctx->state.xer);

	/* LR and CTR on the same line */
	printf("     lr = 0x%08X\t\t  ctr = 0x%08X\n",
		ppc_ctx->state.lr, ppc_ctx->state.ctr);

#ifdef CONFIG_ENABLE_FPU
	/* Floating-point registers (two per line) */
	for (i = 0; i < 32; i += 2) {
		printf(&"   fpr%d = 0x%016I64X\t"[i >= 10],
			i, ppc_ctx->state.fpr[i]);
		printf(&" fpr%d = 0x%016I64X\n"[i >= 9],
			i + 1, ppc_ctx->state.fpr[i + 1]);
	}

	printf("  fpscr = 0x%08X\n", ppc_ctx->state.fpscr);
#endif


	printf("[CPUPowerPC] ------- Register dump ends here -------\n");
}

static void _cpu_powerpc_on_stop(void *cpu_ctx)
{
	printf("[CPUPowerPC] CPU stopped.\n");

	/* Print registers for debugging purposes */
	_cpu_powerpc_print_registers(cpu_ctx);
}

static cpu_register_t _cpu_powerpc_get_register(void *cpu_ctx,
	unsigned int regnum)
{
	struct _PPCContext *const ppc_ctx = cpu_ctx;

	switch (regnum) {
		case PPC_REGISTER_R0: return ppc_ctx->state.gpr[0];
		case PPC_REGISTER_R1: return ppc_ctx->state.gpr[1];
		case PPC_REGISTER_R2: return ppc_ctx->state.gpr[2];
		case PPC_REGISTER_R3: return ppc_ctx->state.gpr[3];
		case PPC_REGISTER_R4: return ppc_ctx->state.gpr[4];
		case PPC_REGISTER_R5: return ppc_ctx->state.gpr[5];
		case PPC_REGISTER_R6: return ppc_ctx->state.gpr[6];
		case PPC_REGISTER_R7: return ppc_ctx->state.gpr[7];
		case PPC_REGISTER_R8: return ppc_ctx->state.gpr[8];
		case PPC_REGISTER_R9: return ppc_ctx->state.gpr[9];
		case PPC_REGISTER_R10: return ppc_ctx->state.gpr[10];
		case PPC_REGISTER_R11: return ppc_ctx->state.gpr[11];
		case PPC_REGISTER_R12: return ppc_ctx->state.gpr[12];
		case PPC_REGISTER_R13: return ppc_ctx->state.gpr[13];
		case PPC_REGISTER_R14: return ppc_ctx->state.gpr[14];
		case PPC_REGISTER_R15: return ppc_ctx->state.gpr[15];
		case PPC_REGISTER_R16: return ppc_ctx->state.gpr[16];
		case PPC_REGISTER_R17: return ppc_ctx->state.gpr[17];
		case PPC_REGISTER_R18: return ppc_ctx->state.gpr[18];
		case PPC_REGISTER_R19: return ppc_ctx->state.gpr[19];
		case PPC_REGISTER_R20: return ppc_ctx->state.gpr[20];
		case PPC_REGISTER_R21: return ppc_ctx->state.gpr[21];
		case PPC_REGISTER_R22: return ppc_ctx->state.gpr[22];
		case PPC_REGISTER_R23: return ppc_ctx->state.gpr[23];
		case PPC_REGISTER_R24: return ppc_ctx->state.gpr[24];
		case PPC_REGISTER_R25: return ppc_ctx->state.gpr[25];
		case PPC_REGISTER_R26: return ppc_ctx->state.gpr[26];
		case PPC_REGISTER_R27: return ppc_ctx->state.gpr[27];
		case PPC_REGISTER_R28: return ppc_ctx->state.gpr[28];
		case PPC_REGISTER_R29: return ppc_ctx->state.gpr[29];
		case PPC_REGISTER_R30: return ppc_ctx->state.gpr[30];
		case PPC_REGISTER_R31: return ppc_ctx->state.gpr[31];
		case PPC_REGISTER_PC: return ppc_ctx->state.cia;
		case PPC_REGISTER_CR0: return ppc_ctx->state.cr[0];
		case PPC_REGISTER_CR1: return ppc_ctx->state.cr[1];
		case PPC_REGISTER_CR2: return ppc_ctx->state.cr[2];
		case PPC_REGISTER_CR3: return ppc_ctx->state.cr[3];
		case PPC_REGISTER_CR4: return ppc_ctx->state.cr[4];
		case PPC_REGISTER_CR5: return ppc_ctx->state.cr[5];
		case PPC_REGISTER_CR6: return ppc_ctx->state.cr[6];
		case PPC_REGISTER_CR7: return ppc_ctx->state.cr[7];
		case PPC_REGISTER_XER: return ppc_ctx->state.xer;
		case PPC_REGISTER_LR: return ppc_ctx->state.lr;
		case PPC_REGISTER_CTR: return ppc_ctx->state.ctr;
#ifdef CONFIG_ENABLE_FPU
		case PPC_REGISTER_FPR0: return ppc_ctx->state.fpr[0];
		case PPC_REGISTER_FPR1: return ppc_ctx->state.fpr[1];
		case PPC_REGISTER_FPR2: return ppc_ctx->state.fpr[2];
		case PPC_REGISTER_FPR3: return ppc_ctx->state.fpr[3];
		case PPC_REGISTER_FPR4: return ppc_ctx->state.fpr[4];
		case PPC_REGISTER_FPR5: return ppc_ctx->state.fpr[5];
		case PPC_REGISTER_FPR6: return ppc_ctx->state.fpr[6];
		case PPC_REGISTER_FPR7: return ppc_ctx->state.fpr[7];
		case PPC_REGISTER_FPR8: return ppc_ctx->state.fpr[8];
		case PPC_REGISTER_FPR9: return ppc_ctx->state.fpr[9];
		case PPC_REGISTER_FPR10: return ppc_ctx->state.fpr[10];
		case PPC_REGISTER_FPR11: return ppc_ctx->state.fpr[11];
		case PPC_REGISTER_FPR12: return ppc_ctx->state.fpr[12];
		case PPC_REGISTER_FPR13: return ppc_ctx->state.fpr[13];
		case PPC_REGISTER_FPR14: return ppc_ctx->state.fpr[14];
		case PPC_REGISTER_FPR15: return ppc_ctx->state.fpr[15];
		case PPC_REGISTER_FPR16: return ppc_ctx->state.fpr[16];
		case PPC_REGISTER_FPR17: return ppc_ctx->state.fpr[17];
		case PPC_REGISTER_FPR18: return ppc_ctx->state.fpr[18];
		case PPC_REGISTER_FPR19: return ppc_ctx->state.fpr[19];
		case PPC_REGISTER_FPR20: return ppc_ctx->state.fpr[20];
		case PPC_REGISTER_FPR21: return ppc_ctx->state.fpr[21];
		case PPC_REGISTER_FPR22: return ppc_ctx->state.fpr[22];
		case PPC_REGISTER_FPR23: return ppc_ctx->state.fpr[23];
		case PPC_REGISTER_FPR24: return ppc_ctx->state.fpr[24];
		case PPC_REGISTER_FPR25: return ppc_ctx->state.fpr[25];
		case PPC_REGISTER_FPR26: return ppc_ctx->state.fpr[26];
		case PPC_REGISTER_FPR27: return ppc_ctx->state.fpr[27];
		case PPC_REGISTER_FPR28: return ppc_ctx->state.fpr[28];
		case PPC_REGISTER_FPR29: return ppc_ctx->state.fpr[29];
		case PPC_REGISTER_FPR30: return ppc_ctx->state.fpr[30];
		case PPC_REGISTER_FPR31: return ppc_ctx->state.fpr[31];
		case PPC_REGISTER_FPSCR: return ppc_ctx->state.fpscr;
#endif
	}
	assert(0 && "invalid register number");
	return 0;
}

static void _cpu_powerpc_set_register(void *cpu_ctx, unsigned int regnum,
	cpu_register_t value)
{
	struct _PPCContext *const ppc_ctx = cpu_ctx;
	const unsigned int v = (unsigned int)value;

	switch (regnum) {
		case PPC_REGISTER_R0: ppc_ctx->state.gpr[0] = v; return;
		case PPC_REGISTER_R1: ppc_ctx->state.gpr[1] = v; return;
		case PPC_REGISTER_R2: ppc_ctx->state.gpr[2] = v; return;
		case PPC_REGISTER_R3: ppc_ctx->state.gpr[3] = v; return;
		case PPC_REGISTER_R4: ppc_ctx->state.gpr[4] = v; return;
		case PPC_REGISTER_R5: ppc_ctx->state.gpr[5] = v; return;
		case PPC_REGISTER_R6: ppc_ctx->state.gpr[6] = v; return;
		case PPC_REGISTER_R7: ppc_ctx->state.gpr[7] = v; return;
		case PPC_REGISTER_R8: ppc_ctx->state.gpr[8] = v; return;
		case PPC_REGISTER_R9: ppc_ctx->state.gpr[9] = v; return;
		case PPC_REGISTER_R10: ppc_ctx->state.gpr[10] = v; return;
		case PPC_REGISTER_R11: ppc_ctx->state.gpr[11] = v; return;
		case PPC_REGISTER_R12: ppc_ctx->state.gpr[12] = v; return;
		case PPC_REGISTER_R13: ppc_ctx->state.gpr[13] = v; return;
		case PPC_REGISTER_R14: ppc_ctx->state.gpr[14] = v; return;
		case PPC_REGISTER_R15: ppc_ctx->state.gpr[15] = v; return;
		case PPC_REGISTER_R16: ppc_ctx->state.gpr[16] = v; return;
		case PPC_REGISTER_R17: ppc_ctx->state.gpr[17] = v; return;
		case PPC_REGISTER_R18: ppc_ctx->state.gpr[18] = v; return;
		case PPC_REGISTER_R19: ppc_ctx->state.gpr[19] = v; return;
		case PPC_REGISTER_R20: ppc_ctx->state.gpr[20] = v; return;
		case PPC_REGISTER_R21: ppc_ctx->state.gpr[21] = v; return;
		case PPC_REGISTER_R22: ppc_ctx->state.gpr[22] = v; return;
		case PPC_REGISTER_R23: ppc_ctx->state.gpr[23] = v; return;
		case PPC_REGISTER_R24: ppc_ctx->state.gpr[24] = v; return;
		case PPC_REGISTER_R25: ppc_ctx->state.gpr[25] = v; return;
		case PPC_REGISTER_R26: ppc_ctx->state.gpr[26] = v; return;
		case PPC_REGISTER_R27: ppc_ctx->state.gpr[27] = v; return;
		case PPC_REGISTER_R28: ppc_ctx->state.gpr[28] = v; return;
		case PPC_REGISTER_R29: ppc_ctx->state.gpr[29] = v; return;
		case PPC_REGISTER_R30: ppc_ctx->state.gpr[30] = v; return;
		case PPC_REGISTER_R31: ppc_ctx->state.gpr[31] = v; return;
		case PPC_REGISTER_PC: ppc_ctx->state.cia = v; return;
		case PPC_REGISTER_CR0: ppc_ctx->state.cr[0] = v; return;
		case PPC_REGISTER_CR1: ppc_ctx->state.cr[1] = v; return;
		case PPC_REGISTER_CR2: ppc_ctx->state.cr[2] = v; return;
		case PPC_REGISTER_CR3: ppc_ctx->state.cr[3] = v; return;
		case PPC_REGISTER_CR4: ppc_ctx->state.cr[4] = v; return;
		case PPC_REGISTER_CR5: ppc_ctx->state.cr[5] = v; return;
		case PPC_REGISTER_CR6: ppc_ctx->state.cr[6] = v; return;
		case PPC_REGISTER_CR7: ppc_ctx->state.cr[7] = v; return;
		case PPC_REGISTER_XER: ppc_ctx->state.xer = v; return;
		case PPC_REGISTER_LR: ppc_ctx->state.lr = v; return;
		case PPC_REGISTER_CTR: ppc_ctx->state.ctr = v; return;
#ifdef CONFIG_ENABLE_FPU
		case PPC_REGISTER_FPR0: ppc_ctx->state.fpr[0] = v; return;
		case PPC_REGISTER_FPR1: ppc_ctx->state.fpr[1] = v; return;
		case PPC_REGISTER_FPR2: ppc_ctx->state.fpr[2] = v; return;
		case PPC_REGISTER_FPR3: ppc_ctx->state.fpr[3] = v; return;
		case PPC_REGISTER_FPR4: ppc_ctx->state.fpr[4] = v; return;
		case PPC_REGISTER_FPR5: ppc_ctx->state.fpr[5] = v; return;
		case PPC_REGISTER_FPR6: ppc_ctx->state.fpr[6] = v; return;
		case PPC_REGISTER_FPR7: ppc_ctx->state.fpr[7] = v; return;
		case PPC_REGISTER_FPR8: ppc_ctx->state.fpr[8] = v; return;
		case PPC_REGISTER_FPR9: ppc_ctx->state.fpr[9] = v; return;
		case PPC_REGISTER_FPR10: ppc_ctx->state.fpr[10] = v; return;
		case PPC_REGISTER_FPR11: ppc_ctx->state.fpr[11] = v; return;
		case PPC_REGISTER_FPR12: ppc_ctx->state.fpr[12] = v; return;
		case PPC_REGISTER_FPR13: ppc_ctx->state.fpr[13] = v; return;
		case PPC_REGISTER_FPR14: ppc_ctx->state.fpr[14] = v; return;
		case PPC_REGISTER_FPR15: ppc_ctx->state.fpr[15] = v; return;
		case PPC_REGISTER_FPR16: ppc_ctx->state.fpr[16] = v; return;
		case PPC_REGISTER_FPR17: ppc_ctx->state.fpr[17] = v; return;
		case PPC_REGISTER_FPR18: ppc_ctx->state.fpr[18] = v; return;
		case PPC_REGISTER_FPR19: ppc_ctx->state.fpr[19] = v; return;
		case PPC_REGISTER_FPR20: ppc_ctx->state.fpr[20] = v; return;
		case PPC_REGISTER_FPR21: ppc_ctx->state.fpr[21] = v; return;
		case PPC_REGISTER_FPR22: ppc_ctx->state.fpr[22] = v; return;
		case PPC_REGISTER_FPR23: ppc_ctx->state.fpr[23] = v; return;
		case PPC_REGISTER_FPR24: ppc_ctx->state.fpr[24] = v; return;
		case PPC_REGISTER_FPR25: ppc_ctx->state.fpr[25] = v; return;
		case PPC_REGISTER_FPR26: ppc_ctx->state.fpr[26] = v; return;
		case PPC_REGISTER_FPR27: ppc_ctx->state.fpr[27] = v; return;
		case PPC_REGISTER_FPR28: ppc_ctx->state.fpr[28] = v; return;
		case PPC_REGISTER_FPR29: ppc_ctx->state.fpr[29] = v; return;
		case PPC_REGISTER_FPR30: ppc_ctx->state.fpr[30] = v; return;
		case PPC_REGISTER_FPR31: ppc_ctx->state.fpr[31] = v; return;
		case PPC_REGISTER_FPSCR: ppc_ctx->state.fpscr = v; return;
#endif
	}
	assert(0 && "invalid register number");
}

static void _cpu_powerpc_save_register_state(void *cpu_ctx, void *buffer)
{
	struct _PPCContext *const ppc_ctx = cpu_ctx;
	struct _PPCState *const state = buffer;
	*state = ppc_ctx->state;
}

static void _cpu_powerpc_load_register_state(void *cpu_ctx, const void *buffer)
{
	struct _PPCContext *const ppc_ctx = cpu_ctx;
	const struct _PPCState *const state = buffer;
	ppc_ctx->state = *state;
}

static int _cpu_powerpc_run_step(void *cpu_ctx)
{
	unsigned int instruction;
	unsigned char buffer[sizeof(instruction)];
#ifdef DEBUG_DISASSEMBLE
	cs_insn *insns;
	size_t insn_count;
#endif
	struct _PPCContext *const ppc_ctx = cpu_ctx;

	/* Execute (read) instruction */
	if (!mem_execute_ok(ppc_ctx->state.cia, buffer, sizeof(buffer))) {
		fprintf(stderr, "[CPUPowerPC] Reading instruction at 0x%X failed, "
			"stopping\n", ppc_ctx->state.cia);
#ifdef DEBUG_SHOW_REGISTERS_ON_ERROR
		_cpu_powerpc_print_registers(ppc_ctx);
#endif
		return -1;
	}

	/* Convert to native endianness */
	if (ppc_ctx->state.big_endian)
		instruction = read_u32be(buffer);
	else
		instruction = read_u32le(buffer);

#ifdef DEBUG_DISASSEMBLE
#ifndef DEBUG_DISASSEMBLE_ONLY_ON_ERROR
	/* Disassemble this instruction */
	insn_count = cs_disasm(_dis, buffer, sizeof(buffer),
		ppc_ctx->state.cia, 0, &insns);
	if (insn_count == 0)
		printf("0x%08X: <disassembly failed>\n", ppc_ctx->state.cia);
	else {
		size_t i;
		for (i = 0; i < insn_count; ++i) {
			printf("%08I64X:  %08X\t\t%s\t%s\n", insns[i].address,
				instruction, insns[i].mnemonic, insns[i].op_str);
		}
		cs_free(insns, insn_count);
	}
#endif
#endif

	/* Dispatch to instruction handler. If it fails, stop execution */
	if (!_ppc_execute_insn(ppc_ctx, instruction)) {
		printf("[CPUPowerPC] Stopping CPU because of execution error\n");
#ifdef DEBUG_DISASSEMBLE
#ifdef DEBUG_DISASSEMBLE_ONLY_ON_ERROR
		/* Disassemble instruction which failed */
		insn_count = cs_disasm(_dis, buffer, sizeof(buffer),
			ppc_ctx->state.cia, 0, &insns);
		if (insn_count == 0)
			printf("0x%08X: <disassembly failed>\n", ppc_ctx->state.cia);
		else {
			size_t i;
			for (i = 0; i < insn_count; ++i) {
				printf("%08I64X:  %08X\t\t%s\t%s\n", insns[i].address,
					instruction, insns[i].mnemonic, insns[i].op_str);
			}
			cs_free(insns, insn_count);
		}
#endif
#endif
#ifdef DEBUG_SHOW_REGISTERS_ON_ERROR
		_cpu_powerpc_print_registers(ppc_ctx);
#endif
		return -1;
	}
	return 0;
}


/*
 * C interface
 */

const struct CPUInterface cpu_interface_powerpc = {
	CPU_ARCHITECTURE_POWERPC_32,
	sizeof(struct _PPCContext),
	sizeof(struct _PPCState),
	PPC_REGISTER_LAST,

	/* Functions */
	_cpu_powerpc_initialize,
	_cpu_powerpc_destroy,
	_cpu_powerpc_reset,
	_cpu_powerpc_on_stop,
	_cpu_powerpc_print_registers,
	_cpu_powerpc_get_register,
	_cpu_powerpc_set_register,
	_cpu_powerpc_save_register_state,
	_cpu_powerpc_load_register_state,
	_cpu_powerpc_run_step
};
