/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef HW_PPC_IMPL_H
#define HW_PPC_IMPL_H

/*!
 * \file src/hw/ppc_impl.h
 * \brief Implementation details of a the 32-bit PowerPC processor simulation.
 */

#include "powerpc.h"
#include "cpu_impl.h"
#include "../portab/stdbool.h"


#ifdef __cplusplus
extern "C" {
#endif


/*!
 * Register state of the PowerPC CPU
 */
struct _PPCState
{
	/*! General purpose registers (r0-r31) */
	unsigned int gpr[32];

	/*! Current instruction address */
	unsigned int cia;

	/*! 8x 4-bit condition register */
	unsigned char cr[8];

	/*! Integer exception register */
	unsigned int xer;

	/*! Link register */
	unsigned int lr;

	/*! Counter register */
	unsigned int ctr;

#ifdef CONFIG_ENABLE_FPU
	/*! Floating point registers (fpr0-fpr31) */
	uint64_t fpr[32];

	/*! Floating point status register */
	unsigned int fpscr;
#endif

    /*! CPU endianness */
	unsigned int big_endian;
};


/*!
 * PowerPC CPU context
 */
struct _PPCContext
{
	/* Must be inherited by all CPU implementations */
	struct CPUContext common;

	/* System call handler */
	PowerPCSyscallHandler syscall_handler;
	void *syscall_user;

	/* Internal state tracking */
	unsigned int pc_advanced;

	/* CPU state (can be saved and restored) */
	struct _PPCState state;
};


#ifdef CONFIG_ENABLE_FPU
/*!
 * \fn _ppc_fpu_opcode_63
 * \brief
 *    Executes FPU instructions under opcode 63.
 * \param ppc_ctx
 *    CPU context
 * \param opcode
 *    Always 63.
 * \param instruction
 *    CPU instruction (as a 32-bit word)
 * \return
 *    true if successfully executed, false otherwise
 */
bool _ppc_fpu_opcode_63(struct _PPCContext *ppc_ctx, uint32_t opcode,
	uint32_t instruction);
#endif

/*!
 * \fn _ppc_execute_insn
 * \brief
 *    Executes one PowerPC instruction.
 * \param ppc_ctx
 *    CPU context
 * \param instruction
 *    CPU instruction (as a 32-bit word)
 * \return
 *    true if successfully executed, false otherwise
 */
bool _ppc_execute_insn(struct _PPCContext *ppc_ctx, uint32_t instruction);


#ifdef __cplusplus
}
#endif

#endif
