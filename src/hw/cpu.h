/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef HW_CPU_H
#define HW_CPU_H

/*!
 * \file src/hw/cpu.h
 * \brief Interface for running code in a simulated processor.
 */

#include "../iface/memory.h"


#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned __int64 cpu_register_t;

/* Known CPU architectures */
enum CPUArchitecture
{
	CPU_ARCHITECTURE_POWERPC_32 = 0,
};

struct CPUInterface;
struct CPUContext;

/* Creates a new CPU using the specified implementation. */
int cpu_create(const struct CPUInterface *cpu_iface, struct CPUContext **ctx);

/* Destroys the CPU. */
int cpu_destroy(struct CPUContext **ctx);

/* Returns the architecture the CPU implements. */
int cpu_get_arch(struct CPUContext *ctx, enum CPUArchitecture *arch);

/* Attaches memory to the CPU. */
int cpu_set_memory(struct CPUContext *ctx,
	const struct MemoryInterface *memory_iface, void *memory_ctx);

/* Resets all CPU registers to their default state. */
int cpu_reset(struct CPUContext *ctx);

/* Stops the CPU after the current instruction has been completed. This
   can also be called from callback routines. */
int cpu_stop(struct CPUContext *ctx, unsigned int intended);

/* Checks whether a CPU stop was intended. This function fails if the
   CPU did not stop. */
int cpu_is_stop_intended(struct CPUContext *ctx, unsigned int *intended);

/* Prints the CPU's registers. */
int cpu_print_registers(struct CPUContext *ctx);

/* Returns the value of a CPU register. */
int cpu_get_register(struct CPUContext *ctx, unsigned int regnum,
	cpu_register_t *value);

/* Sets the value of a CPU register. */
int cpu_set_register(struct CPUContext *ctx, unsigned int regnum,
	cpu_register_t value);

/* Saves the current register state into `buffer`. */
int cpu_save_register_state(struct CPUContext *ctx, void *buffer,
	unsigned int *buffer_size);

/* Loads the current register state from `buffer`. */
int cpu_load_register_state(struct CPUContext *ctx, const void *buffer,
	unsigned int buffer_size);

/* Operates the CPU until an error occurs or the specified address has
   been reached. */
int cpu_run(struct CPUContext *ctx, memory_address_t exec_stop_address);

#ifdef __cplusplus
}
#endif

#endif
