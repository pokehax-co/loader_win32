/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef HW_POWERPC_H
#define HW_POWERPC_H

/*!
 * \file src/hw/powerpc.h
 * \brief Partial simulation of a 32-bit PowerPC processor, sufficient for
 *        executing user-mode code.
 */

#include "cpu.h"
#include "../portab/stdint.h"


#ifdef __cplusplus
extern "C" {
#endif

/* CPU register numbers */
enum PowerPCRegister
{
	PPC_REGISTER_PC = 0,
	PPC_REGISTER_R0,
	PPC_REGISTER_R1,
	PPC_REGISTER_R2,
	PPC_REGISTER_R3,
	PPC_REGISTER_R4,
	PPC_REGISTER_R5,
	PPC_REGISTER_R6,
	PPC_REGISTER_R7,
	PPC_REGISTER_R8,
	PPC_REGISTER_R9,
	PPC_REGISTER_R10,
	PPC_REGISTER_R11,
	PPC_REGISTER_R12,
	PPC_REGISTER_R13,
	PPC_REGISTER_R14,
	PPC_REGISTER_R15,
	PPC_REGISTER_R16,
	PPC_REGISTER_R17,
	PPC_REGISTER_R18,
	PPC_REGISTER_R19,
	PPC_REGISTER_R20,
	PPC_REGISTER_R21,
	PPC_REGISTER_R22,
	PPC_REGISTER_R23,
	PPC_REGISTER_R24,
	PPC_REGISTER_R25,
	PPC_REGISTER_R26,
	PPC_REGISTER_R27,
	PPC_REGISTER_R28,
	PPC_REGISTER_R29,
	PPC_REGISTER_R30,
	PPC_REGISTER_R31,
	PPC_REGISTER_CR0,
	PPC_REGISTER_CR1,
	PPC_REGISTER_CR2,
	PPC_REGISTER_CR3,
	PPC_REGISTER_CR4,
	PPC_REGISTER_CR5,
	PPC_REGISTER_CR6,
	PPC_REGISTER_CR7,
	PPC_REGISTER_XER,
	PPC_REGISTER_LR,
	PPC_REGISTER_CTR,
#ifdef CONFIG_ENABLE_FPU
	PPC_REGISTER_FPR0,
	PPC_REGISTER_FPR1,
	PPC_REGISTER_FPR2,
	PPC_REGISTER_FPR3,
	PPC_REGISTER_FPR4,
	PPC_REGISTER_FPR5,
	PPC_REGISTER_FPR6,
	PPC_REGISTER_FPR7,
	PPC_REGISTER_FPR8,
	PPC_REGISTER_FPR9,
	PPC_REGISTER_FPR10,
	PPC_REGISTER_FPR11,
	PPC_REGISTER_FPR12,
	PPC_REGISTER_FPR13,
	PPC_REGISTER_FPR14,
	PPC_REGISTER_FPR15,
	PPC_REGISTER_FPR16,
	PPC_REGISTER_FPR17,
	PPC_REGISTER_FPR18,
	PPC_REGISTER_FPR19,
	PPC_REGISTER_FPR20,
	PPC_REGISTER_FPR21,
	PPC_REGISTER_FPR22,
	PPC_REGISTER_FPR23,
	PPC_REGISTER_FPR24,
	PPC_REGISTER_FPR25,
	PPC_REGISTER_FPR26,
	PPC_REGISTER_FPR27,
	PPC_REGISTER_FPR28,
	PPC_REGISTER_FPR29,
	PPC_REGISTER_FPR30,
	PPC_REGISTER_FPR31,
	PPC_REGISTER_FPSCR,
	PPC_REGISTER_LAST = PPC_REGISTER_FPSCR
#else
	PPC_REGISTER_LAST = PPC_REGISTER_CTR
#endif
};

/* Handler function when a system call instruction has been reached.
   The "sc" instruction does not hold system call numbers. */
typedef unsigned int (*PowerPCSyscallHandler)(struct CPUContext *ctx,
	void *user);

/* Configures the endianness of the CPU. */
int cpu_powerpc_set_big_endian(struct CPUContext *ctx,
	unsigned int big_endian);

/* Sets the system call handler function. */
int cpu_powerpc_set_syscall_handler(struct CPUContext *ctx,
	PowerPCSyscallHandler handler, void *user);

/* Reads a 16-bit word in the CPU's native endianness. */
enum MemoryStatus cpu_powerpc_read_16(struct CPUContext *ctx,
	memory_address_t address, unsigned short *value);

/* Reads a 32-bit word in the CPU's native endianness. */
enum MemoryStatus cpu_powerpc_read_32(struct CPUContext *ctx,
	memory_address_t address, unsigned int *value);

/* Reads a 64-bit word in the CPU's native endianness. */
enum MemoryStatus cpu_powerpc_read_64(struct CPUContext *ctx,
	memory_address_t address, uint64_t *value);

/* Writes a 16-bit word in the CPU's native endianness. */
enum MemoryStatus cpu_powerpc_write_16(struct CPUContext *ctx,
	memory_address_t address, unsigned short value);

/* Writes a 32-bit word in the CPU's native endianness. */
enum MemoryStatus cpu_powerpc_write_32(struct CPUContext *ctx,
	memory_address_t address, unsigned int value);

/* Writes a 64-bit word in the CPU's native endianness. */
enum MemoryStatus cpu_powerpc_write_64(struct CPUContext *ctx,
	memory_address_t address, uint64_t value);

/* Implementation interface of the PowerPC CPU. */
extern const struct CPUInterface cpu_interface_powerpc;

#ifdef __cplusplus
}
#endif

#endif
