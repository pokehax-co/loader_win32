/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

/*
 * PowerPC emulation, opcode implementations.
 * This file is embedded by powerpc.c.
 */


/*
 * Fallback function if the opcode is not implemented
 */
static bool _ppc_opcode_unhandled(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	(void)ppc_ctx;
	(void)instruction;
	fprintf(stderr, "[CPUPowerPC] Unsupported opcode %u\n", opcode);
	return 0;
}

/*
 * Opcode 3 (twi) checks for traps.
 */
static bool _ppc_opcode_3(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	int SIMM = sign16_extend(get_bits32_big(instruction, 16, 31));
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int TO = get_bits32_big(instruction, 6, 10);

	const int a = (int)ppc_ctx->state.gpr[A];

	(void)opcode;

	if (a < SIMM && (TO & 0x10)) {
		printf("[CPUPowerPC] Trap: %d < %d\n", a, SIMM);
		goto trap;
	}
	if (a > SIMM && (TO & 0x08)) {
		printf("[CPUPowerPC] Trap: %d > %d\n", a, SIMM);
		goto trap;
	}
	if (a == SIMM && (TO & 0x04)) {
		printf("[CPUPowerPC] Trap: %d == %d\n", a, SIMM);
		goto trap;
	}
	if (ppc_ctx->state.gpr[A] < (unsigned int)SIMM && (TO & 0x02)) {
		printf("[CPUPowerPC] Trap: %u <U %d\n",
			ppc_ctx->state.gpr[A], (unsigned int)SIMM);
		goto trap;
	}
	if (ppc_ctx->state.gpr[A] > (unsigned int)SIMM && (TO & 0x01)) {
		printf("[CPUPowerPC] Trap: %u >U %d\n",
			ppc_ctx->state.gpr[A], (unsigned int)SIMM);
		goto trap;
	}

	/* No trap raised */
	return 1;

trap:
	// FIXME: We don't handle traps yet
	cpu_stop(&ppc_ctx->common, 0);
	return 0;
}


/*
 * Opcode 7 (mulli) multiplies rA with a 16-bit signed integer.
 */
static bool _ppc_opcode_7(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	int SIMM = sign16_extend(get_bits32_big(instruction, 16, 31));
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);

	const uint64_t result = (uint64_t)(
		((int64_t)(int)ppc_ctx->state.gpr[A]) *
		((int64_t)SIMM)
	);

	(void)opcode;

	ppc_ctx->state.gpr[D] = (unsigned int)(result & 0xFFFFFFFFu);
	return 1;
}

/*
 * Opcode 8 (subfic) subtracts rA from a 16-bit signed immediate.
 */
static bool _ppc_opcode_8(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	int SIMM = sign16_extend(get_bits32_big(instruction, 16, 31));
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);

	const int result = SIMM - ((int)ppc_ctx->state.gpr[A]);

	(void)opcode;

	_ppc_set_xer_ca(ppc_ctx, ppc_ctx->state.gpr[A] == 0 ||
		_ppc_carry(0 - ppc_ctx->state.gpr[A], SIMM));

	ppc_ctx->state.gpr[D] = result & 0xFFFFFFFFu;
	return 1;
}

/*
 * Opcode 10 (cmpl(w)i) compares a register with an immediate (unsigned).
 */
static bool _ppc_opcode_10(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int UIMM = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int crfD = get_bits32_big(instruction, 6, 8);

	(void)opcode;

	if (get_bit32_big(instruction, 9)) {
		fprintf(stderr, "[CPUPowerPC] cmpl(w)i reserved set\n");
		return 0;
	}
	if (get_bit32_big(instruction, 10)) {
		fprintf(stderr, "[CPUPowerPC] cmpl(w)i this is a 32-bit CPU\n");
		return 0;
	}

	/* Determine CR bits */
	ppc_ctx->state.cr[crfD] = _ppc_cr_unsigned(ppc_ctx,
		ppc_ctx->state.gpr[A], UIMM);
	return 1;
}

/*
 * Opcode 11 (cmp(w)i) compares a register with an immediate (signed).
 */
static bool _ppc_opcode_11(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const int SIMM = sign16_extend(get_bits32_big(instruction, 16, 31));
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int crfD = get_bits32_big(instruction, 6, 8);

	(void)opcode;

	if (get_bit32_big(instruction, 9)) {
		fprintf(stderr, "[CPUPowerPC] cmp(w)i reserved set\n");
		return 0;
	}
	if (get_bit32_big(instruction, 10)) {
		fprintf(stderr, "[CPUPowerPC] cmp(w)i this is a 32-bit CPU\n");
		return 0;
	}

	/* Determine CR bits */
	ppc_ctx->state.cr[crfD] = _ppc_cr_signed(ppc_ctx,
		ppc_ctx->state.gpr[A], SIMM);
	return 1;
}

/*
 * Opcode 14 (addi) / 15 (addis) adds a signed immediate to a register.
 */
static bool _ppc_opcode_14_15(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	int SIMM = sign16_extend(get_bits32_big(instruction, 16, 31));
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);

	/* We can't shift here because the number is signed */
	if (opcode == 15)
		SIMM *= 0x10000;

	ppc_ctx->state.gpr[D] = (A == 0 ? 0 : ppc_ctx->state.gpr[A]) + SIMM;
	return 1;
}

/*
 * Opcode 16 (bc/bca/bcl/bcla) performs a conditional branch.
 * The branch can be relative to CIA (bc/bcl) or absolute (bca/bcla),
 * and it can update the link register (bcl/bcla -- function call) or not
 * (bc/bca -- jump).
 */
static bool _ppc_opcode_16(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int LK = get_bit32_big(instruction, 31);
	const unsigned int AA = get_bit32_big(instruction, 30);
	const int BD = sign16_extend(get_bits32_big(instruction, 16, 29) << 2);
	const unsigned int BI = get_bits32_big(instruction, 11, 15);
	const unsigned int BO = get_bits32_big(instruction, 6, 10);
	unsigned int ctr_ok, cond_ok;

	(void)opcode;

	/* Update link register */
	if (LK)
		ppc_ctx->state.lr = ppc_ctx->state.cia + 4;

	/* Decrement counter if BO bit 2 is unset */
	if (!(BO & 0x4))
		--ppc_ctx->state.ctr;

	/* Perform counter and condition check */
	_ppc_cond_check(ppc_ctx, BI, BO, &ctr_ok, &cond_ok);
	if (ctr_ok && cond_ok) {
		if (AA)
			ppc_ctx->state.cia = BD;
		else
			ppc_ctx->state.cia += BD;
		ppc_ctx->pc_advanced = 0;
	}
	return 1;
}

/*
 * Opcode 17 (sc) performs a system call. We're a simulator, so let's call the
 * callback routine and return as if nothing has happened.
 * Note that we can't provide a system call number because it cannot be
 * embedded into the instruction encoding.
 */
static bool _ppc_opcode_17(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	(void)opcode;

	if (instruction != 0x44000002u) {
		fprintf(stderr, "[CPUPowerPC] Invalid sc encoding 0x%X\n",
			instruction);
		return 0;
	}
	if (!ppc_ctx->syscall_handler) {
		fprintf(stderr, "[CPUPowerPC] No syscall handler defined\n");
		return 0;
	}
	if (!ppc_ctx->syscall_handler(&ppc_ctx->common, ppc_ctx->syscall_user)) {
		fprintf(stderr, "Syscall handler failed\n");
		return 0;
	}
	return 1;
}

/*
 * Opcode 18 (b/ba/bl/bla) performs an unconditional branch.
 * The branch can be relative to CIA (b/bl) or absolute (ba/bla),
 * and it can update the link register (bl/bla -- function call) or not
 * (b/ba -- jump).
 */
static bool _ppc_opcode_18(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int LK = get_bit32_big(instruction, 31);
	const unsigned int AA = get_bit32_big(instruction, 30);
	const int LI = sign24_extend(
		get_bits32_big(instruction, 6, 29)
	);

	/* Next instruction address */
	const unsigned int NIA = (LI * 4) + (AA ? 0 : ppc_ctx->state.cia);

	(void)opcode;

	/* Update link register */
	if (LK)
		ppc_ctx->state.lr = ppc_ctx->state.cia + 4;

	/* success */
	ppc_ctx->state.cia = NIA;
	ppc_ctx->pc_advanced = 0;
	return 1;
}

/*
 * Opcode 19 performs a conditional branch with non-GP registers.
 */
static bool _ppc_opcode_19(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int LK = get_bit32_big(instruction, 31);
	const unsigned int subopcode = get_bits32_big(instruction, 21, 30);
	const unsigned int reserved = get_bits32_big(instruction, 16, 20);
	const unsigned int BI = get_bits32_big(instruction, 11, 15);
	const unsigned int BO = get_bits32_big(instruction, 6, 10);

	(void)opcode;

	/* bclr / bclrl -- link register */
	if (subopcode == 16) {
		unsigned int ctr_ok, cond_ok;

		if (reserved != 0) {
			fprintf(stderr, "[CPUPowerPC] bclr(l) reserved != 0\n");
			return 0;
		}

		/* Decrement counter if BO bit 2 is unset */
		if (!(BO & 0x4))
			--ppc_ctx->state.ctr;

		/* Perform counter and condition check */
		_ppc_cond_check(ppc_ctx, BI, BO, &ctr_ok, &cond_ok);
		if (ctr_ok && cond_ok) {
			const unsigned int NIA = ppc_ctx->state.lr & ~0x3u;
			if (LK)
				ppc_ctx->state.lr = ppc_ctx->state.cia + 4;

			/* success */
			ppc_ctx->state.cia = NIA;
			ppc_ctx->pc_advanced = 0;
		}
		return 1;
	}

	/* bcctr/cctrl -- counter register */
	else if (subopcode == 528) {
		unsigned int cond_ok;

		if (reserved != 0) {
			fprintf(stderr, "[CPUPowerPC] bcctr(l) reserved != 0\n");
			return 0;
		}

		/* Perform condition check (ignore counter check) */
		_ppc_cond_check(ppc_ctx, BI, BO, NULL, &cond_ok);
		if (cond_ok) {
			const unsigned int NIA = ppc_ctx->state.ctr & ~0x3u;
			if (LK)
				ppc_ctx->state.lr = ppc_ctx->state.cia + 4;

			/* success */
			ppc_ctx->state.cia = NIA;
			ppc_ctx->pc_advanced = 0;
		}
		return 1;
	}

	else {
		fprintf(stderr, "[CPUPowerPC] unsupported 19 subopcode %u\n",
			subopcode);
		return 0;
	}
}

/*
 * Opcode 21 (rlwinm) is the all-round talent of the PowerPC CPU, which
 * performs a left rotation followed by mask application.
 *
 * Opcode 20 (rlwimi) is like rlwinm, but also ORs the result with rA
 * (inverted mask applied).
 *
 * Raymond Chen says: "This instruction does everything except wash your
 * floor."
 */
static bool _ppc_opcode_20_21(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int ME = get_bits32_big(instruction, 26, 30);
	const unsigned int MB = get_bits32_big(instruction, 21, 25);
	const unsigned int SH = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	const unsigned int r = tools_rotl32(ppc_ctx->state.gpr[S], SH);
	const unsigned int m = mask32(MB, ME);
	unsigned int result = r & m;
	if (opcode == 20)
		result |= (ppc_ctx->state.gpr[A] & ~m);
	ppc_ctx->state.gpr[A] = result;

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * Opcode 24 (ori) / 25 (oris) performs a logical OR between a register
 * and a 16-bit unsigned immediate.
 */
static bool _ppc_opcode_24_25(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	unsigned int UIMM = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	if (opcode == 25)
		UIMM <<= 16;

	ppc_ctx->state.gpr[A] = ppc_ctx->state.gpr[S] | UIMM;

	return 1;
}

/*
 * Opcode 26 (xori) / 27 (xoris) performs a logical XOR between a register
 * and a 16-bit unsigned immediate (lower half or upper half).
 */
static bool _ppc_opcode_26_27(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	unsigned int UIMM = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	if (opcode == 27)
		UIMM <<= 16;

	ppc_ctx->state.gpr[A] = ppc_ctx->state.gpr[S] ^ UIMM;

	return 1;
}

/*
 * Opcode 28 (andi) / 29 (andis) performs a logical AND between a register
 * and a 16-bit unsigned immediate (lower half or upper half).
 */
static bool _ppc_opcode_28_29(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	unsigned int UIMM = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	const unsigned int result = ppc_ctx->state.gpr[S] & (
		UIMM << ((opcode & 0x1u) << 4)
	);
	ppc_ctx->state.gpr[A] = result;

	/* Update cr0 */
	_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * Opcode 31 implements various instructions with only registers as their
 * operand.
 */
#include "ppcins31.inl"

/*
 * Opcodes 32 (lwz) and 33 (lwzu) load a word from memory (base address in
 * register plus a displacement).
 * In addition, lwzu updates a register with the calculated address.
 */
static bool _ppc_opcode_32_33(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode == 33;
	const unsigned int d = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int EA;

	/* Can't update address to r0 or to rA if D == A */
	const unsigned int b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && (A == 0 || A == D)) {
		fprintf(stderr, "[CPUPowerPC] lwzu can't be used with r0 "
			"or rA = rD\n");
		return 0;
	}
	EA = b + sign16_extend(d);

#ifdef DEBUG_PPC_SHOW_R2_LOADS
	/* Print EA if loading a word from the TOC */
	if (A == 2)
		printf("[CPUPowerPC] lwz(u) from TOC, EA = 0x%X\n", EA);
#endif

	/* Perform memory operation */
	if (cpu_powerpc_read_32(&ppc_ctx->common, EA, &ppc_ctx->state.gpr[D]) !=
			MEMORY_STATUS_OK) {
		fprintf(stderr, "[CPUPowerPC] lwz(u) failed, EA=0x%X\n", EA);
		return 0;
	}

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * Opcodes 34 (lbz) and 35 (lbzu) load a byte from memory (base address in
 * register plus a displacement).
 * In addition, lbzu updates a register with the calculated address.
 */
static bool _ppc_opcode_34_35(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode == 35;
	const unsigned int d = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int EA;
	unsigned char byte;

	/* Can't update address to r0 or to rA if D == A */
	const unsigned int b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && (A == 0 || A == D)) {
		fprintf(stderr, "[CPUPowerPC] lbzu can't be used with r0 "
			"or rA = rD\n");
		return 0;
	}
	EA = b + sign16_extend(d);

#ifdef DEBUG_PPC_SHOW_R2_LOADS
	/* Print EA if loading a word from the TOC */
	if (A == 2)
		printf("[CPUPowerPC] lbz(u) from TOC, EA = 0x%X\n", EA);
#endif

	/* Perform memory operation */
	if (!mem_read_ok(EA, &byte, 1)) {
		fprintf(stderr, "[CPUPowerPC] lbz(u) failed, EA=0x%X\n", EA);
		return 0;
	}
	ppc_ctx->state.gpr[D] = byte;

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * Opcodes 36 (stw) and 37 (stwu) write a word to memory (base address in
 * register plus a displacement).
 * In addition, stwu updates a register with the calculated address.
 */
static bool _ppc_opcode_36_37(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode == 37;
	const unsigned int d = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);
	unsigned int EA;

	/* Can't update address to r0 */
	const unsigned int b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && A == 0) {
		fprintf(stderr, "[CPUPowerPC] stwu can't be used with r0\n");
		return 0;
	}
	EA = b + sign16_extend(d);

	/* Perform memory operation */
	if (cpu_powerpc_write_32(&ppc_ctx->common, EA, ppc_ctx->state.gpr[S]) !=
			MEMORY_STATUS_OK) {
		fprintf(stderr, "[CPUPowerPC] stw(u) failed, EA=0x%X\n", EA);
		return 0;
	}

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * Opcodes 38 (stb) and 39 (stbu) write a word to memory (base address in
 * register plus a displacement).
 * In addition, stbu updates a register with the calculated address.
 */
static bool _ppc_opcode_38_39(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode == 39;
	const unsigned int d = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);
	unsigned int EA;
	unsigned char byte;

	/* Can't update address to r0 */
	const unsigned int b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && A == 0) {
		fprintf(stderr, "[CPUPowerPC] stbu can't be used with r0\n");
		return 0;
	}
	EA = b + sign16_extend(d);

	/* Perform memory operation */
	byte = (unsigned char)ppc_ctx->state.gpr[S];
	if (!mem_write_ok(EA, &byte, 1)) {
		fprintf(stderr, "[CPUPowerPC] stb(u) failed, EA=0x%X\n", EA);
		return 0;
	}

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * Opcode 40 (lhz) loads a halfword from memory (base address in
 * register plus a displacement).
 * Opcode 42 (lha) sign-extends the read value.
 * Opcodes 41 (lhzu) and 43 (lhzu) update a register with the calculated
 * address.
 */
static bool _ppc_opcode_40_41_42_43(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode & 0x1u;
	const unsigned int extend_sign = opcode & 0x2u;
	const unsigned int d = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int EA;
	unsigned short halfword;

	/* Can't update address to r0 or to rA if D == A */
	const unsigned int b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && (A == 0 || A == D)) {
		fprintf(stderr, "[CPUPowerPC] lh%cu can't be used with r0 "
			"or rA = rD\n", extend_sign ? 'a' : 'z');
		return 0;
	}
	EA = b + sign16_extend(d);

#ifdef DEBUG_PPC_SHOW_R2_LOADS
	/* Print EA if loading a word from the TOC */
	if (A == 2)
		printf("[CPUPowerPC] lhz(u) from TOC, EA = 0x%X\n", EA);
#endif

	/* Perform memory operation */
	if (cpu_powerpc_read_16(&ppc_ctx->common, EA, &halfword) !=
			MEMORY_STATUS_OK) {
		fprintf(stderr, "[CPUPowerPC] lh%c(u) failed, EA=0x%X\n",
			extend_sign ? 'a' : 'z', EA);
		return 0;
	}
	ppc_ctx->state.gpr[D] = extend_sign ?
		sign16_extend(halfword) : halfword;

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * Opcodes 44 (sth) and 45 (sthu) write a halfword to memory (base address in
 * register plus a displacement).
 * In addition, sthu updates a register with the calculated address.
 */
static bool _ppc_opcode_44_45(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode == 45;
	const unsigned int d = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);
	unsigned int EA;

	/* Can't update address to r0 */
	const unsigned int b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && A == 0) {
		fprintf(stderr, "[CPUPowerPC] sthu can't be used with r0\n");
		return 0;
	}
	EA = b + sign16_extend(d);

	/* Perform memory operation */
	if (cpu_powerpc_write_16(&ppc_ctx->common, EA, ppc_ctx->state.gpr[S]) !=
			MEMORY_STATUS_OK) {
		fprintf(stderr, "[CPUPowerPC] sth(u) failed, EA=0x%X\n", EA);
		return 0;
	}

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

#ifdef CONFIG_ENABLE_FPU

/*
 * Opcodes 50 (lfd) and 51 (lfdu) load a double-precision value from memory
 * (base address in register plus a displacement) into a floating-point
 * register.
 * In addition, sthu updates a register with the calculated address.
 */
static bool _ppc_fpu_opcode_lfd_lfdu(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const bool update = !!(opcode & 0x1u);
	const unsigned int d = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int EA;
	uint64_t fpvalue;

	/* Can't update address to r0 */
	const unsigned int b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && A == 0) {
		fprintf(stderr, "[CPUPowerPC] lfdu can't be used with r0\n");
		return 0;
	}
	EA = b + sign16_extend(d);

#ifdef DEBUG_PPC_SHOW_R2_LOADS
	/* Print EA if loading a word from the TOC */
	if (A == 2)
		printf("[CPUPowerPC] lfd(u) from TOC, EA = 0x%X\n", EA);
#endif

	/* Perform memory operation */
	if (!MEMOP_OK(cpu_powerpc_read_64(&ppc_ctx->common, EA, &fpvalue))) {
		fprintf(stderr, "[CPUPowerPC] lfd(u) failed, EA=0x%X\n", EA);
		return 0;
	}
	ppc_ctx->state.fpr[D] = fpvalue;

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * Opcodes 54 (stfd) and 55 (stfdu) store a double-precision value to memory
 * (base address in register plus a displacement) into a floating-point
 * register.
 * In addition, stfdu updates a register with the calculated address.
 */
static bool _ppc_fpu_opcode_stfd_stfdu(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const bool update = !!(opcode & 0x1u);
	const unsigned int d = get_bits32_big(instruction, 16, 31);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);
	unsigned int EA;

	/* Can't update address to r0 */
	const unsigned int b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && A == 0) {
		fprintf(stderr, "[CPUPowerPC] stfdu can't be used with r0\n");
		return 0;
	}
	EA = b + sign16_extend(d);

	/* Perform memory operation */
	if (!MEMOP_OK(cpu_powerpc_write_64(&ppc_ctx->common, EA,
	        ppc_ctx->state.fpr[S]))) {
		fprintf(stderr, "[CPUPowerPC] stfd(u) failed, EA=0x%X\n", EA);
		return 0;
	}

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

#else /* CONFIG_ENABLE_FPU */

/*
 * Dummy function in case FPU support has been disabled.
 */
static bool _ppc_fpu_disabled(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	(void)ppc_ctx;
	(void)opcode;
	(void)instruction;

	fprintf(stderr, "[CPUPowerPC] FPU support has not been compiled in\n");
	return 0;
}
#endif


/*
 * Dispatch table, used by _ppc_execute()
 */
static const _PPCOpcodeFunction _ppc_opcode_dispatch[64] = {
	/*  0 */ _ppc_opcode_unhandled,
	/*  1 */ _ppc_opcode_unhandled,
	/*  2 */ _ppc_opcode_unhandled,
	/*  3 */ _ppc_opcode_3,
	/*  4 */ _ppc_opcode_unhandled,
	/*  5 */ _ppc_opcode_unhandled,
	/*  6 */ _ppc_opcode_unhandled,
	/*  7 */ _ppc_opcode_7,
	/*  8 */ _ppc_opcode_8,
	/*  9 */ _ppc_opcode_unhandled,
	/* 10 */ _ppc_opcode_10,
	/* 11 */ _ppc_opcode_11,
	/* 12 */ _ppc_opcode_unhandled,
	/* 13 */ _ppc_opcode_unhandled,
	/* 14 */ _ppc_opcode_14_15,
	/* 15 */ _ppc_opcode_14_15,
	/* 16 */ _ppc_opcode_16,
	/* 17 */ _ppc_opcode_17,
	/* 18 */ _ppc_opcode_18,
	/* 19 */ _ppc_opcode_19,
	/* 20 */ _ppc_opcode_20_21,
	/* 21 */ _ppc_opcode_20_21,
	/* 22 */ _ppc_opcode_unhandled,
	/* 23 */ _ppc_opcode_unhandled,
	/* 24 */ _ppc_opcode_24_25,
	/* 25 */ _ppc_opcode_24_25,
	/* 26 */ _ppc_opcode_26_27,
	/* 27 */ _ppc_opcode_26_27,
	/* 28 */ _ppc_opcode_28_29,
	/* 29 */ _ppc_opcode_28_29,
	/* 30 */ _ppc_opcode_unhandled,
	/* 31 */ _ppc_opcode_31,
	/* 32 */ _ppc_opcode_32_33,
	/* 33 */ _ppc_opcode_32_33,
	/* 34 */ _ppc_opcode_34_35,
	/* 35 */ _ppc_opcode_34_35,
	/* 36 */ _ppc_opcode_36_37,
	/* 37 */ _ppc_opcode_36_37,
	/* 38 */ _ppc_opcode_38_39,
	/* 39 */ _ppc_opcode_38_39,
	/* 40 */ _ppc_opcode_40_41_42_43,
	/* 41 */ _ppc_opcode_40_41_42_43,
	/* 42 */ _ppc_opcode_40_41_42_43,
	/* 43 */ _ppc_opcode_40_41_42_43,
	/* 44 */ _ppc_opcode_44_45,
	/* 45 */ _ppc_opcode_44_45,
	/* 46 */ _ppc_opcode_unhandled,
	/* 47 */ _ppc_opcode_unhandled,
	/* 48 */ _ppc_opcode_unhandled,
	/* 49 */ _ppc_opcode_unhandled,
#ifdef CONFIG_ENABLE_FPU
	/* 50 */ _ppc_fpu_opcode_lfd_lfdu,
	/* 51 */ _ppc_fpu_opcode_lfd_lfdu,
#else
	/* 50 */ _ppc_fpu_disabled,
	/* 51 */ _ppc_fpu_disabled,
#endif
	/* 52 */ _ppc_opcode_unhandled,
	/* 53 */ _ppc_opcode_unhandled,
#ifdef CONFIG_ENABLE_FPU
	/* 54 */ _ppc_fpu_opcode_stfd_stfdu,
	/* 55 */ _ppc_fpu_opcode_stfd_stfdu,
#else
	/* 54 */ _ppc_fpu_disabled,
	/* 55 */ _ppc_fpu_disabled,
#endif
	/* 56 */ _ppc_opcode_unhandled,
	/* 57 */ _ppc_opcode_unhandled,
	/* 58 */ _ppc_opcode_unhandled,
	/* 59 */ _ppc_opcode_unhandled,
	/* 60 */ _ppc_opcode_unhandled,
	/* 61 */ _ppc_opcode_unhandled,
	/* 62 */ _ppc_opcode_unhandled,
#ifdef CONFIG_ENABLE_FPU
	/* 63 */ _ppc_fpu_opcode_63
#else
	/* 63 */ _ppc_fpu_disabled
#endif
};
