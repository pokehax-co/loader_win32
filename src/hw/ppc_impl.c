/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "ppc_impl.h"

#include "../misc/tools.h"

#include <stdio.h>


/*
 * Macros to make memory accesses more readable
 */

#define mem_read_ok(address, buffer, size) \
	(ppc_ctx->common.memory_iface->read(ppc_ctx->common.memory_ctx, \
		(address), (buffer), (size)) == MEMORY_STATUS_OK)

#define mem_write_ok(address, buffer, size) \
	(ppc_ctx->common.memory_iface->write(ppc_ctx->common.memory_ctx, \
		(address), (buffer), (size)) == MEMORY_STATUS_OK)


/*
 * XER register accessors
 */

#define _ppc_get_xer_so(ppc_ctx) \
    (!!(ppc_ctx->state.xer & 0x80000000u))

#define _ppc_set_xer_so(ppc_ctx, v) \
    (ppc_ctx->state.xer = (v) ? \
        (ppc_ctx->state.xer | 0x80000000u) : \
        (ppc_ctx->state.xer & ~0x80000000u))


#define _ppc_get_xer_ov(ppc_ctx) \
    (!!(ppc_ctx->state.xer & 0x40000000u))

#define _ppc_set_xer_ov(ppc_ctx, v) \
    (ppc_ctx->state.xer = (v) ? \
        (ppc_ctx->state.xer | 0x40000000u) : \
        (ppc_ctx->state.xer & ~0x40000000u))


#define _ppc_get_xer_ca(ppc_ctx) \
    (!!(ppc_ctx->state.xer & 0x20000000u))

#define _ppc_set_xer_ca(ppc_ctx, v) \
    (ppc_ctx->state.xer = (v) ? \
        (ppc_ctx->state.xer | 0x20000000u) : \
        (ppc_ctx->state.xer & ~0x20000000u))


/* Function signature of an opcode implementation */
typedef bool (*_PPCOpcodeFunction)(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction);


/*
 * Carry and overflow checking
 */

static __inline unsigned int _ppc_carry(unsigned int a, unsigned int b)
{
	return b > (~a);
}

static __inline unsigned int _ppc_overflow(unsigned int a, unsigned int b,
	unsigned int c)
{
	/* Get MSBs */
	a = get_bit32_big(a, 0);
	b = get_bit32_big(b, 0);
	c = get_bit32_big(c, 0);
	return (a ^ c) & (b ^ c);
}


/*
 * Performs conditional and counter checks.
 * These are identical to the Freescale documentation.
 *
 * Table for understanding BO values:
 * https://blogs.msdn.microsoft.com/oldnewthing/20180815-00/?p=99495
 */
static void _ppc_cond_check(struct _PPCContext *ppc_ctx,
	const unsigned int BI, const unsigned int BO,
	unsigned int *ctr_ok, unsigned int *cond_ok)
{
	if (ctr_ok) {
		*ctr_ok = (BO & 0x4) ||
			((ppc_ctx->state.ctr != 0) ^ ((BO & 0x2) >> 1));
	}
	if (cond_ok) {
		const unsigned int cr_BI = !!(ppc_ctx->state.cr[BI >> 2] &
			(1u << (3 - (BI & 0x3))));
		*cond_ok = (BO & 0x10) || (cr_BI == !!(BO & 0x8));
	}
}


/*
 * Conditional register functions
 */

/* Determines CR bits using unsigned comparison */
static unsigned int _ppc_cr_unsigned(struct _PPCContext *ppc_ctx,
	const unsigned int a, const unsigned int b)
{
	unsigned int cr = _ppc_get_xer_so(ppc_ctx) ? 0x1u : 0x0u;
	if (a < b)
		cr |= 0x8u;
	else if (a > b)
		cr |= 0x4u;
	else
		cr |= 0x2u;
	return cr;
}

/* Determines CR bits using signed comparison */
static unsigned int _ppc_cr_signed(struct _PPCContext *ppc_ctx,
	const signed int a, const signed int b)
{
	unsigned int cr = _ppc_get_xer_so(ppc_ctx) ? 0x1u : 0x0u;
	if (a < b)
		cr |= 0x8u;
	else if (a > b)
		cr |= 0x4u;
	else
		cr |= 0x2u;
	return cr;
}

/* Sets CR0 by comparing v against 0 (signed) */
static __inline void _ppc_set_cr0(struct _PPCContext *ppc_ctx,
	const signed int v)
{
	ppc_ctx->state.cr[0] = _ppc_cr_signed(ppc_ctx, v, 0);
}


/*
 * Opcode implementations
 */

#include "ppcinsns.inl"


/*
 * Main execution function
 */

bool _ppc_execute_insn(struct _PPCContext *ppc_ctx, uint32_t instruction)
{
	bool opcode_ok;

	/* Extract opcode number and use dispatch table */
	const unsigned int opcode = get_bits32_big(instruction, 0, 5);

	ppc_ctx->pc_advanced = 1;
	opcode_ok = _ppc_opcode_dispatch[opcode](ppc_ctx, opcode,
		instruction);

	/* Increment current instruction address on success. This is what most
	   opcodes do, except those which branch to another address. */
	if (opcode_ok) {
		if (!ppc_ctx->common.stopped && ppc_ctx->pc_advanced)
			ppc_ctx->state.cia += 4;
	}
	return !!opcode_ok;
}
