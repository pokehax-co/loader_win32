/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

/*
 * PowerPC emulation, implementation of opcode 31.
 * This file is embedded by powerpc.c.
 */


/*
 * cmp/cmpw (31->0) perform a signed comparison between integers in
 * registers.
 * cmpl/cmplw (31->32) does the same with an unsigned comparison.
 */
static bool _ppc_opcode_31_cmp_cmpl(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int is_unsigned = opcode & 0x20;
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int crfD = get_bits32_big(instruction, 6, 8);

	if (get_bit32_big(instruction, 31)) {
		fprintf(stderr, "[CPUPowerPC] cmp(l)(w) reserved set\n");
		return 0;
	}
	if (get_bit32_big(instruction, 9)) {
		fprintf(stderr, "[CPUPowerPC] cmp(l)(w) reserved set\n");
		return 0;
	}
	if (is_unsigned && get_bit32_big(instruction, 10)) {
		fprintf(stderr, "[CPUPowerPC] cmpl(w) this is a 32-bit CPU\n");
		return 0;
	}

	/* Determine CR bits */
	if (is_unsigned) {
		ppc_ctx->state.cr[crfD] = _ppc_cr_unsigned(ppc_ctx,
			ppc_ctx->state.gpr[A], ppc_ctx->state.gpr[B]);
	} else {
		ppc_ctx->state.cr[crfD] = _ppc_cr_signed(ppc_ctx,
			ppc_ctx->state.gpr[A], ppc_ctx->state.gpr[B]);
	}
	return 1;
}

/*
 * subfc (31->8) subtracts "from carrying".
 */
static bool _ppc_opcode_31_subfc(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);

	/* Perform subtraction */
	const unsigned int a = ~ppc_ctx->state.gpr[A];
	const unsigned int b = ppc_ctx->state.gpr[B];
	const unsigned int result = a + b + 1;
	ppc_ctx->state.gpr[D] = result;

	/* Set carry */
	_ppc_set_xer_ca(ppc_ctx, a == 0xFFFFFFFFu || _ppc_carry(b, a + 1));

	/* Perform overflow checking */
	if (get_bit32_big(instruction, 21)) {
		unsigned int overflowed = _ppc_overflow(a, b, result);
		_ppc_set_xer_ov(ppc_ctx, overflowed);
		_ppc_set_xer_so(ppc_ctx, overflowed || _ppc_get_xer_so(ppc_ctx));
	}

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * mfcr (31->19) dumps all CR registers into a GPR.
 */
static bool _ppc_opcode_31_mfcr(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int D = get_bits32_big(instruction, 6, 10);

	if (get_bit32_big(instruction, 31) ||
			get_bits32_big(instruction, 16, 20) ||
			get_bits32_big(instruction, 11, 15)) {
		fprintf(stderr, "[CPUPowerPC] mfcr reserved set\n");
		return 0;
	}

	ppc_ctx->state.gpr[D] = (
		(((unsigned int)ppc_ctx->state.cr[0]) << 28) |
		(((unsigned int)ppc_ctx->state.cr[1]) << 24) |
		(((unsigned int)ppc_ctx->state.cr[2]) << 20) |
		(((unsigned int)ppc_ctx->state.cr[3]) << 16) |
		(((unsigned int)ppc_ctx->state.cr[4]) << 12) |
		(((unsigned int)ppc_ctx->state.cr[5]) << 8) |
		(((unsigned int)ppc_ctx->state.cr[6]) << 4) |
		(((unsigned int)ppc_ctx->state.cr[7]) << 0)
	);

	return 1;
}

/*
 * lwzx (31->23) performs an indirect load with all operands in registers,
 * loading 4 bytes.
 * lwzux (31->55) also updates a GPR with the load address.
 */
static bool _ppc_opcode_31_lwzx(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode & 0x20u;
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int b, EA;

	if (get_bit32_big(instruction, 31)) {
		fprintf(stderr, "[CPUPowerPC] lwz(u)x reserved set\n");
		return 0;
	}

	b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && (A == 0 || A == D)) {
		fprintf(stderr, "[CPUPowerPC] lwzux can't be used with r0 "
			"or rA = rD\n");
		return 0;
	}
	EA = b + ppc_ctx->state.gpr[B];

#ifdef DEBUG_PPC_SHOW_R2_LOADS
	if (A == 2 || B == 2)
		printf("[CPUPowerPC] lwz(u)x from TOC, EA = 0x%X\n", EA);
#endif

	/* Perform memory operation */
	if (!MEMOP_OK(cpu_powerpc_read_32(&ppc_ctx->common, EA,
			&ppc_ctx->state.gpr[D]))) {
		fprintf(stderr, "[CPUPowerPC] lwz(u)x failed, EA=0x%X\n", EA);
		return 0;
	}

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * slw (31->24) performs a left shift.
 */
static bool _ppc_opcode_31_slw(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	unsigned int rB = ppc_ctx->state.gpr[B];
	if (rB & 0x20)
		ppc_ctx->state.gpr[A] = 0u;
	else
		ppc_ctx->state.gpr[A] = ppc_ctx->state.gpr[S] << rB;

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, ppc_ctx->state.gpr[A]);
	return 1;
}

/*
 * subf (32->40) performs a subtraction.
 */
static bool _ppc_opcode_31_subf(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);

	const unsigned int a = ~ppc_ctx->state.gpr[A];
	const unsigned int b = ppc_ctx->state.gpr[B];
	const unsigned int result = a + b + 1;
	ppc_ctx->state.gpr[D] = result;

	/* Set overflow */
	if (get_bit32_big(instruction, 21)) {
		unsigned int overflowed = _ppc_overflow(a, b, result);
		_ppc_set_xer_ov(ppc_ctx, overflowed);
		_ppc_set_xer_so(ppc_ctx, overflowed || _ppc_get_xer_so(ppc_ctx));
	}

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * and (28) performs a binary AND between two registers.
 * andc (60) inverts the second register before the AND.
 */
static bool _ppc_opcode_31_and_andc(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	unsigned int rB = ppc_ctx->state.gpr[B];
	if (opcode & 0x20)
		rB = ~rB;
	ppc_ctx->state.gpr[A] = ppc_ctx->state.gpr[S] & rB;

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, ppc_ctx->state.gpr[A]);
	return 1;
}

/*
 * lbzx (31->87) performs an indirect load with all operands in registers,
 * loading 1 byte.
 * lbzux (31->119) also updates a GPR with the load address.
 */
static bool _ppc_opcode_31_lbzx(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode & 0x20u;
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int b, EA;
	unsigned char byte;

	if (get_bit32_big(instruction, 31)) {
		fprintf(stderr, "[CPUPowerPC] lbz(u)x reserved set\n");
		return 0;
	}

	b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && (A == 0 || A == D)) {
		fprintf(stderr, "[CPUPowerPC] lbzux can't be used with r0 "
			"or rA = rD\n");
		return 0;
	}
	EA = b + ppc_ctx->state.gpr[B];

#ifdef DEBUG_PPC_SHOW_R2_LOADS
	if (A == 2 || B == 2)
		printf("[CPUPowerPC] lbz(u)x from TOC, EA = 0x%X\n", EA);
#endif

	/* Perform memory operation */
	if (!mem_read_ok(EA, &byte, 1)) {
		fprintf(stderr, "[CPUPowerPC] lbz(u)x failed, EA=0x%X\n", EA);
		return 0;
	}
	ppc_ctx->state.gpr[D] = byte;

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * neg (31->104) negates a register
 * nego (31->616) also performs overflow checking.
 */
static bool _ppc_opcode_31_neg(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int a, result;

	if (get_bits32_big(instruction, 16, 20)) {
		fprintf(stderr, "[CPUPowerPC] neg reserved set\n");
		return 0;
	}

	/* Perform negation */
	a = ppc_ctx->state.gpr[A];
	result = (~a) + 1u;
	ppc_ctx->state.gpr[D] = result;

	/* Set overflow */
	if (opcode & 0x200u) {
		unsigned int overflowed = result == 0x80000000u;
		_ppc_set_xer_ov(ppc_ctx, overflowed);
		_ppc_set_xer_so(ppc_ctx, overflowed || _ppc_get_xer_so(ppc_ctx));
	}

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * nor (124) performs a binary NOR between two registers.
 */
static bool _ppc_opcode_31_nor(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	ppc_ctx->state.gpr[A] = ~(ppc_ctx->state.gpr[S] | ppc_ctx->state.gpr[B]);

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, ppc_ctx->state.gpr[A]);
	return 1;
}

/*
 * subfe (31->136) subtracts "from extended".
 * subfeo (31->648) also does overflow checking.
 */
static bool _ppc_opcode_31_subfe(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);

	/* Perform subtraction */
	const unsigned int a = ~ppc_ctx->state.gpr[A];
	const unsigned int b = ppc_ctx->state.gpr[B];
	const unsigned int xer_ca = _ppc_get_xer_ca(ppc_ctx);
	const unsigned int result = a + b + xer_ca;
	ppc_ctx->state.gpr[D] = result;

	/* Set carry */
	_ppc_set_xer_ca(ppc_ctx, _ppc_carry(a, b) || _ppc_carry(a + b, xer_ca));

	/* Perform overflow checking */
	if (get_bit32_big(instruction, 21)) {
		unsigned int overflowed = _ppc_overflow(a, b, result);
		_ppc_set_xer_ov(ppc_ctx, overflowed);
		_ppc_set_xer_so(ppc_ctx, overflowed || _ppc_get_xer_so(ppc_ctx));
	}

	/* Update cr0 if requested */
	if (instruction & 1u)
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * mtcrf (31->144) loads the condition register with a value from a register.
 */
static bool _ppc_opcode_31_mtcrf(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int CRM = get_bits32_big(instruction, 12, 19);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	if (get_bit32_big(instruction, 31) ||
			get_bit32_big(instruction, 20) ||
			get_bit32_big(instruction, 11)) {
		fprintf(stderr, "[CPUPowerPC] mtcrf reserved set\n");
		return 0;
	}

	/* We have an array */
	if (CRM & 0x80u)
		ppc_ctx->state.cr[0] = (ppc_ctx->state.gpr[S] >> 28) & 0xFu;
	if (CRM & 0x40u)
		ppc_ctx->state.cr[1] = (ppc_ctx->state.gpr[S] >> 24) & 0xFu;
	if (CRM & 0x20u)
		ppc_ctx->state.cr[2] = (ppc_ctx->state.gpr[S] >> 20) & 0xFu;
	if (CRM & 0x10u)
		ppc_ctx->state.cr[3] = (ppc_ctx->state.gpr[S] >> 16) & 0xFu;
	if (CRM & 0x8u)
		ppc_ctx->state.cr[4] = (ppc_ctx->state.gpr[S] >> 12) & 0xFu;
	if (CRM & 0x4u)
		ppc_ctx->state.cr[5] = (ppc_ctx->state.gpr[S] >> 8) & 0xFu;
	if (CRM & 0x2u)
		ppc_ctx->state.cr[6] = (ppc_ctx->state.gpr[S] >> 4) & 0xFu;
	if (CRM & 0x1u)
		ppc_ctx->state.cr[7] = (ppc_ctx->state.gpr[S] >> 0) & 0xFu;

	return 1;
}

/*
 * stwx (31->151) performs an indirect store with all operands in registers,
 * storing 4 bytes.
 * stwux (31->183) also updates a GPR with the store address.
 */
static bool _ppc_opcode_31_stwx(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode & 0x20u;
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);
	unsigned int b, EA;

	if (get_bit32_big(instruction, 31)) {
		fprintf(stderr, "[CPUPowerPC] stw(u)x reserved set\n");
		return 0;
	}

	b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && A == 0) {
		fprintf(stderr, "[CPUPowerPC] stwux can't be used with r0\n");
		return 0;
	}
	EA = b + ppc_ctx->state.gpr[B];

	/* Perform memory operation */
	if (!MEMOP_OK(cpu_powerpc_write_32(&ppc_ctx->common, EA,
			ppc_ctx->state.gpr[S]))) {
		fprintf(stderr, "[CPUPowerPC] stw(u)x failed, EA=0x%X\n", EA);
		return 0;
	}

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * addze (31->202) adds the carry to a register.
 * addzeo (31->714) also performs overflow checking.
 */
static bool _ppc_opcode_31_addze(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int a, carry, result;

	if (get_bits32_big(instruction, 16, 20)) {
		fprintf(stderr, "[CPUPowerPC] addze reserved set\n");
		return 0;
	}

	/* Perform addition */
	a = ppc_ctx->state.gpr[A];
	carry = _ppc_get_xer_ca(ppc_ctx);
	result = a + carry;
	ppc_ctx->state.gpr[D] = result;

	/* Set carry bit */
	_ppc_set_xer_ca(ppc_ctx, _ppc_carry(a, carry));

	/* Set overflow */
	if (opcode & 0x200u) {
		unsigned int overflowed = _ppc_overflow(a, 0, result);
		_ppc_set_xer_ov(ppc_ctx, overflowed);
		_ppc_set_xer_so(ppc_ctx, overflowed || _ppc_get_xer_so(ppc_ctx));
	}

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * stbx (31->215) performs an indirect store with all operands in registers,
 * storing 1 byte.
 * stbux (31->247) also updates a GPR with the store address.
 */
static bool _ppc_opcode_31_stbx(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode & 0x20u;
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);
	unsigned int b, EA;
	unsigned char byte;

	if (get_bit32_big(instruction, 31)) {
		fprintf(stderr, "[CPUPowerPC] stb(u)x reserved set\n");
		return 0;
	}

	b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && A == 0) {
		fprintf(stderr, "[CPUPowerPC] stbux can't be used with r0\n");
		return 0;
	}
	EA = b + ppc_ctx->state.gpr[B];

	/* Perform memory operation */
	byte = (unsigned char)ppc_ctx->state.gpr[S];
	if (!mem_write_ok(EA, &byte, 1)) {
		fprintf(stderr, "[CPUPowerPC] stb(u)x failed, EA=0x%X\n", EA);
		return 0;
	}

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * mullw(o) (31->235) multiples two 32-bit words and returns the 32-bit
 * result.
 */
static bool _ppc_opcode_31_mullw(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);

	/* Multiply two 32-bit signed numbers and truncate */
	const uint64_t result64 = (uint64_t)(
		((int64_t)(int)ppc_ctx->state.gpr[A]) *
		((int64_t)(int)ppc_ctx->state.gpr[B])
	);
	const unsigned int result = (unsigned int)(result64 & 0xFFFFFFFFu);
	ppc_ctx->state.gpr[D] = result;

	/* Perform overflow checking */
	if (get_bit32_big(instruction, 21)) {
		unsigned int overflowed = _ppc_overflow(ppc_ctx->state.gpr[A],
			ppc_ctx->state.gpr[B], result);
		_ppc_set_xer_ov(ppc_ctx, overflowed);
		_ppc_set_xer_so(ppc_ctx, overflowed || _ppc_get_xer_so(ppc_ctx));
	}

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * add (31->266) performs addition between registers.
 * addo (31->778) also performs overflow checking.
 */
static bool _ppc_opcode_31_add(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);

	/* Add registers together */
	ppc_ctx->state.gpr[D] = ppc_ctx->state.gpr[A] + ppc_ctx->state.gpr[B];

	/* Perform overflow checking */
	if (opcode & 0x200u) {
		unsigned int overflowed = _ppc_overflow(ppc_ctx->state.gpr[A],
			ppc_ctx->state.gpr[B], ppc_ctx->state.gpr[D]);
		_ppc_set_xer_ov(ppc_ctx, overflowed);
		_ppc_set_xer_so(ppc_ctx, overflowed || _ppc_get_xer_so(ppc_ctx));
	}

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, ppc_ctx->state.gpr[D]);
	return 1;
}

/*
 * lhzx (31->279) performs an indirect load with all operands in registers,
 * loading 2 bytes.
 * lhzux (31->311) also updates a GPR with the load address.
 */
static bool _ppc_opcode_31_lhzx(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode & 0x20u;
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int b, EA;
	unsigned short v;

	/* Check reserved bits */
	if (get_bit32_big(instruction, 31)) {
		fprintf(stderr, "[CPUPowerPC] lhz(u)x reserved set\n");
		return 0;
	}

	/* Check A register */
	b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && (A == 0 || A == D)) {
		fprintf(stderr, "[CPUPowerPC] lhzux can't be used with r0 "
			"or rA = rD\n");
		return 0;
	}
	EA = b + ppc_ctx->state.gpr[B];

#ifdef DEBUG_PPC_SHOW_R2_LOADS
	if (A == 2 || B == 2)
		printf("[CPUPowerPC] lhz(u)x from TOC, EA = 0x%X\n", EA);
#endif

	/* Perform memory operation */
	if (cpu_powerpc_read_16(&ppc_ctx->common, EA, &v) != MEMORY_STATUS_OK) {
		fprintf(stderr, "[CPUPowerPC] lhz(u)x failed, EA=0x%X\n", EA);
		return 0;
	}
	ppc_ctx->state.gpr[D] = v;

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * mfspr (31->339) and mtspr (31->467) move between GPRs and special
 * registers XER, LR, and CTR.
 * The "spr" field (10 bits) has its two 5-bit fields reversed in the
 * instruction encoding, so it must be undone.
 */
static bool _ppc_opcode_31_mfspr_mtspr(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int spr =
		(get_bits32_big(instruction, 16, 20) << 5) |
		(get_bits32_big(instruction, 11, 15));

	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int *const reg_ptr = &ppc_ctx->state.gpr[D];

	/* Select special register based on spr */
	unsigned int *spr_ptr;
	switch (spr) {
		case ((0u << 5) | (1u << 0)):
			spr_ptr = &ppc_ctx->state.xer;
			break;
		case ((0u << 5) | (8u << 0)):
			spr_ptr = &ppc_ctx->state.lr;
			break;
		case ((0u << 5) | (9u << 0)):
			spr_ptr = &ppc_ctx->state.ctr;
			break;
		default:
			fprintf(stderr, "[CPUPowerPC] m%cspr: invalid spr %d\n",
				(opcode & 0x80u) ? 't' : 'f', spr);
			return 0;
	}

	/* Perform exchange */
	if (opcode & 0x80u)
		*spr_ptr = *reg_ptr;
	else
		*reg_ptr = *spr_ptr;
	return 1;
}

/*
 * lhax (31->343) performs an indirect load with all operands in registers,
 * loading 2 bytes and sign-extending them.
 * lhaux (31->375) also updates a GPR with the load address.
 */
static bool _ppc_opcode_31_lhax(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode & 0x20u;
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int b, EA;
	unsigned short v;

	/* Check reserved bits */
	if (get_bit32_big(instruction, 31)) {
		fprintf(stderr, "[CPUPowerPC] lha(u)x reserved set\n");
		return 0;
	}

	/* Check A register */
	b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && (A == 0 || A == D)) {
		fprintf(stderr, "[CPUPowerPC] lhaux can't be used with r0 "
			"or rA = rD\n");
		return 0;
	}
	EA = b + ppc_ctx->state.gpr[B];

#ifdef DEBUG_PPC_SHOW_R2_LOADS
	if (A == 2 || B == 2)
		printf("[CPUPowerPC] lha(u)x from TOC, EA = 0x%X\n", EA);
#endif

	/* Perform memory operation */
	if (cpu_powerpc_read_16(&ppc_ctx->common, EA, &v) != MEMORY_STATUS_OK) {
		fprintf(stderr, "[CPUPowerPC] lha(u)x failed, EA=0x%X\n", EA);
		return 0;
	}
	ppc_ctx->state.gpr[D] = sign16_extend(v);

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * sthx (31->407) performs an indirect store with all operands in registers,
 * storing 2 bytes.
 * stbux (31->439) also updates a GPR with the store address.
 */
static bool _ppc_opcode_31_sthx(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int update = opcode & 0x20u;
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);
	unsigned int b, EA;

	if (get_bit32_big(instruction, 31)) {
		fprintf(stderr, "[CPUPowerPC] sth(u)x reserved set\n");
		return 0;
	}

	b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	if (update && A == 0) {
		fprintf(stderr, "[CPUPowerPC] sthux can't be used with r0\n");
		return 0;
	}
	EA = b + ppc_ctx->state.gpr[B];

	/* Perform memory operation */
	if (!MEMOP_OK(cpu_powerpc_write_16(&ppc_ctx->common, EA,
			ppc_ctx->state.gpr[S] & 0xFFFFu))) {
		fprintf(stderr, "[CPUPowerPC] sth(u)x failed, EA=0x%X\n", EA);
		return 0;
	}

	/* Update rA with destination address */
	if (update)
		ppc_ctx->state.gpr[A] = EA;
	return 1;
}

/*
 * orc (31->412) performs a logical OR between two registers, the second one
 * is negated.
 */
static bool _ppc_opcode_31_orc(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	const unsigned int result = ppc_ctx->state.gpr[S] |
		(~ppc_ctx->state.gpr[B]);
	ppc_ctx->state.gpr[A] = result;

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * or (31->444) performs a logical OR between two registers.
 */
static bool _ppc_opcode_31_or(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	const unsigned int result = ppc_ctx->state.gpr[S] |
		ppc_ctx->state.gpr[B];
	ppc_ctx->state.gpr[A] = result;

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * divwu (31->459) divides a 32-bit unsigned integer rA by the 32-bit
 * unsigned integer rB.
 *
 * divwuo (31->971) also performs overflow checking.
 */
static bool _ppc_opcode_31_divwu(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);

	const unsigned int dividend = ppc_ctx->state.gpr[A];
	const unsigned int divisor = ppc_ctx->state.gpr[B];
	unsigned int overflowed = !divisor;

	/* If we overflow, return 0 */
	unsigned int result;
	if (overflowed)
		result = 0u;

	/* Perform division */
	else
		result = dividend / divisor;

	ppc_ctx->state.gpr[D] = result;

	/* Set overflow bits */
	if (opcode & 0x200u) {
		_ppc_set_xer_ov(ppc_ctx, overflowed);
		_ppc_set_xer_so(ppc_ctx, overflowed || _ppc_get_xer_so(ppc_ctx));
	}

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * xor (316) performs a binary XOR between two registers.
 */
static bool _ppc_opcode_31_xor(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	ppc_ctx->state.gpr[A] = ppc_ctx->state.gpr[S] ^ ppc_ctx->state.gpr[B];

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, ppc_ctx->state.gpr[A]);
	return 1;
}

/*
 * nand (31->476) performs a binary NAND between two registers.
 */
static bool _ppc_opcode_31_nand(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	const unsigned int result = ~(ppc_ctx->state.gpr[S] &
		ppc_ctx->state.gpr[B]);
	ppc_ctx->state.gpr[A] = result;

	/* Update cr0 if requested */
	if (instruction & 1u)
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * divw (31->491) divides a 32-bit signed integer rA by the 32-bit
 * signed integer rB.
 *
 * divwo (31->1003) also performs overflow checking.
 */
static bool _ppc_opcode_31_divw(struct _PPCContext *ppc_ctx,
	uint32_t opcode, uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);

	const signed int dividend = (int)ppc_ctx->state.gpr[A];
	const signed int divisor = (int)ppc_ctx->state.gpr[B];

	unsigned int overflowed = !divisor ||
		(ppc_ctx->state.gpr[A] & 0x80000000u && divisor == -1);

	/* If we overflow, return either 0 or 0xFFFFFFFF */
	unsigned int result;
	if (overflowed) {
		if (dividend < 0)
			result = 0xFFFFFFFFu;
		else
			result = 0u;
	}
	
	/* Perform division */
	else
		result = (unsigned int)(dividend / divisor);

	ppc_ctx->state.gpr[D] = result;

	/* Set overflow bits */
	if (opcode & 0x200u) {
		_ppc_set_xer_ov(ppc_ctx, overflowed);
		_ppc_set_xer_so(ppc_ctx, overflowed || _ppc_get_xer_so(ppc_ctx));
	}

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}


/*
 * lwbrx (31->534) performs an indirect load with all operands in registers,
 * loading 4 bytes byte-swapped.
 */
static bool _ppc_opcode_31_lwbrx(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	unsigned int b, EA;
	unsigned int v;

	/* Check reserved bits */
	if (get_bit32_big(instruction, 31)) {
		fprintf(stderr, "[CPUPowerPC] lwbrx reserved set\n");
		return 0;
	}

	/* Check A register */
	b = (A == 0) ? 0 : ppc_ctx->state.gpr[A];
	EA = b + ppc_ctx->state.gpr[B];

#ifdef DEBUG_PPC_SHOW_R2_LOADS
	if (A == 2 || B == 2)
		printf("[CPUPowerPC] lwbrx from TOC, EA = 0x%X\n", EA);
#endif

	/* Perform memory operation */
	if (cpu_powerpc_read_32(&ppc_ctx->common, EA, &v) != MEMORY_STATUS_OK) {
		fprintf(stderr, "[CPUPowerPC] lwbrx failed, EA=0x%X\n", EA);
		return 0;
	}
	ppc_ctx->state.gpr[D] = tools_byteswap32(v);
	return 1;
}

/*
 * srw (536) performs a logical shift right with amount specified by a
 * register.
 */
static bool _ppc_opcode_31_srw(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	const unsigned int result = ppc_ctx->state.gpr[S] >>
		(ppc_ctx->state.gpr[B] & 31u);
	ppc_ctx->state.gpr[A] = result;

	/* Update cr0 if requested */
	if (instruction & 1u)
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * sraw (31->792) performs an arithmetic right-shift of a register.
 */
static bool _ppc_opcode_31_sraw(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	const unsigned int rB = ppc_ctx->state.gpr[B];

	if (rB & 0x20) {
		/* Out of shift bounds */
		bool carry = !!(ppc_ctx->state.gpr[S] & 0x80000000u);
		ppc_ctx->state.gpr[A] = carry ? 0xFFFFFFFFu : 0x0u;
		_ppc_set_xer_ca(ppc_ctx, carry);
	} else {
		/* Arithmetic right-shift */
		const unsigned int SH = rB & 0x1Fu;
		const int rS = (int)ppc_ctx->state.gpr[S];
		ppc_ctx->state.gpr[A] = rS >> SH;

		_ppc_set_xer_ca(ppc_ctx, rS < 0 && SH > 0 &&
			(ppc_ctx->state.gpr[S] << (32 - SH)) != 0);
	}

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, ppc_ctx->state.gpr[A]);
	return 1;
}

/*
 * srawi (31->824) performs a arithmetic right-shift of a register.
 */
static bool _ppc_opcode_31_srawi(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int SH = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);

	/* signed shift right */
	const int rS = (int)ppc_ctx->state.gpr[S];
	ppc_ctx->state.gpr[A] = rS >> SH;

	/* set carry */
	_ppc_set_xer_ca(ppc_ctx, rS < 0 && SH > 0 &&
		(ppc_ctx->state.gpr[S] << (32 - SH)) != 0);

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, ppc_ctx->state.gpr[A]);
	return 1;
}

/*
 * extsh (31->922) sign-extends a 16-bit value.
 */
static bool _ppc_opcode_31_extsh(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);
	unsigned int result;

	if (get_bits32_big(instruction, 16, 20) != 0) {
		fprintf(stderr, "[CPUPowerPC] extsh reserved set\n");
		return 0;
	}

	/* Perform sign extension */
	result = sign16_extend(ppc_ctx->state.gpr[S] & 0xFFFFu);
	ppc_ctx->state.gpr[A] = result;

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}

/*
 * extsb (31->954) sign-extends an 8-bit value.
 */
static bool _ppc_opcode_31_extsb(struct _PPCContext *ppc_ctx,
	uint32_t instruction)
{
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int S = get_bits32_big(instruction, 6, 10);
	unsigned int result;

	if (get_bits32_big(instruction, 16, 20) != 0) {
		fprintf(stderr, "[CPUPowerPC] extsb reserved set\n");
		return 0;
	}

	/* Perform sign extension */
	result = sign8_extend(ppc_ctx->state.gpr[S] & 0xFFu);
	ppc_ctx->state.gpr[A] = result;

	/* Update cr0 if requested */
	if (get_bit32_big(instruction, 31))
		_ppc_set_cr0(ppc_ctx, result);
	return 1;
}


/*
 * Main dispatch function
 */
static bool _ppc_opcode_31(struct _PPCContext *ppc_ctx,
	unsigned int subopcode, unsigned int instruction)
{
	/* Extract operation */
	subopcode = get_bits32_big(instruction, 21, 30);

	if (subopcode == 0 || subopcode == 32)
		return _ppc_opcode_31_cmp_cmpl(ppc_ctx, subopcode, instruction);
	else if (subopcode == 8)
		return _ppc_opcode_31_subfc(ppc_ctx, instruction);
	else if (subopcode == 19)
		return _ppc_opcode_31_mfcr(ppc_ctx, instruction);
	else if (subopcode == 23 || subopcode == 55)
		return _ppc_opcode_31_lwzx(ppc_ctx, subopcode, instruction);
	else if (subopcode == 24)
		return _ppc_opcode_31_slw(ppc_ctx, instruction);
	else if (subopcode == 28 || subopcode == 60)
		return _ppc_opcode_31_and_andc(ppc_ctx, subopcode, instruction);
	else if (subopcode == 40)
		return _ppc_opcode_31_subf(ppc_ctx, instruction);
	else if (subopcode == 87 || subopcode == 119)
		return _ppc_opcode_31_lbzx(ppc_ctx, subopcode, instruction);
	else if ((subopcode & ~0x200u) == 104)
		return _ppc_opcode_31_neg(ppc_ctx, subopcode, instruction);
	else if (subopcode == 124)
		return _ppc_opcode_31_nor(ppc_ctx, instruction);
	else if ((subopcode & ~0x200u) == 136)
		return _ppc_opcode_31_subfe(ppc_ctx, instruction);
	else if (subopcode == 144)
		return _ppc_opcode_31_mtcrf(ppc_ctx, instruction);
	else if (subopcode == 151 || subopcode == 183)
		return _ppc_opcode_31_stwx(ppc_ctx, subopcode, instruction);
	else if ((subopcode & ~0x200u) == 202)
		return _ppc_opcode_31_addze(ppc_ctx, subopcode, instruction);
	else if (subopcode == 215 || subopcode == 247)
		return _ppc_opcode_31_stbx(ppc_ctx, subopcode, instruction);
	else if (subopcode == 235)
		return _ppc_opcode_31_mullw(ppc_ctx, instruction);
	else if ((subopcode & ~0x200u) == 266)
		return _ppc_opcode_31_add(ppc_ctx, subopcode, instruction);
	else if (subopcode == 279 || subopcode == 311)
		return _ppc_opcode_31_lhzx(ppc_ctx, subopcode, instruction);
	else if (subopcode == 316)
		return _ppc_opcode_31_xor(ppc_ctx, instruction);
	else if (subopcode == 339 || subopcode == 467)
		return _ppc_opcode_31_mfspr_mtspr(ppc_ctx, subopcode, instruction);
	else if (subopcode == 343 || subopcode == 375)
		return _ppc_opcode_31_lhax(ppc_ctx, subopcode, instruction);
	else if (subopcode == 407 || subopcode == 439)
		return _ppc_opcode_31_sthx(ppc_ctx, subopcode, instruction);
	else if (subopcode == 412)
		return _ppc_opcode_31_orc(ppc_ctx, instruction);
	else if (subopcode == 444)
		return _ppc_opcode_31_or(ppc_ctx, instruction);
	else if ((subopcode & ~0x200u) == 459)
		return _ppc_opcode_31_divwu(ppc_ctx, subopcode, instruction);
	else if (subopcode == 476)
		return _ppc_opcode_31_nand(ppc_ctx, instruction);
	else if ((subopcode & ~0x200u) == 491)
		return _ppc_opcode_31_divw(ppc_ctx, subopcode, instruction);
	else if (subopcode == 534)
		return _ppc_opcode_31_lwbrx(ppc_ctx, instruction);
	else if (subopcode == 536)
		return _ppc_opcode_31_srw(ppc_ctx, instruction);
	else if (subopcode == 792)
		return _ppc_opcode_31_sraw(ppc_ctx, instruction);
	else if (subopcode == 824)
		return _ppc_opcode_31_srawi(ppc_ctx, instruction);
	else if (subopcode == 922)
		return _ppc_opcode_31_extsh(ppc_ctx, instruction);
	else if (subopcode == 954)
		return _ppc_opcode_31_extsb(ppc_ctx, instruction);

	else {
		fprintf(stderr, "[CPUPowerPC] Unsupported 31 subopcode %u\n",
			subopcode);
		return 0;
	}
}
