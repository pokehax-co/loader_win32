/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "cpu.h"
#include "cpu_impl.h"

#include <stdlib.h>
#include <assert.h>


int cpu_create(const struct CPUInterface *cpu_iface, struct CPUContext **ctx)
{
	if (!cpu_iface || !ctx)
		return -1;

	assert(cpu_iface->allocation_size >= sizeof(struct CPUInterface));
	assert(cpu_iface->initialize);
	assert(cpu_iface->destroy);
	assert(cpu_iface->reset);
	assert(cpu_iface->on_stop);
	assert(cpu_iface->print_registers);
	assert(cpu_iface->get_register);
	assert(cpu_iface->set_register);
	assert(cpu_iface->save_register_state);
	assert(cpu_iface->load_register_state);
	assert(cpu_iface->run_step);

	*ctx = malloc(cpu_iface->allocation_size);
	if (!*ctx)
		return -1;

	(*ctx)->cpu_iface = cpu_iface;
	(*ctx)->memory_iface = NULL;
	(*ctx)->memory_ctx = NULL;
	(*ctx)->stopped = 0;

	(*ctx)->cpu_iface->initialize(*ctx);
	return 0;
}

int cpu_destroy(struct CPUContext **ctx)
{
	if (!ctx || !*ctx)
		return -1;

	(*ctx)->cpu_iface->destroy(*ctx);
	free(*ctx);
	*ctx = NULL;
	return 0;
}

int cpu_get_arch(struct CPUContext *ctx, enum CPUArchitecture *arch)
{
	if (!ctx || !arch)
		return -1;

	*arch = ctx->cpu_iface->architecture;
	return 0;
}

int cpu_set_memory(struct CPUContext *ctx,
	const struct MemoryInterface *memory_iface, void *memory_ctx)
{
	if (!ctx || !memory_iface)
		return -1;

	assert(memory_iface->read);
	assert(memory_iface->execute);
	assert(memory_iface->write);

	ctx->memory_iface = memory_iface;
	ctx->memory_ctx = memory_ctx;
	return 0;
}

int cpu_reset(struct CPUContext *ctx)
{
	if (!ctx)
		return -1;

	ctx->cpu_iface->reset(ctx);
	return 0;
}

int cpu_stop(struct CPUContext *ctx, unsigned int intended)
{
	if (!ctx)
		return -1;

	ctx->stopped = 1;
	ctx->stop_intended = !!intended;
	ctx->cpu_iface->on_stop(ctx);
	return 0;
}

int cpu_is_stop_intended(struct CPUContext *ctx, unsigned int *intended)
{
	if (!ctx || !intended)
		return -1;
	if (!ctx->stopped)
		return -1;

	*intended = ctx->stop_intended;
	return 0;
}

int cpu_print_registers(struct CPUContext *ctx)
{
	if (!ctx)
		return -1;

	ctx->cpu_iface->print_registers(ctx);
	return 0;
}

int cpu_get_register(struct CPUContext *ctx, unsigned int regnum,
	cpu_register_t *value)
{
	if (!ctx || regnum > ctx->cpu_iface->max_regnum || !value)
		return -1;

	*value = ctx->cpu_iface->get_register(ctx, regnum);
	return 0;
}

int cpu_set_register(struct CPUContext *ctx, unsigned int regnum,
	cpu_register_t value)
{
	if (!ctx || regnum > ctx->cpu_iface->max_regnum)
		return -1;

	ctx->cpu_iface->set_register(ctx, regnum, value);
	return 0;
}

int cpu_save_register_state(struct CPUContext *ctx, void *buffer,
	unsigned int *buffer_size)
{
	if (!ctx || !buffer_size)
		return -1;
	if (*buffer_size < ctx->cpu_iface->register_state_size) {
		*buffer_size = ctx->cpu_iface->register_state_size;
		return -2;
	}
	if (!ctx || !buffer_size)
		return -1;

	ctx->cpu_iface->save_register_state(ctx, buffer);
	return 0;
}

int cpu_load_register_state(struct CPUContext *ctx, const void *buffer,
	unsigned int buffer_size)
{
	if (!ctx || buffer_size < ctx->cpu_iface->register_state_size || !buffer)
		return -1;

	ctx->cpu_iface->load_register_state(ctx, buffer);
	return 0;
}

int cpu_run(struct CPUContext *ctx, memory_address_t exec_stop_address)
{
	if (!ctx)
		return -1;

	/* Reset stop status */
	ctx->stopped = 0;
	ctx->stop_intended = 0;

	while (!ctx->stopped) {
		/* Register 0 must return the program counter */
		cpu_register_t pc = ctx->cpu_iface->get_register(ctx, 0);

		/* Intended stop if we reach the stop address */
		if (pc == exec_stop_address) {
			ctx->stopped = 1;
			ctx->stop_intended = 1;
		}

		/* Run a step. If it fails, this stop is unintended */
		else if (ctx->cpu_iface->run_step(ctx) < 0) {
			ctx->stopped = 1;
			ctx->stop_intended = 0;
		}
	}
	return 0;
}
