/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/*
 * This file provides support for FPU instructions under opcode 63. The
 * implementation has been adapted from Dolphin's PowerPC interpreter, which is
 * licensed under the GPL2.
 *
 * If this license is not desired, disable the CONFIG_ENABLE_FPU option in
 * src/config.h.
 */

#include "../config.h"


#ifdef CONFIG_ENABLE_FPU

#include "ppc_impl.h"

#include "../misc/tools.h"

#include <stdio.h>
#include <math.h>
#include <assert.h>


/*
 * Type punning union between 64-bit integer, 64-bit floating point, and 64-bit
 * IEEE bit representation
 */
union _PPCFPValue
{
	double f;
	uint64_t u;

	/* IEEE format */
	struct {
		uint64_t fraction : 52;
		uint64_t exponent : 11;
		uint64_t sign : 1;
	} bits;
};


/* Visual C++ compatibility */
#ifdef _MSC_VER
#include <float.h>

static __inline int isnan(const double x)
{
	return _isnan(x);
}

static __inline int isinf(const double x)
{
	return !_finite(x);
}

static __inline int signbit(const double x)
{
	union _PPCFPValue v;
	v.f = x;
	return !!(v.u >> 63);
}
#endif


/*
 * Macros to read and write FPSCR bits
 */

/* bit 13 */
#define GET_FPSCR_FR(ppc_ctx) \
	(!!(ppc_ctx->state.fpscr & 0x40000u))

#define SET_FPSCR_FR(ppc_ctx, v) \
	(ppc_ctx->state.fpscr = (v) ? \
		(ppc_ctx->state.fpscr | 0x40000u) : \
		(ppc_ctx->state.fpscr & ~0x40000u))

/* bit 14 */
#define GET_FPSCR_FI(ppc_ctx) \
	(!!(ppc_ctx->state.fpscr & 0x20000u))

#define SET_FPSCR_FI(ppc_ctx, v) \
	(ppc_ctx->state.fpscr = (v) ? \
		(ppc_ctx->state.fpscr | 0x20000u) : \
		(ppc_ctx->state.fpscr & ~0x20000u))

/* bits 15-19 */
#define GET_FPSCR_FPRF(ppc_ctx) \
	((ppc_ctx->state.fpscr >> 12) & 0x1Fu)

#define SET_FPSCR_FPRF(ppc_ctx, v) \
	(ppc_ctx->state.fpscr = \
		(ppc_ctx->state.fpscr & ~0x1F000u) | ((v) & 0x1F) << 12)

/* bit 24 */
#define GET_FPSCR_VE(ppc_ctx) \
	(!!(ppc_ctx->state.fpscr & 0x80u))

#define SET_FPSCR_VE(ppc_ctx, v) \
	(ppc_ctx->state.fpscr = (v) ? \
		(ppc_ctx->state.fpscr | 0x80u) : \
		(ppc_ctx->state.fpscr & ~0x80u))


/*
 * Floating-point operations handling special cases
 */

static double _fp_sub(double a, double b)
{
	const double result = a - b;

	if (isnan(result)) {
		// FIXME: implement this
		assert(0 && "subtraction led to NaN");
	}

	if (isinf(a) || isinf(b)) {
		// FIXME: implement this
		assert(0 && "a or b is infinity");
	}

	return result;
}

static double _fp_mul(double a, double b)
{
	const double result = a * b;

	if (isnan(result)) {
		// FIXME: implement this
		assert(0 && "multiplication led to NaN");
	}

	return result;
}


/*
 * Sets the CR1 register, which is used for floating-point comparisons.
 */
static void _ppc_set_cr1(struct _PPCContext *ppc_ctx)
{
	// FIXME!
	(void)ppc_ctx;
	assert(0 && "CR1 update not implemented");
}


/*
 * Classifies a double-precision floating point number. The returned written
 * should be written into the FPRF bits of the FPSCR register.
 */
static uint32_t _fp_classify_double(const double value)
{
	union _PPCFPValue v;
	v.f = value;

	/* Normalized? */
	if (v.bits.exponent > 0 && v.bits.exponent < 0x7FFu)
		return v.bits.sign ? 0x8u : 0x4u;

	if (v.bits.fraction) {
		/* Quiet NaN */
		if (v.bits.exponent)
			return 0x11u;

		/* Denormalized */
		return v.bits.sign ? 0x18u : 0x14u;
	}

	/* 0x7FF = infinity */
	if (v.bits.exponent)
		return v.bits.sign ? 0x9u : 0x5u;

	/* 0x0 = zero */
	return v.bits.sign ? 0x12u : 0x2u;
}


/*
 * fctiwz (63->15) converts a floating-point value to an integer, while
 * rounding towards zero.
 */
static bool _ppc_op63_fctiwz(struct _PPCContext *ppc_ctx, uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	union _PPCFPValue b, d;
	bool exception = false;

	/* Check reserved bits */
	if (get_bits32_big(instruction, 11, 15)) {
		fprintf(stderr, "[CPUPowerPC] fctiwz reserved set\n");
		return 0;
	}

	b.u = ppc_ctx->state.fpr[B];
	if (isnan(b.f)) {
		// FIXME: detect SNaN and set VXSNAN

		d.u = 0x80000000u;
		// FIXME: VCXVI exception
		exception = true;
	} else if (b.f > (double)0x7FFFFFFFu) {
		d.u = 0x7FFFFFFFu;
		// FIXME: VCXVI exception
		exception = true;
	} else if (b.f < -(double)0x80000000u) {
		d.u = 0x80000000u;
		// FIXME: VCXVI exception
		exception = true;
	} else {
		/* Casting rounds to zero */
		int32_t integer = (int32_t)b.f;
		double integer_as_double = integer;

		if (integer_as_double == b.f) {
			SET_FPSCR_FI(ppc_ctx, 0);
			SET_FPSCR_FR(ppc_ctx, 0);
		} else {
			SET_FPSCR_FI(ppc_ctx, 1);
			SET_FPSCR_FR(ppc_ctx, fabs(integer_as_double) > fabs(b.f));
		}

		d.u = (uint32_t)integer;
	}

	if (exception) {
		SET_FPSCR_FI(ppc_ctx, 0);
		SET_FPSCR_FR(ppc_ctx, 0);
	}

	if (!GET_FPSCR_VE(ppc_ctx) || !exception) {
		/* This is valid for Gekko/Broadway, but for the 603? */
		ppc_ctx->state.fpr[D] = (((uint64_t)0xFFF8u) << 48) | d.u;
		if (!d.u && signbit(b.f))
			ppc_ctx->state.fpr[D] |= ((uint64_t)1u) << 32;
	}

	/* Update CR1 */
	if (instruction & 1)
		_ppc_set_cr1(ppc_ctx);
	return 1;
}

/*
 * fsub (63->21) subtracts two registers.
 */
static bool _ppc_op63_fsub(struct _PPCContext *ppc_ctx, uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	union _PPCFPValue a, b, result;

	/* Check reserved bits */
	if (get_bits32_big(instruction, 21, 25)) {
		fprintf(stderr, "[CPUPowerPC] fsub reserved set\n");
		return 0;
	}

	/* Perform the subtraction */
	a.u = ppc_ctx->state.fpr[A];
	b.u = ppc_ctx->state.fpr[B];
	result.f = _fp_sub(a.f, b.f);

	/* No exceptions (or disabled)? Write the result and update value
	   classification */
	// XXX: currently no instruction generates an exception
	if (!GET_FPSCR_VE(ppc_ctx) || 1) {
		// XXX: no NI bit support
		ppc_ctx->state.fpr[D] = result.u;
		SET_FPSCR_FPRF(ppc_ctx, _fp_classify_double(result.f));
	}

	/* Update CR1 */
	if (instruction & 1)
		_ppc_set_cr1(ppc_ctx);
	return 1;
}

/*
 * fmul (63->25) multiplies two registers.
 */
static bool _ppc_op63_fmul(struct _PPCContext *ppc_ctx, uint32_t instruction)
{
	const unsigned int C = get_bits32_big(instruction, 21, 25);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int D = get_bits32_big(instruction, 6, 10);
	union _PPCFPValue a, c, result;

	/* Check reserved bits */
	if (get_bits32_big(instruction, 16, 20)) {
		fprintf(stderr, "[CPUPowerPC] fmul reserved set\n");
		return 0;
	}

	/* Perform the multiplication */
	a.u = ppc_ctx->state.fpr[A];
	c.u = ppc_ctx->state.fpr[C];
	result.f = _fp_mul(a.f, c.f);

	/* No exceptions (or disabled)? Write the result and update value
	   classification */
	// XXX: currently no instruction generates an exception
	if (!GET_FPSCR_VE(ppc_ctx) || 1) {
		// XXX: no NI bit support
		ppc_ctx->state.fpr[D] = result.u;
		SET_FPSCR_FI(ppc_ctx, 0);
		SET_FPSCR_FR(ppc_ctx, 0);
		SET_FPSCR_FPRF(ppc_ctx, _fp_classify_double(result.f));
	}

	/* Update CR1 */
	if (instruction & 1)
		_ppc_set_cr1(ppc_ctx);
	return 1;
}

/*
 * fcmpo (63->32) compares two registers, ordered.
 */
static bool _ppc_op63_fcmpo(struct _PPCContext *ppc_ctx, uint32_t instruction)
{
	const unsigned int B = get_bits32_big(instruction, 16, 20);
	const unsigned int A = get_bits32_big(instruction, 11, 15);
	const unsigned int crfD = get_bits32_big(instruction, 6, 8);
	union _PPCFPValue a, b;
	unsigned int c;

	/* Check reserved bits */
	if (get_bits32_big(instruction, 9, 10) || instruction & 1) {
		fprintf(stderr, "[CPUPowerPC] fcmpo reserved set\n");
		return 0;
	}

	a.u = ppc_ctx->state.fpr[A];
	b.u = ppc_ctx->state.fpr[B];

	/* Translate to CR bits */
	if (isnan(a.f) || isnan(b.f)) {
		c = 0x1u;

		// XXX: No NaN support
		assert(0 && "fcmpo NaN");
	}
	else if (a.f < b.f)
		c = 0x8u;
	else if (a.f > b.f)
		c = 0x4u;
	else
		c = 0x2u;

	/* Write into both FPCC (lower 4 bits of FPRF) and CR */
	SET_FPSCR_FPRF(ppc_ctx, (GET_FPSCR_FPRF(ppc_ctx) & ~0xFu) | c);
	ppc_ctx->state.cr[crfD] = c;
	return 1;
}


/*
 * Dispatch function for opcode 63
 */
bool _ppc_fpu_opcode_63(struct _PPCContext *ppc_ctx, uint32_t unused,
	uint32_t instruction)
{
	/* Extract subopcode. Bits 21 to 25 may alternatively be used as operand,
	   so pass all 10 bits to the functions */
	const uint32_t subopcode = get_bits32_big(instruction, 21, 30);

	(void)unused;

	if (subopcode == 15)
		return _ppc_op63_fctiwz(ppc_ctx, instruction);
	else if (subopcode == 20)
		return _ppc_op63_fsub(ppc_ctx, instruction);
	else if ((subopcode & 31u) == 25)
		return _ppc_op63_fmul(ppc_ctx, instruction);
	else if (subopcode == 32)
		return _ppc_op63_fcmpo(ppc_ctx, instruction);

	fprintf(stderr, "[CPUPowerPC] Unsupported 63 subopcode %u\n",
		subopcode);
	return false;
}

#endif /* CONFIG_ENABLE_FPU */
