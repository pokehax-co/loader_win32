/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

/*!
 * \file src/misc/log.hpp
 * \brief Logging support for the loader.
 */

#include <stddef.h>
#include <stdarg.h>

#include "../iface/memory.h"
#include "../portab/portab.h"


class Log
{
private:
	static const char *_argv0;

	static void _printv(const char *severity, const char *format,
		va_list args);

public:
	/*!
	 * \brief Initializes the logger.
	 * \param argv0 Process name
	 *
	 * \note
	 * The process name is used to prefix log messages.
	 */
	static void init(const char *argv0);

	/*!
	 * \brief Prints an error message over the logger.
	 * \param format printf()-compatible format string
	 * \param ... Format arguments
	 */
	GNU_PRINTF_ATTRIBUTE(1, 2)
	static void error(const char *format, ...);

	/*!
	 * \brief Prints a warning message over the logger.
	 * \param format printf()-compatible format string
	 * \param ... Format arguments
	 */
	GNU_PRINTF_ATTRIBUTE(1, 2)
	static void warn(const char *format, ...);

	/*!
	 * \brief Prints a debug message over the logger.
	 * \param format printf()-compatible format string
	 * \param ... Format arguments
	 */
	GNU_PRINTF_ATTRIBUTE(1, 2)
	static void trace(const char *format, ...);

	/**
	 * \brief Prints an message over the logger containing the description of
	 *        the error stored in errno.
	 * \param s Prefix printed before the errno description
	 */
	static void perror(const char *s);

	/**
	 * \brief Prints a memory dump.
	 * \param addr Address to display
	 * \param p Address at which to begin dumping
	 * \param size Number of bytes to dump
	 */
	static void mem(memory_address_t addr, const void *p, size_t size);

	/**
	 * \brief Prints a memory dump.
	 * \param p Address at which to begin dumping
	 * \param size Number of bytes to dump
	 */
	static void mem(const void *p, size_t size);
};
