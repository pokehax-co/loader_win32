/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef MISC_TOOLS_H
#define MISC_TOOLS_H

/*!
 * \file src/misc/tools.h
 * \brief Various inline functions for working with bits and bytes.
 */

#include <limits.h>
#include <stddef.h>

#ifndef NDEBUG
#include <assert.h>
#endif

#include "../portab/portab.h"
#include "../portab/stdint.h"


#ifdef __cplusplus
extern "C" {
#endif

/*
 * Bit operations
 */

/* Sign extension of signed integer of width 8 */
static __inline int32_t sign8_extend(uint32_t v)
{
	const uint32_t mask = (1u << 8) - 1;
	const uint32_t bit = 1u << (8 - 1);
	return ((v & mask) ^ bit) - bit;
}

/* Sign extension of signed integer of width 16 */
static __inline int32_t sign16_extend(uint32_t v)
{
	const uint32_t mask = (1u << 16) - 1;
	const uint32_t bit = 1u << (16 - 1);
	return ((v & mask) ^ bit) - bit;
}

/* Sign extension of signed integer of width 24 */
static __inline int32_t sign24_extend(uint32_t v)
{
	const uint32_t mask = (1u << 24) - 1u;
	const uint32_t bit = 1u << (24 - 1);
	return ((v & mask) ^ bit) - bit;
}

/* Left rotation of 32-bit integer */
static __inline uint32_t tools_rotl32(uint32_t x, uint32_t shift)
{
	return (x << shift) | (x >> (32 - shift));
}

/* Mask creation */
/* from https://github.com/umedaikiti/PowerPC64Simulator */
static __inline uint32_t mask32(uint32_t x, uint32_t y)
{
	uint32_t m1 = x == 0u ? 0xFFFFFFFFu : (1u << (32 - x)) - 1u;
	uint32_t m2 = (1u << (31 - y)) - 1u;
	if (x <= y)
		return m1 & ~m2;
	else
		return m1 | ~m2;
}

/*
 * Bit extraction operations on 32-bit words
 */

/* Returns bit 'bit', where 0 is the leftmost bit. */
static __inline uint32_t get_bit32_big(const uint32_t v, const uint32_t bit)
{
	return !!(v & (1u << ((sizeof(v) * CHAR_BIT) - 1 - bit)));
}

/* Returns bits 'start' to 'end', where 0 is the leftmost bit. */
static __inline uint32_t get_bits32_big(const uint32_t v,
	const uint32_t start, const uint32_t end)
{
	const uint32_t total_bits = end - start + 1;
#ifndef NDEBUG
	assert(start < end);
#endif
	return (v >> ((sizeof(v) * CHAR_BIT) - 1 - end)) &
		((1u << total_bits) - 1);
}

/*
 * 16/32/64-bit byte-swapping operations
 */

static __inline uint16_t tools_byteswap16(const uint16_t v)
{
#ifdef __GNUC__
	return __builtin_bswap16(v);
#elif defined(_MSC_VER) && (_MSC_VER + 0) >= 1400
	unsigned short __cdecl _byteswap_ushort(unsigned short);
	return _byteswap_ushort(v);
#else
	return (v >> 8) | ((v & 0xFFu) << 8);
#endif
}

static __inline uint32_t tools_byteswap32(const uint32_t v)
{
#ifdef __GNUC__
	return __builtin_bswap32(v);
#elif defined(_MSC_VER) && (_MSC_VER + 0) >= 1400
	unsigned long __cdecl _byteswap_ulong (unsigned long);
	return _byteswap_ulong(v);
#else
	return ((uint32_t)tools_byteswap16(v >> 16)) |
		(((uint32_t)tools_byteswap16(v & 0xFFFFu)) << 16);
#endif
}

static __inline uint64_t tools_byteswap64(const uint64_t v)
{
#ifdef __GNUC__
	return __builtin_bswap64(v);
#elif defined(_MSC_VER) && (_MSC_VER + 0) >= 1400
	unsigned __int64 __cdecl _byteswap_uint64(unsigned __int64);
	return _byteswap_uint64(v);
#else
	return ((uint64_t)tools_byteswap32((uint32_t)(v >> 32))) |
		(((uint64_t)tools_byteswap32((uint32_t)(v & 0xFFFFFFFFu))) << 32);
#endif
}

/*
 * 16/32/64-bit read operations
 */

/* Reads an unsigned 16-bit little endian value from p. */
static __inline uint16_t read_u16le(const uint8_t *const p)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	return *(const uint16_t *)p;
#else
	return (
		((uint16_t)p[1] << 8) | ((uint16_t)p[0] << 0)
	);
#endif
}

/* Reads an unsigned 16-bit big endian value from p. */
static __inline uint16_t read_u16be(const uint8_t *const p)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	return tools_byteswap16(*(const uint16_t *)p);
#else
	return (
		((uint16_t)p[0] << 8) | ((uint16_t)p[1] << 0)
	);
#endif
}

/* Reads an unsigned 32-bit little endian value from p. */
static __inline uint32_t read_u32le(const uint8_t *const p)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	return *(const uint32_t *)p;
#else
	return (
		((uint32_t)read_u16le(p + 2) << 16) |
		((uint32_t)read_u16le(p + 0) << 0)
	);
#endif
}

/* Reads an unsigned 32-bit big endian value from p. */
static __inline uint32_t read_u32be(const uint8_t *const p)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	return tools_byteswap32(*(const uint32_t *)p);
#else
	return (
		((uint32_t)read_u16be(p + 0) << 16) |
		((uint32_t)read_u16be(p + 2) << 0)
	);
#endif
}

/* Reads an unsigned 64-bit little endian value from p. */
static __inline uint64_t read_u64le(const uint8_t *const p)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	return *(const uint64_t *)p;
#else
	return (
		((uint64_t)read_u32le(p + 4) << 32) |
		((uint64_t)read_u32le(p + 0) << 0)
	);
#endif
}

/* Reads an unsigned 64-bit big endian value from p. */
static __inline uint64_t read_u64be(const uint8_t *const p)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	return tools_byteswap64(*(const uint64_t *)p);
#else
	return (
		((uint64_t)read_u32be(p + 0) << 32) |
		((uint64_t)read_u32be(p + 4) << 0)
	);
#endif
}

/*
 * 16/32/64-bit write operations
 */

/* Writes an unsigned 16-bit little endian value to p. */
static __inline void write_u16le(uint8_t *const p, const uint16_t v)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	*(uint16_t *)p = v;
#else
	p[1] = (v >> 8) & 0xFFu;
	p[0] = (v >> 0) & 0xFFu;
#endif
}

/* Writes an unsigned 16-bit big endian value to p. */
static __inline void write_u16be(uint8_t *const p, const uint16_t v)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	*(uint16_t *)p = tools_byteswap16(v);
#else
	p[0] = (v >> 8) & 0xFFu;
	p[1] = (v >> 0) & 0xFFu;
#endif
}

/* Writes an unsigned 32-bit little endian value to p. */
static __inline void write_u32le(uint8_t *const p, const uint32_t v)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	*(uint32_t *)p = v;
#else
	write_u16le(p + 2, (v >> 16) & 0xFFFFu);
	write_u16le(p + 0, (v >>  0) & 0xFFFFu);
#endif
}

/* Writes an unsigned 32-bit big endian value to p. */
static __inline void write_u32be(uint8_t *const p, const uint32_t v)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	*(uint32_t *)p = tools_byteswap32(v);
#else
	write_u16le(p + 0, (v >> 16) & 0xFFFFu);
	write_u16le(p + 2, (v >>  0) & 0xFFFFu);
#endif
}

/* Writes an unsigned 64-bit little endian value to p. */
static __inline void write_u64le(uint8_t *const p, const uint64_t v)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	*(uint64_t *)p = v;
#else
	write_u32le(p + 4, (uint32_t)((v >> 32) & 0xFFFFFFFFu));
	write_u32le(p + 0, (uint32_t)((v >>  0) & 0xFFFFFFFFu));
#endif
}

/* Writes an unsigned 64-bit big endian value to p. */
static __inline void write_u64be(uint8_t *const p, const uint64_t v)
{
#ifdef CONFIG_USE_POINTER_ALIASING
	*(uint64_t *)p = tools_byteswap64(v);
#else
	write_u32le(p + 0, (uint32_t)((v >> 32) & 0xFFFFFFFFu));
	write_u32le(p + 4, (uint32_t)((v >>  0) & 0xFFFFFFFFu));
#endif
}

/* Calculates the CRC32 checksum of 'data'. */
uint32_t tools_crc32(uint32_t old_crc, const void *data, size_t length);

#ifdef __cplusplus
}
#endif

#endif
