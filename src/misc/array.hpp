/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef MISC_ARRAY_HPP
#define MISC_ARRAY_HPP

/*!
 * \file src/misc/array.hpp
 * \brief Dynamically expanding array.
 */

#include <stddef.h>


template<typename T>
class Array
{
public:
    Array(void) :
        _alloc(NULL), _count(0)
    {}

    ~Array(void) {
        delete[] _alloc;
        _alloc = NULL;
    }

    bool append(const T &value)
    {
        /*
         * Expand allocation without using realloc().
         */
        T *new_alloc = new T[_count + 1];
        if (_alloc) {
            for (size_t i = 0; i < _count; ++i)
                new_alloc[i] = _alloc[i];
            delete[] _alloc;
        }
        _alloc = new_alloc;

        new_alloc[_count++] = value;
        return true;
    }

    bool remove(const T &value)
    {
        for (size_t i = 0; i < _count; ++i) {
            if (_alloc[i] == value) {
                for (size_t j = i; j < _count - 1; ++j)
                    _alloc[j] = _alloc[j + 1];
                --_count;
                return true;
            }
        }
        return false;
    }

    size_t size(void) const
    {
        return _count;
    }

    T &operator[](size_t index)
    {
        return _alloc[index];
    }

    const T &operator[](size_t index) const
    {
        return _alloc[index];
    }

private:
    T *_alloc;
    size_t _count;
};

#endif
