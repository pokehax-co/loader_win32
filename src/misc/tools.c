/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "tools.h"


static const uint32_t _crc32_table[16] =
{
	0x00000000u, 0xfdb71064u, 0xfb6e20c8u, 0x06d930acu,
	0xf6dc4190u, 0x0b6b51f4u, 0x0db26158u, 0xf005713cu,
	0xedb88320u, 0x100f9344u, 0x16d6a3e8u, 0xeb61b38cu,
	0x1b64c2b0u, 0xe6d3d2d4u, 0xe00ae278u, 0x1dbdf21cu
};


uint32_t tools_crc32(uint32_t old_crc, const void *data, size_t length)
{
	const char *datap = data;
	while (length--) {
		old_crc ^= *datap++;
		old_crc = (old_crc >> 4) ^ _crc32_table[old_crc & 0xFu];
		old_crc = (old_crc >> 4) ^ _crc32_table[old_crc & 0xFu];
	}
	return old_crc;
}
