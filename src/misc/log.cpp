/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "log.hpp"

#include <stdio.h>
#include <stddef.h>
#include <string.h>


const char *Log::_argv0 = NULL;

void Log::_printv(const char *severity, const char *format,
	va_list args)
{
	fprintf(stderr, "%s: %s: ", _argv0, severity);
	vfprintf(stderr, format, args);
}

void Log::init(const char *argv0)
{
	/* If there is a slash (or backslash) in it, cut the argv down */
	const char *save = argv0;
	while (*argv0) {
		if (*argv0 == '/'
#ifdef _WIN32
			|| *argv0 == '\\'
#endif
			)
			save = argv0 + 1;
		++argv0;
	}
	_argv0 = save;
}

void Log::error(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	_printv("error", format, args);
	va_end(args);
}

void Log::warn(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	_printv("warning", format, args);
	va_end(args);
}

void Log::trace(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	_printv("trace", format, args);
	va_end(args);
}

void Log::perror(const char *s)
{
	fprintf(stderr, "%s: %s: %s\n", _argv0, s, strerror(errno));
}

void Log::mem(memory_address_t addr, const void *p, size_t size)
{
	const unsigned char *address =
		static_cast<const unsigned char *>(p);

	while (size) {
		const unsigned int line_max = size > 16 ? 16 : size;

		/* Precompute values to display for each of the 16 bytes */
		int line_values[16];
		for (unsigned int i = 0; i < 16; ++i) {
			if (i >= line_max)
				line_values[i] = -1; /* whitespace */
			else
				line_values[i] = address[i];
		}

		/* Print the text */
		printf("%" PRIXMA " | ", addr);
		for (unsigned int i = 0; i < 16; ++i) {
			if (line_values[i] == -1)
				printf("   ");
			else
				printf("%02X ", line_values[i]);
		}
		printf("| ");
		for (unsigned int i = 0; i < 16; ++i) {
			int c;
			if (line_values[i] == -1)
				c = ' ';
			else if (line_values[i] >= 0x20 && line_values[i] <= 0x7E)
				c = line_values[i];
			else if (line_values[i] >= 0xA0 && line_values[i] <= 0xFF) {
#ifdef _WIN32
				/* Windows can't UTF-8 on console for compat reasons */
				c = line_values[i];
#else
				unsigned char v1 = 0xC0u | ((line_values[i] >> 6u) & 0x3u);
				unsigned char v2 = 0x80u | (line_values[i] & 0x3Fu);
				putc(v1, stdout);
				c = v2;
#endif
			} else
				c = '.';
			putc(c, stdout);
		}
		putc('\n', stdout);

		addr += line_max;
		address += line_max;
		size -= line_max;
	}
}

void Log::mem(const void *p, size_t size)
{
	Log::mem(reinterpret_cast<memory_address_t>(p), p, size);
}
