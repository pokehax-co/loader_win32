#pragma once

/*!
 * \file src/os/win32.hpp
 * \brief Win32 userspace simulation for running Windows executables using
 *        foreign CPU architectures
 */

#include "../hw/cpu.h"
#include "../process/virtmem.h"
#include "../misc/array.hpp"


/* src/exefmt/pe.hpp */
class PEExecutable;

/* src/mem.hpp */
class AddressSpace;

/* src/os/ntppc.hpp */
class CallingConventionNTPowerPC;

/* src/os/win32mod.hpp */
class Win32Module;
class Win32PEModule;


/*
 * Provides a Windows environment for an executable file.
 */
class Win32Environment
{
public:
	Win32Environment(int argc, char **argv, char **envp);
	~Win32Environment(void);

	bool load_process(PEExecutable *&exe);
	bool run(int &exit_code);

	/* Loads a new module */
	Win32Module *load_module(const char *name);

	/* Returns a loaded module by its name */
	Win32Module *get_module(const char *name);

	/* Returns a loaded module by its base address */
	Win32Module *get_module(memory_address_t base_address);

	/* Handlers for the PE import resolver. These must be public because
	   they're used by Win32PEModule. */
	static bool pe_module_callback(struct VirtualMemoryContext *memory,
		const char *module_name, void *user);
	static bool pe_function_callback(struct VirtualMemoryContext *memory,
		const char *module_name, const char *function_name,
		memory_address_t &function_pointer, void *user);

	/* Accessors for private fields */
	Win32PEModule *get_main_executable(void) const {
		return _main_module;
	}

	PEExecutable *get_executable(void) const;

	AddressSpace *get_memory(void) const {
		return _memory;
	}

	struct CPUContext *get_cpu(void) const {
		return _cpu_c;
	}

	memory_address_t get_process_heap(void) const {
		return _process_heap;
	}

	void get_program_arguments(int &argc, char **&argv, char **&envp) {
		argc = _program_argc;
		argv = _program_argv;
		envp = _program_envp;
	}

	memory_address_t sbrk(int increment) {
		const memory_address_t old_sbrk_address = _sbrk_address;
		_sbrk_address += increment;
		return old_sbrk_address;
	}

	void get_stack_limits(memory_address_t &top, memory_address_t &bottom) const {
		top = _stack_top;
		bottom = _stack_bottom;
	}

	int get_process_exit_code(void) const {
		return _process_exit_code;
	}

	void set_process_exit_code(int code) {
		_process_exit_code = code;
	}

private:
	int _program_argc;
	char **_program_argv;
	char **_program_envp;

	Win32PEModule *_main_module;
	char *_main_module_path;

	AddressSpace *_memory;
	struct CPUContext *_cpu_c;
	CallingConventionNTPowerPC *_callconv;

	/* List of loaded builtin DLLs */
	Array<Win32Module *> _builtin_dlls;

	int _process_exit_code;

	/* process heap */
	memory_address_t _sbrk_address;
	memory_address_t _process_heap;

	/* stack information */
	memory_address_t _stack_bottom;
	memory_address_t _stack_top;

	/* Syscall handler for the CPU */
	static unsigned int _powerpc32_syscall_handler(
		struct CPUContext *cpu, void *user);

	/*
	 * Callback called when a page is not present or when the protection
	 * settings prevent the operation.
	 */
	static void _default_fault_callback(struct VirtualMemoryContext *ctx,
		memory_address_t fault_address, unsigned int fault_size,
		enum MemoryStatus failure_status, enum MemoryProtection failure_op,
		void *callback_user);
};
