/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "ntdll.hpp"

#include "../exefmt/pe.hpp"
#include "../mem.hpp"
#include "../misc/log.hpp"
#include "../misc/array.hpp"
#include "win32.hpp"

#include <assert.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>


/*
 * NT heap functions
 */
typedef struct _RTL_HEAP_PARAMETERS RTL_HEAP_PARAMETERS, *PRTL_HEAP_PARAMETERS;

extern "C" HANDLE NTAPI RtlCreateHeap(ULONG Flags, PVOID Addr,
	SIZE_T TotalSize, SIZE_T CommitSize, PVOID Lock,
	PRTL_HEAP_PARAMETERS Parameters);
extern "C" PVOID NTAPI RtlAllocateHeap(PVOID HeapPtr, ULONG Flags,
	SIZE_T Size);
extern "C" PVOID NTAPI RtlReAllocateHeap(PVOID HeapPtr, ULONG Flags,
	PVOID Ptr, SIZE_T Size);
extern "C" BOOLEAN NTAPI RtlFreeHeap(HANDLE HeapPtr, LONG Flags, PVOID Ptr);
extern "C" HANDLE NTAPI RtlDestroyHeap(HANDLE HeapPtr);
extern "C" SIZE_T NTAPI RtlSizeHeap(HANDLE HeapPtr, ULONG Flags,
	PVOID Ptr);


namespace Win32DLLs {

/*
 * Implementation of a single heap
 */
struct _NTDLLHeap
{
	HANDLE allocation;
	size_t allocation_size;
	bool initialized;

	_NTDLLHeap(void) : allocation(NULL), allocation_size(0),
		initialized(false)
	{
	}

	bool preallocate(size_t size)
	{
		/* Already allocated? */
		if (allocation)
			return true;

		/* Reserve memory as the NT API won't do it for us */
		void *heap_alloc = VirtualAlloc(NULL, size, MEM_RESERVE,
			PAGE_READWRITE);
		if (!heap_alloc) {
			Log::error("[_NTDLLHeap] VirtualAlloc() reservation failed "
				"(code %lu)\n", GetLastError());
			return false;
		}

		/* Save allocation, but the heap is not initialized yet */
		allocation = heap_alloc;
		allocation_size = size;
		return true;
	}

	bool create(unsigned int flags)
	{
		/* Already initialized? */
		if (initialized)
			return true;
		if (!allocation)
			return false;

		/* Commit the virtual allocation */
		if (!VirtualAlloc(allocation, allocation_size, MEM_COMMIT,
				PAGE_READWRITE)) {
			Log::error("[_NTDLLHeap] VirtualAlloc() commit failed "
				"(code %lu)\n", GetLastError());
			return false;
		}

		/* Create the new heap using the NT API. */
		void *heap = RtlCreateHeap(flags, allocation, allocation_size,
			allocation_size, NULL, NULL);
		if (!heap) {
			Log::error("[_NTDLLHeap] RtlCreateHeap() failed\n");
			return false;
		}

		/* Heap is now initialized */
		initialized = true;
		return true;
	}

	bool alloc(unsigned int size, void *&block) {
		block = RtlAllocateHeap(allocation, 0, size);
		return block != NULL;
	}

	bool allocz(unsigned int size, void *&block) {
		block = RtlAllocateHeap(allocation, HEAP_ZERO_MEMORY, size);
		return block != NULL;
	}

	bool realloc(unsigned int size, void *&block) {
		block = RtlReAllocateHeap(allocation, 0, block, size);
		return block != NULL;
	}

	bool size(void *block, size_t &ret) {
		ret = RtlSizeHeap(allocation, 0, block);
		return ret != (size_t)-1;
	}

	bool free(void *block) {
		return !!RtlFreeHeap(allocation, 0, block);
	}

	~_NTDLLHeap(void)
	{
		if (allocation) {
			if (initialized)
				RtlDestroyHeap(allocation);
			VirtualFree(allocation, 0, MEM_RELEASE);
		}
	}
};

struct NTDLL::_Internal
{
	/* Array of heaps */
	Array<struct _NTDLLHeap *> heaps;
};


/*
 * NTDLL implementation
 */

NTDLL::NTDLL(CallingConventionNTPowerPC &callconv,
		Win32Environment &environment) :
	Win32Module(environment, "ntdll.dll", 0x7E020000u),
	_callconv(callconv)
{
	/* Initialize internal structure */
	_internal = new _Internal;

	// FIXME: register functions

#ifdef DEBUG_NTDLL
	Log::trace("NTDLL loaded\n");
#endif
}

NTDLL::~NTDLL(void)
{
	/* Delete all allocated heaps. Removing from the array is not necessary
	   as the array itself is freed when the internal structure is deleted. */
	for (size_t i = 0; i < _internal->heaps.size(); ++i)
		delete _internal->heaps[i];

#ifdef DEBUG_NTDLL
	Log::trace("NTDLL unloaded\n");
#endif

	/* Delete internal structure */
	delete _internal;
}

bool NTDLL::get_function_address(const char *function_name,
	memory_address_t &function_address) const
{
	/* For now, no functions are exposed to the simulation */
	(void)function_address;
	Log::error("[NTDLL] get_function_address '%s'\n", function_name);
	return false;
}

void NTDLL::get_syscall_range(unsigned int &first, unsigned int &last) const
{
	/* This is a native DLL */
	first = 0x0;
	last = 0x4FF;
}

bool NTDLL::handle_syscall(unsigned int syscall_number)
{
	/* For now, no functions are exposed to the simulation */
	Log::error("[NTDLL] handle_syscall 0x%X\n", syscall_number);
	return false;
}

bool NTDLL::dll_main(unsigned int reason)
{
#if 0
	if (reason == 1) {
		// process attach
	}
	else if (reason == 0) {
		// process detach
	}
	else if (reason == 2) {
		// thread attach
	}
	else if (reason == 3) {
		// thread detach
	}
#else
	(void)reason;
#endif
	return true;
}

/*
 * NTDLL class-specific functions
 */

#define NTDLL_HEAP_RETRIES 50

bool NTDLL::heap_create(unsigned int flags, unsigned int total_size,
	unsigned int commit_size, memory_address_t &heap_address)
{
	/* If maximum size is 0, allocate a heap of total_size bytes */
	if (total_size != 0) {
		if (commit_size < total_size) {
			Log::error("[NTDLL] heap_create() commit_size < total_size\n");
			return false;
		}
	}

	/* Otherwise allocate a fixed size of 32 MiB, as we can't track which pages
	   the NT heap implementation has occupied. */
	else {
		total_size = commit_size = 32u * 1048576u;
	}

	struct _NTDLLHeap *new_heap = NULL;
	AddressSpace *const memory = _environment.get_memory();

	/*
	 * We have NTDLL_HEAP_RETRIES to pre-allocate memory
	 */

	size_t i;
	struct _NTDLLHeap *burned_heaps[NTDLL_HEAP_RETRIES];

	for (i = 0; i < NTDLL_HEAP_RETRIES; ++i) {
		const enum MemoryProtection heap_protection =
			(enum MemoryProtection)(MEMORY_PROT_READ | MEMORY_PROT_WRITE);

		/* Allocate host memory for a new heap somewhere */
		burned_heaps[i] = new _NTDLLHeap();
		if (!burned_heaps[i]->preallocate(total_size)) {
			Log::error("[NTDLL] can't allocate %u bytes of heap\n",
				total_size);
			break;
		}

#ifdef DEBUG_NTDLL
		Log::trace("[NTDLL] trying to map virtmem at 0x%p\n",
			burned_heaps[i]->allocation);
#endif

		/* Try to map it at the same address */
		memory_address_t vaddr = reinterpret_cast<memory_address_t>(
			burned_heaps[i]->allocation
		);

		if (virtual_memory_allocate(memory->get_virtmem(),
				burned_heaps[i]->allocation, &vaddr, total_size,
				heap_protection) >= 0) {
#ifdef DEBUG_NTDLL
			Log::trace("[NTDLL] mapping succeeded, we have a heap\n");
#endif
			new_heap = burned_heaps[i];
			break;
		}
	}

	/* Free up burned addresses */
	for (size_t j = 0; new_heap ? (i > 0 && j < i - 1) : (j < i); ++j) {
#ifdef DEBUG_NTDLL
		Log::trace("[NTDLL] releasing burned address 0x%p\n",
			burned_heaps[j]->allocation);
#endif
		delete burned_heaps[j];
	}

	if (!new_heap)
		return false;

	/* Create the heap in the new allocation */
	if (!new_heap->create(flags)) {
		Log::error("[NTDLL] can't initialize heap at 0x%p\n",
			new_heap->allocation);
		delete new_heap;
		return false;
	}

#ifdef DEBUG_NTDLL
	Log::trace("[NTDLL] heap_create(), created heap at 0x%p\n",
		new_heap->allocation);
#endif

	/* Return heap address */
	heap_address = reinterpret_cast<memory_address_t>(new_heap->allocation);

	/* Add heap to list */
	_internal->heaps.append(new_heap);
	return true;
}

bool NTDLL::heap_alloc(memory_address_t heap_address, unsigned int size,
	memory_address_t &allocation_address, bool zero)
{
	/* Find heap with this address */
	HANDLE _heap_address = reinterpret_cast<HANDLE>(heap_address);

	_NTDLLHeap *it = NULL;
	for (size_t i = 0; i < _internal->heaps.size(); ++i) {
		if (_internal->heaps[i]->allocation == _heap_address) {
			it = _internal->heaps[i];
			break;
		}
	}
	if (!it) {
		Log::error("[NTDLL] heap_alloc(), unknown heap 0x%" PRIXMA "\n",
			heap_address);
		return false;
	}

	/* Perform standard allocation */
	void *_allocation_address;
	bool ret;
	if (zero)
		ret = it->allocz(size, _allocation_address);
	else
		ret = it->alloc(size, _allocation_address);
	if (!ret) {
		Log::error("[NTDLL] heap_alloc(), size %u failed\n", size);
		return false;
	}

	/* Return allocation address */
	allocation_address =
		reinterpret_cast<memory_address_t>(_allocation_address);
	return true;
}

bool NTDLL::heap_realloc(memory_address_t heap_address, unsigned int size,
	memory_address_t &allocation_address)
{
	/* Find heap with this address */
	HANDLE _heap_address = reinterpret_cast<HANDLE>(heap_address);

	_NTDLLHeap *it = NULL;
	for (size_t i = 0; i < _internal->heaps.size(); ++i) {
		if (_internal->heaps[i]->allocation == _heap_address) {
			it = _internal->heaps[i];
			break;
		}
	}
	if (!it) {
		Log::error("[NTDLL] heap_realloc(), unknown heap 0x%" PRIXMA "\n",
			heap_address);
		return false;
	}

	/* Perform standard reallocation */
	void *new_allocation_address = (void *)allocation_address;
	if (!it->realloc(size, new_allocation_address)) {
		Log::error("[NTDLL] heap_realloc(), size %u failed\n", size);
		return false;
	}

	/* Return allocation address */
	allocation_address =
		reinterpret_cast<memory_address_t>(new_allocation_address);
	return true;
}

bool NTDLL::heap_size(memory_address_t heap_address,
	memory_address_t allocation_address, unsigned int &allocation_size)
{
	/* Find heap with this address */
	HANDLE _heap_address = reinterpret_cast<HANDLE>(heap_address);

	_NTDLLHeap *it = NULL;
	for (size_t i = 0; i < _internal->heaps.size(); ++i) {
		if (_internal->heaps[i]->allocation == _heap_address) {
			it = _internal->heaps[i];
			break;
		}
	}
	if (!it) {
		Log::error("[NTDLL] heap_size(), unknown heap 0x%" PRIXMA "\n",
			heap_address);
		return false;
	}

	/* Query the allocation size */
	void *const _allocation_address = reinterpret_cast<void *>(
		allocation_address
	);
	if (!it->size(_allocation_address, allocation_size)) {
		Log::error("[NTDLL] heap_size() of 0x%" PRIXMA " failed\n",
			allocation_address);
		return false;
	}
	return true;
}

bool NTDLL::heap_free(memory_address_t heap_address,
	memory_address_t allocation_address)
{
	/* Find heap with this address */
	HANDLE _heap_address = reinterpret_cast<HANDLE>(heap_address);

	_NTDLLHeap *it = NULL;
	for (size_t i = 0; i < _internal->heaps.size(); ++i) {
		if (_internal->heaps[i]->allocation == _heap_address) {
			it = _internal->heaps[i];
			break;
		}
	}
	if (!it) {
		Log::error("[NTDLL] heap_free(), unknown heap 0x%" PRIXMA "\n",
			heap_address);
		return false;
	}

	/* Perform standard deallocation */
	void *const _allocation_address = reinterpret_cast<void *>(
		allocation_address
	);
	if (!it->free(_allocation_address)) {
		Log::error("[NTDLL] heap_free() of 0x%" PRIXMA " failed\n",
			allocation_address);
		return false;
	}
	return true;
}

bool NTDLL::heap_destroy(memory_address_t heap_address)
{
	/* Find heap with this address */
	HANDLE _heap_address = reinterpret_cast<HANDLE>(heap_address);

	_NTDLLHeap *it = NULL;
	for (size_t i = 0; i < _internal->heaps.size(); ++i) {
		if (_internal->heaps[i]->allocation == _heap_address) {
			it = _internal->heaps[i];
			break;
		}
	}
	if (!it) {
		Log::error("[NTDLL] heap_destroy(), unknown heap 0x%" PRIXMA "\n",
			heap_address);
		return false;
	}
	if (it == _internal->heaps[0]) {
		Log::error("[NTDLL] heap_destroy(), can't destroy process heap!\n");
		return false;
	}

	/* Unmap heap allocation from AddressSpace */
	AddressSpace *const memory = _environment.get_memory();
	if (virtual_memory_free(memory->get_virtmem(), heap_address) < 0) {
		Log::error("[NTDLL] heap_destroy(), unmapping heap page 0x%" PRIXMA " "
			"failed\n", heap_address);
		return false;
	}

	/* Delete the heap and remove from list */
	_internal->heaps.remove(it);
	delete it;
	return true;
}

}
