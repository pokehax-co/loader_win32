/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "kernel32.hpp"

#include "../misc/log.hpp"
#include "../misc/array.hpp"
#include "ntppc.hpp"
#include "ntdll.hpp"
#include "../mem.hpp"
#include "../misc/tools.h"
#include "win32.hpp"
#include "win32mod.hpp"
#include "../exefmt/pe.hpp"
#include "../portab/swprintf.h"

#define _WIN32_LEAN_AND_MEAN
#include <windows.h>


/* XXX: copied from crtdll.cpp */
#define READ_U32(addr, value) \
	(cpu_powerpc_read_32(cpu, (addr), (value)))
#define READ_U32_OK(addr, value) \
	(READ_U32(addr, value) == MEMORY_STATUS_OK)
#define WRITE_U32(addr, value) \
	(cpu_powerpc_write_32(cpu, (addr), (unsigned int)(value)))
#define WRITE_U32_OK(addr, value) \
	(WRITE_U32(addr, value) == MEMORY_STATUS_OK)


namespace Win32DLLs {

/*
 * KERNEL32 internals
 */

struct KERNEL32::_Internal
{
	/* Reference to NTDLL */
	Win32DLLs::NTDLL *ntdll;

	/* Original standard handles */
	HANDLE _orig_input, _orig_error, _orig_output;

	/* Console Ctrl-C handlers */
	bool static_handler_added;
	Array<unsigned int> console_ctrl_handlers;

	/* Allocated memory blocks */
	memory_address_t memory_cmdline_environment;

	/* Pointers to command line and environment */
	memory_address_t memory_cmdline;
	memory_address_t memory_environment;

private:
	bool _create_startup_info(Win32Environment &environment);
public:

	static BOOL WINAPI _console_ctrl_handler(DWORD dwCtrlType);

	/* Last error code */
	DWORD last_error;

	_Internal(Win32Environment &environment) :
		_orig_input(INVALID_HANDLE_VALUE),
		_orig_error(INVALID_HANDLE_VALUE),
		_orig_output(INVALID_HANDLE_VALUE),
		static_handler_added(false),
		memory_cmdline_environment(0),
		memory_cmdline(0),
		memory_environment(0),
		last_error(NO_ERROR)
	{
		/* We need NTDLL for the heap */
		ntdll = static_cast<Win32DLLs::NTDLL *>(
			environment.get_module("ntdll.dll")
		);

		if (!ntdll) {
			Log::error("KERNEL32 can't get NTDLL\n");
			exit(1);
		}
	}

	/* Addresses to system call trampolines */
	unsigned int FHeapCreate;
	unsigned int FSetConsoleCtrlHandler;
	unsigned int FGetModuleHandleA;
	unsigned int FGetProcAddress;
	unsigned int FCreateFileA;
	unsigned int FCreateFileMappingA;
	unsigned int FMapViewOfFileEx;
	unsigned int FCloseHandle;
	unsigned int FGetFileSize;
	unsigned int FGetModuleFileNameA;
	unsigned int FLoadLibraryA;
	unsigned int FGetCommandLineA;
	unsigned int FGetEnvironmentStrings;
	unsigned int FGetVersion;
	unsigned int FGetProcessHeap;
	unsigned int FInitializeCriticalSection;
	unsigned int FEnterCriticalSection;
	unsigned int FLeaveCriticalSection;
	unsigned int FDeleteCriticalSection;
	unsigned int FTlsAlloc;
	unsigned int FHeapAlloc;
	unsigned int FTlsSetValue;
	unsigned int FGetCurrentThreadId;
	unsigned int FGetStartupInfoA;
	unsigned int FGetStdHandle;
	unsigned int FGetFileType;
	unsigned int FMultiByteToWideChar;
	unsigned int FGetTimeZoneInformation;
	unsigned int FGetLocalTime;
	unsigned int FTlsGetValue;
	unsigned int FHeapSize;
	unsigned int FGetFileAttributesA;
	unsigned int FGetLastError;
	unsigned int FSetLastError;
	unsigned int FHeapFree;
	unsigned int FWriteFile;
	unsigned int FGetCurrentDirectoryA;
	unsigned int FDuplicateHandle;
	unsigned int FRtlMoveMemory;
	unsigned int FRtlFillMemory;
	unsigned int FFindFirstFileA;
	unsigned int FFileTimeToSystemTime;
	unsigned int FFindClose;
	unsigned int FSetFilePointer;
	unsigned int FReadFile;
	unsigned int FUnmapViewOfFile;
	unsigned int FSetEndOfFile;
	unsigned int FDeleteFileA;
	unsigned int FHeapDestroy;
	unsigned int FExitProcess;
	unsigned int FHeapReAlloc;
	unsigned int FGetSystemTime;
	unsigned int FSetStdHandle;
	unsigned int FCreateProcessA;
	unsigned int FWaitForSingleObject;
	unsigned int FGetExitCodeProcess;
	unsigned int FGetCurrentProcessId;
	unsigned int FGetConsoleScreenBufferInfo;
	unsigned int FGetCurrentProcess;
	unsigned int FGetConsoleCP;
	unsigned int FGetCPInfo;
	unsigned int FSetErrorMode;
	unsigned int FMapViewOfFile;

	/* Implementations */
	bool IHeapCreate(KERNEL32 &module);
	bool ISetConsoleCtrlHandler(KERNEL32 &module);
	bool IGetModuleHandleA(KERNEL32 &module);
	bool IGetProcAddress(KERNEL32 &module);
	bool ICreateFileA(KERNEL32 &module);
	bool ICreateFileMappingA(KERNEL32 &module);
	bool IMapViewOfFileEx(KERNEL32 &module, bool have_base_address);
	bool ICloseHandle(KERNEL32 &module);
	bool IGetFileSize(KERNEL32 &module);
	bool IGetModuleFileNameA(KERNEL32 &module);
	bool ILoadLibraryA(KERNEL32 &module);
	bool IGetCommandLineA(KERNEL32 &module);
	bool IGetEnvironmentStrings(KERNEL32 &module);
	bool IGetVersion(KERNEL32 &module);
	bool IGetProcessHeap(KERNEL32 &module);
	bool IxxxCriticalSection(KERNEL32 &module, unsigned int op);
	bool ITlsAlloc(KERNEL32 &module);
	bool IHeapAlloc(KERNEL32 &module);
	bool ITlsSetValue(KERNEL32 &module);
	bool IGetCurrentThreadId(KERNEL32 &module);
	bool IGetStartupInfoA(KERNEL32 &module);
	bool IGetStdHandle(KERNEL32 &module);
	bool IGetFileType(KERNEL32 &module);
	bool IMultiByteToWideChar(KERNEL32 &module);
	bool IGetTimeZoneInformation(KERNEL32 &module);
	bool IGetLocalTime(KERNEL32 &module);
	bool ITlsGetValue(KERNEL32 &module);
	bool IHeapSize(KERNEL32 &module);
	bool IGetFileAttributesA(KERNEL32 &module);
	bool IxxxLastError(KERNEL32 &module, bool set);
	bool IHeapFree(KERNEL32 &module);
	bool IReadWriteFile(KERNEL32 &module, bool write);
	bool IGetCurrentDirectoryA(KERNEL32 &module);
	bool IDuplicateHandle(KERNEL32 &module);
	bool IRtlMoveMemory(KERNEL32 &module);
	bool IRtlFillMemory(KERNEL32 &module);
	bool IFindFirstFileA(KERNEL32 &module);
	bool IFileTimeToSystemTime(KERNEL32 &module);
	bool IFindClose(KERNEL32 &module);
	bool ISetFilePointer(KERNEL32 &module);
	bool IUnmapViewOfFile(KERNEL32 &module);
	bool ISetEndOfFile(KERNEL32 &module);
	bool IDeleteFileA(KERNEL32 &module);
	bool IHeapDestroy(KERNEL32 &module);
	bool IExitProcess(KERNEL32 &module);
	bool IHeapReAlloc(KERNEL32 &module);
	bool IGetSystemTime(KERNEL32 &module);
	bool ISetStdHandle(KERNEL32 &module);
	bool ICreateProcessA(KERNEL32 &module);
	bool IWaitForSingleObject(KERNEL32 &module);
	bool IGetExitCodeProcess(KERNEL32 &module);
	bool IGetCurrentProcessId(KERNEL32 &module);
	bool IGetConsoleScreenBufferInfo(KERNEL32 &module);
	bool IGetCurrentProcess(KERNEL32 &module);
	bool IGetConsoleCP(KERNEL32 &module);
	bool IGetCPInfo(KERNEL32 &module);
	bool ISetErrorMode(KERNEL32 &module);
};

/* Ctrl-C handler */
BOOL WINAPI KERNEL32::_Internal::_console_ctrl_handler(DWORD dwCtrlType)
{
	// XXX: Not implemented
	Log::warn("Console Ctrl-C handler dwCtrlType=0x%lX\n", dwCtrlType);
	return TRUE;
}

bool KERNEL32::_Internal::_create_startup_info(Win32Environment &environment)
{
	/* Ask environment for program arguments */
	int host_argc;
	char **host_argv, **host_envp;
	environment.get_program_arguments(host_argc, host_argv, host_envp);

	/*
	 * Determine how much memory we need
	 */
	unsigned int strings_size = 0;

	/* argv */
	for (int i = 0; i < host_argc; ++i) {
		unsigned int string_size = 0;
		size_t p_len = strlen(host_argv[i]);

		/* https://stackoverflow.com/a/47469792 */
		if (strcspn(host_argv[i], " \t\n\v\"") == p_len)
			string_size += p_len;
		else {
			/* quotation mark */
			++string_size;

			for (char *p = host_argv[i]; *p; ++p) {
				unsigned int backslashes = 0;

				while (*p && *p == '\\') {
					++backslashes;
					++p;
				}

				/* escape backslashes */
				if (!*p)
					string_size += 2 * backslashes;

				/* escape backslashes + quotation mark */
				else if (*p == '"') {
					string_size += 2 * backslashes + 1;
					++string_size;
				}

				/* any other character */
				else {
					string_size += backslashes;
					++string_size;
				}
			}

			/* quotation mark */
			++string_size;
		}

		/* Not the last one? Add a space */
		if (i < host_argc - 1)
			++string_size;

		strings_size += string_size;
	}
	++strings_size;

	/* envp -- make sure every string is null-terminated */
	for (char **p = host_envp; *p; ++p)
		strings_size += 1 + strlen(*p);
	++strings_size;

	/* Allocate up to 128 KiB (32 pages) */
	if (strings_size > 128u * 1024u) {
		Log::error("not enough memory for allocating startup data\n");
		return false;
	}

	/* Local allocation, write it at once later */
	char *const startup_data = new char[strings_size];
	char *startup_p = startup_data;

	/*
	 * Build command line and environment strings
	 */
	for (int i = 0; i < host_argc; ++i) {
		size_t p_len = strlen(host_argv[i]);

		/* https://stackoverflow.com/a/47469792 */
		if (strcspn(host_argv[i], " \t\n\v\"") == p_len) {
			memcpy(startup_p, host_argv[i], p_len);
			startup_p += p_len;
		}
		else {
			/* quotation mark */
			*startup_p++ = '"';

			for (char *p = host_argv[i]; *p; ++p) {
				unsigned int backslashes = 0;

				while (*p && *p == '\\') {
					++backslashes;
					++p;
				}

				/* escape backslashes */
				if (!*p) {
					memset(startup_p, '\\', 2 * backslashes);
					startup_p += 2 * backslashes;
				}

				/* escape backslashes + quotation mark */
				else if (*p == '"') {
					memset(startup_p, '\\', 2 * backslashes + 1);
					startup_p += 2 * backslashes + 1;
					*startup_p++ = '"';
				}

				/* any other character */
				else {
					memset(startup_p, '\\', backslashes);
					startup_p += backslashes;
					*startup_p++ = *p;
				}
			}

			/* quotation mark */
			*startup_p++ = '"';
		}

		/* Not the last one? Add a space */
		if (i < host_argc - 1)
			*startup_p++ = ' ';
	}

	*startup_p++ = '\0';
#ifdef DEBUG_KERNEL32
	Log::trace("Command line: >%s<\n", startup_data);
#endif

	/* envp */
	char *const startup_data_environment = startup_p;

	for (char **p = host_envp; *p; ++p) {
		size_t p_len = strlen(*p) + 1;
		memcpy(startup_p, *p, p_len);
		startup_p += p_len;
	}

	*startup_p++ = '\0';
#ifdef DEBUG_KERNEL32
	Log::trace("First environment variable: >%s<\n", startup_data_environment);
#endif

	/*
	 * Copy to virtual memory and assign addresses
	 */
	struct VirtualMemoryContext *const memory =
		environment.get_memory()->get_virtmem();

	if (virtual_memory_allocate(memory, NULL, &memory_cmdline_environment,
			strings_size, (enum MemoryProtection)(MEMORY_PROT_READ |
			MEMORY_PROT_WRITE)) < 0) {
		Log::error("mapping memory for startup data failed\n");
		return false;
	}

	memory_cmdline = memory_cmdline_environment;
	memory_environment = memory_cmdline_environment +
		(startup_data_environment - startup_data);

	if (!MEMOP_OK(virtual_memory_host_write(memory,
			memory_cmdline_environment, startup_data, strings_size))) {
		Log::error("writing startup data failed\n");
		return false;
	}
	return true;
}


/*
 * HANDLE __stdcall HeapCreate(DWORD flOptions, SIZE_T dwInitialSize,
 *     SIZE_T dwMaximumSize);
 *
 * Allocates a new heap.
 */
bool KERNEL32::_Internal::IHeapCreate(KERNEL32 &module)
{
	unsigned int arg_flOptions;
	unsigned int arg_dwInitialSize;
	unsigned int arg_dwMaximumSize;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_flOptions);
	module._callconv.get_function_argument(1, arg_dwInitialSize);
	module._callconv.get_function_argument(2, arg_dwMaximumSize);

	/* Pass to NTDLL */
	memory_address_t new_heap;
	if (!ntdll->heap_create(arg_flOptions, arg_dwMaximumSize,
			arg_dwInitialSize, new_heap))
		return false;

	/* Return the new heap */
	module._callconv.set_return_value((unsigned int)new_heap);
	return true;
}

/*
 * BOOL WINAPI SetConsoleCtrlHandler(PHANDLER_ROUTINE HandlerRoutine,
 *     BOOL Add);
 *
 * Sets a handler to be called when Ctrl-C is pressed in the console.
 */
bool KERNEL32::_Internal::ISetConsoleCtrlHandler(KERNEL32 &module)
{
	unsigned int arg_HandlerRoutine;
	unsigned int arg_Add;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_HandlerRoutine);
	module._callconv.get_function_argument(1, arg_Add);

#ifdef DEBUG_KERNEL32
	Log::trace("[KERNEL32] SetConsoleCtrlHandler(0x%X, %u)\n",
		arg_HandlerRoutine, arg_Add);
#endif

	/* Adding a new handler */
	BOOL ret;
	if (arg_HandlerRoutine != 0) {
		if (arg_Add) {
			/* Add handler */
			console_ctrl_handlers.append(arg_HandlerRoutine);
			if (!static_handler_added) {
				static_handler_added = true;
				ret = SetConsoleCtrlHandler(_console_ctrl_handler, TRUE);
			} else {
				/* Already added */
				ret = TRUE;
			}
		} else {
			/* Remove handler */
			console_ctrl_handlers.remove(arg_HandlerRoutine);
			ret = TRUE;
		}
	} else {
		/* Enable or disable Ctrl-C */
		ret = SetConsoleCtrlHandler(NULL, arg_Add);
	}

	// FIXME: Save last error on failure
	if (!ret) {
		Log::warn("kernel32!SetConsoleCtrlHandler() failed\n");
	}

	/* Return the API return value */
	module._callconv.set_return_value((unsigned int)ret);
	return true;
}

bool KERNEL32::_Internal::IGetModuleHandleA(KERNEL32 &module)
{
	AddressSpace *const memory = module._environment.get_memory();
	unsigned int arg_lpModuleName;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpModuleName);

	/* Read module name */
	char *actual_lpModuleName;
	if (memory->read_string(arg_lpModuleName, actual_lpModuleName) !=
			MEMORY_STATUS_OK)
		return false;

	/* Ask the environment */
	Win32Module *queried_module = module._environment.get_module(
		actual_lpModuleName);
	if (!queried_module) {
		Log::error("[KERNEL32] GetModuleHandleA() cannot find %s\n",
			actual_lpModuleName);
		delete[] actual_lpModuleName;
		return false;
	}

	/* Return its image base */
	module._callconv.set_return_value(
		(unsigned int)queried_module->get_image_base()
	);
	delete[] actual_lpModuleName;
	return true;
}

bool KERNEL32::_Internal::IGetProcAddress(KERNEL32 &module)
{
	AddressSpace *const memory = module._environment.get_memory();
	unsigned int arg_hModule;
	unsigned int arg_lpProcName;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hModule);
	module._callconv.get_function_argument(1, arg_lpProcName);

	/* Read the procedure name */
	char *actual_lpProcName;
	if (memory->read_string(arg_lpProcName, actual_lpProcName) !=
			MEMORY_STATUS_OK)
		return false;

	/* Ask the environment for the module */
	Win32Module *queried_module = module._environment.get_module(
		arg_hModule);
	if (!queried_module) {
		Log::error("[KERNEL32] GetModuleHandleA() cannot find module 0x%X\n",
			arg_hModule);
		delete[] actual_lpProcName;
		return false;
	}

	/* Then ask the module */
	memory_address_t address;
	if (!queried_module->get_function_address(actual_lpProcName, address)) {
		Log::error("[KERNEL32] GetModuleHandleA() cannot find %s\n",
			actual_lpProcName);
		delete[] actual_lpProcName;
		return false;
	}

	/* Return the function address */
	module._callconv.set_return_value((unsigned int)address);
	delete[] actual_lpProcName;
	return true;
}

/*
 * HANDLE WINAPI CreateFileA(LPCSTR lpFileName, DWORD dwDesiredAccess,
 *     DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes,
 *     DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes,
 *     HANDLE hTemplateFile);
 */
bool KERNEL32::_Internal::ICreateFileA(KERNEL32 &module)
{
	unsigned int arg_lpFileName;
	unsigned int arg_dwDesiredAccess;
	unsigned int arg_dwShareMode;
	unsigned int arg_lpSecurityAttributes;
	unsigned int arg_dwCreationDisposition;
	unsigned int arg_dwFlagsAndAttributes;
	unsigned int arg_hTemplateFile;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpFileName);
	module._callconv.get_function_argument(1, arg_dwDesiredAccess);
	module._callconv.get_function_argument(2, arg_dwShareMode);
	module._callconv.get_function_argument(3, arg_lpSecurityAttributes);
	module._callconv.get_function_argument(4, arg_dwCreationDisposition);
	module._callconv.get_function_argument(5, arg_dwFlagsAndAttributes);
	module._callconv.get_function_argument(6, arg_hTemplateFile);

	/* Must have filename */
	if (!arg_lpFileName) {
		// FIXME!
		Log::error("kernel32!CreateFileA: no lpFileName\n");
		return false;
	}

	AddressSpace *const memory = module._environment.get_memory();

	/* Copy security attributes */
	SECURITY_ATTRIBUTES secattr, *psecattr = NULL;
	if (arg_lpSecurityAttributes) {
		if (!MEMOP_OK(virtual_memory_read(memory->get_virtmem(),
				arg_lpSecurityAttributes, &secattr, sizeof(secattr))))
			return false;

		/* We don't support security descriptors */
		if (secattr.lpSecurityDescriptor) {
			Log::error("kernel32!CreateFileA: security descriptors not "
				"supported\n");
			return false;
		}

		/* Use it for bInheritHandle */
		psecattr = &secattr;
	}

	/* Read it */
	char *actual_lpFileName;
	if (memory->read_string(arg_lpFileName, actual_lpFileName) !=
			MEMORY_STATUS_OK)
		return false;

	/* Pass to the original */
	HANDLE file_handle = CreateFileA(actual_lpFileName, arg_dwDesiredAccess,
		arg_dwShareMode, psecattr, arg_dwCreationDisposition,
		arg_dwFlagsAndAttributes, (HANDLE)arg_hTemplateFile);

#ifdef DEBUG_KERNEL32
	Log::trace("kernel32!CreateFileA: opened %s with access 0x%X\n",
		actual_lpFileName, arg_dwDesiredAccess);
#endif

	if (file_handle == INVALID_HANDLE_VALUE) {
		last_error = GetLastError();
#ifdef DEBUG_KERNEL32
		Log::trace("kernel32!CreateFileA failed (0x%lX)\n", last_error);
#endif
	} else {
#ifdef DEBUG_KERNEL32
		Log::trace("kernel32!CreateFileA: returning 0x%p\n", file_handle);
#endif
	}

	/* Return the file handle */
	module._callconv.set_return_value((unsigned int)file_handle);
	delete[] actual_lpFileName;
	return true;
}

/*
 * HANDLE WINAPI CreateFileMappingA(HANDLE hFile,
 *     LPSECURITY_ATTRIBUTES lpFileMappingAttributes, DWORD flProtect,
 *     DWORD dwMaximumSizeHigh, DWORD dwMaximumSizeLow, LPCSTR lpName);
 */
bool KERNEL32::_Internal::ICreateFileMappingA(KERNEL32 &module)
{
	unsigned int arg_hFile;
	unsigned int arg_lpFileMappingAttributes;
	unsigned int arg_flProtect;
	unsigned int arg_dwMaximumSizeHigh;
	unsigned int arg_dwMaximumSizeLow;
	unsigned int arg_lpName;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hFile);
	module._callconv.get_function_argument(1, arg_lpFileMappingAttributes);
	module._callconv.get_function_argument(2, arg_flProtect);
	module._callconv.get_function_argument(3, arg_dwMaximumSizeHigh);
	module._callconv.get_function_argument(4, arg_dwMaximumSizeLow);
	module._callconv.get_function_argument(5, arg_lpName);

	/* We don't support security attributes (they have to be copied) */
	if (arg_lpFileMappingAttributes) {
		Log::error("kernel32!CreateFileMappingA: security attributes not "
			"supported\n");
		return false;
	}

	/* We don't want to copy strings */
	if (arg_lpName) {
		Log::error("kernel32!CreateFileMappingA: named mappings are not "
			"supported\n");
		return false;
	}

	/* Pass to the original */
	HANDLE file_handle = CreateFileMappingA((HANDLE)arg_hFile, NULL,
		arg_flProtect, arg_dwMaximumSizeHigh, arg_dwMaximumSizeLow,
		NULL);

	if (!file_handle) {
		// FIXME: handle failure
		Log::error("kernel32!CreateFileMappingA failed!\n");
		return false;
	}

	/* Return the file handle */
#ifdef DEBUG_KERNEL32
	Log::trace("kernel32!CreateFileMappingA: returning 0x%p\n", file_handle);
#endif
	module._callconv.set_return_value((unsigned int)file_handle);
	return true;
}

/*
 * LPVOID WINAPI MapViewOfFileEx(HANDLE hFileMappingObject,
 *     DWORD dwDesiredAccess, DWORD dwFileOffsetHigh, DWORD dwFileOffsetLow,
 *     SIZE_T dwNumberOfBytesToMap, LPVOID lpBaseAddress);
 */
bool KERNEL32::_Internal::IMapViewOfFileEx(KERNEL32 &module,
	bool have_base_address)
{
	unsigned int arg_hFileMappingObject;
	unsigned int arg_dwDesiredAccess;
	unsigned int arg_dwFileOffsetHigh;
	unsigned int arg_dwFileOffsetLow;
	unsigned int arg_dwNumberOfBytesToMap;
	unsigned int arg_lpBaseAddress;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hFileMappingObject);
	module._callconv.get_function_argument(1, arg_dwDesiredAccess);
	module._callconv.get_function_argument(2, arg_dwFileOffsetHigh);
	module._callconv.get_function_argument(3, arg_dwFileOffsetLow);
	module._callconv.get_function_argument(4, arg_dwNumberOfBytesToMap);
	if (have_base_address)
		module._callconv.get_function_argument(5, arg_lpBaseAddress);
	else
		arg_lpBaseAddress = 0;

	/* Pass to the original */
	void *mapped_data = MapViewOfFileEx((HANDLE)arg_hFileMappingObject,
		arg_dwDesiredAccess, arg_dwFileOffsetHigh, arg_dwFileOffsetLow,
		arg_dwNumberOfBytesToMap, (void *)arg_lpBaseAddress);

	if (!mapped_data) {
		// FIXME: handle failure
		Log::error("kernel32!MapViewOfFileEx failed!\n");
		return false;
	}

	/* Query memory info */
	MEMORY_BASIC_INFORMATION mbi;
	VirtualQuery(mapped_data, &mbi, sizeof(mbi));

	/* Translate memory protection flags */
	unsigned int prot;
	switch (mbi.AllocationProtect) {
		case PAGE_NOACCESS:
			prot = 0u;
			break;
		case PAGE_READONLY:
			prot = MEMORY_PROT_READ;
			break;
		case PAGE_READWRITE:
			prot = MEMORY_PROT_READ | MEMORY_PROT_WRITE;
			break;
		case PAGE_EXECUTE:
			prot = MEMORY_PROT_EXECUTE;
			break;
		case PAGE_EXECUTE_READ:
			prot = MEMORY_PROT_EXECUTE | MEMORY_PROT_READ;
			break;
		case PAGE_EXECUTE_READWRITE:
			prot = MEMORY_PROT_EXECUTE | MEMORY_PROT_READ | MEMORY_PROT_WRITE;
			break;
		default:
			Log::error("kernel32!MapViewOfFileEx: unsupported protection "
				"flags 0x%lX\n", mbi.AllocationProtect);
			UnmapViewOfFile(mapped_data);
			return false;
	}

#ifdef DEBUG_KERNEL32
	Log::trace("kernel32!MapViewOfFileEx(): mapping 0x%p of size 0x%lX "
		"bytes\n", mapped_data, mbi.RegionSize);
#endif
	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	/* Identity map the file mapping into memory */
	memory_address_t mapped_data_addr =
		reinterpret_cast<memory_address_t>(mapped_data);

	if (virtual_memory_allocate(memory, mapped_data, &mapped_data_addr,
			mbi.RegionSize, (enum MemoryProtection)prot) < 0) {
		Log::error("kernel32!MapViewOfFileEx: VM map failed\n");
		UnmapViewOfFile(mapped_data);
		return false;
	}

	/* Return the memory address */
	module._callconv.set_return_value((unsigned int)mapped_data);
	return true;
}

/*
 * BOOL WINAPI CloseHandle(HANDLE hObject);
 */
bool KERNEL32::_Internal::ICloseHandle(KERNEL32 &module)
{
	unsigned int arg_hObject;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hObject);

	/*
	 * Just close the handle.
	 * According to MSDN, closing the file mapping object without calling
	 * UnmapViewOfFile() will keep the view open.
	 */
	module._callconv.set_return_value(CloseHandle((HANDLE)arg_hObject));
	return true;
}

/*
 * DWORD WINAPI GetFileSize(HANDLE hFile, LPDWORD lpFileSizeHigh);
 */
bool KERNEL32::_Internal::IGetFileSize(KERNEL32 &module)
{
	unsigned int arg_hFile;
	unsigned int arg_lpFileSizeHigh;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hFile);
	module._callconv.get_function_argument(1, arg_lpFileSizeHigh);

	/* Pass to original function */
	DWORD ret;
	if (!arg_lpFileSizeHigh)
		ret = GetFileSize((HANDLE)arg_hFile, NULL);
	else {
		DWORD high = 0;
		ret = GetFileSize((HANDLE)arg_hFile, &high);

		/* Write it back */
		CPUContext *const cpu = module._environment.get_cpu();
		if (!WRITE_U32_OK(arg_lpFileSizeHigh, high))
			return false;
	}

	/* If we fail, save the last error */
	if (ret == INVALID_FILE_SIZE)
		last_error = GetLastError();

	/* Done */
	module._callconv.set_return_value(ret);
	return true;
}

/*
 * DWORD WINAPI GetModuleFileNameA(HMODULE hModule, LPSTR lpFilename,
 *     DWORD nSize);
 */
bool KERNEL32::_Internal::IGetModuleFileNameA(KERNEL32 &module)
{
	unsigned int arg_hModule;
	unsigned int arg_lpFilename;
	unsigned int arg_nSize;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hModule);
	module._callconv.get_function_argument(1, arg_lpFilename);
	module._callconv.get_function_argument(2, arg_nSize);

	/* Find by image base */
	Win32Module *const requested_module =
		module._environment.get_module(arg_hModule);
	if (!requested_module) {
		Log::warn("kernel32!GetModuleFileNameA: 0x%X not found\n",
			arg_hModule);

		module._callconv.set_return_value(0);
		last_error = 126 /* ERROR_MOD_NOT_FOUND */;
		return true;
	}

	AddressSpace *const memory = module._environment.get_memory();

	/* Real modules have their full path in the PEExecutable structure, copy
	   it directly */
	if (requested_module->is_real_module()) {
		Win32PEModule *const pemod = static_cast<Win32PEModule *>(
			requested_module);

		/* Set return value */
		const char *str = pemod->get_pe_executable()->get_exe_path();
		module._callconv.set_return_value(strlen(str));

		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				arg_lpFilename, str, arg_nSize)))
			return false;
	}

	/* Fake a system32 path */
	else {
		char buffer[260];
		_snprintf(buffer, sizeof(buffer), "C:\\WINNT35\\system32\\%s",
			requested_module->get_module_name());

		/* Set return value */
		module._callconv.set_return_value(strlen(buffer));

		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				arg_lpFilename, buffer, arg_nSize)))
			return false;
	}
	return true;
}

/*
 * HMODULE WINAPI LoadLibraryA(LPCSTR lpFileName);
 */
bool KERNEL32::_Internal::ILoadLibraryA(KERNEL32 &module)
{
	unsigned int arg_lpFileName;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpFileName);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read module name */
	char *actual_lpFileName;
	if (memory->read_string(arg_lpFileName, actual_lpFileName) !=
			MEMORY_STATUS_OK)
		return false;

	/* Pass loading to Win32 environment */
	Win32Module *loaded_module = module._environment.load_module(
		actual_lpFileName);
	if (!loaded_module) {
		Log::warn("kernel32!LoadLibraryA: %s not found\n", actual_lpFileName);

		module._callconv.set_return_value(0);
		last_error = 126 /* ERROR_MOD_NOT_FOUND */;
	} else {
		/* Return the module's image base */
		module._callconv.set_return_value(
			(unsigned int)loaded_module->get_image_base()
		);
	}

	delete[] actual_lpFileName;
	return true;
}

/*
 * LPSTR WINAPI GetCommandLineA(void);
 */
bool KERNEL32::_Internal::IGetCommandLineA(KERNEL32 &module)
{
	/* If not initialized already, read command line and environment */
	if (!memory_cmdline) {
		if (!_create_startup_info(module._environment))
			return false;
	}

	/* Return the pointer */
#ifdef DEBUG_KERNEL32
	Log::trace("kernel32!GetCommandLineA: command line is at 0x%" PRIXMA "\n",
		memory_cmdline);
#endif
	module._callconv.set_return_value((unsigned int)memory_cmdline);
	return true;
}

/*
 * LPTCH WINAPI GetEnvironmentStrings(void);
 */
bool KERNEL32::_Internal::IGetEnvironmentStrings(KERNEL32 &module)
{
	/* If not initialized already, read command line and environment */
	if (!memory_environment) {
		if (!_create_startup_info(module._environment))
			return false;
	}

	/* Return the pointer */
#ifdef DEBUG_KERNEL32
	Log::trace("kernel32!GetEnvironmentStrings: environment is at "
		"0x%" PRIXMA "\n", memory_environment);
#endif
	module._callconv.set_return_value((unsigned int)memory_environment);
	return true;
}

/*
 * DWORD WINAPI GetVersion(void);
 */
bool KERNEL32::_Internal::IGetVersion(KERNEL32 &module)
{
	/* This is NT 3.51 */
	const unsigned int version =
		(0u << 31) |	/* bit 31 is only set on Win9x */
		(1057u << 16) |	/* build number */
		(51u << 8) |	/* minor version */
		3u;				/* major version */

	module._callconv.set_return_value(version);
	return true;
}

/*
 * HANDLE WINAPI GetProcessHeap(void);
 */
bool KERNEL32::_Internal::IGetProcessHeap(KERNEL32 &module)
{
	const memory_address_t process_heap =
		module._environment.get_process_heap();

	module._callconv.set_return_value((unsigned int)process_heap);
	return true;
}

/*
 * void WINAPI InitializeCriticalSection(
 *     LPCRITICAL_SECTION lpCriticalSection);
 * void WINAPI EnterCriticalSection(
 *     LPCRITICAL_SECTION lpCriticalSection);
 * void WINAPI LeaveCriticalSection(
 *     LPCRITICAL_SECTION lpCriticalSection);
 * void WINAPI DeleteCriticalSection(
 *     LPCRITICAL_SECTION lpCriticalSection);
 *
 * NT PowerPC's CRITICAL_SECTION has the same size as x86's (24 bytes),
 * so we can point to it directly.
 */
bool KERNEL32::_Internal::IxxxCriticalSection(KERNEL32 &module,
	unsigned int op)
{
	unsigned int arg_lpCriticalSection;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpCriticalSection);

	AddressSpace *const memory = module._environment.get_memory();

	/* Ask for physical allocation of address */
	struct VirtualMemoryInformation info;
	if (virtual_memory_query(memory->get_virtmem(), arg_lpCriticalSection,
			&info) < 0)
		return false;

	/* Calculate address */
	CRITICAL_SECTION *critsec = reinterpret_cast<CRITICAL_SECTION *>(
		static_cast<char *>(info.host_pointer) +
		(arg_lpCriticalSection - info.base_address)
	);

	/* Run it */
	switch (op) {
		case 0:
			InitializeCriticalSection(critsec);
			break;
		case 1:
			EnterCriticalSection(critsec);
			break;
		case 2:
			LeaveCriticalSection(critsec);
			break;
		case 3:
			DeleteCriticalSection(critsec);
			break;
	}
	return true;
}

/*
 * DWORD WINAPI TlsAlloc(void);
 */
bool KERNEL32::_Internal::ITlsAlloc(KERNEL32 &module)
{
	const DWORD tls_alloc = TlsAlloc();

	if (tls_alloc == TLS_OUT_OF_INDEXES)
		last_error = GetLastError();

	module._callconv.set_return_value(tls_alloc);
	return true;
}

/*
 * void *WINAPI HeapAlloc(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes);
 */
bool KERNEL32::_Internal::IHeapAlloc(KERNEL32 &module)
{
	unsigned int arg_hHeap;
	unsigned int arg_dwFlags;
	unsigned int arg_dwBytes;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hHeap);
	module._callconv.get_function_argument(1, arg_dwFlags);
	module._callconv.get_function_argument(2, arg_dwBytes);

	if (arg_dwFlags & ~HEAP_ZERO_MEMORY) {
		Log::warn("kernel32!HeapAlloc: unsupported flags 0x%X\n",
			arg_dwFlags & ~HEAP_ZERO_MEMORY);
	}

	/* need to zero out memory? */
	bool zero = !!(arg_dwFlags & HEAP_ZERO_MEMORY);

	/* Allocate memory */
	memory_address_t alloc_address;
	if (!ntdll->heap_alloc(arg_hHeap, arg_dwBytes, alloc_address, zero)) {
		Log::error("kernel32!HeapAlloc: heap allocation failed\n");
		return false;
	}

	module._callconv.set_return_value((unsigned int)alloc_address);
	return true;
}

/*
 * BOOL WINAPI TlsSetValue(DWORD dwTlsIndex, void *lpTlsValue);
 */
bool KERNEL32::_Internal::ITlsSetValue(KERNEL32 &module)
{
	unsigned int arg_dwTlsIndex;
	unsigned int arg_lpTlsValue;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_dwTlsIndex);
	module._callconv.get_function_argument(1, arg_lpTlsValue);

	BOOL ret = TlsSetValue(arg_dwTlsIndex, (void *)arg_lpTlsValue);
	if (!ret)
		last_error = GetLastError();

	module._callconv.set_return_value(ret);
	return true;
}

/*
 * DWORD WINAPI GetCurrentThreadId(void);
 */
bool KERNEL32::_Internal::IGetCurrentThreadId(KERNEL32 &module)
{
	const DWORD thread_id = GetCurrentThreadId();
	module._callconv.set_return_value(thread_id);
	return true;
}

/*
 * void WINAPI GetStartupInfoA(LPSTARTUPINFOA lpStartupInfo);
 */
bool KERNEL32::_Internal::IGetStartupInfoA(KERNEL32 &module)
{
	unsigned int arg_lpStartupInfo;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpStartupInfo);

	/* Get our own startup info */
	STARTUPINFOA si;
	GetStartupInfoA(&si);

	/* Write a new structure */
	unsigned char new_si[0x44];
	write_u32le(new_si + 0x00, sizeof(new_si));	// cb
	write_u32le(new_si + 0x04, 0);				// lpReserved
	write_u32le(new_si + 0x08, 0);				// lpDesktop
	write_u32le(new_si + 0x0C, 0);				// lpTitle
	write_u32le(new_si + 0x10, si.dwX);
	write_u32le(new_si + 0x14, si.dwY);
	write_u32le(new_si + 0x18, si.dwXSize);
	write_u32le(new_si + 0x1C, si.dwYSize);
	write_u32le(new_si + 0x20, si.dwXCountChars);
	write_u32le(new_si + 0x24, si.dwYCountChars);
	write_u32le(new_si + 0x28, si.dwFillAttribute);
	write_u32le(new_si + 0x2C, si.dwFlags);
	write_u16le(new_si + 0x30, si.wShowWindow);
	write_u16le(new_si + 0x32, 0);				// cbReserved2
	write_u32le(new_si + 0x34, 0);				// lpReserved2
	write_u32le(new_si + 0x38, (unsigned int)si.hStdInput);
	write_u32le(new_si + 0x3C, (unsigned int)si.hStdOutput);
	write_u32le(new_si + 0x40, (unsigned int)si.hStdError);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	if (!MEMOP_OK(virtual_memory_write(memory, arg_lpStartupInfo, new_si,
			sizeof(new_si))))
		return false;

	return true;
}

/*
 * HANDLE WINAPI GetStdHandle(DWORD nStdHandle);
 */
bool KERNEL32::_Internal::IGetStdHandle(KERNEL32 &module)
{
	unsigned int arg_nStdHandle;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_nStdHandle);

	const HANDLE std_handle = GetStdHandle(arg_nStdHandle);
	module._callconv.set_return_value((unsigned int)std_handle);
	return true;
}

/*
 * DWORD WINAPI GetFileType(HANDLE hFile);
 */
bool KERNEL32::_Internal::IGetFileType(KERNEL32 &module)
{
	unsigned int arg_hFile;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hFile);

	const DWORD file_type = GetFileType((HANDLE)arg_hFile);
	module._callconv.set_return_value(file_type);
	return true;
}

/*
 * int WINAPI MultiByteToWideChar(UINT CodePage, DWORD dwFlags,
 *     LPCCH lpMultiByteStr, int cbMultiByte, LPWSTR lpWideCharStr,
 *     int cchWideChar);
 */
bool KERNEL32::_Internal::IMultiByteToWideChar(KERNEL32 &module)
{
	unsigned int arg_CodePage;
	unsigned int arg_dwFlags;
	unsigned int arg_lpMultiByteStr;
	unsigned int arg_cbMultiByte;
	unsigned int arg_lpWideCharStr;
	unsigned int arg_cchWideChar;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_CodePage);
	module._callconv.get_function_argument(1, arg_dwFlags);
	module._callconv.get_function_argument(2, arg_lpMultiByteStr);
	module._callconv.get_function_argument(3, arg_cbMultiByte);
	module._callconv.get_function_argument(4, arg_lpWideCharStr);
	module._callconv.get_function_argument(5, arg_cchWideChar);

	// FIXME: unsupported combinations
	if ((int)arg_cbMultiByte != -1) {
		Log::error("MultiByteToWideChar cbMultiByte != -1\n");
		return false;
	}
	if (arg_lpWideCharStr != 0) {
		Log::error("MultiByteToWideChar lpWideCharStr != NULL\n");
		return false;
	}
	if (arg_cchWideChar != 0) {
		Log::error("MultiByteToWideChar cchWideChar != 0\n");
		return false;
	}

	AddressSpace *const memory = module._environment.get_memory();

	/* Read the source string */
	char *actual_lpMultiByteStr;
	if (!MEMOP_OK(memory->read_string(arg_lpMultiByteStr,
			actual_lpMultiByteStr)))
		return false;

	/* Pass to original function */
	int ret = MultiByteToWideChar(arg_CodePage, arg_dwFlags,
		actual_lpMultiByteStr, -1, NULL, 0);

	if (ret == 0)
		last_error = GetLastError();

	module._callconv.set_return_value(ret);
	delete[] actual_lpMultiByteStr;
	return true;
}

/*
 * DWORD WINAPI GetTimeZoneInformation(LPTIME_ZONE_INFORMATION
 *     lpTimeZoneInformation);
 */
bool KERNEL32::_Internal::IGetTimeZoneInformation(KERNEL32 &module)
{
	unsigned int arg_lpTimeZoneInformation;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpTimeZoneInformation);

	/* We go first */
	TIME_ZONE_INFORMATION tzi;
	DWORD ret = GetTimeZoneInformation(&tzi);
	module._callconv.set_return_value(ret);

	if (ret == TIME_ZONE_ID_INVALID)
		last_error = GetLastError();
	else {
		struct VirtualMemoryContext *const memory =
			module._environment.get_memory()->get_virtmem();

		/* Copy timezone info back */
		if (!MEMOP_OK(virtual_memory_write(memory, arg_lpTimeZoneInformation,
				&tzi, sizeof(tzi))))
			return false;
	}
	return true;
}

/*
 * void WINAPI GetLocalTime(LPSYSTEMTIME lpSystemTime);
 */
bool KERNEL32::_Internal::IGetLocalTime(KERNEL32 &module)
{
	unsigned int arg_lpSystemTime;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpSystemTime);

	/* Get local time */
	SYSTEMTIME st;
	GetLocalTime(&st);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	/* Copy time info back */
	return MEMOP_OK(virtual_memory_write(memory, arg_lpSystemTime, &st,
		sizeof(st)));
}

/*
 * LPVOID WINAPI TlsGetValue(DWORD dwTlsIndex);
 */
bool KERNEL32::_Internal::ITlsGetValue(KERNEL32 &module)
{
	unsigned int arg_dwTlsIndex;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_dwTlsIndex);

	LPVOID ret = TlsGetValue(arg_dwTlsIndex);
	last_error = GetLastError();

	module._callconv.set_return_value((unsigned int)ret);
	return true;
}

/*
 * SIZE_T WINAPI HeapSize(HANDLE hHeap, DWORD dwFlags, LPCVOID lpMem);
 */
bool KERNEL32::_Internal::IHeapSize(KERNEL32 &module)
{
	unsigned int arg_hHeap;
	unsigned int arg_dwFlags;
	unsigned int arg_lpMem;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hHeap);
	module._callconv.get_function_argument(1, arg_dwFlags);
	module._callconv.get_function_argument(2, arg_lpMem);

	if (arg_dwFlags & ~0u) {
		Log::warn("kernel32!HeapSize: unsupported flags 0x%X\n",
			arg_dwFlags & ~0u);
	}

	/* Query allocation size */
	size_t allocation_size;
	if (!ntdll->heap_size(arg_hHeap, arg_lpMem, allocation_size)) {
		Log::error("kernel32!HeapSize: querying allocation size failed for "
			"0x%X\n", arg_lpMem);
		return false;
	}

	module._callconv.set_return_value((unsigned int)allocation_size);
	return true;
}

/*
 * DWORD WINAPI GetFileAttributesA(LPCSTR lpFileName);
 */
bool KERNEL32::_Internal::IGetFileAttributesA(KERNEL32 &module)
{
	unsigned int arg_lpFileName;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpFileName);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read the filename */
	char *actual_lpFileName;
	if (!MEMOP_OK(memory->read_string(arg_lpFileName, actual_lpFileName)))
		return false;

	/* Pass to original function */
	DWORD ret = GetFileAttributesA(actual_lpFileName);

	if (ret == INVALID_FILE_ATTRIBUTES)
		last_error = GetLastError();

	module._callconv.set_return_value(ret);
	delete[] actual_lpFileName;
	return true;
}

/*
 * DWORD WINAPI GetLastError(void);
 * void WINAPI SetLastError(DWORD dwErrCode);
 */
bool KERNEL32::_Internal::IxxxLastError(KERNEL32 &module, bool set)
{
	if (set) {
		/* Write argument into last error value directly */
		unsigned int v;
		module._callconv.get_function_argument(0, v);
		last_error = v;
	} else {
		/* Return last error value directly */
		module._callconv.set_return_value(last_error);
	}
	return true;
}

/*
 * BOOL WINAPI HeapFree(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem);
 */
bool KERNEL32::_Internal::IHeapFree(KERNEL32 &module)
{
	unsigned int arg_hHeap;
	unsigned int arg_dwFlags;
	unsigned int arg_lpMem;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hHeap);
	module._callconv.get_function_argument(1, arg_dwFlags);
	module._callconv.get_function_argument(2, arg_lpMem);

	if (arg_dwFlags & ~0u) {
		Log::warn("kernel32!HeapFree: unsupported flags 0x%X\n",
			arg_dwFlags & ~0u);
	}

	/* Free allocation */
	if (!ntdll->heap_free(arg_hHeap, arg_lpMem)) {
		Log::error("kernel32!HeapFree: freeing allocation 0x%X failed\n",
			arg_lpMem);
		return false;
	}

	module._callconv.set_return_value(1);
	return true;
}

/*
 * BOOL WINAPI WriteFile(HANDLE hFile, LPCVOID lpBuffer,
 *     DWORD nNumberOfBytesToWrite, LPDWORD lpNumberOfBytesWritten,
 *     LPOVERLAPPED lpOverlapped);
 */
bool KERNEL32::_Internal::IReadWriteFile(KERNEL32 &module, bool write)
{
	unsigned int arg_hFile;
	unsigned int arg_lpBuffer;
	unsigned int arg_nNumberOfBytesToX;
	unsigned int arg_lpNumberOfBytesX;
	unsigned int arg_lpOverlapped;

	const char *const fn = write ? "WriteFile" : "ReadFile";

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hFile);
	module._callconv.get_function_argument(1, arg_lpBuffer);
	module._callconv.get_function_argument(2, arg_nNumberOfBytesToX);
	module._callconv.get_function_argument(3, arg_lpNumberOfBytesX);
	module._callconv.get_function_argument(4, arg_lpOverlapped);

	/* Non-blocking writes are not supported */
	if (arg_lpOverlapped) {
		Log::error("kernel32!%s: non-blocking writes are not "
			"supported\n", fn);
		return false;
	}

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	/* Read existing buffer (write) / Allocate buffer (read) */
	unsigned char *actual_lpBuffer = new unsigned char[arg_nNumberOfBytesToX];
	if (write) {
		if (!MEMOP_OK(virtual_memory_read(memory, arg_lpBuffer,
				actual_lpBuffer, arg_nNumberOfBytesToX)))
			return false;
	}

	/* Read or write buffer */
	DWORD operated_bytes;
	BOOL ret;
	if (write) {
		ret = WriteFile((HANDLE)arg_hFile,
			static_cast<LPCVOID>(actual_lpBuffer), arg_nNumberOfBytesToX,
			&operated_bytes, NULL);
	} else {
		ret = ReadFile((HANDLE)arg_hFile,
			static_cast<LPVOID>(actual_lpBuffer), arg_nNumberOfBytesToX,
			&operated_bytes, NULL);
	}

	/* Save last error */
	if (!ret)
		last_error = GetLastError();

	/* Write number of read/written bytes back */
	CPUContext *const cpu = module._environment.get_cpu();
	if (!WRITE_U32_OK(arg_lpNumberOfBytesX, operated_bytes))
		return false;

	/* Write buffer back */
	if (!write) {
		if (!MEMOP_OK(virtual_memory_write(memory, arg_lpBuffer,
				actual_lpBuffer, arg_nNumberOfBytesToX)))
			return false;
	}

	module._callconv.set_return_value(ret);
	delete[] actual_lpBuffer;
	return true;
}

/*
 * DWORD WINAPI GetCurrentDirectory(DWORD nBufferLength, LPTSTR lpBuffer);
 */
bool KERNEL32::_Internal::IGetCurrentDirectoryA(KERNEL32 &module)
{
	unsigned int arg_nBufferLength;
	unsigned int arg_lpBuffer;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_nBufferLength);
	module._callconv.get_function_argument(1, arg_lpBuffer);

	/* Get current directory */
	char *curdir = arg_lpBuffer ? new char[arg_nBufferLength] : NULL;
	DWORD ret = GetCurrentDirectoryA(arg_nBufferLength, curdir);

	if (ret == 0)
		last_error = GetLastError();
	else if (curdir) {
		const unsigned int writesize = arg_nBufferLength < (ret + 1) ?
			arg_nBufferLength : ret + 1;

		struct VirtualMemoryContext *const memory =
			module._environment.get_memory()->get_virtmem();

		if (!MEMOP_OK(virtual_memory_write(memory, arg_lpBuffer, curdir,
				writesize)))
			return false;
	}

	module._callconv.set_return_value(ret);
	delete[] curdir;
	return true;
}

/*
 * DWORD WINAPI DuplicateHandle(HANDLE hSourceProcessHandle,
 *     HANDLE hSourceHandle, HANDLE hTargetProcessHandle,
 *     LPHANDLE lpTargetHandle, DWORD dwDesiredAccess, BOOL bInheritHandle,
 *     DWORD dwOptions);
 */
bool KERNEL32::_Internal::IDuplicateHandle(KERNEL32 &module)
{
	unsigned int arg_hSourceProcessHandle;
	unsigned int arg_hSourceHandle;
	unsigned int arg_hTargetProcessHandle;
	unsigned int arg_lpTargetHandle;
	unsigned int arg_dwDesiredAccess;
	unsigned int arg_bInheritHandle;
	unsigned int arg_dwOptions;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hSourceProcessHandle);
	module._callconv.get_function_argument(1, arg_hSourceHandle);
	module._callconv.get_function_argument(2, arg_hTargetProcessHandle);
	module._callconv.get_function_argument(3, arg_lpTargetHandle);
	module._callconv.get_function_argument(4, arg_dwDesiredAccess);
	module._callconv.get_function_argument(5, arg_bInheritHandle);
	module._callconv.get_function_argument(6, arg_dwOptions);

	/* Call it */
	HANDLE target_handle = NULL;
	BOOL ret = DuplicateHandle((HANDLE)arg_hSourceProcessHandle,
		(HANDLE)arg_hSourceHandle, (HANDLE)arg_hTargetProcessHandle,
		arg_lpTargetHandle ? &target_handle : NULL, arg_dwDesiredAccess,
		arg_bInheritHandle, arg_dwOptions);

	if (!ret)
		last_error = GetLastError();
	else if (arg_lpTargetHandle) {
		/* Write handle back */
		CPUContext *const cpu = module._environment.get_cpu();
		if (!WRITE_U32_OK(arg_lpTargetHandle, target_handle))
			return false;
	}

	module._callconv.set_return_value(ret);
	return true;
}

/*
 * void WINAPI RtlMoveMemory(PVOID Destination, const VOID *Source,
 *     SIZE_T Length);
 */
bool KERNEL32::_Internal::IRtlMoveMemory(KERNEL32 &module)
{
	unsigned int arg_Destination;
	unsigned int arg_Source;
	unsigned int arg_Length;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_Destination);
	module._callconv.get_function_argument(1, arg_Source);
	module._callconv.get_function_argument(2, arg_Length);

#ifdef DEBUG_KERNEL32
	//Log::trace("RtlMoveMemory(0x%X,0x%X,0x%X)\n", arg_Destination, arg_Source,
	//	arg_Length);
#endif

	/* Use copy functionality of virtmem, it behaves like memmove() */
	AddressSpace *const memory = module._environment.get_memory();
	return MEMOP_OK(virtual_memory_copy(memory->get_virtmem(), arg_Destination,
		arg_Source, arg_Length));
}

/*
 * void WINAPI RtlFillMemory(PVOID Destination, SIZE_T Length,
 *     BYTE Fill);
 */
bool KERNEL32::_Internal::IRtlFillMemory(KERNEL32 &module)
{
	unsigned int arg_Destination;
	unsigned int arg_Length;
	unsigned int arg_Fill;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_Destination);
	module._callconv.get_function_argument(1, arg_Length);
	module._callconv.get_function_argument(2, arg_Fill);
	arg_Fill &= 0xFFu;

#ifdef DEBUG_KERNEL32
	//Log::trace("RtlFillMemory(0x%X,0x%X,0x%X)\n", arg_Destination, arg_Length,
	//	arg_Fill);
#endif

	/* Use copy functionality of virtmem, it behaves like memmove() */
	AddressSpace *const memory = module._environment.get_memory();
	return MEMOP_OK(virtual_memory_set(memory->get_virtmem(), arg_Destination,
		arg_Fill, arg_Length));
}

/*
 * HANDLE WINAPI FindFirstFileA(LPCSTR lpFileName,
 *     LPWIN32_FIND_DATAA lpFindFileData);
 */
bool KERNEL32::_Internal::IFindFirstFileA(KERNEL32 &module)
{
	unsigned int arg_lpFileName;
	unsigned int arg_lpFindFileData;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpFileName);
	module._callconv.get_function_argument(1, arg_lpFindFileData);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read the filename */
	char *actual_lpFileName;
	if (!MEMOP_OK(memory->read_string(arg_lpFileName, actual_lpFileName)))
		return false;

	/* Call the function */
	WIN32_FIND_DATAA finddata;
	HANDLE ret = FindFirstFileA(actual_lpFileName, &finddata);
	delete[] actual_lpFileName;

	if (ret == INVALID_HANDLE_VALUE)
		last_error = GetLastError();
	else {
		if (!MEMOP_OK(virtual_memory_write(memory->get_virtmem(),
				arg_lpFindFileData, &finddata, sizeof(finddata))))
			return false;
	}

	module._callconv.set_return_value((unsigned int)ret);
	return true;
}

/*
 * BOOL WINAPI FileTimeToSystemTime(const FILETIME *lpFileTime,
 *     LPSYSTEMTIME lpSystemTime);
 */
bool KERNEL32::_Internal::IFileTimeToSystemTime(KERNEL32 &module)
{
	unsigned int arg_lpFileTime;
	unsigned int arg_lpSystemTime;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpFileTime);
	module._callconv.get_function_argument(1, arg_lpSystemTime);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	/* Read file time */
	FILETIME filetime;
	if (!MEMOP_OK(virtual_memory_read(memory, arg_lpFileTime, &filetime,
			sizeof(filetime))))
		return false;

	/* Convert to system time */
	SYSTEMTIME systime;
	BOOL ret = FileTimeToSystemTime(&filetime, &systime);

	if (!ret)
		last_error = GetLastError();
	else {
		/* Write system time */
		if (!MEMOP_OK(virtual_memory_write(memory, arg_lpSystemTime, &systime,
				sizeof(systime))))
			return false;
	}

	module._callconv.set_return_value((unsigned int)ret);
	return true;
}

/*
 * BOOL WINAPI FindClose(HANDLE hFindFile);
 */
bool KERNEL32::_Internal::IFindClose(KERNEL32 &module)
{
	unsigned int arg_hFindFile;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hFindFile);

	/* Close find handle */
	BOOL ret = FindClose((HANDLE)arg_hFindFile);

	if (!ret)
		last_error = GetLastError();

	module._callconv.set_return_value((unsigned int)ret);
	return true;
}

/*
 * DWORD WINAPI SetFilePointer(HANDLE hFile, LONG lDistanceToMove,
 *     PLONG lpDistanceToMoveHigh, DWORD dwMoveMethod);
 */
bool KERNEL32::_Internal::ISetFilePointer(KERNEL32 &module)
{
	unsigned int arg_hFile;
	unsigned int arg_lDistanceToMove;
	unsigned int arg_lpDistanceToMoveHigh;
	unsigned int arg_dwMoveMethod;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hFile);
	module._callconv.get_function_argument(1, arg_lDistanceToMove);
	module._callconv.get_function_argument(2, arg_lpDistanceToMoveHigh);
	module._callconv.get_function_argument(3, arg_dwMoveMethod);

	/* Read high-dword of distance if present */
	LONG distance_high, *distance_highp = NULL;
	if (arg_lpDistanceToMoveHigh) {
		CPUContext *const cpu = module._environment.get_cpu();
		if (!READ_U32_OK(arg_lpDistanceToMoveHigh,
				reinterpret_cast<unsigned int *>(&distance_high)))
			return false;
		distance_highp = &distance_high;
	}

	/* Call the original function */
	DWORD ret = SetFilePointer((HANDLE)arg_hFile, arg_lDistanceToMove,
		distance_highp, arg_dwMoveMethod);

	if (ret == INVALID_SET_FILE_POINTER)
		last_error = GetLastError();
	else if (arg_lpDistanceToMoveHigh) {
		/* Write high-dword back */
		CPUContext *const cpu = module._environment.get_cpu();
		if (!WRITE_U32_OK(arg_lpDistanceToMoveHigh, distance_high))
			return false;
	}

	module._callconv.set_return_value((unsigned int)ret);
	return true;
}

/*
 * BOOL WINAPI UnmapViewOfFile(LPCVOID lpBaseAddress);
 */
bool KERNEL32::_Internal::IUnmapViewOfFile(KERNEL32 &module)
{
	unsigned int arg_lpBaseAddress;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpBaseAddress);

	AddressSpace *const memory = module._environment.get_memory();

	/* Query virtual memory on this mapping */
	// FIXME: really return false?
	struct VirtualMemoryInformation vmi;
	if (virtual_memory_query(memory->get_virtmem(), arg_lpBaseAddress,
			&vmi) < 0) {
		Log::error("VM query for 0x%X failed\n", arg_lpBaseAddress);
		return false;
	}

	/* Do the actual unmapping */
	BOOL ret = UnmapViewOfFile(reinterpret_cast<LPCVOID>(arg_lpBaseAddress));

	if (!ret)
		last_error = GetLastError();
	else {
		/* Unmap the mapping from virtual memory, addresses are now invalid */
		if (virtual_memory_free(memory->get_virtmem(),
				arg_lpBaseAddress) < 0) {
			Log::error("Unmapping 0x%X failed\n", arg_lpBaseAddress);
			return false;
		}
	}

	module._callconv.set_return_value(ret);
	return true;
}

/*
 * BOOL WINAPI SetEndOfFile(HANDLE hFile);
 */
bool KERNEL32::_Internal::ISetEndOfFile(KERNEL32 &module)
{
	unsigned int arg_hFile;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hFile);

	/* Close find handle */
	BOOL ret = SetEndOfFile((HANDLE)arg_hFile);

	if (!ret)
		last_error = GetLastError();

	module._callconv.set_return_value((unsigned int)ret);
	return true;
}

/*
 * BOOL WINAPI DeleteFileA(LPCSTR lpFileName);
 */
bool KERNEL32::_Internal::IDeleteFileA(KERNEL32 &module)
{
	unsigned int arg_lpFileName;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpFileName);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read file name to delete */
	char *actual_lpFileName;
	if (memory->read_string(arg_lpFileName, actual_lpFileName) !=
			MEMORY_STATUS_OK)
		return false;

	/* Delete file */
	BOOL ret = DeleteFileA(actual_lpFileName);

	if (!ret)
		last_error = GetLastError();

	module._callconv.set_return_value(ret);
	delete[] actual_lpFileName;
	return true;
}

/*
 * BOOL WINAPI HeapDestroy(HANDLE hHeap);
 */
bool KERNEL32::_Internal::IHeapDestroy(KERNEL32 &module)
{
	unsigned int arg_hHeap;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hHeap);

	/* Delete heap */
	if (!ntdll->heap_destroy(arg_hHeap)) {
		Log::error("kernel32!HeapDestroy: freeing heap 0x%X failed\n",
			arg_hHeap);
		return false;
	}

	module._callconv.set_return_value(1);
	return true;
}

/*
 * void WINAPI ExitProcess(UINT uExitCode);
 */
bool KERNEL32::_Internal::IExitProcess(KERNEL32 &module)
{
	unsigned int arg_uExitCode;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_uExitCode);

	/* Report exit code to environment */
	module._environment.set_process_exit_code(arg_uExitCode);

	/* Manipulate the instruction pointer so the environment knows that this is
	   a clean shutdown. Will be incremented later, so subtract 4. */
	CPUContext *const cpu = module._environment.get_cpu();
	cpu_set_register(cpu, PPC_REGISTER_PC, 0x55A855A8u - 4u);
	return true;
}

/*
 * void *WINAPI HeapReAlloc(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes);
 */
bool KERNEL32::_Internal::IHeapReAlloc(KERNEL32 &module)
{
	unsigned int arg_hHeap;
	unsigned int arg_dwFlags;
	unsigned int arg_lpMem;
	unsigned int arg_dwBytes;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hHeap);
	module._callconv.get_function_argument(1, arg_dwFlags);
	module._callconv.get_function_argument(2, arg_lpMem);
	module._callconv.get_function_argument(3, arg_dwBytes);

	if (arg_dwFlags & ~0) {
		Log::warn("kernel32!HeapReAlloc: unsupported flags 0x%X\n",
			arg_dwFlags & ~0);
	}

	/* Allocate memory */
	memory_address_t alloc_address = arg_lpMem;
	if (!ntdll->heap_realloc(arg_hHeap, arg_dwBytes, alloc_address)) {
		Log::error("kernel32!HeapReAlloc: heap reallocation failed\n");
		return false;
	}

	module._callconv.set_return_value((unsigned int)alloc_address);
	return true;
}

/*
 * void WINAPI GetSystemTime(LPSYSTEMTIME lpSystemTime);
 */
bool KERNEL32::_Internal::IGetSystemTime(KERNEL32 &module)
{
	unsigned int arg_lpSystemTime;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpSystemTime);

	/* Get system time */
	SYSTEMTIME st;
	GetSystemTime(&st);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	/* Copy time info back */
	return MEMOP_OK(virtual_memory_write(memory, arg_lpSystemTime, &st,
		sizeof(st)));
}

/*
 * BOOL WINAPI SetStdHandle(DWORD nStdHandle, HANDLE hHandle);
 */
bool KERNEL32::_Internal::ISetStdHandle(KERNEL32 &module)
{
	unsigned int arg_nStdHandle;
	unsigned int arg_nHandle;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_nStdHandle);
	module._callconv.get_function_argument(1, arg_nHandle);

	BOOL ret = SetStdHandle(arg_nStdHandle, (HANDLE)arg_nHandle);
	if (!ret)
		last_error = GetLastError();

	module._callconv.set_return_value(ret);
	return true;
}

/*
 * BOOL WINAPI CreateProcessA(LPCSTR lpApplicationName, LPSTR lpCommandLine,
 *     LPSECURITY_ATTRIBUTES lpProcessAttributes,
 *     LPSECURITY_ATTRIBUTES lpThreadAttributes, BOOL bInheritHandles,
 *     DWORD dwCreationFlags, LPVOID lpEnvironment, LPCSTR lpCurrentDirectory,
 *     LPSTARTUPINFOA lpStartupInfo, LPPROCESS_INFORMATION lpProcessInformation
 * );
 */
bool KERNEL32::_Internal::ICreateProcessA(KERNEL32 &module)
{
	bool function_ret = false;
	unsigned int arg_lpApplicationName;
	unsigned int arg_lpCommandLine;
	unsigned int arg_lpProcessAttributes;
	unsigned int arg_lpThreadAttributes;
	unsigned int arg_bInheritHandles;
	unsigned int arg_dwCreationFlags;
	unsigned int arg_lpEnvironment;
	unsigned int arg_lpCurrentDirectory;
	unsigned int arg_lpStartupInfo;
	unsigned int arg_lpProcessInformation;
	char *actual_lpApplicationName = NULL;
	char *actual_lpCommandLine = NULL;
	uint8_t *actual_lpEnvironment = NULL;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_lpApplicationName);
	module._callconv.get_function_argument(1, arg_lpCommandLine);
	module._callconv.get_function_argument(2, arg_lpProcessAttributes);
	module._callconv.get_function_argument(3, arg_lpThreadAttributes);
	module._callconv.get_function_argument(4, arg_bInheritHandles);
	module._callconv.get_function_argument(5, arg_dwCreationFlags);
	module._callconv.get_function_argument(6, arg_lpEnvironment);
	module._callconv.get_function_argument(7, arg_lpCurrentDirectory);
	module._callconv.get_function_argument(8, arg_lpStartupInfo);
	module._callconv.get_function_argument(9, arg_lpProcessInformation);

	AddressSpace *const memory = module._environment.get_memory();

	// FIXME: only this case is supported
	if (!arg_lpApplicationName || !arg_lpCommandLine) {
		Log::error("kernel32!CreateProcessA: application name and command "
			"line are required\n");
		return false;
	}

	/* No security attributes are supported */
	if (arg_lpProcessAttributes || arg_lpThreadAttributes) {
		Log::error("kernel32!CreateProcessA: security attributes are "
			"not supported\n");
		return false;
	}

	if (arg_lpEnvironment) {
		/* Unicode lpEnvironment is not supported */
		if (arg_dwCreationFlags & CREATE_UNICODE_ENVIRONMENT) {
			Log::error("kernel32!CreateProcessA: Unicode lpEnvironment is "
				"not supported\n");
			return false;
		}

		/*
		 * Determine size of environment
		 */
		size_t envvar_size, environment_size = 1u;
		memory_address_t envvar;

		envvar = arg_lpEnvironment;
		do {
			if (!MEMOP_OK(virtual_memory_read_string(memory->get_virtmem(),
					envvar, NULL, &envvar_size)))
				return false;
			environment_size += envvar_size;
			envvar += envvar_size;
		} while (envvar_size > 1);

		/*
		 * Allocate memory and copy strings
		 */
		actual_lpEnvironment = new uint8_t[environment_size];

		char *envvarp = (char *)actual_lpEnvironment;
		envvar = arg_lpEnvironment;
		do {
			if (!MEMOP_OK(virtual_memory_read_string(memory->get_virtmem(),
					envvar, envvarp, &envvar_size)))
				return false;
			envvar += envvar_size;
			envvarp += envvar_size;
		} while (envvar_size > 1);
	}

	/* lpCurrentDirectory is not supported */
	if (arg_lpCurrentDirectory) {
		Log::error("kernel32!CreateProcessA: lpCurrentDirectory is "
			"not supported\n");
		return false;
	}

	/* Read application name */
	if (arg_lpApplicationName) {
		if (!MEMOP_OK(memory->read_string(arg_lpApplicationName,
				actual_lpApplicationName)))
			goto cleanup;
	}

	/* Read command line */
	if (arg_lpCommandLine) {
		if (!MEMOP_OK(memory->read_string(arg_lpCommandLine,
				actual_lpCommandLine)))
			goto cleanup;
	}

	do {
		/* Read startup info */
		STARTUPINFOA si_ansi;
		if (!MEMOP_OK(virtual_memory_read(memory->get_virtmem(),
				arg_lpStartupInfo, &si_ansi, sizeof(si_ansi))))
			break;

		/* Strings in startup info are not supported */
		if (si_ansi.lpReserved || si_ansi.lpDesktop || si_ansi.lpTitle) {
			Log::error("kernel32!CreateProcessA: strings in STARTUPINFO are "
				"not supported\n");
			break;
		}

		/* Convert to Unicode */
		STARTUPINFOW si = {
			sizeof(STARTUPINFOW),
			NULL,
			NULL,
			NULL,
			si_ansi.dwX,
			si_ansi.dwY,
			si_ansi.dwXSize,
			si_ansi.dwYSize,
			si_ansi.dwXCountChars,
			si_ansi.dwYCountChars,
			si_ansi.dwFillAttribute,
			si_ansi.dwFlags,
			si_ansi.wShowWindow,
			si_ansi.cbReserved2,
			si_ansi.lpReserved2,
			si_ansi.hStdInput,
			si_ansi.hStdOutput,
			si_ansi.hStdError
		};

		/* Get path to the loader */
		WCHAR loader_path[MAX_PATH];
		GetModuleFileNameW(NULL, loader_path, MAX_PATH);

		/* Create command line */
		const size_t cmdline_chars = wcslen(loader_path) +
			8 /* _--app=" */ + strlen(actual_lpApplicationName) +
			5 /* "_--_ */ + strlen(actual_lpCommandLine) + 1;

		WCHAR *loader_cmdline = new WCHAR[cmdline_chars];
		tools_swprintf(loader_cmdline, cmdline_chars,
			L"%s --app=\"%S\" -- %S", loader_path,
			actual_lpApplicationName, actual_lpCommandLine);

		/* Create the process */
		PROCESS_INFORMATION pi;
		BOOL ret = CreateProcessW(loader_path, loader_cmdline, NULL, NULL,
			!!arg_bInheritHandles, arg_dwCreationFlags, actual_lpEnvironment,
			NULL, &si, &pi);
		module._callconv.set_return_value(ret);
		delete[] loader_cmdline;

		if (!ret) {
			last_error = GetLastError();
			Log::error("kernel32!CreateProcessA: creating loader process for "
				"%s failed (%lu)\n", actual_lpApplicationName, last_error);
		} else {
			/* Write process information back to caller */
			if (!MEMOP_OK(virtual_memory_write(memory->get_virtmem(),
					arg_lpProcessInformation, &pi, sizeof(pi)))) {
				CloseHandle(pi.hProcess);
				CloseHandle(pi.hThread);
				break;
			}
		}

		function_ret = true;
	} while (0);

cleanup:
	delete[] actual_lpEnvironment;
	delete[] actual_lpCommandLine;
	delete[] actual_lpApplicationName;
	return function_ret;
}

/*
 * DWORD WINAPI WaitForSingleObject(HANDLE hHandle, DWORD dwMilliseconds);
 */
bool KERNEL32::_Internal::IWaitForSingleObject(KERNEL32 &module)
{
	unsigned int arg_hHandle;
	unsigned int arg_dwMilliseconds;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hHandle);
	module._callconv.get_function_argument(1, arg_dwMilliseconds);

	/* Call the function */
	DWORD ret = WaitForSingleObject((HANDLE)arg_hHandle, arg_dwMilliseconds);
	if (ret == WAIT_FAILED)
		last_error = GetLastError();

	module._callconv.set_return_value(ret);
	return true;
}

/*
 * BOOL WINAPI GetExitCodeProcess(HANDLE hHandle, LPDWORD lpExitCode);
 */
bool KERNEL32::_Internal::IGetExitCodeProcess(KERNEL32 &module)
{
	unsigned int arg_hHandle;
	unsigned int arg_lpExitCode;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hHandle);
	module._callconv.get_function_argument(1, arg_lpExitCode);

	/* Call the function */
	DWORD exit_code;
	BOOL ret = GetExitCodeProcess((HANDLE)arg_hHandle, &exit_code);

	if (!ret)
		last_error = GetLastError();
	else {
		/* Write exit code back */
		CPUContext *const cpu = module._environment.get_cpu();
		if (!WRITE_U32_OK(arg_lpExitCode, exit_code))
			return false;
	}

	module._callconv.set_return_value(ret);
	return true;
}

/*
 * DWORD WINAPI GetCurrentProcessId(void);
 */
bool KERNEL32::_Internal::IGetCurrentProcessId(KERNEL32 &module)
{
	const DWORD process_id = GetCurrentProcessId();
	module._callconv.set_return_value(process_id);
	return true;
}

/*
 * BOOL WINAPI GetConsoleScreenBufferInfo(HANDLE hConsoleOutput,
 *     PCONSOLE_SCREEN_BUFFER_INFO lpConsoleScreenBufferInfo);
 */
bool KERNEL32::_Internal::IGetConsoleScreenBufferInfo(KERNEL32 &module)
{
	unsigned int arg_hConsoleOutput;
	unsigned int arg_lpConsoleScreenBufferInfo;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hConsoleOutput);
	module._callconv.get_function_argument(1, arg_lpConsoleScreenBufferInfo);

	/* Call the function */
	CONSOLE_SCREEN_BUFFER_INFO console_screen_buffer;
	BOOL ret = GetConsoleScreenBufferInfo((HANDLE)arg_hConsoleOutput,
		&console_screen_buffer);

	if (!ret)
		last_error = GetLastError();
	else {
		/* Copy info to caller */
		AddressSpace *const memory = module._environment.get_memory();
		if (!MEMOP_OK(virtual_memory_write(memory->get_virtmem(),
				arg_lpConsoleScreenBufferInfo, &console_screen_buffer,
				sizeof(console_screen_buffer))))
			return false;
	}

	module._callconv.set_return_value(ret);
	return true;
}

/*
 * HANDLE WINAPI GetCurrentProcess(void);
 */
bool KERNEL32::_Internal::IGetCurrentProcess(KERNEL32 &module)
{
	HANDLE ret = GetCurrentProcess();
	module._callconv.set_return_value((unsigned int)ret);
	return true;
}

/*
 * UINT WINAPI GetConsoleCP(void);
 */
bool KERNEL32::_Internal::IGetConsoleCP(KERNEL32 &module)
{
	UINT ret = GetConsoleCP();
	module._callconv.set_return_value(ret);
	return true;
}

/*
 * BOOL WINAPI GetCPInfo(UINT CodePage, LPCPINFO lpCPInfo);
 */
bool KERNEL32::_Internal::IGetCPInfo(KERNEL32 &module)
{
	unsigned int arg_CodePage;
	unsigned int arg_lpCPInfo;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_CodePage);
	module._callconv.get_function_argument(1, arg_lpCPInfo);

	/* Call the function */
	CPINFO cpi;
	BOOL ret = GetCPInfo(arg_CodePage, &cpi);

	if (!ret)
		last_error = GetLastError();
	else {
		/* Copy info to caller */
		AddressSpace *const memory = module._environment.get_memory();
		if (!MEMOP_OK(virtual_memory_write(memory->get_virtmem(),
				arg_lpCPInfo, &cpi, sizeof(cpi))))
			return false;
	}

	module._callconv.set_return_value(ret);
	return true;
}

/*
 * UINT WINAPI SetErrorMode(UINT uMode);
 */
bool KERNEL32::_Internal::ISetErrorMode(KERNEL32 &module)
{
	unsigned int arg_uMode;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_uMode);

	/* Call the function */
	UINT ret = SetErrorMode(arg_uMode);

	module._callconv.set_return_value(ret);
	return true;
}


/*
 * KERNEL32 implementation
 */

KERNEL32::KERNEL32(CallingConventionNTPowerPC &callconv,
		Win32Environment &environment) :
	Win32Module(environment, "kernel32.dll", 0x7E010000u),
	_callconv(callconv)
{
	/* Initialize internal structure */
	_internal = new _Internal(environment);

	/* Assign addresses to virtual memory allocations */
	_internal->memory_cmdline_environment = _image_base;

	/* Register functions */
	callconv.allocate_syscall(0x900, _internal->FHeapCreate);
	callconv.allocate_syscall(0x901, _internal->FSetConsoleCtrlHandler);
	callconv.allocate_syscall(0x902, _internal->FGetModuleHandleA);
	callconv.allocate_syscall(0x903, _internal->FGetProcAddress);
	callconv.allocate_syscall(0x904, _internal->FCreateFileA);
	callconv.allocate_syscall(0x905, _internal->FCreateFileMappingA);
	callconv.allocate_syscall(0x906, _internal->FMapViewOfFileEx);
	callconv.allocate_syscall(0x907, _internal->FCloseHandle);
	callconv.allocate_syscall(0x908, _internal->FGetFileSize);
	callconv.allocate_syscall(0x909, _internal->FGetModuleFileNameA);
	callconv.allocate_syscall(0x90A, _internal->FLoadLibraryA);
	callconv.allocate_syscall(0x90B, _internal->FGetCommandLineA);
	callconv.allocate_syscall(0x90C, _internal->FGetEnvironmentStrings);
	callconv.allocate_syscall(0x90D, _internal->FGetVersion);
	callconv.allocate_syscall(0x90E, _internal->FGetProcessHeap);
	callconv.allocate_syscall(0x90F, _internal->FInitializeCriticalSection);
	callconv.allocate_syscall(0x910, _internal->FEnterCriticalSection);
	callconv.allocate_syscall(0x911, _internal->FLeaveCriticalSection);
	callconv.allocate_syscall(0x912, _internal->FDeleteCriticalSection);
	callconv.allocate_syscall(0x913, _internal->FTlsAlloc);
	callconv.allocate_syscall(0x914, _internal->FHeapAlloc);
	callconv.allocate_syscall(0x915, _internal->FTlsSetValue);
	callconv.allocate_syscall(0x916, _internal->FGetCurrentThreadId);
	callconv.allocate_syscall(0x917, _internal->FGetStartupInfoA);
	callconv.allocate_syscall(0x918, _internal->FGetStdHandle);
	callconv.allocate_syscall(0x919, _internal->FGetFileType);
	callconv.allocate_syscall(0x91A, _internal->FMultiByteToWideChar);
	callconv.allocate_syscall(0x91B, _internal->FGetTimeZoneInformation);
	callconv.allocate_syscall(0x91C, _internal->FGetLocalTime);
	callconv.allocate_syscall(0x91D, _internal->FTlsGetValue);
	callconv.allocate_syscall(0x91E, _internal->FHeapSize);
	callconv.allocate_syscall(0x91F, _internal->FGetFileAttributesA);
	callconv.allocate_syscall(0x920, _internal->FGetLastError);
	callconv.allocate_syscall(0x921, _internal->FSetLastError);
	callconv.allocate_syscall(0x922, _internal->FHeapFree);
	callconv.allocate_syscall(0x923, _internal->FWriteFile);
	callconv.allocate_syscall(0x924, _internal->FGetCurrentDirectoryA);
	callconv.allocate_syscall(0x925, _internal->FDuplicateHandle);
	callconv.allocate_syscall(0x926, _internal->FRtlMoveMemory);
	callconv.allocate_syscall(0x927, _internal->FRtlFillMemory);
	callconv.allocate_syscall(0x928, _internal->FFindFirstFileA);
	callconv.allocate_syscall(0x929, _internal->FFileTimeToSystemTime);
	callconv.allocate_syscall(0x92A, _internal->FFindClose);
	callconv.allocate_syscall(0x92B, _internal->FSetFilePointer);
	callconv.allocate_syscall(0x92C, _internal->FReadFile);
	callconv.allocate_syscall(0x92D, _internal->FUnmapViewOfFile);
	callconv.allocate_syscall(0x92E, _internal->FSetEndOfFile);
	callconv.allocate_syscall(0x92F, _internal->FDeleteFileA);
	callconv.allocate_syscall(0x930, _internal->FHeapDestroy);
	callconv.allocate_syscall(0x931, _internal->FExitProcess);
	callconv.allocate_syscall(0x932, _internal->FHeapReAlloc);
	callconv.allocate_syscall(0x933, _internal->FGetSystemTime);
	callconv.allocate_syscall(0x934, _internal->FSetStdHandle);
	callconv.allocate_syscall(0x935, _internal->FCreateProcessA);
	callconv.allocate_syscall(0x936, _internal->FWaitForSingleObject);
	callconv.allocate_syscall(0x937, _internal->FGetExitCodeProcess);
	callconv.allocate_syscall(0x938, _internal->FGetCurrentProcessId);
	callconv.allocate_syscall(0x939, _internal->FGetConsoleScreenBufferInfo);
	callconv.allocate_syscall(0x93A, _internal->FGetCurrentProcess);
	callconv.allocate_syscall(0x93B, _internal->FGetConsoleCP);
	callconv.allocate_syscall(0x93C, _internal->FGetCPInfo);
	callconv.allocate_syscall(0x93D, _internal->FSetErrorMode);
	callconv.allocate_syscall(0x93E, _internal->FMapViewOfFile);

#ifdef DEBUG_KERNEL32
	Log::trace("[KERNEL32] loaded\n");
#endif
}

KERNEL32::~KERNEL32(void)
{
#ifdef DEBUG_KERNEL32
	Log::trace("[KERNEL32] unloaded\n");
#endif

	/* Delete internal structure */
	delete _internal;
}

bool KERNEL32::get_function_address(const char *function_name,
	memory_address_t &function_address) const
{
	/* The Visual C++ linker tries to detect Phar Lap's TNT emulation layer,
	   we don't offer it. */
	if (!strcmp(function_name, "IsTNT")) {
		function_address = 0;
		return true;
	}

	/* Standard kernel32 functions */
	if (!strcmp(function_name, "HeapCreate")) {
		function_address = _internal->FHeapCreate;
		return true;
	} else if (!strcmp(function_name, "SetConsoleCtrlHandler")) {
		function_address = _internal->FSetConsoleCtrlHandler;
		return true;
	} else if (!strcmp(function_name, "GetModuleHandleA")) {
		function_address = _internal->FGetModuleHandleA;
		return true;
	} else if (!strcmp(function_name, "GetProcAddress")) {
		function_address = _internal->FGetProcAddress;
		return true;
	} else if (!strcmp(function_name, "CreateFileA")) {
		function_address = _internal->FCreateFileA;
		return true;
	} else if (!strcmp(function_name, "CreateFileMappingA")) {
		function_address = _internal->FCreateFileMappingA;
		return true;
	} else if (!strcmp(function_name, "MapViewOfFileEx")) {
		function_address = _internal->FMapViewOfFileEx;
		return true;
	} else if (!strcmp(function_name, "CloseHandle")) {
		function_address = _internal->FCloseHandle;
		return true;
	} else if (!strcmp(function_name, "GetFileSize")) {
		function_address = _internal->FGetFileSize;
		return true;
	} else if (!strcmp(function_name, "GetModuleFileNameA")) {
		function_address = _internal->FGetModuleFileNameA;
		return true;
	} else if (!strcmp(function_name, "LoadLibraryA")) {
		function_address = _internal->FLoadLibraryA;
		return true;
	} else if (!strcmp(function_name, "GetCommandLineA")) {
		function_address = _internal->FGetCommandLineA;
		return true;
	} else if (!strcmp(function_name, "GetEnvironmentStrings")) {
		function_address = _internal->FGetEnvironmentStrings;
		return true;
	} else if (!strcmp(function_name, "GetVersion")) {
		function_address = _internal->FGetVersion;
		return true;
	} else if (!strcmp(function_name, "GetProcessHeap")) {
		function_address = _internal->FGetProcessHeap;
		return true;
	} else if (!strcmp(function_name, "InitializeCriticalSection")) {
		function_address = _internal->FInitializeCriticalSection;
		return true;
	} else if (!strcmp(function_name, "EnterCriticalSection")) {
		function_address = _internal->FEnterCriticalSection;
		return true;
	} else if (!strcmp(function_name, "LeaveCriticalSection")) {
		function_address = _internal->FLeaveCriticalSection;
		return true;
	} else if (!strcmp(function_name, "DeleteCriticalSection")) {
		function_address = _internal->FDeleteCriticalSection;
		return true;
	} else if (!strcmp(function_name, "TlsAlloc")) {
		function_address = _internal->FTlsAlloc;
		return true;
	} else if (!strcmp(function_name, "HeapAlloc")) {
		function_address = _internal->FHeapAlloc;
		return true;
	} else if (!strcmp(function_name, "TlsSetValue")) {
		function_address = _internal->FTlsSetValue;
		return true;
	} else if (!strcmp(function_name, "GetCurrentThreadId")) {
		function_address = _internal->FGetCurrentThreadId;
		return true;
	} else if (!strcmp(function_name, "GetStartupInfoA")) {
		function_address = _internal->FGetStartupInfoA;
		return true;
	} else if (!strcmp(function_name, "GetStdHandle")) {
		function_address = _internal->FGetStdHandle;
		return true;
	} else if (!strcmp(function_name, "GetFileType")) {
		function_address = _internal->FGetFileType;
		return true;
	} else if (!strcmp(function_name, "MultiByteToWideChar")) {
		function_address = _internal->FMultiByteToWideChar;
		return true;
	} else if (!strcmp(function_name, "GetTimeZoneInformation")) {
		function_address = _internal->FGetTimeZoneInformation;
		return true;
	} else if (!strcmp(function_name, "GetLocalTime")) {
		function_address = _internal->FGetLocalTime;
		return true;
	} else if (!strcmp(function_name, "TlsGetValue")) {
		function_address = _internal->FTlsGetValue;
		return true;
	} else if (!strcmp(function_name, "HeapSize")) {
		function_address = _internal->FHeapSize;
		return true;
	} else if (!strcmp(function_name, "GetFileAttributesA")) {
		function_address = _internal->FGetFileAttributesA;
		return true;
	} else if (!strcmp(function_name, "GetLastError")) {
		function_address = _internal->FGetLastError;
		return true;
	} else if (!strcmp(function_name, "SetLastError")) {
		function_address = _internal->FSetLastError;
		return true;
	} else if (!strcmp(function_name, "HeapFree")) {
		function_address = _internal->FHeapFree;
		return true;
	} else if (!strcmp(function_name, "WriteFile")) {
		function_address = _internal->FWriteFile;
		return true;
	} else if (!strcmp(function_name, "GetCurrentDirectoryA")) {
		function_address = _internal->FGetCurrentDirectoryA;
		return true;
	} else if (!strcmp(function_name, "DuplicateHandle")) {
		function_address = _internal->FDuplicateHandle;
		return true;
	} else if (!strcmp(function_name, "RtlMoveMemory")) {
		function_address = _internal->FRtlMoveMemory;
		return true;
	} else if (!strcmp(function_name, "RtlFillMemory")) {
		function_address = _internal->FRtlFillMemory;
		return true;
	} else if (!strcmp(function_name, "FindFirstFileA")) {
		function_address = _internal->FFindFirstFileA;
		return true;
	} else if (!strcmp(function_name, "FileTimeToSystemTime")) {
		function_address = _internal->FFileTimeToSystemTime;
		return true;
	} else if (!strcmp(function_name, "FindClose")) {
		function_address = _internal->FFindClose;
		return true;
	} else if (!strcmp(function_name, "SetFilePointer")) {
		function_address = _internal->FSetFilePointer;
		return true;
	} else if (!strcmp(function_name, "ReadFile")) {
		function_address = _internal->FReadFile;
		return true;
	} else if (!strcmp(function_name, "UnmapViewOfFile")) {
		function_address = _internal->FUnmapViewOfFile;
		return true;
	} else if (!strcmp(function_name, "SetEndOfFile")) {
		function_address = _internal->FSetEndOfFile;
		return true;
	} else if (!strcmp(function_name, "DeleteFileA")) {
		function_address = _internal->FDeleteFileA;
		return true;
	} else if (!strcmp(function_name, "HeapDestroy")) {
		function_address = _internal->FHeapDestroy;
		return true;
	} else if (!strcmp(function_name, "ExitProcess")) {
		function_address = _internal->FExitProcess;
		return true;
	} else if (!strcmp(function_name, "HeapReAlloc")) {
		function_address = _internal->FHeapReAlloc;
		return true;
	} else if (!strcmp(function_name, "GetSystemTime")) {
		function_address = _internal->FGetSystemTime;
		return true;
	} else if (!strcmp(function_name, "SetStdHandle")) {
		function_address = _internal->FSetStdHandle;
		return true;
	} else if (!strcmp(function_name, "CreateProcessA")) {
		function_address = _internal->FCreateProcessA;
		return true;
	} else if (!strcmp(function_name, "WaitForSingleObject")) {
		function_address = _internal->FWaitForSingleObject;
		return true;
	} else if (!strcmp(function_name, "GetExitCodeProcess")) {
		function_address = _internal->FGetExitCodeProcess;
		return true;
	} else if (!strcmp(function_name, "GetCurrentProcessId")) {
		function_address = _internal->FGetCurrentProcessId;
		return true;
	} else if (!strcmp(function_name, "GetConsoleScreenBufferInfo")) {
		function_address = _internal->FGetConsoleScreenBufferInfo;
		return true;
	} else if (!strcmp(function_name, "GetCurrentProcess")) {
		function_address = _internal->FGetCurrentProcess;
		return true;
	} else if (!strcmp(function_name, "GetConsoleCP")) {
		function_address = _internal->FGetConsoleCP;
		return true;
	} else if (!strcmp(function_name, "GetCPInfo")) {
		function_address = _internal->FGetCPInfo;
		return true;
	} else if (!strcmp(function_name, "SetErrorMode")) {
		function_address = _internal->FSetErrorMode;
		return true;
	} else if (!strcmp(function_name, "MapViewOfFile")) {
		function_address = _internal->FMapViewOfFile;
		return true;
	}

	/* Calculate the CRC32 checksum of the name */
	unsigned int name_crc = tools_crc32(0x1337C0DEu, function_name,
		strlen(function_name));

#ifdef DEBUG_KERNEL32
	// FIXME!
	Log::warn("[KERNEL32] unknown function '%s' crc:%08X\n", function_name,
		name_crc);
#endif
	function_address = name_crc;
	return true;
}

void KERNEL32::get_syscall_range(unsigned int &first, unsigned int &last) const
{
	first = 0x900;
	last = 0x9FF;
}

bool KERNEL32::handle_syscall(unsigned int syscall_number)
{
	switch (syscall_number) {
		case 0x900:
			return _internal->IHeapCreate(*this);
		case 0x901:
			return _internal->ISetConsoleCtrlHandler(*this);
		case 0x902:
			return _internal->IGetModuleHandleA(*this);
		case 0x903:
			return _internal->IGetProcAddress(*this);
		case 0x904:
			return _internal->ICreateFileA(*this);
		case 0x905:
			return _internal->ICreateFileMappingA(*this);
		case 0x906:
			return _internal->IMapViewOfFileEx(*this, 1);
		case 0x907:
			return _internal->ICloseHandle(*this);
		case 0x908:
			return _internal->IGetFileSize(*this);
		case 0x909:
			return _internal->IGetModuleFileNameA(*this);
		case 0x90A:
			return _internal->ILoadLibraryA(*this);
		case 0x90B:
			return _internal->IGetCommandLineA(*this);
		case 0x90C:
			return _internal->IGetEnvironmentStrings(*this);
		case 0x90D:
			return _internal->IGetVersion(*this);
		case 0x90E:
			return _internal->IGetProcessHeap(*this);
		case 0x90F:
			return _internal->IxxxCriticalSection(*this, 0);
		case 0x910:
			return _internal->IxxxCriticalSection(*this, 1);
		case 0x911:
			return _internal->IxxxCriticalSection(*this, 2);
		case 0x912:
			return _internal->IxxxCriticalSection(*this, 3);
		case 0x913:
			return _internal->ITlsAlloc(*this);
		case 0x914:
			return _internal->IHeapAlloc(*this);
		case 0x915:
			return _internal->ITlsSetValue(*this);
		case 0x916:
			return _internal->IGetCurrentThreadId(*this);
		case 0x917:
			return _internal->IGetStartupInfoA(*this);
		case 0x918:
			return _internal->IGetStdHandle(*this);
		case 0x919:
			return _internal->IGetFileType(*this);
		case 0x91A:
			return _internal->IMultiByteToWideChar(*this);
		case 0x91B:
			return _internal->IGetTimeZoneInformation(*this);
		case 0x91C:
			return _internal->IGetLocalTime(*this);
		case 0x91D:
			return _internal->ITlsGetValue(*this);
		case 0x91E:
			return _internal->IHeapSize(*this);
		case 0x91F:
			return _internal->IGetFileAttributesA(*this);
		case 0x920:
			return _internal->IxxxLastError(*this, false);
		case 0x921:
			return _internal->IxxxLastError(*this, true);
		case 0x922:
			return _internal->IHeapFree(*this);
		case 0x923:
			return _internal->IReadWriteFile(*this, true);
		case 0x924:
			return _internal->IGetCurrentDirectoryA(*this);
		case 0x925:
			return _internal->IDuplicateHandle(*this);
		case 0x926:
			return _internal->IRtlMoveMemory(*this);
		case 0x927:
			return _internal->IRtlFillMemory(*this);
		case 0x928:
			return _internal->IFindFirstFileA(*this);
		case 0x929:
			return _internal->IFileTimeToSystemTime(*this);
		case 0x92A:
			return _internal->IFindClose(*this);
		case 0x92B:
			return _internal->ISetFilePointer(*this);
		case 0x92C:
			return _internal->IReadWriteFile(*this, false);
		case 0x92D:
			return _internal->IUnmapViewOfFile(*this);
		case 0x92E:
			return _internal->ISetEndOfFile(*this);
		case 0x92F:
			return _internal->IDeleteFileA(*this);
		case 0x930:
			return _internal->IHeapDestroy(*this);
		case 0x931:
			return _internal->IExitProcess(*this);
		case 0x932:
			return _internal->IHeapReAlloc(*this);
		case 0x933:
			return _internal->IGetSystemTime(*this);
		case 0x934:
			return _internal->ISetStdHandle(*this);
		case 0x935:
			return _internal->ICreateProcessA(*this);
		case 0x936:
			return _internal->IWaitForSingleObject(*this);
		case 0x937:
			return _internal->IGetExitCodeProcess(*this);
		case 0x938:
			return _internal->IGetCurrentProcessId(*this);
		case 0x939:
			return _internal->IGetConsoleScreenBufferInfo(*this);
		case 0x93A:
			return _internal->IGetCurrentProcess(*this);
		case 0x93B:
			return _internal->IGetConsoleCP(*this);
		case 0x93C:
			return _internal->IGetCPInfo(*this);
		case 0x93D:
			return _internal->ISetErrorMode(*this);
		case 0x93E:
			return _internal->IMapViewOfFileEx(*this, 0);
	}

	/* Unsupported system call */
	Log::error("[KERNEL32] handle_syscall 0x%X\n", syscall_number);
	return false;
}

bool KERNEL32::dll_main(unsigned int reason)
{
	/* Process attach */
	if (reason == 1) {
		/*
		 * Duplicate the original standard handles, as the emulated
		 * application is allowed to close them
		 */
#define DUPLICATE_STD_HANDLE(id, new_handle) \
	do { \
		DWORD handle_flags; \
		HANDLE current_handle = GetStdHandle(id); \
		if (!GetHandleInformation(current_handle, &handle_flags)) \
			handle_flags = 0u; \
		if (!DuplicateHandle(GetCurrentProcess(), current_handle, \
				GetCurrentProcess(), &(new_handle), \
				0, !!(handle_flags & HANDLE_FLAG_INHERIT), \
				DUPLICATE_SAME_ACCESS)) \
			new_handle = INVALID_HANDLE_VALUE; \
	} while (0)

		DUPLICATE_STD_HANDLE(STD_INPUT_HANDLE, _internal->_orig_input);
		DUPLICATE_STD_HANDLE(STD_OUTPUT_HANDLE, _internal->_orig_output);
		DUPLICATE_STD_HANDLE(STD_ERROR_HANDLE, _internal->_orig_error);

#undef DUPLICATE_STD_HANDLE
	}

	/* Process detach */
	else if (reason == 0) {
		/*
		 * Restore duplicated standard handles unless the handle is still
		 * valid
		 */
#define RESTORE_STD_HANDLE(id, old_handle) \
	do { \
		DWORD _; \
		HANDLE std_handle = GetStdHandle(id); \
		if (GetHandleInformation(std_handle, &_)) \
			CloseHandle(old_handle); \
		else \
			SetStdHandle(id, old_handle); \
	} while (0)

		RESTORE_STD_HANDLE(STD_INPUT_HANDLE, _internal->_orig_input);
		RESTORE_STD_HANDLE(STD_OUTPUT_HANDLE, _internal->_orig_output);
		RESTORE_STD_HANDLE(STD_ERROR_HANDLE, _internal->_orig_error);

#undef RESTORE_STD_HANDLE
	}
	return true;
}

/*
 * KERNEL32 class-specific functions
 */
}
