/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef OS_PRINTF_H
#define OS_PRINTF_H

/*!
 * \file src/os/printf.c
 * \brief printf() implementation compatible with old Visual C++ runtimes, for
 *        use within the builtin crtdll.
 */

#include "../process/virtmem.h"

#ifdef __cplusplus
extern "C" {
#endif

struct MSVCPrintfContext
{
	/* Callback for printing a character */
	int (*write_cb)(int character, void *user);
	void *write_cb_user;

	/* Callback for fetching an argument */
	memory_address_t (*va_arg_cb)(unsigned int index, void *user);
	void *va_arg_cb_user;

	/* Virtual memory */
	struct VirtualMemoryContext *memory;

	/* Number of characters written */
	unsigned int written_chars;
};

/* Initializes a MSVCPrintfContext. */
void msvc_printf_init(struct MSVCPrintfContext *ctx);

/* See printf() */
int msvc_printf(struct MSVCPrintfContext *ctx, const char *format,
	unsigned int args_start);

#ifdef __cplusplus
}
#endif

#endif
