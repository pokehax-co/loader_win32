/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "crtdll.hpp"

#include "../misc/log.hpp"
#include "../misc/array.hpp"
#include "../misc/tools.h"
#include "../mem.hpp"
#include "ntppc.hpp"
#include "win32.hpp"
#include "ntdll.hpp"
#include "printf.h"
#include "../process/virtmem.h"

#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <io.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#define READ_U32(addr, value) \
	(cpu_powerpc_read_32(cpu, (addr), (value)))
#define WRITE_U32(addr, value) \
	(cpu_powerpc_write_32(cpu, (addr), (unsigned int)(value)))
#define READ_U32_OK(addr, value) \
	(READ_U32(addr, value) == MEMORY_STATUS_OK)
#define WRITE_U32_OK(addr, value) \
	(WRITE_U32(addr, value) == MEMORY_STATUS_OK)


namespace Win32DLLs {

/*
 * CRTDLL internals
 */

struct CRTDLL::_Internal
{
	/* Reference to NTDLL */
	Win32DLLs::NTDLL *ntdll;

	/* Allocated memory blocks */
	memory_address_t _memory_variables;
	memory_address_t _memory_argv_envp;
	memory_address_t _memory_arrays;

	_Internal(Win32Environment &environment) :
		_program_arguments_argc(-1),
		_program_arguments_argv(0),
		_program_arguments_envp(0)
	{
		/* We need NTDLL for the heap */
		ntdll = static_cast<Win32DLLs::NTDLL *>(
			environment.get_module("ntdll.dll")
		);

		if (!ntdll) {
			Log::error("CRTDLL can't get NTDLL\n");
			exit(1);
		}
	}

private:
	/* Copies argv and endp to virtual memory. */
	bool _copy_program_arguments(Win32Environment &environment);

	/* getenv() for the host */
	const char *_get_host_env(Win32Environment &environment,
		const char *varname);
public:

	/* _ctype array */
	static const unsigned short ctype_array[257];

	/* Addresses to program arguments */
	int _program_arguments_argc;
	memory_address_t _program_arguments_argv;
	memory_address_t _program_arguments_envp;

	/* atexit() function list */
	Array<unsigned int> _atexit_list;

	/* Last error code */
	int _crtdll_errno;

private:
	/* msvc_printf() write callback to Array */
	static int _internal_printf_to_array(int c, void *user)
	{
		Array<char> *s = static_cast<Array<char> *>(user);
		s->append((char)c);
		return c;
	}

	/* msvc_printf() write callback to FILE */
	static int _internal_printf_to_file(int c, void *user)
	{
		FILE *fp = static_cast<FILE *>(user);
		return putc(c, fp);
	}

	/* msvc_printf() va_arg callback */
	static memory_address_t _internal_printf_va_arg(unsigned int index,
		void *user)
	{
		CallingConventionNTPowerPC *callconv =
			static_cast<CallingConventionNTPowerPC *>(user);

		unsigned int v = 0;
		callconv->get_function_argument(index, v);
		return v;
	}
public:

	/* Addresses to variables */
	unsigned int V_commode_dll;
	unsigned int V_fmode_dll;
	unsigned int V_iob;
	unsigned int V_pctype_dll;
	unsigned int V__mb_cur_max_dll;

	/* Addresses to system call trampolines */
	unsigned int F__GetMainArgs;
	unsigned int F_initterm;
	unsigned int Fatexit;
	unsigned int Fsprintf;
	unsigned int Fstrncmp;
	unsigned int Fgetenv;
	unsigned int Fcalloc;
	unsigned int F_splitpath;
	unsigned int F_strupr;
	unsigned int F_strnicmp;
	unsigned int Fsetvbuf;
	unsigned int F_stricmp;
	unsigned int Fstrcpy;
	unsigned int Fstrchr;
	unsigned int Fstrlen;
	unsigned int Fmalloc;
	unsigned int F_strdup;
	unsigned int Fstrcmp;
	unsigned int Fprintf;
	unsigned int Ffflush;
	unsigned int Fputs;
	unsigned int Fexit;
	unsigned int Fstrpbrk;
	unsigned int F_access;
	unsigned int F_searchenv;
	unsigned int Ffree;
	unsigned int F_fullpath;
	unsigned int Fmemcpy;
	unsigned int Fmemcmp;
	unsigned int Fstrncpy;
	unsigned int Fstrcat;
	unsigned int Fmemset;
	unsigned int Frealloc;

	/* Implementations */
	bool I__GetMainArgs(CRTDLL &module);
	bool I_initterm(CRTDLL &module);
	bool Iatexit(CRTDLL &module);
	bool Isprintf(CRTDLL &module);
	bool Istrncmp(CRTDLL &module);
	bool Igetenv(CRTDLL &module);
	bool Icalloc(CRTDLL &module);
	bool I_splitpath(CRTDLL &module);
	bool I_strupr(CRTDLL &module);
	bool I_strnicmp(CRTDLL &module);
	bool Isetvbuf(CRTDLL &module);
	bool I_stricmp(CRTDLL &module);
	bool Istrcpy(CRTDLL &module);
	bool Istrchr(CRTDLL &module);
	bool Istrlen(CRTDLL &module);
	bool Imalloc(CRTDLL &module);
	bool I_strdup(CRTDLL &module);
	bool Istrcmp(CRTDLL &module);
	bool Iprintf(CRTDLL &module);
	bool Ifflush(CRTDLL &module);
	bool Iputs(CRTDLL &module);
	bool Iexit(CRTDLL &module);
	bool Istrpbrk(CRTDLL &module);
	bool I_access(CRTDLL &module);
	bool I_searchenv(CRTDLL &module);
	bool Ifree(CRTDLL &module);
	bool I_fullpath(CRTDLL &module);
	bool Imemcpy(CRTDLL &module);
	bool Imemcmp(CRTDLL &module);
	bool Istrncpy(CRTDLL &module);
	bool Istrcat(CRTDLL &module);
	bool Imemset(CRTDLL &module);
	bool Irealloc(CRTDLL &module);
};

bool CRTDLL::_Internal::_copy_program_arguments(Win32Environment &environment)
{
	const unsigned int POINTER_SIZE = 4;
	int i;

	/* Already allocated? */
	if (_program_arguments_argc >= 0)
		return true;

	/* Ask environment for program arguments */
	char **host_argv, **host_envp;
	environment.get_program_arguments(_program_arguments_argc,
		host_argv, host_envp);

	/*
	 * Determine how much memory we need
	 */
	unsigned int strings_size = 0;

	/* argv */
	unsigned int argc_pointer_count = 1;
	for (i = 0; i < _program_arguments_argc; ++i) {
		++argc_pointer_count;
		strings_size += 1 + strlen(host_argv[i]);
	}

	/* envp */
	unsigned int envp_pointer_count = 1;
	for (char **p = host_envp; *p; ++p) {
		++envp_pointer_count;
		strings_size += 1 + strlen(*p);
	}

	const unsigned int allocation_size = strings_size +
		POINTER_SIZE * (argc_pointer_count + envp_pointer_count);

	// XXX: We don't have much memory reserved
	if (allocation_size > 2 * MEMORY_PAGE_SIZE) {
		Log::error("too many program arguments or environment variables "
			"(need %u bytes)\n", allocation_size);
		return false;
	}

	/* Assign addresses */
	_program_arguments_argv = _memory_argv_envp;
	_program_arguments_envp = _memory_argv_envp +
		argc_pointer_count * POINTER_SIZE;

	/* Place strings after pointers */
	memory_address_t string_address = _program_arguments_envp +
		envp_pointer_count * POINTER_SIZE;

	AddressSpace *const memory = environment.get_memory();
	CPUContext *const cpu = environment.get_cpu();

	/* Copy argv */
	for (i = 0; i < _program_arguments_argc; ++i) {
		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				string_address, host_argv[i], (size_t)-1))) {
			Log::error("[CRTDLL] writing argv[%d] failed\n", i);
			return false;
		}
		if (!WRITE_U32_OK(_program_arguments_argv + POINTER_SIZE * i,
				string_address)) {
			Log::error("[CRTDLL] writing argv[%d] address failed\n", i);
			return false;
		}
		string_address += strlen(host_argv[i]) + 1;
	}

	/* Write null terminator */
	if (!WRITE_U32_OK(_program_arguments_argv +
			_program_arguments_argc * POINTER_SIZE, 0)) {
		Log::error("[CRTDLL] writing argv null failed\n");
		return false;
	}

	/* Copy envp */
	for (i = 0; host_envp[i]; ++i) {
		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				string_address, host_envp[i], (size_t)-1))) {
			Log::error("[CRTDLL] writing envp[%d] failed\n", i);
			return false;
		}
		if (!WRITE_U32_OK(_program_arguments_envp + POINTER_SIZE * i,
				string_address)) {
			Log::error("[CRTDLL] writing envp[%d] address failed\n", i);
			return false;
		}
		string_address += strlen(host_envp[i]) + 1;
	}

	/* Write null terminator for envp */
	if (!WRITE_U32_OK(_program_arguments_envp + i * POINTER_SIZE, 0)) {
		Log::error("[CRTDLL] writing envp null failed\n");
		return false;
	}

	return true;
}

const char *CRTDLL::_Internal::_get_host_env(Win32Environment &environment,
	const char *varname)
{
	/* Ask environment for environment variables */
	int unused1;
	char **unused2, **host_envp;
	environment.get_program_arguments(unused1, unused2, host_envp);

	/* Find the environment variable */
	const size_t varname_len = strlen(varname);
	while (*host_envp) {
		if (strlen(*host_envp) > varname_len &&
				(*host_envp)[varname_len] == '=' &&
				!strncmp(*host_envp, varname, varname_len)) {
			return (*host_envp) + varname_len + 1;
		}
		++host_envp;
	}
	return NULL;
}

const unsigned short CRTDLL::_Internal::ctype_array[257] = {
	0,												// skip this entry
	0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,	// 0x00
	0x20, 0x28, 0x28, 0x28, 0x28, 0x28, 0x20, 0x20,
	0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,	// 0x10
	0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
	0x48, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10,	// 0x20
	0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10,
	0x84, 0x84, 0x84, 0x84, 0x84, 0x84, 0x84, 0x84,	// 0x30
	0x84, 0x84, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10,
	0x10, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x01,	// 0x40
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,	// 0x50
	0x01, 0x01, 0x01, 0x10, 0x10, 0x10, 0x10, 0x10,
	0x10, 0x82, 0x82, 0x82, 0x82, 0x82, 0x82, 0x02,	// 0x60
	0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02,
	0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02,	// 0x70
	0x02, 0x02, 0x02, 0x10, 0x10, 0x10, 0x10, 0x20
};


/*
 * void __cdecl __GetMainArgs(int *pargc, char ***pargv, char ***penvp,
 *     int dowildcard);
 *
 * Parses argc, argv, envp from the command line. In our case, we already have
 * them.
 *
 * Unsupported features:
 *  - dowildcard != 0
 */
bool CRTDLL::_Internal::I__GetMainArgs(CRTDLL &module)
{
	unsigned int arg_pargc;
	unsigned int arg_pargv;
	unsigned int arg_penvp;
	unsigned int arg_dowildcard;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_pargc);
	module._callconv.get_function_argument(1, arg_pargv);
	module._callconv.get_function_argument(2, arg_penvp);
	module._callconv.get_function_argument(3, arg_dowildcard);

	/* Warn on dowildcard */
	if (arg_dowildcard)
		Log::warn("crtdll!__GetMainArgs() non-zero 'dowildcard'\n");

	/* We need program arguments now */
	if (!_copy_program_arguments(module._environment))
		return false;

	/* Write addresses */
	CPUContext *const cpu = module._environment.get_cpu();
	if (!WRITE_U32_OK(arg_pargc, _program_arguments_argc))
		goto error;
	if (!WRITE_U32_OK(arg_pargv, _program_arguments_argv))
		goto error;
	if (!WRITE_U32_OK(arg_penvp, _program_arguments_envp))
		goto error;
	return true;

error:
	Log::error("crtdll!__GetMainArgs: writing back arguments failed\n");
	return false;
}

/*
 * void __cdecl _initterm(PF *pfbegin, PF *pfend);
 *
 * Calls the static constructors. We save all registers before a recursive
 * call to the interpreter and restore them afterwards.
 *
 * XXX: We should use PowerPC assembly for this
 */
bool CRTDLL::_Internal::I_initterm(CRTDLL &module)
{
	unsigned int arg_pfbegin;
	unsigned int arg_pfend;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_pfbegin);
	module._callconv.get_function_argument(1, arg_pfend);

	CPUContext *const cpu = module._environment.get_cpu();

	/*
	 * Save all registers
	 */
	unsigned int regsave_size = 0;
	cpu_save_register_state(cpu, NULL, &regsave_size);

	unsigned char *regsave = new unsigned char[regsave_size];
	cpu_save_register_state(cpu, regsave, &regsave_size);

	/* Call each function and restore registers after each call */
	while (arg_pfbegin < arg_pfend) {
		unsigned int function_ptr;

		/* Read function address and skip null pointers */
		if (!READ_U32_OK(arg_pfbegin, &function_ptr))
			goto error;
		arg_pfbegin += 4;
		if (!function_ptr)
			continue;

		/* Read function pointer and r2 */
		unsigned int r2;
		if (!READ_U32_OK(function_ptr + 0x4, &r2))
			goto error;
		if (!READ_U32_OK(function_ptr + 0x0, &function_ptr))
			goto error;

		Log::trace("crtdll!_initterm: calling 0x%X\n", function_ptr);

		/* Run the CPU on this function recursively */
		cpu_set_register(cpu, PPC_REGISTER_R2, r2);
		cpu_set_register(cpu, PPC_REGISTER_PC, function_ptr);
		cpu_run(cpu, 0xABCDABCDu);

		/* Check program counter on return */
		cpu_register_t pc;
		cpu_get_register(cpu, PPC_REGISTER_PC, &pc);

		/* This magic value indicates program exit */
		if (pc == 0x55A855A8u)
			return true;

		/* Must have returned */
		if (pc != 0xABCDABCDu)
			goto error;

		Log::trace("crtdll!_initterm: leaving 0x%X\n", function_ptr);

		/* Restore registers */
		cpu_load_register_state(cpu, regsave, regsave_size);
	}
	delete[] regsave;

	// FIXME!
	Log::warn("crtdll!_initterm: all functions terminated\n");
	cpu_stop(cpu, true);
	return true;

error:
	Log::error("crtdll!_initterm failed\n");
	delete[] regsave;
	return false;
}

/*
 * int __cdecl atexit(void (__cdecl *func)(void));
 *
 * Adds a callback to run at exit.
 */
bool CRTDLL::_Internal::Iatexit(CRTDLL &module)
{
	unsigned int arg_func;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_func);

	/* Add to atexit() list */
	Log::trace("crtdll!atexit adding 0x%X\n", arg_func);
	_atexit_list.append(arg_func);

	/* Success */
	module._callconv.set_return_value(0);
	return true;
}

/*
 * int __cdecl sprintf(const char *s, const char *format, ...);
 *
 * Formats the string 'format' into the string 's'.
 */
bool CRTDLL::_Internal::Isprintf(CRTDLL &module)
{
	unsigned int arg_s;
	unsigned int arg_format;

	/* Grab non-vararg arguments */
	module._callconv.get_function_argument(0, arg_s);
	module._callconv.get_function_argument(1, arg_format);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read the format string */
	char *actual_format;
	if (memory->read_string(arg_format, actual_format) != MEMORY_STATUS_OK)
		return false;

	/* Initialize MSVC printf() context */
	MSVCPrintfContext printf_ctx;
	msvc_printf_init(&printf_ctx);

	/*
	 * Perform the printf() call. Write the string into an Array, so we can
	 * write it once the call has finished.
	 */
	Array<char> formatted_string;
	printf_ctx.write_cb = _internal_printf_to_array;
	printf_ctx.write_cb_user = &formatted_string;
	printf_ctx.va_arg_cb = _internal_printf_va_arg;
	printf_ctx.va_arg_cb_user = &module._callconv;
	printf_ctx.memory = memory->get_virtmem();

	int ret = msvc_printf(&printf_ctx, actual_format, 2);
	delete[] actual_format;

	/* Write all characters (plus null terminator) to memory */
	if (formatted_string.size() == 0) {
		const char null = '\0';
		if (!MEMOP_OK(virtual_memory_write(memory->get_virtmem(), arg_s,
				&null, 1))) {
			Log::error("crtdll!sprintf: writing formatted string failed\n");
			return false;
		}
	} else {
		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				arg_s, &formatted_string[0], formatted_string.size()))) {
			Log::error("crtdll!sprintf: writing formatted string failed\n");
			return false;
		}
	}

	if (ret < 0) {
		// FIXME: remove this
		Log::error("crtdll!sprintf failed\n");
		return false;
	}

	/* Return number of characters written to buffer */
	module._callconv.set_return_value(ret);
	return true;
}

/*
 * int strncmp(const char *string1, const char *string2,
 *     size_t count);
 *
 * Compares two strings for equality (case-sensitive, up to 'count'
 * characters).
 */
bool CRTDLL::_Internal::Istrncmp(CRTDLL &module)
{
	unsigned int arg_string1;
	unsigned int arg_string2;
	unsigned int arg_count;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_string1);
	module._callconv.get_function_argument(1, arg_string2);
	module._callconv.get_function_argument(2, arg_count);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	char first = 0, last = 0;
	if (arg_count) {
		do {
			if (!MEMOP_OK(virtual_memory_read(memory, arg_string1, &first,
					1))) {
				Log::error("crtdll!strncmp read string1 failed\n");
				return false;
			}
			if (!MEMOP_OK(virtual_memory_read(memory, arg_string2, &last,
					1))) {
				Log::error("crtdll!strncmp read string2 failed\n");
				return false;
			}
			++arg_string1;
			++arg_string2;
		} while (--arg_count && first && first == last);
	}

	/* Difference between characters is the result */
	module._callconv.set_return_value(first - last);
	return true;
}

/*
 * char *getenv(const char *varname);
 *
 * Returns the value of environment variable 'varname'.
 */
bool CRTDLL::_Internal::Igetenv(CRTDLL &module)
{
	unsigned int arg_varname;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_varname);

	/* NULL pointer if failing */
	module._callconv.set_return_value(0);
	if (!arg_varname)
		return true;

	AddressSpace *const memory = module._environment.get_memory();

	/* Read the variable name */
	char *actual_varname;
	if (memory->read_string(arg_varname, actual_varname) != MEMORY_STATUS_OK)
		return false;

	/* We need environment variables */
	if (!_copy_program_arguments(module._environment))
		return false;

	/* Get the backing page to simplify searches */
	struct VirtualMemoryInformation env_page;
	if (virtual_memory_query(memory->get_virtmem(), _memory_argv_envp,
			&env_page) < 0)
		return false;

	/* Jump to environment variables */
	unsigned int *virt_envp = reinterpret_cast<unsigned int *>(
		static_cast<char *>(env_page.host_pointer) +
		(_program_arguments_envp - _memory_argv_envp)
	);

	/* Scan each environment variable for the name */
	const size_t actual_varname_len = strlen(actual_varname);
	while (*virt_envp) {
		/* Translate current argument */
		char *virt_envp_cur = reinterpret_cast<char *>(
			static_cast<char *>(env_page.host_pointer) +
			(*virt_envp - _memory_argv_envp)
		);

		if (strlen(virt_envp_cur) > actual_varname_len &&
				virt_envp_cur[actual_varname_len] == '=' &&
				!strncmp(virt_envp_cur, actual_varname, actual_varname_len)) {
			/*
			 * Translate address back
			 */
			char *result = virt_envp_cur + actual_varname_len + 1;
			memory_address_t translated_result =
				reinterpret_cast<memory_address_t>(result) -
				_memory_argv_envp;

			Log::trace("crtdll!getenv: '%s'='%s'\n", actual_varname, result);

			module._callconv.set_return_value(
				(unsigned int)translated_result
			);
			break;
		}
		++virt_envp;
	}

	if (!*virt_envp)
		Log::trace("crtdll!getenv: '%s' not found\n", actual_varname);

	delete[] actual_varname;
	return true;
}

/*
 * void *calloc(size_t count, size_t size);
 *
 * Allocates count * size bytes and zeroes it.
 */
bool CRTDLL::_Internal::Icalloc(CRTDLL &module)
{
	unsigned int arg_count;
	unsigned int arg_size;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_count);
	module._callconv.get_function_argument(1, arg_size);

	/* Check for overflow */
	size_t needed_size = arg_count * arg_size;
	if (needed_size > UINT_MAX || needed_size / arg_size != arg_count) {
		Log::error("crtdll!calloc: overflow\n");
		return false;
	}

	/* Allocate memory */
	memory_address_t address;
	if (!ntdll->heap_alloc(module._environment.get_process_heap(),
			(unsigned int)needed_size, address, true)) {
		Log::error("crtdll!calloc heap allocation failed\n");
		return false;
	}

	module._callconv.set_return_value((unsigned int)address);
	return true;
}

/*
 * void _splitpath(const char *path, char *drive, char *dir, char *fname,
 *     char *ext);
 *
 * Splits a path into its components.
 */
bool CRTDLL::_Internal::I_splitpath(CRTDLL &module)
{
	unsigned int arg_path;
	unsigned int arg_drive;
	unsigned int arg_dir;
	unsigned int arg_fname;
	unsigned int arg_ext;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_path);
	module._callconv.get_function_argument(1, arg_drive);
	module._callconv.get_function_argument(2, arg_dir);
	module._callconv.get_function_argument(3, arg_fname);
	module._callconv.get_function_argument(4, arg_ext);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read the path */
	char *actual_path;
	if (memory->read_string(arg_path, actual_path) != MEMORY_STATUS_OK)
		return false;

	/* Allocate buffers and call _splitpath */
	char *buf_drive = NULL;
	char *buf_dir = NULL;
	char *buf_fname = NULL;
	char *buf_ext = NULL;

	if (arg_drive)
		buf_drive = new char[3];
	if (arg_dir)
		buf_dir = new char[256];
	if (arg_fname)
		buf_fname = new char[256];
	if (arg_ext)
		buf_ext = new char[256];

	/* Use host _splitpath() */
	_splitpath(actual_path, buf_drive, buf_dir, buf_fname, buf_ext);

	/* Write strings back */
	bool ret = false;
	if (arg_drive) {
		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				arg_drive, buf_drive, 3)))
			goto error;
	}
	if (arg_dir) {
		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				arg_dir, buf_dir, 256)))
			goto error;
	}
	if (arg_fname) {
		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				arg_fname, buf_fname, 256)))
			goto error;
	}
	if (arg_ext) {
		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				arg_ext, buf_ext, 256)))
			goto error;
	}

	ret = true;
error:
	delete[] buf_ext;
	delete[] buf_fname;
	delete[] buf_dir;
	delete[] buf_drive;
	delete[] actual_path;
	return ret;
}

/*
 * char *_strupr(char *str);
 *
 * Makes a string uppercase.
 */
bool CRTDLL::_Internal::I_strupr(CRTDLL &module)
{
	unsigned int arg_str;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_str);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	for (memory_address_t p = arg_str; ; ++p) {
		/* Read character */
		char c;
		if (!MEMOP_OK(virtual_memory_read(memory, p, &c, 1)))
			return false;
		if (!c)
			break;

		/* Make it uppercase and write it back */
		if (c >= 'a' && c <= 'z') {
			c = c - 'a' + 'A';
			if (!MEMOP_OK(virtual_memory_write(memory, p, &c, 1)))
				return false;
		}
	}

	module._callconv.set_return_value(arg_str);
	return true;
}

/*
 * int _strnicmp(const char *string1, const char *string2,
 *     size_t count);
 *
 * Compares two strings for equality (case-insensitive, up to 'count'
 * characters).
 */
bool CRTDLL::_Internal::I_strnicmp(CRTDLL &module)
{
	unsigned int arg_string1;
	unsigned int arg_string2;
	unsigned int arg_count;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_string1);
	module._callconv.get_function_argument(1, arg_string2);
	module._callconv.get_function_argument(2, arg_count);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	int ret = 0;
	if (arg_count) {
		char first, last;
		do {
			if (!MEMOP_OK(virtual_memory_read(memory, arg_string1, &first,
					1))) {
				Log::error("crtdll!_strnicmp read string1 failed\n");
				return false;
			}
			if (!MEMOP_OK(virtual_memory_read(memory, arg_string2, &last,
					1))) {
				Log::error("crtdll!_strnicmp read string2 failed\n");
				return false;
			}
			if (first != last) {
				if (first >= 'A' && first <= 'Z')
					first = first - 'A' + 'a';
				if (last >= 'A' && last <= 'Z')
					last = last - 'A' + 'a';
				if (first != last) {
					ret = first - last;
					break;
				}
			}
			++arg_string1;
			++arg_string2;
		} while (--arg_count && first);
	}

	/* Difference between characters is the result */
	module._callconv.set_return_value(ret);
	return true;
}

/*
 * int setvbuf(FILE *stream, char *buffer, int mode, size_t size);
 *
 * Changes buffering settings of 'stream'.
 */
bool CRTDLL::_Internal::Isetvbuf(CRTDLL &module)
{
	unsigned int arg_stream;
	unsigned int arg_buffer;
	unsigned int arg_mode;
	unsigned int arg_size;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_stream);
	module._callconv.get_function_argument(1, arg_buffer);
	module._callconv.get_function_argument(2, arg_mode);
	module._callconv.get_function_argument(3, arg_size);

	/* Translate file stream -- XXX: only std{in,out,err} supported */
#if 0
	FILE *fp = NULL;
	if (arg_stream == V_iob)
		fp = stdin;
	else if (arg_stream == V_iob + 0x20)
		fp = stdout;
	else if (arg_stream == V_iob + 0x40)
		fp = stderr;
	else {
		Log::error("crtdll!setvbuf: unknown stream 0x%X\n", arg_stream);
		return false;
	}
#endif

	/* XXX: unsupported */
	if (arg_buffer || arg_mode) {
		Log::error("crtdll!setvbuf: buffer=0x%X, mode=0x%X unsupported\n",
			arg_buffer, arg_mode);
		return false;
	}

	/* Call the real setvbuf */
#if 0
	int ret = setvbuf(fp, NULL, 0, arg_size);
	if (ret < 0)
		_crtdll_errno = errno;
#else
	// FIXME!
	Log::trace("setvbuf is currently disabled, faking success\n");
	int ret = 0;
#endif

	/* Pass return value through */
	module._callconv.set_return_value(ret);
	return true;
}

/*
 * int _stricmp(const char *string1, const char *string2);
 *
 * Compares two strings for equality (case-insensitive).
 */
bool CRTDLL::_Internal::I_stricmp(CRTDLL &module)
{
	unsigned int arg_string1;
	unsigned int arg_string2;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_string1);
	module._callconv.get_function_argument(1, arg_string2);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	int ret = 0;
	char first, last;
	do {
		if (!MEMOP_OK(virtual_memory_read(memory, arg_string1, &first, 1))) {
			Log::error("crtdll!_stricmp read string1 failed\n");
			return false;
		}
		if (!MEMOP_OK(virtual_memory_read(memory, arg_string2, &last, 1))) {
			Log::error("crtdll!_stricmp read string2 failed\n");
			return false;
		}
		if (first != last) {
			if (first >= 'A' && first <= 'Z')
				first = first - 'A' + 'a';
			if (last >= 'A' && last <= 'Z')
				last = last - 'A' + 'a';
			if (first != last) {
				ret = first - last;
				break;
			}
		}
		++arg_string1;
		++arg_string2;
	} while (first);

	/* Difference between characters is the result */
	module._callconv.set_return_value(ret);
	return true;
}

/*
 * char *strcpy(char *destination, const char * source);
 *
 * Copies string 'source' to 'destination'.
 */
bool CRTDLL::_Internal::Istrcpy(CRTDLL &module)
{
	unsigned int arg_destination;
	unsigned int arg_source;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_destination);
	module._callconv.get_function_argument(1, arg_source);

	/* Set the return value already */
	module._callconv.set_return_value(arg_destination);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	/* Copy character by character */
	char c;
	do {
		if (!MEMOP_OK(virtual_memory_read(memory, arg_source, &c, 1))) {
			Log::error("crtdll!strcpy: reading src failed\n");
			return false;
		}
		if (!MEMOP_OK(virtual_memory_write(memory, arg_destination, &c, 1))) {
			Log::error("crtdll!strcpy: writing dest failed\n");
			return false;
		}
		++arg_source;
		++arg_destination;
	} while (c);

	return true;
}

/*
 * char *strchr(char *s, int c);
 *
 * Finds c in s.
 */
bool CRTDLL::_Internal::Istrchr(CRTDLL &module)
{
	unsigned int arg_s;
	unsigned int arg_c_;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_s);
	module._callconv.get_function_argument(1, arg_c_);
	const char arg_c = (char)(arg_c_ & 0xFFu);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	/* Search character by character */
	unsigned int ret = 0;
	char current_c;
	do {
		if (!MEMOP_OK(virtual_memory_read(memory, arg_s, &current_c, 1))) {
			Log::error("crtdll!strchr: reading src failed\n");
			return false;
		}
		if (current_c == arg_c) {
			ret = arg_s;
			break;
		}
		++arg_s;
	} while (current_c);

	module._callconv.set_return_value(ret);
	return true;
}

/*
 * size_t strlen(char *s);
 *
 * Determines the length of s.
 */
bool CRTDLL::_Internal::Istrlen(CRTDLL &module)
{
	unsigned int arg_s;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_s);
	const unsigned int orig_s = arg_s;

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	/* Count by character */
	char current_c;
	for (;;) {
		if (!MEMOP_OK(virtual_memory_read(memory, arg_s, &current_c, 1))) {
			Log::error("crtdll!strlen: reading src failed\n");
			return false;
		}
		if (!current_c)
			break;
		++arg_s;
	};

	module._callconv.set_return_value(arg_s - orig_s);
	return true;
}

/*
 * void *malloc(size_t size);
 *
 * Allocates size bytes.
 */
bool CRTDLL::_Internal::Imalloc(CRTDLL &module)
{
	unsigned int arg_size;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_size);

	/* Allocate memory */
	memory_address_t alloc_address;
	if (!ntdll->heap_alloc(module._environment.get_process_heap(),
			arg_size, alloc_address)) {
		Log::error("crtdll!malloc: heap allocation failed\n");
		return false;
	}

	module._callconv.set_return_value((unsigned int)alloc_address);
	return true;
}

/*
 * char *_strdup(const char *s);
 *
 * Duplicates s.
 */
bool CRTDLL::_Internal::I_strdup(CRTDLL &module)
{
	unsigned int arg_s;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_s);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	/* Get the length of s */
	size_t actual_s_length;
	if (!MEMOP_OK(virtual_memory_read_string(memory, arg_s, NULL,
			&actual_s_length)))
		return false;

	/* Allocate memory */
	memory_address_t alloc_address;
	if (!ntdll->heap_alloc(module._environment.get_process_heap(),
			actual_s_length + 1, alloc_address)) {
		Log::error("crtdll!_strdup: heap allocation failed\n");
		return false;
	}

	/* Copy into allocation */
	char *datap = reinterpret_cast<char *>(alloc_address);
	char c;
	do {
		if (!MEMOP_OK(virtual_memory_read(memory, arg_s, &c, 1))) {
			Log::error("crtdll!_strdup: reading s failed\n");
			return false;
		}
		*datap++ = c;
		++arg_s;
	} while (c);

	/* Return allocation address */
	module._callconv.set_return_value((unsigned int)alloc_address);
	return true;
}

/*
 * int strcmp(const char *string1, const char *string2);
 *
 * Compares two strings for equality (case-sensitive).
 */
bool CRTDLL::_Internal::Istrcmp(CRTDLL &module)
{
	unsigned int arg_string1;
	unsigned int arg_string2;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_string1);
	module._callconv.get_function_argument(1, arg_string2);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	int ret = 0;
	char first = 0, last = 0;
	do {
		if (!MEMOP_OK(virtual_memory_read(memory, arg_string1, &first, 1))) {
			Log::error("crtdll!strncmp read string1 failed\n");
			return false;
		}
		if (!MEMOP_OK(virtual_memory_read(memory, arg_string2, &last, 1))) {
			Log::error("crtdll!strncmp read string2 failed\n");
			return false;
		}
		++arg_string1;
		++arg_string2;
	} while (first == last && last);

	/* Don't return difference span, but only -1, 0, or +1 */
	if (first < last)
		ret = -1;
	else if (first > last)
		ret = 1;

	/* Difference between characters is the result */
	module._callconv.set_return_value(ret);
	return true;
}

/*
 * int __cdecl printf(const char *format, ...);
 *
 * Formats the string 'format' into standard output.
 */
bool CRTDLL::_Internal::Iprintf(CRTDLL &module)
{
	unsigned int arg_format;

	/* Grab non-vararg arguments */
	module._callconv.get_function_argument(0, arg_format);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read the format string */
	char *actual_format;
	if (memory->read_string(arg_format, actual_format) != MEMORY_STATUS_OK)
		return false;

	/* Initialize MSVC printf() context */
	MSVCPrintfContext printf_ctx;
	msvc_printf_init(&printf_ctx);

	/*
	 * Perform the printf() call. Write the string into an Array, so we can
	 * write it once the call has finished.
	 */
	printf_ctx.write_cb = _internal_printf_to_file;
	printf_ctx.write_cb_user = stdout;
	printf_ctx.va_arg_cb = _internal_printf_va_arg;
	printf_ctx.va_arg_cb_user = &module._callconv;
	printf_ctx.memory = memory->get_virtmem();

	int ret = msvc_printf(&printf_ctx, actual_format, 1);
	delete[] actual_format;

	if (ret < 0) {
		// FIXME: remove this
		Log::error("crtdll!printf failed\n");
		return false;
	}

	/* Return number of characters written to buffer */
	module._callconv.set_return_value(ret);
	return true;
}

/*
 * int fflush(FILE *stream);
 *
 * Flushes a file stream.
 */
bool CRTDLL::_Internal::Ifflush(CRTDLL &module)
{
	unsigned int arg_stream;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_stream);

	/* Translate file stream -- XXX: only std{in,out,err} supported */
	FILE *fp = NULL;
	if (arg_stream == V_iob)
		fp = stdin;
	else if (arg_stream == V_iob + 0x20)
		fp = stdout;
	else if (arg_stream == V_iob + 0x40)
		fp = stderr;
	else {
		Log::error("crtdll!fflush: unknown stream 0x%X\n", arg_stream);
		return false;
	}

	/* Call the real fflush */
	int ret = fflush(fp);
	if (ret < 0)
		_crtdll_errno = errno;

	/* Pass return value through */
	module._callconv.set_return_value(ret);
	return true;
}

/*
 * int puts(char *s);
 *
 * Prints s to stdout.
 */
bool CRTDLL::_Internal::Iputs(CRTDLL &module)
{
	unsigned int arg_s;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_s);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read the string */
	char *actual_s;
	if (memory->read_string(arg_s, actual_s) != MEMORY_STATUS_OK)
		return false;

	/* Call the real puts */
	int ret = puts(actual_s);
	if (ret < 0)
		_crtdll_errno = errno;

	/* Pass return value through */
	module._callconv.set_return_value(ret);
	delete[] actual_s;
	return true;
}

/*
 * void exit(int code);
 *
 * Exits the program with return code 'code'.
 */
bool CRTDLL::_Internal::Iexit(CRTDLL &module)
{
	unsigned int arg_code;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_code);

	/* Run atexit callbacks */
	for (size_t i = 0; i < _atexit_list.size(); ++i) {
		// FIXME: Run atexit callbacks
		Log::warn("crtdll!exit: run atexit callback 0x%X\n", _atexit_list[i]);
	}

	/* Report exit code to environment */
	module._environment.set_process_exit_code(arg_code);

	/* Shut down and manipulate the instruction pointer */
	CPUContext *const cpu = module._environment.get_cpu();
	cpu_set_register(cpu, PPC_REGISTER_PC, 0x55A855A8u);
	cpu_stop(cpu, true);
	return true;
}

/*
 * char *strpbrk(const char *s, const char *accept);
 *
 * Searches for the first character of 'accept' encountered in 's', and
 * returns its address.
 */
bool CRTDLL::_Internal::Istrpbrk(CRTDLL &module)
{
	unsigned int arg_s;
	unsigned int arg_accept;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_s);
	module._callconv.get_function_argument(1, arg_accept);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read string to search */
	char *actual_s;
	if (memory->read_string(arg_s, actual_s) != MEMORY_STATUS_OK)
		return false;

	/* Read accept string */
	char *actual_accept;
	if (memory->read_string(arg_accept, actual_accept) != MEMORY_STATUS_OK) {
		delete[] actual_s;
		return false;
	}

	/* Convert accept string to bit mask */
	unsigned char accept_mask[32] = {0};
	for (char *p = actual_accept; *p; ++p) {
		const unsigned int pp = (unsigned int)*p;
		accept_mask[pp >> 3] |= 1u << (pp & 0x7);
	}

	delete[] actual_accept;

	/* The first character which appears in 'accept' stops the search */
	unsigned int ret = 0;
	for (char *p = actual_s; *p; ++p) {
		const unsigned int pp = (unsigned int)*p;
		if (accept_mask[pp >> 3] & (1u << (pp & 0x7))) {
			ret = (p - actual_s) + arg_s;
			break;
		}
	}
	delete[] actual_s;

	/* Pass return value back */
	module._callconv.set_return_value(ret);
	return true;
}

/*
 * int _access(const char *path, int mode);
 *
 * Checks whether 'path' can be access with mode 'mode'.
 */
bool CRTDLL::_Internal::I_access(CRTDLL &module)
{
	unsigned int arg_path;
	unsigned int arg_mode;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_path);
	module._callconv.get_function_argument(1, arg_mode);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read filename */
	char *actual_path;
	if (memory->read_string(arg_path, actual_path) != MEMORY_STATUS_OK)
		return false;

	/* Pass to real _access() function */
	// XXX: what if arg_mode is incompatible?
	int ret = _access(actual_path, (int)arg_mode);
	delete[] actual_path;

	if (ret < 0)
		_crtdll_errno = errno;

	/* Pass return value back */
	module._callconv.set_return_value(ret);
	return true;
}

/*
 * int _searchenv(const char *filename, const char *varname,
 *     char *pathname);
 *
 * Searches the paths in the environment 'varname' for the file
 * 'filename', and returns the full path to 'pathname'.
 */
bool CRTDLL::_Internal::I_searchenv(CRTDLL &module)
{
	unsigned int arg_filename;
	unsigned int arg_varname;
	unsigned int arg_pathname;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_filename);
	module._callconv.get_function_argument(1, arg_varname);
	module._callconv.get_function_argument(2, arg_pathname);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read filename */
	char *actual_filename;
	if (memory->read_string(arg_filename, actual_filename) !=
			MEMORY_STATUS_OK)
		return false;

	/* Read variable name */
	char *actual_varname;
	if (memory->read_string(arg_varname, actual_varname) !=
			MEMORY_STATUS_OK) {
		delete[] actual_filename;
		return false;
	}

	bool ok = false;
	char found_path[_MAX_PATH];

	Log::trace("crtdll!_searchenv('%s', '%s', 0x%X)\n",
		actual_filename, actual_varname, arg_pathname);

	/* Try to find it in the current directory */
	if (GetFileAttributesA(actual_filename) != INVALID_FILE_ATTRIBUTES) {
		GetFullPathNameA(actual_filename, MAX_PATH, found_path, NULL);
		ok = true;
	}
	else {
		/* Search in environment variables */
		const char *environment_value = _get_host_env(module._environment,
			actual_varname);
		if (!environment_value) {
			Log::error("crtdll!_searchenv: cannot find variable '%s'\n",
				actual_varname);
			goto error;
		}

		/* Go through paths in value */
		const char *end;
		const unsigned int actual_filename_length = strlen(actual_filename);
		for (const char *p = environment_value; *p;
				p = *end ? (end + 1) : end) {
			/* Find end of current path in value */
			for (end = p; *end && *end != ';'; ++end);

			/* Skip path? */
			ULONG_PTR path_length = end - p;
			if (!path_length || path_length >= MAX_PATH)
				continue;

			/* Copy it, add backlash if missing */
			memcpy(found_path, p, path_length);
			switch (found_path[path_length - 1] ) {
				default:
					found_path[path_length++] = '\\';
					/* fall through */
				case '/':
				case '\\':
					break;
			}

			/* Append filename */
			if (actual_filename_length >= MAX_PATH - path_length)
				continue;
			memcpy(found_path + path_length, actual_filename,
				actual_filename_length + 1);

			/* Does it exist? */
			if (GetFileAttributesA(found_path) != INVALID_FILE_ATTRIBUTES) {
				ok = true;
				break;
			}
		}
	}

	if (ok) {
		/* Write found path to 'arg_pathname' */
		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				arg_pathname, found_path, (size_t)-1)))
			return false;
	} else {
		/* Write empty string to 'arg_pathname' */
		Log::error("crtdll!_searchenv: can't find '%s' in working directory "
			"or %s\n", actual_filename, actual_varname);
		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				arg_pathname, "", 1)))
			return false;
	}

error:
	delete[] actual_varname;
	delete[] actual_filename;
	return true;
}

/*
 * void free(void *p);
 *
 * Frees p, allocated with malloc()/calloc()/strdup()/...
 */
bool CRTDLL::_Internal::Ifree(CRTDLL &module)
{
	unsigned int arg_p;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_p);

	/* Free memory */
	if (arg_p) {
		if (!ntdll->heap_free(module._environment.get_process_heap(), arg_p)) {
			Log::error("crtdll!free: heap deallocation failed\n");
			return false;
		}
	}
	return true;
}

/*
 * char *_fullpath(char *abspath, const char *relpath, unsigned int size);
 *
 * Converts a relative path to an absolute one.
 */
bool CRTDLL::_Internal::I_fullpath(CRTDLL &module)
{
	bool ret = false;
	unsigned int arg_abspath;
	unsigned int arg_relpath;
	unsigned int arg_size;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_abspath);
	module._callconv.get_function_argument(1, arg_relpath);
	module._callconv.get_function_argument(2, arg_size);

	AddressSpace *const memory = module._environment.get_memory();

	/* Is relpath empty? */
	bool relpath_empty = !arg_relpath;
	if (!relpath_empty) {
		char c;
		if (!MEMOP_OK(virtual_memory_read(memory->get_virtmem(), arg_relpath,
				&c, 1)))
			return false;
		relpath_empty = !c;
	}

	/* If it is, do a getcwd() into abspath */
	if (relpath_empty) {
		// FIXME: return getcwd(arg_abspath, arg_size);
		Log::error("crtdll!_fullpath: relpath is empty\n");
		return false;
	}

	/* If the caller does not provide a buffer, allocate one */
	memory_address_t dest_buffer;
	if (!arg_abspath) {
		// FIXME: buffer = MSVCRT_malloc(MAX_PATH);
		// FIXME: size = MAX_PATH;
		// FIXME: alloced = TRUE;
		Log::error("crtdll!_fullpath: abspath is NULL\n");
		return false;
	} else {
		/* Use caller-provided buffer */
		dest_buffer = arg_abspath;
	}

	/* Reject small buffers */
	if (arg_size < 4) {
		_crtdll_errno = ERANGE;
		module._callconv.set_return_value(0);
		return true;
	}

	/* Read relative path */
	char *actual_relpath;
	if (memory->read_string(arg_relpath, actual_relpath) !=
			MEMORY_STATUS_OK)
		return false;

	/* Resolve pathname */
	char *unused, *temp_buffer = new char[arg_size];
	DWORD rc = GetFullPathNameA(actual_relpath, arg_size, temp_buffer,
		&unused);

	if (rc > 0 && rc < arg_size) {
		/* OK, copy to destination buffer (allocation or abspath) */
		if (!MEMOP_OK(virtual_memory_write_string(memory->get_virtmem(),
				dest_buffer, temp_buffer, rc + 1)))
			goto error;

		/* Return buffer address */
		module._callconv.set_return_value((unsigned int)dest_buffer);
	} else {
		// FIXME: free allocated buffer

		module._callconv.set_return_value(0);
	}

	ret = true;
error:
	delete[] temp_buffer;
	delete[] actual_relpath;
	return ret;
}

/*
 * void *memcpy(void *dest, const void *source, size_t size);
 *
 * Copies 'size' bytes from 'source' to 'dest'.
 */
bool CRTDLL::_Internal::Imemcpy(CRTDLL &module)
{
	unsigned int arg_dest;
	unsigned int arg_source;
	unsigned int arg_size;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_dest);
	module._callconv.get_function_argument(1, arg_source);
	module._callconv.get_function_argument(2, arg_size);

	AddressSpace *const memory = module._environment.get_memory();

	/* Do it */
	if (virtual_memory_copy(memory->get_virtmem(), arg_dest, arg_source,
			arg_size) != MEMORY_STATUS_OK) {
		Log::error("crtdll!memcpy(0x%X, 0x%X, 0x%X) failed\n", arg_dest,
			arg_source, arg_size);
		return false;
	}

	/* Return destination address */
	module._callconv.set_return_value(arg_dest);
	return true;
}

/*
 * int memcmp(const void *ptr1, const void *ptr2, size_t count);
 *
 * Compares two blobs of size 'count' for equality.
 */
bool CRTDLL::_Internal::Imemcmp(CRTDLL &module)
{
	unsigned int arg_ptr1;
	unsigned int arg_ptr2;
	unsigned int arg_count;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_ptr1);
	module._callconv.get_function_argument(1, arg_ptr2);
	module._callconv.get_function_argument(2, arg_count);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	unsigned char first = 0, last = 0;
	if (arg_count) {
		do {
			if (!MEMOP_OK(virtual_memory_read(memory, arg_ptr1, &first, 1))) {
				Log::error("crtdll!memcmp read ptr1 failed\n");
				return false;
			}
			if (!MEMOP_OK(virtual_memory_read(memory, arg_ptr2, &last, 1))) {
				Log::error("crtdll!memcmp read ptr2 failed\n");
				return false;
			}
			++arg_ptr1;
			++arg_ptr2;
		} while (--arg_count && first == last);
	}

	/* Difference between characters is the result */
	module._callconv.set_return_value(first - last);
	return true;
}

/*
 * char *strncpy(char *destination, const char * source, size_t size);
 *
 * Copies string 'source' to 'destination'.
 */
bool CRTDLL::_Internal::Istrncpy(CRTDLL &module)
{
	unsigned int arg_destination;
	unsigned int arg_source;
	unsigned int arg_size;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_destination);
	module._callconv.get_function_argument(1, arg_source);
	module._callconv.get_function_argument(2, arg_size);

	/* Set the return value already */
	module._callconv.set_return_value(arg_destination);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	/* Copy character by character */
	if (arg_size) {
		char c;
		do {
			if (!MEMOP_OK(virtual_memory_read(memory, arg_source, &c, 1))) {
				Log::error("crtdll!strncpy: reading src failed\n");
				return false;
			}
			if (!MEMOP_OK(virtual_memory_write(memory, arg_destination,
					&c, 1))) {
				Log::error("crtdll!strncpy: writing dest failed\n");
				return false;
			}
			++arg_source;
			++arg_destination;
		} while (--arg_size && c);
	}

	return true;
}

/*
 * char *strcat(char *dest, const char *src);
 *
 * Appends 'src' to 'dest'.
 */
bool CRTDLL::_Internal::Istrcat(CRTDLL &module)
{
	unsigned int arg_destination;
	unsigned int arg_source;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_destination);
	module._callconv.get_function_argument(1, arg_source);

	struct VirtualMemoryContext *const memory =
		module._environment.get_memory()->get_virtmem();

	/* Set the return value already */
	module._callconv.set_return_value(arg_destination);

	/* Get destination length and adjust */
	size_t dest_length;
	if (!MEMOP_OK(virtual_memory_read_string(memory, arg_destination, NULL,
			&dest_length))) {
		Log::error("crtdll!strcat: can't determine destination length\n");
		return false;
	}
	arg_destination += dest_length;

	/* Copy character by character */
	char c;
	do {
		if (!MEMOP_OK(virtual_memory_read(memory, arg_source, &c, 1))) {
			Log::error("crtdll!strcpy: reading src failed\n");
			return false;
		}
		if (!MEMOP_OK(virtual_memory_write(memory, arg_destination, &c, 1))) {
			Log::error("crtdll!strcpy: writing dest failed\n");
			return false;
		}
		++arg_source;
		++arg_destination;
	} while (c);

	return true;
}
/*
 * void *memset(void *dest, int c, size_t size);
 *
 * Fills 'dest' 'size' times with byte 'c'.
 */
bool CRTDLL::_Internal::Imemset(CRTDLL &module)
{
	unsigned int arg_dest;
	unsigned int arg_c;
	unsigned int arg_size;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_dest);
	module._callconv.get_function_argument(1, arg_c);
	module._callconv.get_function_argument(2, arg_size);

	AddressSpace *const memory = module._environment.get_memory();

	/* Do it */
	if (virtual_memory_set(memory->get_virtmem(), arg_dest, arg_c & 0xFFu,
			arg_size) != MEMORY_STATUS_OK) {
		Log::error("crtdll!memset(0x%X, 0x%X, 0x%X) failed\n", arg_dest,
			arg_c & 0xFFu, arg_size);
		return false;
	}

	/* Return destination address */
	module._callconv.set_return_value(arg_dest);
	return true;
}

/*
 * void *realloc(void *p, size_t size);
 *
 * Reallocates 'p' to have 'size' bytes.
 */
bool CRTDLL::_Internal::Irealloc(CRTDLL &module)
{
	unsigned int arg_p;
	unsigned int arg_size;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_p);
	module._callconv.get_function_argument(1, arg_size);

	/* Allocate memory */
	memory_address_t address = arg_p;
	bool ret;
	if (!address) {
		ret = ntdll->heap_alloc(module._environment.get_process_heap(),
			arg_size, address);
	} else {
		ret = ntdll->heap_realloc(module._environment.get_process_heap(),
			arg_size, address);
	}
	if (!ret) {
		Log::error("crtdll!realloc: heap allocation failed\n");
		return false;
	}

	module._callconv.set_return_value((unsigned int)address);
	return true;
}


/*
 * CRTDLL implementation
 */

CRTDLL::CRTDLL(CallingConventionNTPowerPC &callconv,
		Win32Environment &environment) :
	Win32Module(environment, "crtdll.dll", 0x7E000000u),
	_callconv(callconv)
{
	struct VirtualMemoryContext *const memory =
		_environment.get_memory()->get_virtmem();

	/* Initialize internal structure */
	_internal = new _Internal(environment);

	/* Allocate memory for DLL variables */
	_internal->_memory_variables = _image_base;
	if (virtual_memory_allocate(memory, NULL, &_internal->_memory_variables,
			MEMORY_PAGE_SIZE, (enum MemoryProtection)(
			MEMORY_PROT_READ | MEMORY_PROT_WRITE)) < 0) {
		Log::error("[CRTDLL] Can't allocate variables\n");
		exit(1);
	}

	/* Allocate memory for argv and envp */
	_internal->_memory_argv_envp = _image_base + MEMORY_PAGE_SIZE;
	if (virtual_memory_allocate(memory, NULL, &_internal->_memory_argv_envp,
			2 * MEMORY_PAGE_SIZE, (enum MemoryProtection)(
			MEMORY_PROT_READ | MEMORY_PROT_WRITE)) < 0) {
		Log::error("[CRTDLL] Can't allocate program arguments\n");
		exit(1);
	}

	/* Map _ctype array */
	_internal->_memory_arrays = _image_base + 3 * MEMORY_PAGE_SIZE;
	if (virtual_memory_allocate(memory, (void *)CRTDLL::_Internal::ctype_array,
			&_internal->_memory_arrays, MEMORY_PAGE_SIZE,
			(enum MemoryProtection)(MEMORY_PROT_READ |
			MEMORY_PROT_WRITE)) < 0) {
		Log::error("[CRTDLL] Can't map ctype array\n");
		exit(1);
	}

	CPUContext *const cpu = _environment.get_cpu();

	/* Write values to variables */
	if (!MEMOP_OK(cpu_powerpc_write_32(cpu,
			_internal->_memory_variables + 0x8, 1)))
		exit(1);
	if (!MEMOP_OK(cpu_powerpc_write_32(cpu,
			_internal->_memory_variables + 0xC,
			(unsigned int)_internal->_memory_arrays + 0x2)))
		exit(1);

	/* Assign addresses */
#define ASSIGN(var, addr) \
	_internal->V##var = (unsigned int)(_internal->_memory_variables + (addr))

	ASSIGN(_commode_dll, 0x0);
	ASSIGN(_fmode_dll, 0x4);
	ASSIGN(__mb_cur_max_dll, 0x8);
	ASSIGN(_pctype_dll, 0xC);
	ASSIGN(_iob, 0x10);			// reserve 32 * 0x20 bytes

#undef ASSIGN

	/* Register functions */
	callconv.allocate_syscall(0x800, _internal->F__GetMainArgs);
	callconv.allocate_syscall(0x801, _internal->F_initterm);
	callconv.allocate_syscall(0x802, _internal->Fatexit);
	callconv.allocate_syscall(0x803, _internal->Fsprintf);
	callconv.allocate_syscall(0x804, _internal->Fstrncmp);
	callconv.allocate_syscall(0x805, _internal->Fgetenv);
	callconv.allocate_syscall(0x806, _internal->Fcalloc);
	callconv.allocate_syscall(0x807, _internal->F_splitpath);
	callconv.allocate_syscall(0x808, _internal->F_strupr);
	callconv.allocate_syscall(0x809, _internal->F_strnicmp);
	callconv.allocate_syscall(0x80A, _internal->Fsetvbuf);
	callconv.allocate_syscall(0x80B, _internal->F_stricmp);
	callconv.allocate_syscall(0x80C, _internal->Fstrcpy);
	callconv.allocate_syscall(0x80D, _internal->Fstrchr);
	callconv.allocate_syscall(0x80E, _internal->Fstrlen);
	callconv.allocate_syscall(0x80F, _internal->Fmalloc);
	callconv.allocate_syscall(0x810, _internal->F_strdup);
	callconv.allocate_syscall(0x811, _internal->Fstrcmp);
	callconv.allocate_syscall(0x812, _internal->Fprintf);
	callconv.allocate_syscall(0x813, _internal->Ffflush);
	callconv.allocate_syscall(0x814, _internal->Fputs);
	callconv.allocate_syscall(0x815, _internal->Fexit);
	callconv.allocate_syscall(0x816, _internal->Fstrpbrk);
	callconv.allocate_syscall(0x817, _internal->F_access);
	callconv.allocate_syscall(0x818, _internal->F_searchenv);
	callconv.allocate_syscall(0x819, _internal->Ffree);
	callconv.allocate_syscall(0x81A, _internal->F_fullpath);
	callconv.allocate_syscall(0x81B, _internal->Fmemcpy);
	callconv.allocate_syscall(0x81C, _internal->Fmemcmp);
	callconv.allocate_syscall(0x81D, _internal->Fstrncpy);
	callconv.allocate_syscall(0x81E, _internal->Fstrcat);
	callconv.allocate_syscall(0x81F, _internal->Fmemset);
	callconv.allocate_syscall(0x820, _internal->Frealloc);

	Log::trace("[CRTDLL] loaded\n");
}

CRTDLL::~CRTDLL(void)
{
	struct VirtualMemoryContext *const memory =
		_environment.get_memory()->get_virtmem();

	/* Unmap allocated memory */
	virtual_memory_free(memory, _internal->_memory_arrays);
	virtual_memory_free(memory, _internal->_memory_argv_envp);
	virtual_memory_free(memory, _internal->_memory_variables);

	Log::trace("[CRTDLL] unloaded\n");

	/* Delete internal structure */
	delete _internal;
}

bool CRTDLL::get_function_address(const char *function_name,
	memory_address_t &function_address) const
{
	/* Standard crtdll variables */
	if (!strcmp(function_name, "_commode_dll")) {
		function_address = _internal->V_commode_dll;
		return true;
	}
	else if (!strcmp(function_name, "_fmode_dll")) {
		function_address = _internal->V_fmode_dll;
		return true;
	}
	else if (!strcmp(function_name, "_pctype_dll")) {
		function_address = _internal->V_pctype_dll;
		return true;
	}
	else if (!strcmp(function_name, "__mb_cur_max_dll")) {
		function_address = _internal->V__mb_cur_max_dll;
		return true;
	}
	else if (!strcmp(function_name, "_iob")) {
		function_address = _internal->V_iob;
		return true;
	}

	/* Standard crtdll functions */
	else if (!strcmp(function_name, "__GetMainArgs")) {
		function_address = _internal->F__GetMainArgs;
		return true;
	}
	else if (!strcmp(function_name, "_initterm")) {
		function_address = _internal->F_initterm;
		return true;
	}
	else if (!strcmp(function_name, "atexit")) {
		function_address = _internal->Fatexit;
		return true;
	}
	else if (!strcmp(function_name, "sprintf")) {
		function_address = _internal->Fsprintf;
		return true;
	}
	else if (!strcmp(function_name, "strncmp")) {
		function_address = _internal->Fstrncmp;
		return true;
	}
	else if (!strcmp(function_name, "getenv")) {
		function_address = _internal->Fgetenv;
		return true;
	}
	else if (!strcmp(function_name, "calloc")) {
		function_address = _internal->Fcalloc;
		return true;
	}
	else if (!strcmp(function_name, "_splitpath")) {
		function_address = _internal->F_splitpath;
		return true;
	}
	else if (!strcmp(function_name, "_strupr")) {
		function_address = _internal->F_strupr;
		return true;
	}
	else if (!strcmp(function_name, "_strnicmp")) {
		function_address = _internal->F_strnicmp;
		return true;
	}
	else if (!strcmp(function_name, "setvbuf")) {
		function_address = _internal->Fsetvbuf;
		return true;
	}
	else if (!strcmp(function_name, "_stricmp")) {
		function_address = _internal->F_stricmp;
		return true;
	}
	else if (!strcmp(function_name, "strcpy")) {
		function_address = _internal->Fstrcpy;
		return true;
	}
	else if (!strcmp(function_name, "strchr")) {
		function_address = _internal->Fstrchr;
		return true;
	}
	else if (!strcmp(function_name, "strlen")) {
		function_address = _internal->Fstrlen;
		return true;
	}
	else if (!strcmp(function_name, "malloc")) {
		function_address = _internal->Fmalloc;
		return true;
	}
	else if (!strcmp(function_name, "_strdup")) {
		function_address = _internal->F_strdup;
		return true;
	}
	else if (!strcmp(function_name, "strcmp")) {
		function_address = _internal->Fstrcmp;
		return true;
	}
	else if (!strcmp(function_name, "printf")) {
		function_address = _internal->Fprintf;
		return true;
	}
	else if (!strcmp(function_name, "fflush")) {
		function_address = _internal->Ffflush;
		return true;
	}
	else if (!strcmp(function_name, "puts")) {
		function_address = _internal->Fputs;
		return true;
	}
	else if (!strcmp(function_name, "exit")) {
		function_address = _internal->Fexit;
		return true;
	}
	else if (!strcmp(function_name, "strpbrk")) {
		function_address = _internal->Fstrpbrk;
		return true;
	}
	else if (!strcmp(function_name, "_access")) {
		function_address = _internal->F_access;
		return true;
	}
	else if (!strcmp(function_name, "_searchenv")) {
		function_address = _internal->F_searchenv;
		return true;
	}
	else if (!strcmp(function_name, "free")) {
		function_address = _internal->Ffree;
		return true;
	}
	else if (!strcmp(function_name, "_fullpath")) {
		function_address = _internal->F_fullpath;
		return true;
	}
	else if (!strcmp(function_name, "memcpy")) {
		function_address = _internal->Fmemcpy;
		return true;
	}
	else if (!strcmp(function_name, "memcmp")) {
		function_address = _internal->Fmemcmp;
		return true;
	}
	else if (!strcmp(function_name, "strncpy")) {
		function_address = _internal->Fstrncpy;
		return true;
	}
	else if (!strcmp(function_name, "strcat")) {
		function_address = _internal->Fstrcat;
		return true;
	}
	else if (!strcmp(function_name, "memset")) {
		function_address = _internal->Fmemset;
		return true;
	}
	else if (!strcmp(function_name, "realloc")) {
		function_address = _internal->Frealloc;
		return true;
	}

	// FIXME!
	Log::warn("[CRTDLL] unknown function '%s'\n", function_name);
	function_address = 0xC0DEC0DEu;
	return true;
}

void CRTDLL::get_syscall_range(unsigned int &first, unsigned int &last) const
{
	first = 0x800;
	last = 0x8FF;
}

bool CRTDLL::handle_syscall(unsigned int syscall_number)
{
	switch (syscall_number) {
		case 0x800:
			return _internal->I__GetMainArgs(*this);
		case 0x801:
			return _internal->I_initterm(*this);
		case 0x802:
			return _internal->Iatexit(*this);
		case 0x803:
			return _internal->Isprintf(*this);
		case 0x804:
			return _internal->Istrncmp(*this);
		case 0x805:
			return _internal->Igetenv(*this);
		case 0x806:
			return _internal->Icalloc(*this);
		case 0x807:
			return _internal->I_splitpath(*this);
		case 0x808:
			return _internal->I_strupr(*this);
		case 0x809:
			return _internal->I_strnicmp(*this);
		case 0x80A:
			return _internal->Isetvbuf(*this);
		case 0x80B:
			return _internal->I_stricmp(*this);
		case 0x80C:
			return _internal->Istrcpy(*this);
		case 0x80D:
			return _internal->Istrchr(*this);
		case 0x80E:
			return _internal->Istrlen(*this);
		case 0x80F:
			return _internal->Imalloc(*this);
		case 0x810:
			return _internal->I_strdup(*this);
		case 0x811:
			return _internal->Istrcmp(*this);
		case 0x812:
			return _internal->Iprintf(*this);
		case 0x813:
			return _internal->Ifflush(*this);
		case 0x814:
			return _internal->Iputs(*this);
		case 0x815:
			return _internal->Iexit(*this);
		case 0x816:
			return _internal->Istrpbrk(*this);
		case 0x817:
			return _internal->I_access(*this);
		case 0x818:
			return _internal->I_searchenv(*this);
		case 0x819:
			return _internal->Ifree(*this);
		case 0x81A:
			return _internal->I_fullpath(*this);
		case 0x81B:
			return _internal->Imemcpy(*this);
		case 0x81C:
			return _internal->Imemcmp(*this);
		case 0x81D:
			return _internal->Istrncpy(*this);
		case 0x81E:
			return _internal->Istrcat(*this);
		case 0x81F:
			return _internal->Imemset(*this);
		case 0x820:
			return _internal->Irealloc(*this);
	}

	/* Unsupported system call */
	Log::error("[CRTDLL] handle_syscall 0x%X\n", syscall_number);
	return false;
}

bool CRTDLL::dll_main(unsigned int reason)
{
#if 0
	if (reason == 1) {
		// process attach
	}
	else if (reason == 0) {
		// process detach
	}
	else if (reason == 2) {
		// thread attach
	}
	else if (reason == 3) {
		// thread detach
	}
#else
	(void)reason;
#endif
	return true;
}

/*
 * CRTDLL class-specific functions
 */
}
