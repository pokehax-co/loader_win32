/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "win32mod.hpp"

#include "../misc/log.hpp"
#include "../exefmt/pe.hpp"
#include "win32.hpp"
#include "ntppc.hpp"
#include "../mem.hpp"
#include "../hw/powerpc.h"

#include <stdlib.h>
#include <string.h>


/*
 * Win32PEModule
 */

Win32PEModule::Win32PEModule(CallingConventionNTPowerPC &callconv,
		Win32Environment &environment) :
	Win32Module(environment, NULL, 0ULL),
	_callconv(callconv),
	_exe(NULL)
{
}

Win32PEModule::~Win32PEModule(void)
{
	_module_name = NULL;
	if (_exe) {
		// FIXME: unmap module from memory if present
		delete _exe;
		_exe = NULL;
	}
}

bool Win32PEModule::get_function_address(const char *function_name,
	memory_address_t &function_address) const
{
	/* Must have module loaded */
	if (!_exe)
		return false;

	/* Pass to PEExecutable */
	function_address = _exe->get_exported_symbol_address(function_name);
	return function_address > 0;
}

bool Win32PEModule::dll_main(unsigned int reason)
{
	/* Is the module a DLL? If not, there's no DllMain function */
	if (!_exe->is_shared_library())
		return true;

	/* Read its entry point */
	const PEOptionalHeader *const optional_header =
		_exe->get_optional_header();
	const memory_address_t entry_point = (memory_address_t)(
		optional_header->image_base + optional_header->address_of_entry_point
	);

	CPUContext *const cpu = _environment.get_cpu();

	/*
	 * Set up arguments for DllMain:
	 *  - r3: hInstDLL (same as image base)
	 *  - r4: Reason (same as original DllMain)
	 *  - r5: NULL (it's reserved anyway)
	 */
	cpu_set_register(cpu, PPC_REGISTER_R3, optional_header->image_base);
	cpu_set_register(cpu, PPC_REGISTER_R4, reason);
	cpu_set_register(cpu, PPC_REGISTER_R5, 0);

	/* Set stack pointer */
	memory_address_t stack_top, stack_bottom;
	_environment.get_stack_limits(stack_top, stack_bottom);
	cpu_set_register(cpu, PPC_REGISTER_R1, stack_top - 4);

#ifdef DEBUG_DLLMAIN
	Log::trace("==== Calling %s!DllMain ====\n",
		pemod->get_module_name());
#endif

	/* Call DllMain */
	unsigned int ret;
	if (!_callconv.call_function(entry_point, &ret))
		return false;

#ifdef DEBUG_DLLMAIN
	Log::trace("==== Returned from %s!DllMain ====\n",
		pemod->get_module_name());
#endif

	/* Pass original return value */
	return !!ret;
}

bool Win32PEModule::load_module(PEExecutable *const exe)
{
	/* We only support PowerPC executables */
	const PEFileHeader *const file_header = exe->get_file_header();
	const unsigned short machine = file_header->machine;
	if (machine != 0x1F0) {
		Log::error("module machine is 0x%X, not PowerPC (0x1F0)\n", machine);
		return false;
	}

	/* Must run under the Win32 subsystem (console or GUI) */
	const PEOptionalHeader *const optional_header =
		exe->get_optional_header();
	switch (optional_header->subsystem) {
		case 2:
		case 3:
			break;
		default:
			Log::error("module is not for Win32\n");
			return false;
	}

	/* Map the module into the environment's address space */
	if (!exe->map_sections(_environment.get_memory()->get_virtmem())) {
		Log::error("mapping module into address space failed\n");
		return false;
	}

	/* We can now close the underlying file handle */
	exe->close_file();

	/*
	 * The Win32 environment tracks any additional modules loaded into the
	 * process' address space. It provides callbacks so that symbols from
	 * other modules can be resolved.
	 */
	exe->set_import_function_callback(Win32Environment::pe_function_callback,
		&_environment);
	exe->set_import_module_callback(Win32Environment::pe_module_callback,
		&_environment);

	/* Resolve imports */
	if (!exe->resolve_imports(_environment.get_memory()->get_virtmem())) {
		Log::error("import resolution failed\n");
		return false;
	}

	/* Load exports */
	if (!exe->resolve_exports(_environment.get_memory()->get_virtmem())) {
		Log::error("export resolution failed (even if the module has no "
			"exports)\n");
		return false;
	}

	/* Load module filename and image base */
	_image_base = (memory_address_t)optional_header->image_base;

	const char *exe_full_path = exe->get_exe_path();
	_module_name = exe_full_path + strlen(exe_full_path);
	while (_module_name > exe_full_path) {
		if (_module_name[-1] == '/' || _module_name[-1] == '\\')
			break;
		--_module_name;
	}

	/* Export it */
	_exe = exe;
	return true;
}
