/*
 * Copyright (c) 2011-2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

/*
 * This printf() implementation is derived from code I've written earlier
 * in the past, so it might not be up to the latest coding standards.
 */

#include "../config.h"
#include "printf.h"

#include <stdio.h>
#include <string.h>
#include <assert.h>


/*
 * Stuff
 */

enum _PrintArgumentSize
{
	PRINT_SIZE_CHAR,
	PRINT_SIZE_SHORT,
	PRINT_SIZE_INT,
	PRINT_SIZE_LONG,
	PRINT_SIZE_LONGLONG,
	PRINT_SIZE_INTMAX,
	PRINT_SIZE_SIZE,
	PRINT_SIZE_PTRDIFF,
	PRINT_SIZE_DOUBLE,
	PRINT_SIZE_LONGDOUBLE
};

struct _PrintfArgumentValue
{
	enum _PrintArgumentSize size;		/* More a type than a size */
};

#define GET_ARG(type) \
	(ctx->va_arg_cb(args_start++, ctx->va_arg_cb_user))


/*
 * Private functions
 */

static unsigned int _write_char(struct MSVCPrintfContext *ctx, int c,
	const unsigned int unicode)
{
	(void)unicode;

	/* Call the write callback */
	if (ctx->write_cb(c, ctx->write_cb_user) == EOF)
		return 0;

	++ctx->written_chars;
	return 1;
}

static unsigned int _print_string(struct MSVCPrintfContext *ctx,
	enum _PrintArgumentSize size, const unsigned int flags,
	memory_address_t p, const unsigned int use_precision,
	int precision, const int field_width)
{
	const unsigned int is_wide = size == PRINT_SIZE_LONG;
	char sc;

	// XXX: support these
	if (is_wide || !p || flags != 0 || field_width != -1 || use_precision ||
			precision != 0) {
		fprintf(stderr, "msvc_printf(): need to print string with "
			"unsupported parameters\n");
		return 0;
	}

	/* Read string until null character or limit reached */
	for (;;) {
		if (virtual_memory_read(ctx->memory, p++, &sc, 1) !=
				MEMORY_STATUS_OK) {
			fprintf(stderr, "msvc_printf(): can't read string\n");
			return 0;
		}
		if (!sc)
			break;
		if (!_write_char(ctx, sc, 0))
			return 0;
	}

	return 1;
}


/*
 * Public interface
 */

void msvc_printf_init(struct MSVCPrintfContext *ctx)
{
	memset(ctx, 0, sizeof(*ctx));
}

int msvc_printf(struct MSVCPrintfContext *ctx, const char *format,
	unsigned int args_start)
{
	int ret = -1;

	/* Initialize context */
	ctx->written_chars = 0u;

	/* Assertions */
	assert(ctx->write_cb);
	assert(ctx->va_arg_cb);

	// XXX: Retrieve locale information from the C library

	/* Loop until finished */
	while (*format) {
		struct _PrintfArgumentValue argval = {0};
		unsigned int num_flags = 0;
		int field_width = -1;
		unsigned int use_precision = 0;
		int precision = 0;
		const char *pos = format;

		argval.size = PRINT_SIZE_INT;

		/* Direct stream output on all non-% characters */
		if (*format != '%') {
			if (!_write_char(ctx, *format++, 0))
				goto error;
			continue;
		}
		++format;

		// XXX: flags
		// XXX: field width
		// XXX: precision
		// XXX: length

		/* C99: "A conversion specifier character that specifies the type of
           conversion to be applied." */
		switch (*format) {
			memory_address_t valuep;

			case 'd': case 'i': case 'o': case 'u': case 'x': case 'X':
				// XXX: Integer
				goto not_implemented;

			case 'e': case 'E': case 'f': case 'g': case 'G':
				// XXX: Floating-point number
				goto not_implemented;

			case 'c': case 'C':
				// XXX: Single character
				goto not_implemented;

			case 's': case 'S':
				/* 'S' = 'ls' */
				if (*format == 'S')
					argval.size = PRINT_SIZE_LONG;

				/* Print string */
				valuep = GET_ARG(void *);
				if (!_print_string(ctx, argval.size, num_flags, valuep,
						use_precision, precision, field_width))
					goto error;
				break;

			case 'p':
				// XXX: Pointer
				goto not_implemented;

			case 'n':
				// XXX: Written characters so far
				goto not_implemented;

			case 'Z':
				// XXX: NT UNICODE_STRING
				goto not_implemented;

            default:
                /* If the character is not a % character, rewind */
				if (*format != '%')
                    format = pos;

                /* Print current character */
                if (!_write_char(ctx, *format, 0))
                    goto error;
                break;
		}
		++format;
	}

	/* Success */
	ret = ctx->written_chars;

error:
	/* Clean up */
	return ret;

not_implemented:
	fprintf(stderr, "msvc_printf(): unsupported format '%c'\n",
		*format);
	goto error;
}
