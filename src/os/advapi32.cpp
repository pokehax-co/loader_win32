/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "advapi32.hpp"

#include "../misc/log.hpp"
#include "ntppc.hpp"
#include "win32.hpp"
#include "../mem.hpp"
#include "../misc/tools.h"

#include <string.h>

#define _WIN32_LEAN_AND_MEAN
#include <windows.h>


namespace Win32DLLs {

/*
 * ADVAPI32 internals
 */

struct ADVAPI32::_Internal
{
	/* Addresses to system call trampolines */
	unsigned int FRegOpenKeyExA;
	unsigned int FRegCreateKeyExA;
	unsigned int FRegQueryValueExA;
	unsigned int FRegSetValueExA;
	unsigned int FRegCloseKey;

	_Internal(Win32Environment &environment)
	{
		(void)environment;
	}

	/* API function implementations */
	bool IRegOpenKeyExA(ADVAPI32 &module);
	bool IRegCreateKeyExA(ADVAPI32 &module);
	bool IRegQueryValueExA(ADVAPI32 &module);
	bool IRegSetValueExA(ADVAPI32 &module);
	bool IRegCloseKey(ADVAPI32 &module);
};

/*
 * LSTATUS WINAPI RegOpenKeyExA(HKEY hKey, LPCSTR lpSubKey, DWORD ulOptions,
 *     REGSAM samDesired, PHKEY phkResult);
 */
bool ADVAPI32::_Internal::IRegOpenKeyExA(ADVAPI32 &module)
{
	unsigned int arg_hKey;
	unsigned int arg_lpSubKey;
	unsigned int arg_ulOptions;
	unsigned int arg_samDesired;
	unsigned int arg_phkResult;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hKey);
	module._callconv.get_function_argument(1, arg_lpSubKey);
	module._callconv.get_function_argument(2, arg_ulOptions);
	module._callconv.get_function_argument(3, arg_samDesired);
	module._callconv.get_function_argument(4, arg_phkResult);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read subkey name */
	char *actual_lpSubKey = NULL;
	if (arg_lpSubKey) {
		if (!MEMOP_OK(memory->read_string(arg_lpSubKey, actual_lpSubKey)))
			return false;
	}

#ifdef DEBUG_ADVAPI32
	Log::trace("advapi32!RegOpenKeyExA(0x%X, '%s', 0x%X, 0x%X, 0x%X)\n",
		arg_hKey, actual_lpSubKey ? actual_lpSubKey : "<null>", arg_ulOptions,
		arg_samDesired, arg_phkResult);
#endif

	HKEY opened_key;
	LONG ret;

#ifdef CONFIG_ACTIVATE_C8_5
	/* Is this the "Motorola" key? Return a fake key */
	if (!_stricmp(actual_lpSubKey, "SOFTWARE\\Motorola\\Compiler\\8.50")) {
		opened_key = (HKEY)0x44441234u;
		ret = 0;
	} else {
#endif
		ret = RegOpenKeyExA((HKEY)arg_hKey, actual_lpSubKey, arg_ulOptions,
			arg_samDesired, &opened_key);
#ifdef CONFIG_ACTIVATE_C8_5
	}
#endif

#ifdef DEBUG_ADVAPI32
	Log::trace(" -> returning %lu\n", ret);
#endif

	/* Write key back */
	if (ret == 0) {
#ifdef DEBUG_ADVAPI32
		Log::trace(" -> *phkResult = 0x%p\n", opened_key);
#endif

		if (!MEMOP_OK(virtual_memory_write(memory->get_virtmem(),
				arg_phkResult, &opened_key, sizeof(opened_key)))) {
			RegCloseKey(opened_key);
			delete[] actual_lpSubKey;
			return false;
		}
	}

	/* Clean up */
	module._callconv.set_return_value((unsigned int)ret);
	delete[] actual_lpSubKey;
	return true;
}

/*
 * LSTATUS WINAPI RegCreateKeyExA(HKEY hKey, LPCSTR lpSubKey, DWORD Reserved,
 *     LPSTR lpClass, DWORD dwOptions, REGSAM samDesired,
 *     CONST LPSECURITY_ATTRIBUTES lpSecurityAttributes, PHKEY phkResult,
 *     LPDWORD lpdwDisposition);
 */
bool ADVAPI32::_Internal::IRegCreateKeyExA(ADVAPI32 &module)
{
	bool success = false;
	unsigned int arg_hKey;
	unsigned int arg_lpSubKey;
	unsigned int arg_Reserved;
	unsigned int arg_lpClass;
	unsigned int arg_dwOptions;
	unsigned int arg_samDesired;
	unsigned int arg_lpSecurityAttributes;
	unsigned int arg_phkResult;
	unsigned int arg_lpdwDisposition;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hKey);
	module._callconv.get_function_argument(1, arg_lpSubKey);
	module._callconv.get_function_argument(2, arg_Reserved);
	module._callconv.get_function_argument(3, arg_lpClass);
	module._callconv.get_function_argument(4, arg_dwOptions);
	module._callconv.get_function_argument(5, arg_samDesired);
	module._callconv.get_function_argument(6, arg_lpSecurityAttributes);
	module._callconv.get_function_argument(7, arg_phkResult);
	module._callconv.get_function_argument(8, arg_lpdwDisposition);

	/* We don't support security attributes (they have to be copied) */
	if (arg_lpSecurityAttributes) {
		Log::error("advapi32!RegCreateKeyExA: security attributes not "
			"supported\n");
		return false;
	}

	AddressSpace *const memory = module._environment.get_memory();

	/* Read subkey name */
	char *actual_lpSubKey = NULL;
	if (arg_lpSubKey) {
		if (!MEMOP_OK(memory->read_string(arg_lpSubKey, actual_lpSubKey)))
			return false;
	}

	/* Read class name */
	char *actual_lpClass = NULL;
	if (arg_lpClass) {
		if (!MEMOP_OK(memory->read_string(arg_lpClass, actual_lpClass))) {
			delete[] actual_lpSubKey;
			return false;
		}
	}

#ifdef DEBUG_ADVAPI32
	Log::trace("advapi32!RegCreateKeyExA(0x%X, '%s', 0x%X, '%s', 0x%X, 0x%X, "
		"0x%X, 0x%X, 0x%X)\n", arg_hKey, actual_lpSubKey ? actual_lpSubKey :
		"<null>", arg_Reserved, actual_lpClass ? actual_lpClass : "<null>",
		arg_dwOptions, arg_samDesired, arg_lpSecurityAttributes,
		arg_phkResult, arg_lpdwDisposition);
#endif

	/* Pass to the original */
	HKEY opened_key;
	DWORD disposition;
	LONG ret = RegCreateKeyExA((HKEY)arg_hKey, actual_lpSubKey,
		arg_Reserved, actual_lpClass, arg_dwOptions, arg_samDesired, NULL,
		&opened_key, &disposition);

#ifdef DEBUG_ADVAPI32
	Log::trace(" -> returning %lu\n", ret);
#endif

	if (ret == 0) {
#ifdef DEBUG_ADVAPI32
		Log::trace(" -> *phkResult = 0x%p\n", opened_key);
		Log::trace(" -> *lpdwDisposition = 0x%lX\n", disposition);
#endif

		/* Write key back */
		if (!MEMOP_OK(virtual_memory_write(memory->get_virtmem(),
				arg_phkResult, &opened_key, sizeof(opened_key)))) {
			RegCloseKey(opened_key);
			goto error;
		}

		/* Write disposition back */
		if (!MEMOP_OK(virtual_memory_write(memory->get_virtmem(),
				arg_lpdwDisposition, &disposition, sizeof(disposition)))) {
			RegCloseKey(opened_key);
			goto error;
		}
	}

	/* Clean up */
	module._callconv.set_return_value((unsigned int)ret);
	success = true;

error:
	delete[] actual_lpClass;
	delete[] actual_lpSubKey;
	return success;
}

/*
 * LSTATUS WINAPI RegQueryValueExA(HKEY hKey, LPCSTR lpValueName,
 *     LPDWORD lpReserved, LPDWORD lpType, LPBYTE lpData, LPDWORD lpcbData);
 */
bool ADVAPI32::_Internal::IRegQueryValueExA(ADVAPI32 &module)
{
	bool success = false;
	unsigned int arg_hKey;
	unsigned int arg_lpValueName;
	unsigned int arg_lpReserved;
	unsigned int arg_lpType;
	unsigned int arg_lpData;
	unsigned int arg_lpcbData;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hKey);
	module._callconv.get_function_argument(1, arg_lpValueName);
	module._callconv.get_function_argument(2, arg_lpReserved);
	module._callconv.get_function_argument(3, arg_lpType);
	module._callconv.get_function_argument(4, arg_lpData);
	module._callconv.get_function_argument(5, arg_lpcbData);

	/* Unsupported combination */
	if (arg_lpData && !arg_lpcbData) {
		Log::error("advapi32!RegQueryValueExA: lpData != NULL but "
			"lpcbData == NULL\n");
		return false;
	}

	AddressSpace *const memory = module._environment.get_memory();

	/* Read value name */
	char *actual_lpValueName = NULL;
	if (arg_lpValueName) {
		if (!MEMOP_OK(memory->read_string(arg_lpValueName,
				actual_lpValueName)))
			return false;
	}

	DWORD actual_cbData = 0ul;
	uint8_t *actual_lpData = NULL;

	if (arg_lpData && arg_lpcbData) {
		/* Read size of data */
		if (!MEMOP_OK(virtual_memory_read(memory->get_virtmem(),
				arg_lpcbData, &actual_cbData, sizeof(actual_cbData)))) {
			delete[] actual_lpValueName;
			return false;
		}

		/* Allocate memory */
		actual_lpData = new uint8_t[actual_cbData];
	}

#ifdef DEBUG_ADVAPI32
	Log::trace("advapi32!RegQueryValueExA(0x%X, '%s', 0x%X, 0x%X, 0x%X, "
		"0x%lX)\n", arg_hKey, actual_lpValueName, arg_lpReserved, arg_lpType,
		arg_lpData, actual_cbData);
#endif

	/*
	 * Is this the Motorola key? Send specifically crafted values as "Serial
	 * Number" and "Verification" which will keep the product activated.
	 * (I'm too lazy to reverse-engineer the algorithm, sorry.)
	 *
	 * The values depend on the installation date, which is set to 1995-05-30,
	 * the release date of Windows NT 3.51.
	 */
	DWORD value_type;
	LONG ret;
#ifdef CONFIG_ACTIVATE_C8_5
	if (arg_hKey == 0x44441234u) {
		if (_stricmp(actual_lpValueName, "Serial Number") == 0) {
			value_type = 4;
			if (actual_lpData)
				write_u32le(actual_lpData, 0x0004949Bu);
			ret = 0;
		}
		else if (_stricmp(actual_lpValueName, "Verification") == 0) {
			value_type = 4;
			if (actual_lpData)
				write_u32le(actual_lpData, 0xC008CB2Fu);
			ret = 0;
		}
		else
			ret = 6;
	} else {
#endif
		/* Pass to the original */
		ret = RegQueryValueExA((HKEY)arg_hKey, actual_lpValueName,
			(DWORD *)arg_lpReserved, &value_type, actual_lpData, arg_lpcbData ?
			&actual_cbData : NULL);
#ifdef CONFIG_ACTIVATE_C8_5
	}
#endif

#ifdef DEBUG_ADVAPI32
	Log::trace(" -> returning %lu\n", ret);
#endif

	if (ret == 0) {
		/* Write data back */
		if (arg_lpData && !MEMOP_OK(virtual_memory_write(
				memory->get_virtmem(), arg_lpData, actual_lpData,
				actual_cbData)))
			goto error;

		/* Write type back */
		if (arg_lpType && !MEMOP_OK(virtual_memory_write(
				memory->get_virtmem(), arg_lpType, &value_type,
				sizeof(value_type))))
			goto error;

		/* Write size back */
		if (arg_lpcbData && !MEMOP_OK(virtual_memory_write(
				memory->get_virtmem(), arg_lpcbData, &actual_cbData,
				sizeof(actual_cbData))))
			goto error;
	}

	module._callconv.set_return_value((unsigned int)ret);
	success = true;

error:
	delete[] actual_lpData;
	delete[] actual_lpValueName;
	return success;
}

/*
 * LSTATUS WINAPI RegSetValueExA(HKEY hKey, LPCSTR lpValueName,
 *     DWORD Reserved, DWORD dwType, CONST BYTE *lpData, DWORD cbData);
 */
bool ADVAPI32::_Internal::IRegSetValueExA(ADVAPI32 &module)
{
	unsigned int arg_hKey;
	unsigned int arg_lpValueName;
	unsigned int arg_Reserved;
	unsigned int arg_dwType;
	unsigned int arg_lpData;
	unsigned int arg_cbData;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hKey);
	module._callconv.get_function_argument(1, arg_lpValueName);
	module._callconv.get_function_argument(2, arg_Reserved);
	module._callconv.get_function_argument(3, arg_dwType);
	module._callconv.get_function_argument(4, arg_lpData);
	module._callconv.get_function_argument(5, arg_cbData);

	AddressSpace *const memory = module._environment.get_memory();

	/* Read value name */
	char *actual_lpValueName = NULL;
	if (arg_lpValueName) {
		if (!MEMOP_OK(memory->read_string(arg_lpValueName,
				actual_lpValueName)))
			return false;
	}

	/* Read data */
	uint8_t *actual_lpData = NULL;
	if (arg_lpData && arg_cbData) {
		actual_lpData = new uint8_t[arg_cbData];

		if (!MEMOP_OK(virtual_memory_read(memory->get_virtmem(), arg_lpData,
				actual_lpData, arg_cbData))) {
			delete[] actual_lpValueName;
			return false;
		}
	}

#ifdef DEBUG_ADVAPI32
	Log::trace("advapi32!RegSetValueExA(0x%X, '%s', 0x%X, 0x%X, 0x%X, "
		"0x%X)\n", arg_hKey, actual_lpValueName, arg_Reserved, arg_dwType,
		arg_lpData, arg_cbData);

	if (actual_lpData) {
		Log::trace(" -> Data at 0x%X:\n", arg_lpData);
		Log::mem(arg_lpData, actual_lpData, arg_cbData);
	}
#endif

	/* Pass to the original */
	LONG ret = RegSetValueExA((HKEY)arg_hKey, actual_lpValueName,
		arg_Reserved, arg_dwType, actual_lpData, arg_cbData);

#ifdef DEBUG_ADVAPI32
	Log::trace(" -> returning %lu\n", ret);
#endif

	module._callconv.set_return_value((unsigned int)ret);
	delete[] actual_lpData;
	delete[] actual_lpValueName;
	return true;
}

/*
 * LSTATUS WINAPI RegCloseKey(HKEY hKey);
 */
bool ADVAPI32::_Internal::IRegCloseKey(ADVAPI32 &module)
{
	unsigned int arg_hKey;

	/* Grab function arguments */
	module._callconv.get_function_argument(0, arg_hKey);

#ifdef DEBUG_ADVAPI32
	Log::trace("advapi32!RegCloseKey(0x%X)\n", arg_hKey);
#endif

	/* Pass to the original */
	LONG ret;
#ifdef CONFIG_ACTIVATE_C8_5
	if (arg_hKey == 0x44441234u)
		ret = 0;
	else
#endif
		ret = RegCloseKey((HKEY)arg_hKey);

#ifdef DEBUG_ADVAPI32
	Log::trace(" -> returning %lu\n", ret);
#endif

	/* Clean up */
	module._callconv.set_return_value((unsigned int)ret);
	return true;
}


/*
 * ADVAPI32 implementation
 */

ADVAPI32::ADVAPI32(CallingConventionNTPowerPC &callconv,
		Win32Environment &environment) :
	Win32Module(environment, "advapi32.dll", 0x7E030000u),
	_callconv(callconv)
{
	/* Initialize internal structure */
	_internal = new _Internal(environment);

	/* Register functions */
	callconv.allocate_syscall(0xA00, _internal->FRegOpenKeyExA);
	callconv.allocate_syscall(0xA01, _internal->FRegCreateKeyExA);
	callconv.allocate_syscall(0xA02, _internal->FRegQueryValueExA);
	callconv.allocate_syscall(0xA03, _internal->FRegSetValueExA);
	callconv.allocate_syscall(0xA04, _internal->FRegCloseKey);

#ifdef DEBUG_ADVAPI32
	Log::trace("[ADVAPI32] loaded\n");
#endif
}

ADVAPI32::~ADVAPI32(void)
{
#ifdef DEBUG_ADVAPI32
	Log::trace("[ADVAPI32] unloaded\n");
#endif

	/* Delete internal structure */
	delete _internal;
}

bool ADVAPI32::get_function_address(const char *function_name,
	memory_address_t &function_address) const
{
	/* Resolve implemented functions */
	if (!strcmp(function_name, "RegOpenKeyExA")) {
		function_address = _internal->FRegOpenKeyExA;
		return true;
	}
	else if (!strcmp(function_name, "RegCreateKeyExA")) {
		function_address = _internal->FRegCreateKeyExA;
		return true;
	}
	else if (!strcmp(function_name, "RegQueryValueExA")) {
		function_address = _internal->FRegQueryValueExA;
		return true;
	}
	else if (!strcmp(function_name, "RegSetValueExA")) {
		function_address = _internal->FRegSetValueExA;
		return true;
	}
	else if (!strcmp(function_name, "RegCloseKey")) {
		function_address = _internal->FRegCloseKey;
		return true;
	}

	// FIXME!
	Log::warn("[ADVAPI32] unknown function '%s'\n", function_name);
	return false;
}

void ADVAPI32::get_syscall_range(unsigned int &first,
	unsigned int &last) const
{
	first = 0xA00;
	last = 0xAFF;
}

bool ADVAPI32::handle_syscall(unsigned int syscall_number)
{
	switch (syscall_number) {
		case 0xA00:
			return _internal->IRegOpenKeyExA(*this);
		case 0xA01:
			return _internal->IRegCreateKeyExA(*this);
		case 0xA02:
			return _internal->IRegQueryValueExA(*this);
		case 0xA03:
			return _internal->IRegSetValueExA(*this);
		case 0xA04:
			return _internal->IRegCloseKey(*this);
	}

	/* Unsupported system call */
	Log::error("[ADVAPI32] handle_syscall 0x%X\n", syscall_number);
	return false;
}

bool ADVAPI32::dll_main(unsigned int reason)
{
#if 0
	if (reason == 1) {
		// process attach
	}
	else if (reason == 0) {
		// process detach
	}
	else if (reason == 2) {
		// thread attach
	}
	else if (reason == 3) {
		// thread detach
	}
#else
	(void)reason;
#endif
	return true;
}

/*
 * ADVAPI32 class-specific functions
 */
}
