/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

/*!
 * \file src/os/ntdll.hpp
 * \brief Implementation of the builtin ntdll DLL
 */

#include "../iface/memory.h"
#include "win32mod.hpp"


/* src/os/ntppc.hpp */
class CallingConventionNTPowerPC;

/* src/os/win32.hpp */
class Win32Environment;


namespace Win32DLLs {

/*
 * NTDLL simulation
 */
class NTDLL : public Win32Module
{
public:
	/* Constructor and deconstructor */
	NTDLL(CallingConventionNTPowerPC &callconv, Win32Environment &environment);
	~NTDLL(void);

	/*
	 * Inherited functions
	 */
	bool get_function_address(const char *function_name,
		memory_address_t &function_address) const;

	void get_syscall_range(unsigned int &first, unsigned int &last) const;

	bool handle_syscall(unsigned int syscall_number);

	bool dll_main(unsigned int reason);

	/*
	 * Class-specific functions
	 */
	bool heap_create(unsigned int flags, unsigned int total_size,
		unsigned int commit_size, memory_address_t &heap_address);

	bool heap_alloc(memory_address_t heap_address, unsigned int size,
		memory_address_t &allocation_address, bool zero = false);

	bool heap_realloc(memory_address_t heap_address, unsigned int size,
		memory_address_t &allocation_address);

	bool heap_size(memory_address_t heap_address,
		memory_address_t allocation_address, unsigned int &allocation_size);

	bool heap_free(memory_address_t heap_address,
		memory_address_t allocation_address);

	bool heap_destroy(memory_address_t heap_address);

private:
	CallingConventionNTPowerPC &_callconv;

	/* PIMPL */
	struct _Internal;
	_Internal *_internal;
};

}
