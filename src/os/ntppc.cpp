/*
 * Support for the NT PowerPC calling convention
 */

#include "../config.h"
#include "ntppc.hpp"

#include "../mem.hpp"
#include "../misc/log.hpp"
#include "../misc/tools.h"

#include <stdlib.h>


CallingConventionNTPowerPC::CallingConventionNTPowerPC(struct CPUContext *cpu,
		struct VirtualMemoryContext *memory) :
	_cpu(cpu),
	_memory(memory),
	_syscall_base(0x7FFFF000u),
	_syscall_current_addr(_syscall_base)
{
	/* Allocate page for storing system call code */
	if (virtual_memory_allocate(_memory, NULL, &_syscall_base,
			MEMORY_PAGE_SIZE, (enum MemoryProtection)(MEMORY_PROT_READ |
			MEMORY_PROT_EXECUTE)) < 0) {
		Log::error("[CallingConventionNTPowerPC] syscall page alloc failed\n");
		exit(1);
	}
}

CallingConventionNTPowerPC::~CallingConventionNTPowerPC(void)
{
	/* Deallocate code page */
	virtual_memory_free(_memory, _syscall_base);
}

bool CallingConventionNTPowerPC::allocate_syscall(const unsigned int num,
	unsigned int &ptr)
{
	/* Maximum limit imposed by li instruction */
	if (num > 0x7FFFu)
		return false;

	/* Don't overflow the one page we have */
	if (_syscall_current_addr - _syscall_base >= MEMORY_PAGE_SIZE) {
		Log::error("[CallingConventionNTPowerPC] too many syscalls "
			"allocated\n");
		return false;
	}

	/*
	 * Generate code
	 */
	unsigned char code[4 * sizeof(unsigned int)];
	write_u32le(code + 0x0, _syscall_current_addr + 0x4);	// code pointer
	write_u32le(code + 0x4, 0x38000000u | num);				// li r0, <num>
	write_u32le(code + 0x8, 0x44000002u);					// sc
	write_u32le(code + 0xC, 0x4E800020u);					// blr

	/* Write code */
	if (!MEMOP_OK(virtual_memory_host_write(_memory, _syscall_current_addr,
			code, sizeof(code)))) {
		Log::error("[CallingConventionNTPowerPC] writing syscall code "
			"0x%X failed\n", num);
		return false;
	}

	/* Adjust address for next call */
	ptr = _syscall_current_addr;
	_syscall_current_addr += sizeof(code);
	return true;
}

bool CallingConventionNTPowerPC::get_function_argument(unsigned int index,
	unsigned int &value)
{
	cpu_register_t cpureg;

	/* First 8 arguments are stored in registers r3-r10 */
	switch (index) {
		case 0:
			cpu_get_register(_cpu, PPC_REGISTER_R3, &cpureg);
			value = (unsigned int)cpureg;
			return true;
		case 1:
			cpu_get_register(_cpu, PPC_REGISTER_R4, &cpureg);
			value = (unsigned int)cpureg;
			return true;
		case 2:
			cpu_get_register(_cpu, PPC_REGISTER_R5, &cpureg);
			value = (unsigned int)cpureg;
			return true;
		case 3:
			cpu_get_register(_cpu, PPC_REGISTER_R6, &cpureg);
			value = (unsigned int)cpureg;
			return true;
		case 4:
			cpu_get_register(_cpu, PPC_REGISTER_R7, &cpureg);
			value = (unsigned int)cpureg;
			return true;
		case 5:
			cpu_get_register(_cpu, PPC_REGISTER_R8, &cpureg);
			value = (unsigned int)cpureg;
			return true;
		case 6:
			cpu_get_register(_cpu, PPC_REGISTER_R9, &cpureg);
			value = (unsigned int)cpureg;
			return true;
		case 7:
			cpu_get_register(_cpu, PPC_REGISTER_R10, &cpureg);
			value = (unsigned int)cpureg;
			return true;
	}

	/*
	 * Arguments 8 and higher are on the stack. The address of the argument is
	 * calculated as follows:
	 *  - Stack pointer (r1), plus
	 *  - 6 words (at least on NT) for the linkage area, plus
	 *  - 8 words for r3-r10 which are copied to the stack by vararg
	 *    functions, plus
	 *  - (n - 8) words for the current argument.
	 */
	cpu_get_register(_cpu, PPC_REGISTER_R1, &cpureg);
	const memory_address_t stack_argument_addr = (memory_address_t)(
		cpureg + (6 + 8 + (index - 8)) * 4
	);

	/* Read the word */
	return MEMOP_OK(cpu_powerpc_read_32(_cpu, stack_argument_addr, &value));
}

bool CallingConventionNTPowerPC::call_function(
	memory_address_t address, unsigned int *return_value)
{
	/*
	 * On NT PPC, the entry point (as well as any far function) points to an
	 * 8 byte structure containing the real function pointer and the value of
	 * register r2, which is used on this system to access 32-bit words from
	 * a TOC.
	 */

	/*
	 * Read the structure
	 */
	uint8_t addr_data[8];
	if (!MEMOP_OK(virtual_memory_read(_memory, address, addr_data,
			sizeof(addr_data)))) {
		Log::error("Reading entry point failed\n");
		return false;
	}
	const uint32_t pc = read_u32le(addr_data + 0);
	const uint32_t r2 = read_u32le(addr_data + 4);

	/* Set up registers and call the function */
	cpu_set_register(_cpu, PPC_REGISTER_PC, pc);
	cpu_set_register(_cpu, PPC_REGISTER_R2, r2);
	cpu_set_register(_cpu, PPC_REGISTER_LR, 0x55A855A8u);
	cpu_run(_cpu, 0x55A855A8u);

	/* Did the CPU stop properly? */
	unsigned int stop_intended;
	if (cpu_is_stop_intended(_cpu, &stop_intended) < 0) {
		Log::error("CPU did not stop properly\n");
		return false;
	}

	/* Stop must have been intended */
	if (!stop_intended)
		return false;

	/* Read out the return value in R3 */
	if (return_value) {
		cpu_register_t r3;
		cpu_get_register(_cpu, PPC_REGISTER_R3, &r3);
		*return_value = (unsigned int)r3;
	}
	return true;
}

void CallingConventionNTPowerPC::set_return_value(unsigned int value)
{
	/* Return values up to 4 bytes belong in r3 */
	cpu_set_register(_cpu, PPC_REGISTER_R3, value);
}
