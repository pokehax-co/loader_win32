/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

/*!
 * \file src/os/crtdll.hpp
 * \brief Implementation of the builtin crtdll DLL
 */

#include "win32mod.hpp"


/* src/os/ntppc.hpp */
class CallingConventionNTPowerPC;

/* src/os/win32.hpp */
class Win32Environment;


namespace Win32DLLs {

/*
 * CRTDLL simulation
 */
class CRTDLL : public Win32Module
{
public:
	/* Constructor and deconstructor */
	CRTDLL(CallingConventionNTPowerPC &callconv,
		Win32Environment &environment);
	~CRTDLL(void);

	/*
	 * Inherited functions
	 */
	bool get_function_address(const char *function_name,
		memory_address_t &function_address) const;

	void get_syscall_range(unsigned int &first, unsigned int &last) const;

	bool handle_syscall(unsigned int syscall_number);

	bool dll_main(unsigned int reason);

	/*
	 * Class-specific functions
	 */
	// FIXME!

private:
	CallingConventionNTPowerPC &_callconv;

	/* PIMPL */
	struct _Internal;
	_Internal *_internal;
};

}
