/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

/*!
 * \file src/os/win32mod.hpp
 * \brief Native and builtin module (DLL) support
 */

#include "../portab/portab.h"
#include "../iface/memory.h"


/* src/exefmt/pe.hpp */
class PEExecutable;

/* src/os/ntppc.hpp */
class CallingConventionNTPowerPC;

/* src/os/win32.hpp */
class Win32Environment;


/*
 * Abstract class for implementing replacements to Win32 DLLs.
 */

class Win32Module
{
public:
	/* Initializes a new DLL inside the current environment. */
	Win32Module(Win32Environment &environment, const char *module_name,
			memory_address_t image_base) :
		_environment(environment),
		_module_name(module_name),
		_image_base(image_base)
	{
	}

	/* Deinitializes this DLL. */
	virtual ~Win32Module(void) {
	}

	/* Returns true whether this is a real module (for such modules, an
	   initialization function has to be called). */
	virtual bool is_real_module(void) const {
		return false;
	}

	/* Retrieves the module name. */
	const char *get_module_name(void) const {
		return _module_name;
	}

	/* Returns the image base of this DLL. */
	memory_address_t get_image_base(void) const {
		return _image_base;
	}

	/* Resolves the specified symbol to an address. */
	virtual bool get_function_address(const char *function_name,
		memory_address_t &function_address) const = 0;

	/* Provides the range of system call numbers. */
	virtual void get_syscall_range(unsigned int &first,
		unsigned int &last) const = 0;

	/* Handles a system call whose number is in the range returned by the
	   get_syscall_range() method. */
	virtual bool handle_syscall(unsigned int syscall_number) = 0;

	/* Performs module-specific initialization/deinitialization. */
	virtual bool dll_main(unsigned int reason) = 0;

protected:
	Win32Environment &_environment;
	const char *_module_name;
	memory_address_t _image_base;
};


/*
 * Class to load real Win32 modules from disk.
 */

class Win32PEModule : public Win32Module
{
public:
	/* Constructor and deconstructor */
	Win32PEModule(CallingConventionNTPowerPC &callconv,
		Win32Environment &environment);
	~Win32PEModule(void);

	/*
	 * Functions inherited from Win32Module
	 */

	/* Returns true whether this is a real module (for such modules, an
	   initialization function has to be called). */
	bool is_real_module(void) const {
		return true;
	}

	/* Resolves the specified symbol to an address. */
	bool get_function_address(const char *function_name,
		memory_address_t &function_address) const;

	/* System calls cannot be dispatched to real modules */
	void get_syscall_range(unsigned int &first,
		unsigned int &last) const
	{
		first = last = 0;
	}

	/* System calls cannot be dispatched to real modules */
	bool handle_syscall(unsigned int syscall_number)
	{
		(void)syscall_number;
		return false;
	}

	bool dll_main(unsigned int reason);

	/*
	 * Functions in this class
	 */

	/* Loads a module from disk. */
	bool load_module(PEExecutable *const exe);

	/* Returns the executable object. */
	PEExecutable *get_pe_executable(void) const
	{
		return _exe;
	}

private:
	CallingConventionNTPowerPC &_callconv;
	PEExecutable *_exe;
};
