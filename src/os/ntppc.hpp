#pragma once

/*!
 * \file src/os/ntppc.hpp
 * \brief Support for the NT PowerPC calling convention
 */

#include "../iface/memory.h"
#include "../hw/powerpc.h"


/* src/process/virtmem.h */
struct VirtualMemoryContext;


class CallingConventionNTPowerPC
{
public:
	/* Initializes helper class with the specified CPU instance */
	CallingConventionNTPowerPC(struct CPUContext *cpu,
		struct VirtualMemoryContext *memory);
	~CallingConventionNTPowerPC(void);

	/* Allocates a new system call and returns the code pointer for it */
	bool allocate_syscall(const unsigned int num, unsigned int &ptr);

	/* Retrieves a function argument */
	bool get_function_argument(unsigned int index, unsigned int &value);

	/* Calls a function without arguments */
	bool call_function(memory_address_t address, unsigned int *return_value);

	/* Sets the return value of a function */
	void set_return_value(unsigned int value);

private:
	struct CPUContext *_cpu;
	struct VirtualMemoryContext *_memory;

	/* System call code allocation */
	memory_address_t _syscall_base;
	unsigned int _syscall_current_addr;
};
