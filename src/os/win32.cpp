/*
 * Provides a Windows environment for an executable file.
 */

#include "../config.h"
#include "win32.hpp"

#include "../misc/log.hpp"
#include "../exefmt/pe.hpp"
#include "../mem.hpp"
#include "../hw/powerpc.h"
#include "win32mod.hpp"
#include "ntppc.hpp"

#include "advapi32.hpp"
#include "crtdll.hpp"
#include "kernel32.hpp"
#include "ntdll.hpp"

#ifdef _WIN32
#include <io.h>
#endif

#include <string.h>


Win32Environment::Win32Environment(int argc, char **argv, char **envp) :
	_program_argc(argc), _program_argv(argv), _program_envp(envp),
	_main_module(NULL), _main_module_path(NULL),
	_memory(NULL), _cpu_c(NULL), _callconv(NULL),
	_process_exit_code(0), _stack_bottom(0), _stack_top(0)
{
}

Win32Environment::~Win32Environment(void)
{
	/* Unload DLLs */
	for (size_t i = 0; i < _builtin_dlls.size(); ++i)
		delete _builtin_dlls[i];

	cpu_destroy(&_cpu_c);
	delete _callconv;
	delete _memory;
	delete _main_module;
	delete _main_module_path;
}

bool Win32Environment::load_process(PEExecutable *&exe)
{
	/* Create virtual memory */
	_memory = new AddressSpace();

	if (virtual_memory_set_fault_callback(_memory->get_virtmem(),
			_default_fault_callback, this, NULL, NULL) < 0) {
		fprintf(stderr, "virtual_memory_set_fault_callback() failed\n");
		return false;
	}

	/* Initialize CPU emulation */
	if (cpu_create(&cpu_interface_powerpc, &_cpu_c) < 0) {
		Log::error("CPU creation failed\n");
		return false;
	}
	if (cpu_set_memory(_cpu_c, &virtual_memory_interface,
			_memory->get_virtmem()) < 0) {
		Log::error("Attaching memory to CPU failed\n");
		return false;
	}

	/* Initialize PowerPC calling convention */
	_callconv = new CallingConventionNTPowerPC(_cpu_c, _memory->get_virtmem());

	/* The API emulation uses custom syscalls, which will be handled by its
	   own handler. */
	cpu_powerpc_set_syscall_handler(_cpu_c, _powerpc32_syscall_handler, this);

	/* Load builtin NTDLL regardless of whether it is imported */
	Win32DLLs::NTDLL *ntdll = new Win32DLLs::NTDLL(*_callconv, *this);
	_builtin_dlls.append(ntdll);

	/* Copy the executable's path */
	const size_t module_path_length = strlen(exe->get_exe_path()) + 1;
	_main_module_path = new char[module_path_length];
	memcpy(_main_module_path, exe->get_exe_path(), module_path_length);

	/*
	 * Load main executable
	 */
	Win32PEModule *main_module = new Win32PEModule(*_callconv, *this);
	if (!main_module->load_module(exe))
		goto cleanup;

	/* Set the base address for new allocations */
	_sbrk_address = exe->get_memory_end_offset();

	/* Loading succeeded, now we have to get rid of the "exe" pointer in case
	   something goes wrong here. */
	exe = NULL;

	/* Create the process heap */
	if (!ntdll->heap_create(0, 32u * 1048576u, 32u * 1048576u, _process_heap)) {
		Log::error("cannot create process heap!\n");
		goto cleanup;
	} else {
		PEExecutable *const main_pe = main_module->get_pe_executable();

		/* Allocate 1 megabyte of stack */
		const PEOptionalHeader *const optional_header =
			main_pe->get_optional_header();

		/* At this hardcoded address */
		size_t stack_size = (size_t)optional_header->size_of_stack_reserve;
		_stack_top = 0x22220000u;
		_stack_bottom = _stack_top - stack_size;

		if (virtual_memory_allocate(_memory->get_virtmem(), NULL,
				&_stack_bottom, stack_size, (enum MemoryProtection)(
				MEMORY_PROT_READ | MEMORY_PROT_WRITE)) < 0) {
			Log::error("can't map stack memory\n");
			goto cleanup;
		}
	}

	_main_module = main_module;
	return true;

cleanup:
	delete main_module;
	_main_module = NULL;
	return false;
}

bool Win32Environment::run(int &exit_code)
{
	/*
	 * Before we can call the actual program code, we need to run DllMain
	 * on all DLLs with the reason DLL_PROCESS_ATTACH.
	 */
	for (size_t i = 0; i < _builtin_dlls.size(); ++i) {
		Win32Module *const it = _builtin_dlls[i];
		if (!it->dll_main(1)) {
			Log::error("DllMain call failed for %s\n", it->get_module_name());
			return false;
		}
	}

	/* Set stack pointer */
	cpu_set_register(_cpu_c, PPC_REGISTER_R1, _stack_top - 4);

	/* Read entry point address from PE header */
	const PEExecutable *const exe =
		static_cast<Win32PEModule *>(_main_module)->get_pe_executable();
	const PEOptionalHeader *const optional_header =
		exe->get_optional_header();
	const memory_address_t entry_point = (memory_address_t)(
		optional_header->image_base + optional_header->address_of_entry_point
	);

	/* Call it as function */
	if (!_callconv->call_function(entry_point, NULL))
		exit_code = 1;

	/* Call was successful, which means that we reached exit() or ExitProcess()
	   which stop the CPU and save the process' return code. */
	else
		exit_code = _process_exit_code;

	/*
	 * Run DllMain again on all DLLs, this time with reason
	 * DLL_PROCESS_DETACH.
	 * XXX: also on native DLLs
	 */
	for (size_t i = 0; i < _builtin_dlls.size(); ++i) {
		Win32Module *const it = _builtin_dlls[i];

		if (it->is_real_module())
			continue;
		if (!it->dll_main(0)) {
			Log::error("DllMain call failed for %s\n", it->get_module_name());
			return false;
		}
	}
	return true;
}

Win32Module *Win32Environment::load_module(const char *name)
{
	Win32Module *dll = get_module(name);
	if (!dll) {
		/*
		 * Find the DLL in the same path as the executable. Because we run
		 * executables of exotic architectures, searching PATH would be
		 * pointless.
		 */
		const char *exe_path_filename = _main_module_path +
			strlen(_main_module_path);
		while (exe_path_filename > _main_module_path) {
			if (exe_path_filename[-1] == '/' || exe_path_filename[-1] == '\\')
				break;
			--exe_path_filename;
		}

		char expected_module_path[260];
		_snprintf(expected_module_path, sizeof(expected_module_path),
			"%.*s%s", (int)(exe_path_filename - _main_module_path),
			_main_module_path, name);

		/* Is the file present? */
		if (access(expected_module_path, 0) >= 0) {
			do {
				/* Probe the executable first */
				if (!PEExecutable::probe(expected_module_path)) {
					Log::error("%s is not a valid Win32 application.\n",
						expected_module_path);
					break;
				}

				/*
				 * Load external DLL
				 */
				PEExecutable *exe = new PEExecutable();
				if (!exe->load(expected_module_path)) {
					Log::error("Cannot load %s\n", expected_module_path);
					delete exe;
					break;
				}

				/* Load it into memory */
				Win32PEModule *pe_module = new Win32PEModule(*_callconv,
					*this);
				if (!pe_module->load_module(exe)) {
					Log::error("Mapping %s into memory failed\n",
						expected_module_path);
					delete pe_module;
					delete exe;
					break;
				}

				/* Loaded */
				dll = pe_module;
				_builtin_dlls.append(dll);
			} while (0);
		}

		if (!dll) {
			/*
			 * Load builtin DLL as external loading failed
			 */
			if (_stricmp(name, "kernel32.dll") == 0) {
				dll = new Win32DLLs::KERNEL32(*_callconv, *this);
				_builtin_dlls.append(dll);
			}
			else if (_stricmp(name, "crtdll.dll") == 0) {
				dll = new Win32DLLs::CRTDLL(*_callconv, *this);
				_builtin_dlls.append(dll);
			}
			else if (_stricmp(name, "advapi32.dll") == 0) {
				dll = new Win32DLLs::ADVAPI32(*_callconv, *this);
				_builtin_dlls.append(dll);
			}
			else {
				Log::error("load_module(): unknown DLL %s\n", name);
			}
		}
	}
	return dll;
}

Win32Module *Win32Environment::get_module(const char *name)
{
	for (size_t i = 0; i < _builtin_dlls.size(); ++i) {
		Win32Module *const it = _builtin_dlls[i];

		/* Check against module name (with .dll) */
		if (_stricmp(name, it->get_module_name()) == 0)
			return it;

		/* For actual DLLs, check path from PEExecutable.
		   crtdll.dll loads itself from its DllMain routine. */
		if (it->is_real_module()) {
			const char *const exe_path = static_cast<Win32PEModule *>(it)->
				get_pe_executable()->get_exe_path();

			if (_stricmp(name, exe_path) == 0)
				return it;
		}
	}
	return NULL;
}

Win32Module *Win32Environment::get_module(memory_address_t base_address)
{
	/* NULL base address -> return the executable */
	if (!base_address)
		return _main_module;

	for (size_t i = 0; i < _builtin_dlls.size(); ++i) {
		Win32Module *const it = _builtin_dlls[i];
		if (it->get_image_base() == base_address)
			return it;
	}
	return NULL;
}

PEExecutable *Win32Environment::get_executable(void) const
{
	if (!_main_module)
		return NULL;
	return static_cast<Win32PEModule *>(_main_module)->get_pe_executable();
}


/*
 * Private methods
 */

unsigned int Win32Environment::_powerpc32_syscall_handler(
	struct CPUContext *cpu, void *user)
{
	Win32Environment *const this_ = static_cast<Win32Environment *>(user);

	/* System call number on PowerPC is in register r0 */
	cpu_register_t r0_64;
	cpu_get_register(cpu, PPC_REGISTER_R0, &r0_64);
	const unsigned int r0 = (unsigned int)r0_64;

	/* Iterate v2 DLLs */
	for (size_t i = 0; i < this_->_builtin_dlls.size(); ++i) {
		Win32Module *const it = this_->_builtin_dlls[i];

		/* Get range of system calls */
		unsigned int first, last;
		it->get_syscall_range(first, last);

		/* In range? Call it */
		if (r0 >= first && r0 <= last)
			return it->handle_syscall(r0) ? 1 : 0;
	}

	Log::error("[Win32Environment] Cannot run system call 0x%X\n", r0);
	return -1;
}

void Win32Environment::_default_fault_callback(
	struct VirtualMemoryContext *ctx, memory_address_t fault_address,
	unsigned int fault_size, enum MemoryStatus failure_status,
	enum MemoryProtection failure_op, void *callback_user)
{
	(void)ctx;
	(void)callback_user;

	const char *_op = NULL;
	switch (failure_op) {
	case MEMORY_PROT_EXECUTE:
		_op = "execute";
		break;
	case MEMORY_PROT_READ:
		_op = "read";
		break;
	case MEMORY_PROT_WRITE:
		_op = "write";
		break;
	default:
		GNU_UNREACHABLE();
		break;
	}

	const char *_fault_type = NULL;
	switch (failure_status) {
		case MEMORY_STATUS_OK:
			_fault_type = "OK";
			break;
		case MEMORY_STATUS_NOT_PRESENT:
			_fault_type = "not mapped";
			break;
		case MEMORY_STATUS_INVALID_ARGUMENT:
			_fault_type = "invalid argument (programming error)";
			break;
		case MEMORY_STATUS_PROTECTION:
			switch (failure_op) {
			case MEMORY_PROT_EXECUTE:
				_fault_type = "non-executable memory";
				break;
			case MEMORY_PROT_READ:
				_fault_type = "non-readable memory";
				break;
			case MEMORY_PROT_WRITE:
				_fault_type = "non-writable memory";
				break;
			default:
				GNU_UNREACHABLE();
				break;
			}
			break;
		case MEMORY_STATUS_NO_MEMORY:
			_fault_type = "out of memory";
			break;
	}

	fprintf(stderr, "Memory fault (%s) at 0x%" PRIXMA " size %u: %s\n",
		_op, fault_address, fault_size, _fault_type);
}


/*
 * Public methods
 */

bool Win32Environment::pe_module_callback(struct VirtualMemoryContext *memory,
	const char *module_name, void *user)
{
	Win32Environment *const this_ = static_cast<Win32Environment *>(user);
	(void)memory;

	/* Load the module */
	if (!this_->load_module(module_name)) {
		Log::error("[Win32Environment] cannot load '%s'\n", module_name);
		return false;
	}
	return true;
}

bool Win32Environment::pe_function_callback(
	struct VirtualMemoryContext *memory, const char *module_name,
	const char *function_name, memory_address_t &function_pointer, void *user)
{
	Win32Environment *const this_ = static_cast<Win32Environment *>(user);
	(void)memory;

	/* Find the module */
	Win32Module *dll = this_->get_module(module_name);
	if (!dll) {
		Log::error("[Win32Environment] cannot find '%s'\n", module_name);
		return false;
	}

	/* Ask the module to return a function */
	return dll->get_function_address(function_name, function_pointer);
}
