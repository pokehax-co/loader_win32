#pragma once

/*!
 * \file src/exefmt/pe.hpp
 * \brief PE executable parser and loader
 */

#include "pestruct.h"
#include "../iface/memory.h"

#include <stdio.h>


/* src/process/virtmem.h */
struct VirtualMemoryContext;


/*
 * Parses the file header, optional header, and section headers of a Win32
 * PE executable. Both 32-bit and 64-bit versions are supported.
 */

class PEExecutable
{
public:
	PEExecutable(void);
	~PEExecutable(void);

	/* Checks whether an executable is in PE format. */
	static bool probe(const char *path);

	/* Opens a PE executable by its filename and loads its headers. */
	bool load(const char *path);

	/* Maps the sections of the executable into the address space 'memory'. */
	bool map_sections(struct VirtualMemoryContext *memory);

	/* Closes the underlying file handle of the PE executable. This can be
	   performed after it has been mapped into virtual memory. */
	bool close_file(void);

	/* Post-map function: Parses the entries of the executable's export
	   table. (Returns true if there are no exports.) */
	bool resolve_exports(struct VirtualMemoryContext *memory);

	/* Post-map function: Resolves the entries of the executable's import table
	   using the callbacks below. */
	bool resolve_imports(struct VirtualMemoryContext *memory);

	/*
	 * Callback which is invoked for every new module encountered in the
	 * import table.
	 */
	typedef bool (*ImportModuleCallback)(struct VirtualMemoryContext *memory,
		const char *module_name, void *user);

	void set_import_module_callback(ImportModuleCallback callback,
		void *user, ImportModuleCallback *old_callback = NULL,
		void **old_user = NULL);

	/*
	 * Callback which is invoked for every new function encountered in the
	 * import table.
	 */
	typedef bool (*ImportFunctionCallback)(
		struct VirtualMemoryContext *memory, const char *module_name,
		const char *function_name, memory_address_t &function_pointer,
		void *user);

	void set_import_function_callback(ImportFunctionCallback callback,
		void *user, ImportFunctionCallback *old_callback = NULL,
		void **old_user = NULL);

	/* Queries the address of a symbol exported from this module. */
	memory_address_t get_exported_symbol_address(uint16_t ordinal);

	/* Queries the address of a symbol exported from this module. */
	memory_address_t get_exported_symbol_address(const char *name);

	/* Returns the full path to the executable. */
	const char *get_exe_path(void) const
	{
		return _exe_path;
	}

	/* Returns the file header of the executable. */
	const PEFileHeader *get_file_header(void) const
	{
		return &_file_header;
	}

	/* Returns the optional header of the executable. */
	const PEOptionalHeader *get_optional_header(void) const
	{
		return &_optional_header;
	}

	/* Returns the section headers of the executable. */
	const PESectionHeader *get_section_headers(void) const
	{
		return _section_headers;
	}

	/* Returns the file offset to the PE file header (after the signature). */
	long get_file_header_offset(void) const
	{
		return _file_header_offset;
	}

	/* Returns the file offset after the last section header. */
	long get_header_end_offset(void) const
	{
		return _header_end_offset;
	}

	/* Returns the memory offset after the last section. */
	memory_address_t get_memory_end_offset(void) const
	{
		return _memory_end_offset;
	}

	/* Returns the actual base address the executable is loaded at. If it was
	   not possible to load it at the original image base, this value will
	   differ. */
	memory_address_t get_image_base(void) const
	{
		// XXX: Not for now
		return (memory_address_t)_optional_header.image_base;
	}

	/* Returns whether the executable is 32-bit. */
	bool is_pe32(void) const
	{
		return _optional_header.magic == 0x10Bu;
	}

	/* Returns whether the executable is 64-bit. */
	bool is_pe32plus(void) const
	{
		return _optional_header.magic == 0x20Bu;
	}

	/* Returns whether the executable is a shared library (DLL). */
	bool is_shared_library(void) const
	{
		/* 0x2000u == IMAGE_FILE_DLL */
		return !!(_file_header.characteristics & 0x2000u);
	}

private:
	template<typename T>
	T _round_to_next_file_alignment(T value) {
		const uint32_t file_alignment = _optional_header.file_alignment;
		return (value + file_alignment - 1) & ~(file_alignment - 1);
	}

	template<typename T>
	T _round_to_next_section_alignment(T value) {
		const uint32_t section_alignment = _optional_header.section_alignment;
		return (value + section_alignment - 1) & ~(section_alignment - 1);
	}

	FILE *_fp;

	long _file_header_offset;
	PEFileHeader _file_header;
	PEOptionalHeader _optional_header;
	size_t _section_count;
	PESectionHeader *_section_headers;
	long _header_end_offset;

	memory_address_t _memory_end_offset;

	struct _ExportInfo;
	struct _ExportInfo *_export_info;

	ImportModuleCallback _import_module_callback;
	void *_import_module_callback_user;
	ImportFunctionCallback _import_function_callback;
	void *_import_function_callback_user;
	bool _imports_resolved;

	char _exe_path[260];
};
