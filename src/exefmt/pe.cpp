#include "../config.h"
#include "pe.hpp"

#include "../misc/tools.h"
#include "../misc/log.hpp"
#include "../process/virtmem.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>


/* fread() helper which returns false on partial reads */
#define FREAD(buffer, size, fp) \
	(fread(buffer, 1, size, fp) == (size))

/* Forward-declare functions with platform-dependent code */
static bool _get_full_path(const char *path, char *full_path,
	size_t full_path_size);


/*
 * _ExportInfo structure
 */

struct PEExecutable::_ExportInfo
{
	struct OrdinalExport
	{
		uint16_t ordinal;
		uint32_t address_rva;

		static int find(const void *key_, const void *b_)
		{
			const uint16_t *const key = static_cast<const uint16_t *>(key_);
			const struct OrdinalExport *const b =
				static_cast<const struct OrdinalExport *>(b_);
			return (*key > b->ordinal) - (*key - b->ordinal);
		};

		static int compare(const void *a_, const void *b_)
		{
			const struct OrdinalExport *const a =
				static_cast<const struct OrdinalExport *>(a_);
			const struct OrdinalExport *const b =
				static_cast<const struct OrdinalExport *>(b_);
			return (a->ordinal > b->ordinal) - (a->ordinal - b->ordinal);
		};
	};

	struct NamedExport
	{
		char *name;
		uint32_t address_rva;

		static int find(const void *key_, const void *b_)
		{
			const char *const key = static_cast<const char *>(key_);
			const struct NamedExport *const b =
				static_cast<const struct NamedExport *>(b_);
			return strcmp(key, b->name);
		};

		static int compare(const void *a_, const void *b_)
		{
			const struct NamedExport *const a =
				static_cast<const struct NamedExport *>(a_);
			const struct NamedExport *const b =
				static_cast<const struct NamedExport *>(b_);
			return strcmp(a->name, b->name);
		};
	};

	struct OrdinalExport *by_ordinal;
	size_t by_ordinal_count;

	struct NamedExport *by_name;
	size_t by_name_count;

	_ExportInfo(void) :
		by_ordinal(NULL), by_ordinal_count(0),
		by_name(NULL), by_name_count(0)
	{
	}

	~_ExportInfo(void)
	{
		if (by_name) {
			for (size_t i = 0; i < by_name_count; ++i)
				free(static_cast<void *>(by_name[i].name));

			free(static_cast<void *>(by_name));
			by_name = NULL;
		}

		if (by_ordinal) {
			free(static_cast<void *>(by_ordinal));
			by_ordinal = NULL;
		}
	}

	bool insert(uint16_t ordinal, uint32_t address_rva)
	{
		struct OrdinalExport *new_by_ordinal =
			static_cast<struct OrdinalExport *>(
				realloc(by_ordinal, (by_ordinal_count + 1) *
					sizeof(*new_by_ordinal))
			);
		if (!new_by_ordinal)
			return false;
		by_ordinal = new_by_ordinal;

		by_ordinal[by_ordinal_count].ordinal = ordinal;
		by_ordinal[by_ordinal_count].address_rva = address_rva;
		++by_ordinal_count;
		return true;
	}

	bool insert(const char *name, uint32_t address_rva)
	{
		struct NamedExport *new_by_name = static_cast<struct NamedExport *>(
			realloc(by_name, (by_name_count + 1) * sizeof(*new_by_name))
		);
		if (!new_by_name)
			return false;
		by_name = new_by_name;

		by_name[by_name_count].name = strdup(name);
		by_name[by_name_count].address_rva = address_rva;
		++by_name_count;
		return true;
	}

	bool find(uint16_t ordinal, uint32_t &address_rva)
	{
		struct OrdinalExport *result = static_cast<struct OrdinalExport *>(
			bsearch(
				static_cast<const void *>(&ordinal),
				static_cast<const void *>(by_ordinal),
				by_ordinal_count,
				sizeof(struct OrdinalExport),
				OrdinalExport::find
			)
		);
		if (result) {
			address_rva = result->address_rva;
			return true;
		}
		return false;
	}

	bool find(const char *name, uint32_t &address_rva)
	{
		struct NamedExport *result = static_cast<struct NamedExport *>(
			bsearch(
				static_cast<const void *>(name),
				static_cast<const void *>(by_name),
				by_name_count,
				sizeof(struct NamedExport),
				NamedExport::find
			)
		);
		if (result) {
			address_rva = result->address_rva;
			return true;
		}
		return false;
	}

	void sort(void)
	{
		qsort(static_cast<void *>(by_name), by_name_count,
			sizeof(NamedExport), NamedExport::compare);
		qsort(static_cast<void *>(by_ordinal), by_ordinal_count,
			sizeof(OrdinalExport), OrdinalExport::compare);
	}
};


PEExecutable::PEExecutable(void) :
	_fp(NULL),
	_file_header_offset(0ULL),
	_section_headers(NULL),
	_header_end_offset(0ULL), _memory_end_offset(0ULL),
	_export_info(NULL),
	_import_module_callback(NULL), _import_module_callback_user(NULL),
	_import_function_callback(NULL), _import_function_callback_user(NULL),
	_imports_resolved(false)
{
	memset(&_file_header, 0, sizeof(_file_header));
	memset(&_optional_header, 0, sizeof(_optional_header));
	memset(_exe_path, 0, sizeof(_exe_path));
}

PEExecutable::~PEExecutable(void)
{
	delete _export_info;
	_export_info = NULL;

	if (_section_headers) {
		delete[] _section_headers;
		_section_headers = NULL;
	}

	if (_fp) {
		fclose(_fp);
		_fp = NULL;
	}
}

/* static */ bool PEExecutable::probe(const char *path)
{
	bool ret = false;

	if (!path || !*path)
		return false;

	FILE *fp = fopen(path, "rb");
	if (!fp) {
		Log::error("%s: %s\n", path, strerror(errno));
		return false;
	}

	do {
		union {
			uint8_t dos_stub[0x40];
			uint8_t pe_initial[0x1A];
		} read_buffer;

		/* Read DOS stub until e_lfanew */
		if (!FREAD(read_buffer.dos_stub, sizeof(read_buffer.dos_stub), fp)) {
			Log::error("probe failed: file too short (DOS header)\n");
			break;
		}

		/* Check e_magic and e_lfanew. corkami's PE docs establish the
		   e_lfanew minimum at 4. */
		const uint16_t e_magic = read_u16le(read_buffer.dos_stub + 0x0u);
		const uint32_t e_lfanew = read_u32le(read_buffer.dos_stub + 0x3Cu);
		if (e_magic != 0x5A4Du || e_lfanew < 4) {
			Log::error("probe failed: invalid DOS header\n");
			break;
		}

		/* Read the first bytes of the PE executable */
		if (fseek(fp, e_lfanew, SEEK_SET) < 0 ||
				!FREAD(read_buffer.pe_initial, sizeof(read_buffer.pe_initial),
				fp)) {
			Log::error("probe failed: file too short (DOS header)\n");
			break;
		}

		/* Check signature and optional header magic */
		const uint32_t signature = read_u32le(
			read_buffer.pe_initial + 0x0u);
		const uint16_t optional_magic = read_u16le(
			read_buffer.pe_initial + 0x18u);

		if (signature != 0x4550u || (optional_magic != 0x10Bu &&
				optional_magic != 0x20Bu)) {
			Log::error("probe failed: PE signature invalid\n");
			break;
		}

		/* Reject executables of wrong bit size */
#ifdef CONFIG_LOADER_64BIT
		if (optional_magic != 0x20Bu) {
			Log::error("build only supports 64-bit applications, please "
				"undefine CONFIG_LOADER_64BIT and recompile\n");
			break;
		}
#else
		if (optional_magic != 0x10Bu) {
			Log::error("build only supports 32-bit applications, please "
				"define CONFIG_LOADER_64BIT and recompile\n");
			break;
		}
#endif

		/* Executable is valid */
		ret = true;
	} while (0);

	fclose(fp);
	return ret;
}

bool PEExecutable::load(const char *path)
{
	if (!path || !*path)
		return false;

	/* Must not already be loaded */
	if (_file_header.machine || _optional_header.magic || _section_headers) {
		Log::error("executable already opened\n");
		return false;
	}

	_fp = fopen(path, "rb");
	if (!_fp) {
		Log::error("%s: %s\n", path, strerror(errno));
		return false;
	}

	/* File must have been probed first! We won't do any further checks. */

	union {
		uint8_t e_lfanew[4];
		uint8_t file_header[EXEFMT_PE_FILE_HEADER_SIZE];
		uint8_t optional_header[EXEFMT_PE_OPTIONAL_HEADER_SIZE];
		uint8_t section_header[EXEFMT_PE_SECTION_HEADER_SIZE];
	} read_buffer;

	/* Read the offset to the PE file */
	if (fseek(_fp, 0x3Cu, SEEK_SET) < 0 ||
		!FREAD(read_buffer.e_lfanew,
			sizeof(read_buffer.e_lfanew), _fp)) {
		Log::error("parsing failed: file too short (DOS header)\n");
		return false;
	}

	const uint32_t e_lfanew = read_u32le(read_buffer.e_lfanew);
	assert(_file_header_offset <= LONG_MAX - 4);
	_file_header_offset = e_lfanew + 4;

	/* Read file header */
	if (fseek(_fp, _file_header_offset, SEEK_SET) < 0 ||
		!FREAD(read_buffer.file_header,
			sizeof(read_buffer.file_header), _fp) ||
		!pe_file_header_read(read_buffer.file_header,
			sizeof(read_buffer.file_header), &_file_header)) {
		Log::error("parsing failed: file too short (file header)\n");
		return false;
	}

	/* Read optional header */
	const long optional_header_offset = _file_header_offset +
		EXEFMT_PE_FILE_HEADER_SIZE;
	if (!FREAD(read_buffer.optional_header,
			sizeof(read_buffer.optional_header), _fp) ||
		!pe_optional_header_read(read_buffer.optional_header,
			sizeof(read_buffer.optional_header), &_optional_header)) {
		Log::error("parsing failed: file too short (optional header)\n");
		return false;
	}

	/* Read section headers. Because the SizeOfOptionalHeader field is actually
	   a delta between optional_header_offset and the actual offset of the
	   section headers, we have to re-seek first. */
	const long section_headers_offset = optional_header_offset +
		_file_header.size_of_optional_header;
	if (fseek(_fp, section_headers_offset, SEEK_SET) < 0) {
		Log::error("parsing failed: file too short (section headers)\n");
		return false;
	}

	_section_count = _file_header.number_of_sections;
	if (_section_count > 0) {
		_section_headers = new PESectionHeader[_section_count];
		_header_end_offset = section_headers_offset;

		for (size_t i = 0; i < _section_count; ++i) {

			if (!FREAD(read_buffer.section_header,
					sizeof(read_buffer.section_header), _fp) ||
				!pe_section_header_read(read_buffer.section_header,
					sizeof(read_buffer.section_header),
					&_section_headers[i])) {
				Log::error("parsing failed: file too short (section "
					"header %u)\n", i + 1);
				return false;
			}

			_header_end_offset += EXEFMT_PE_SECTION_HEADER_SIZE;
		}
	}

	/* Read full path to the file */
	if (!_get_full_path(path, _exe_path, sizeof(_exe_path))) {
		Log::error("Failed to retrieve full path to executable\n");
		return false;
	}

	return true;
}

bool PEExecutable::map_sections(struct VirtualMemoryContext *memory)
{
	/* Must have all headers */
	if (!_file_header.machine || !_optional_header.magic || !_section_headers)
		return false;

	/* If _memory_end_offset is known, then it's already mapped */
	if (_memory_end_offset)
		return true;

	struct VirtualMemoryInformation vmi;

	/*
	 * We know how large the header is. Round it to the next file alignment,
	 * allocate memory for it, and read it directly into the host allocation
	 * (because it is consecutive).
	 */
	const size_t header_size = _round_to_next_file_alignment(
		_header_end_offset);

	// FIXME: Support relocations. If we fail to map somewhere, then we need
	// to load the sections somewhere else.

	memory_address_t image_base =
		(memory_address_t)_optional_header.image_base;
	if (!MEMOP_OK(virtual_memory_allocate(memory, NULL, &image_base,
			header_size, (enum MemoryProtection)(MEMORY_PROT_READ |
			MEMORY_PROT_WRITE)))) {
		Log::error("map at 0x%" PRIXMA " failed\n", image_base);
		return false;
	}

	if (virtual_memory_query(memory, image_base, &vmi) < 0) {
		Log::error("query at 0x%" PRIXMA " failed\n", image_base);
		return false;
	}

	if (fseek(_fp, 0, SEEK_SET) < 0 ||
			!FREAD(vmi.host_pointer, header_size, _fp)) {
		Log::error("file read to 0x%" PRIXMA " failed\n", image_base);
		return false;
	}

	/* Record end of executable at end of header (rounded to next section
	   alignment) */
	_memory_end_offset = _round_to_next_section_alignment(image_base +
		header_size);

	/* Now to the sections. Map the pages first before reading the entire
	   contents. */
	for (size_t i = 0; i < _file_header.number_of_sections; ++i) {
		const PESectionHeader *const cursec = &_section_headers[i];

		/* Determine protection bits */
		unsigned int protection = 0;
		if (cursec->characteristics & 0x20000000u) /* IMAGE_SCN_MEM_EXECUTE */
			protection |= MEMORY_PROT_EXECUTE;
        if (cursec->characteristics & 0x40000000u) /* IMAGE_SCN_MEM_READ */
            protection |= MEMORY_PROT_READ;
        if (cursec->characteristics & 0x80000000u) /* IMAGE_SCN_MEM_WRITE */
            protection |= MEMORY_PROT_WRITE;

		/* Round section size */
		const uint32_t section_mem_size = _round_to_next_section_alignment(
			cursec->virtual_size);

		/*
		 * Like with the header, allocate memory and read directly into the
		 * host allocation.
		 */

		memory_address_t current_address = image_base +
			cursec->virtual_address;
		if (!MEMOP_OK(virtual_memory_allocate(memory, NULL, &current_address,
				section_mem_size, (enum MemoryProtection)protection))) {
			Log::error("map at 0x%" PRIXMA " for section #%u failed\n",
				current_address, i + 1);
			return false;
		}

		if (virtual_memory_query(memory, current_address,
				&vmi) < 0) {
			Log::error("query at 0x%" PRIXMA " failed\n", current_address);
			return false;
		}

		const uint32_t section_raw_size = cursec->size_of_raw_data;
		if (section_raw_size > 0) {
			if (fseek(_fp, cursec->pointer_to_raw_data, SEEK_SET) < 0 ||
					!FREAD(vmi.host_pointer, section_raw_size, _fp)) {
				Log::error("file read to 0x%" PRIXMA " failed\n", image_base);
				return false;
			}
		}

		/* Update end offset */
		current_address = _round_to_next_section_alignment(current_address +
			section_mem_size);
		if (current_address > _memory_end_offset)
			_memory_end_offset = current_address;
	}

	return true;
}

bool PEExecutable::close_file(void)
{
	/* Is it mapped? */
	if (_memory_end_offset) {
		if (_fp) {
			fclose(_fp);
			_fp = NULL;
		}
		return true;
	}
	return false;
}

bool PEExecutable::resolve_exports(struct VirtualMemoryContext *memory)
{
	/* Must be mapped (_memory_end_offset is known) */
	if (!_memory_end_offset) {
		Log::error("executable not mapped\n");
		return false;
	}

	/* Do we have an export directory? If not, there's nothing to do */
	const PEDataDirectory *export_datadir =
		&_optional_header.directory_entry_export;

	if (!export_datadir->virtual_address || !export_datadir->size)
		return true;

	/* Have we resolved the exports already? */
	if (_export_info)
		return true;

	/*
	 * Read export directory. We also need to know its limits to determine
	 * whether an export is forwarded.
	 */
	const memory_address_t image_base = get_image_base();
	const memory_address_t export_data_start = image_base +
		export_datadir->virtual_address;
	const memory_address_t export_data_end = export_data_start +
		export_datadir->size;

	uint8_t expdir_buffer[EXEFMT_PE_EXPORT_DIRECTORY_SIZE];
	PEExportDirectory expdir;

	if (!MEMOP_OK(virtual_memory_host_read(memory, export_data_start,
			expdir_buffer, sizeof(expdir_buffer))) ||
		!pe_export_directory_read(expdir_buffer, sizeof(expdir_buffer),
			&expdir)) {
		Log::error("descriptor read failed\n");
		return false;
	}

#ifdef DEBUG_PE_EXPORTS
	/* Print export directory */
	pe_export_directory_print(&expdir);
#endif

	/* We need to read from three tables simultaneously */
	const memory_address_t export_address = image_base +
		expdir.address_of_functions;
	const memory_address_t export_names = image_base +
		expdir.address_of_names;
	const memory_address_t export_ordinals = image_base +
		expdir.address_of_name_ordinals;

	/* Go through every function address */
	for (uint32_t i = 0, ordinal = expdir.base;
			i < expdir.number_of_functions; ++i, ++ordinal) {
		uint8_t read_buffer[4];

		/* Read the address */
		if (!MEMOP_OK(virtual_memory_host_read(memory, export_address + i * 4,
				read_buffer, sizeof(read_buffer)))) {
			Log::error("can't read export address\n");
			return false;
		}
		const uint32_t current_export_address = read_u32le(read_buffer);

		/* If it's inside the .edata section, then it is a forwarder RVA
		   without a real implementation. */
		if (current_export_address >= export_data_start &&
				current_export_address < export_data_end) {
			Log::error("sorry, DLLs with forwarded functions are not "
				"supported\n");
			return false;
		}

		/*
		 * Find the name of the export. For this, we iterate the ordinal table
		 * to find the ordinal of the current function. If we found it, we can
		 * use the determined index into the ordinal table for the name table
		 * as well.
		 */
		memory_address_t current_export_name = 0;
		for (uint32_t j = 0; j < expdir.number_of_names; ++j) {
			/* Read the ordinal */
			if (!MEMOP_OK(virtual_memory_host_read(memory,
					export_ordinals + j * 2, read_buffer, 2))) {
				Log::error("can't read export ordinal\n");
				return false;
			}
			const uint16_t name_ordinal = read_u16le(read_buffer);

			/* Found it? */
			if (name_ordinal == i) {
				current_export_name = export_names + 4 * j;
				break;
			}
		}

		/* Allocate export info if not done before */
		if (!_export_info)
			_export_info = new _ExportInfo;

		/* Track all exports by ordinal */
		_export_info->insert(ordinal, current_export_address);

		/* Does it have a name? */
		if (current_export_name) {
			/* Dereference the address in the name array */
			if (!MEMOP_OK(virtual_memory_host_read(memory,
					current_export_name, read_buffer, sizeof(read_buffer)))) {
				Log::error("can't read export name RVA\n");
				return false;
			}
			const uint32_t export_name_rva = read_u32le(read_buffer);

			/* Read the function name */
			char *export_name;
			if (!MEMOP_OK(virtual_memory_host_read_string_alloc(memory,
					image_base + export_name_rva, &export_name))) {
				Log::error("can't read export name\n");
				return false;
			}

			/* Track the export by name. The function name is copied. */
			_export_info->insert(export_name, current_export_address);

#ifdef DEBUG_PE_EXPORTS
			Log::trace("ordinal %3u: 0x%X '%s'\n", ordinal,
				current_export_address, export_name);
#endif

			free((void *)export_name);
		}
#ifdef DEBUG_PE_EXPORTS
		else {
			Log::trace("ordinal %3u: 0x%X <nameless>\n", ordinal,
				current_export_address);
		}
#endif
	}

	/* Sort exports internally for faster lookup */
	_export_info->sort();
	return true;
}

bool PEExecutable::resolve_imports(struct VirtualMemoryContext *memory)
{
	/* Must be mapped (_memory_end_offset is known) */
	if (!_memory_end_offset) {
		Log::error("executable not mapped\n");
		return false;
	}

	/* Do we have an import directory? If not, there's nothing to do */
	const PEDataDirectory *import_datadir =
		&_optional_header.directory_entry_import;

	if (!import_datadir->virtual_address || !import_datadir->size)
		return true;

	/* Have we resolved the imports already? */
	if (_imports_resolved)
		return true;

	/* Read import directory descriptors until we get a NULL one */
	const memory_address_t image_base = get_image_base();
	memory_address_t import_descriptor_offset = image_base +
		import_datadir->virtual_address;

	for (;;) {
		union {
			uint8_t impdesc[EXEFMT_PE_IMPORT_DESCRIPTOR_SIZE];
			uint8_t iat_address[sizeof(memory_address_t)];
			uint8_t u16[2];
		} read_buffer;
		PEImportDescriptor impdesc;

		/* Read the import descriptor */
		if (!MEMOP_OK(virtual_memory_host_read(memory,
				import_descriptor_offset, read_buffer.impdesc,
				sizeof(read_buffer.impdesc))) ||
			!pe_import_descriptor_read(read_buffer.impdesc,
				sizeof(read_buffer.impdesc), &impdesc)) {
			Log::error("descriptor read failed\n");
			return false;
		}

		/* If all entries are 0, then we've processed all import descriptors */
		if (!impdesc.original_first_thunk && !impdesc.time_date_stamp &&
				!impdesc.forwarder_chain && !impdesc.name &&
				!impdesc.first_thunk) {
#ifdef DEBUG_PE_IMPORTS
			Log::trace("all imports processed\n");
#endif
			break;
		}

		// XXX: not all variations supported
        assert(impdesc.original_first_thunk != 0x0);
		assert(impdesc.forwarder_chain == 0x0);
        assert(impdesc.name != 0x0);
        assert(impdesc.first_thunk != 0x0);

		/* Read the module name */
		char *module_name;
		if (!MEMOP_OK(virtual_memory_host_read_string_alloc(memory,
				image_base + impdesc.name, &module_name)))
			return false;

		/* Call the callback to act on the module name */
		if (_import_module_callback) {
			if (!_import_module_callback(memory, module_name,
					_import_module_callback_user)) {
				Log::error("user callback failure\n");
				return false;
			}
		}

#ifdef DEBUG_PE_IMPORTS
		Log::trace("Module name: '%s'\n", module_name);
		pe_import_descriptor_print(&impdesc);
#endif

		/*
		 * Process the import address table (IAT) of the current module. It's
		 * made up of 32/64-bit pointers to the function name, which is
		 * replaced by the actual pointer corresponding to it.
		 */
		memory_address_t actual_iat_pointer = image_base + impdesc.first_thunk;
		memory_address_t original_iat_pointer = image_base +
			impdesc.original_first_thunk;

		for (;;) {
			/*
			 * Always read from the original IAT, which points to the strings,
			 * not the function pointers.
			 * Applications processed with bind.exe from the Win32 SDK have
			 * the offsets for their OS hardcoded in the IAT. If the version
			 * matches, then this module doesn't need to be processed. But we
			 * can't check that easily, so always go the hard route.
			 */
			if (!MEMOP_OK(virtual_memory_host_read(memory,
					original_iat_pointer, read_buffer.iat_address,
					sizeof(read_buffer.iat_address)))) {
				Log::error("reading IAT pointer 0x%" PRIXMA " failed\n",
					original_iat_pointer);
				return false;
			}

			/* Convert to 32-bit/64-bit integer */
			memory_address_t iat_name_pointer =
#ifdef CONFIG_LOADER_64BIT
				read_u64le(read_buffer.iat_address);
#else
				read_u32le(read_buffer.iat_address);
#endif

			/* Pointer is NULL -> IAT processed */
			if (!iat_name_pointer)
				break;

			/* This is a RVA */
			iat_name_pointer += image_base;
#ifdef DEBUG_PE_IMPORTS
			Log::trace("IAT string entry: 0x%" PRIXMA "\n", iat_name_pointer);
#endif

			/* Read 2-byte hint */
			if (!MEMOP_OK(virtual_memory_host_read(memory,
					iat_name_pointer, read_buffer.u16, 2))) {
				Log::error("IAT entry 0x%" PRIXMA " failure\n",
					iat_name_pointer);
				return false;
			}
			const uint16_t hint = read_u16le(read_buffer.u16);

			memory_address_t resolved_address = 0u;

			/* Topmost bit set = import by ordinal */
			if (hint & 0x8000) {
				// XXX: not supported
				Log::error("Import of ordinal-only functions not yet "
					"supported\n");
				return false;
			}

			/* Otherwise it's an import by name */
			else {
				/* Read the string */
				char *import_name;
				if (!MEMOP_OK(virtual_memory_host_read_string_alloc(memory,
						iat_name_pointer + 2, &import_name)))
					return false;

#ifdef DEBUG_PE_IMPORTS
				Log::trace(" - Name: '%s'\n", import_name);
#endif

				/* Call the callback so it can resolve the import */
				if (_import_function_callback) {
					if (!_import_function_callback(memory, module_name,
							import_name, resolved_address,
							_import_module_callback_user)) {
						Log::error("user callback failure\n");
						return false;
					}
				}

				free((void *)import_name);
			}

#ifdef DEBUG_PE_IMPORTS
			Log::trace(" - Resolved address: 0x%" PRIXMA "\n",
				resolved_address);
#endif

			/* Write resolved function pointer back */
#ifdef CONFIG_LOADER_64BIT
			write_u64le(read_buffer.iat_address, resolved_address);
#else
			write_u32le(read_buffer.iat_address, resolved_address);
#endif

			/* Always write to the actual IAT, not the original IAT */
			if (!MEMOP_OK(virtual_memory_host_write(memory,
					actual_iat_pointer, read_buffer.iat_address,
					sizeof(read_buffer.iat_address)))) {
				Log::error("writing to IAT pointer 0x%" PRIXMA " failed\n",
					actual_iat_pointer);
				return false;
			}

#ifdef DEBUG_PE_IMPORTS
			Log::trace(" - Wrote address to: 0x%" PRIXMA "\n",
				actual_iat_pointer);
#endif

			actual_iat_pointer += sizeof(read_buffer.iat_address);
			original_iat_pointer += sizeof(read_buffer.iat_address);
		}

		free((void *)module_name);
		import_descriptor_offset += EXEFMT_PE_IMPORT_DESCRIPTOR_SIZE;
	}

	_imports_resolved = true;
	return true;
}

void PEExecutable::set_import_module_callback(
	PEExecutable::ImportModuleCallback callback, void *user,
	PEExecutable::ImportModuleCallback *old_callback, void **old_user)
{
	if (old_callback)
		*old_callback = _import_module_callback;
	_import_module_callback = callback;

	if (old_user)
		*old_user = _import_module_callback_user;
	_import_module_callback_user = user;
}

void PEExecutable::set_import_function_callback(
	PEExecutable::ImportFunctionCallback callback, void *user,
	PEExecutable::ImportFunctionCallback *old_callback, void **old_user)
{
	if (old_callback)
		*old_callback = _import_function_callback;
	_import_function_callback = callback;

	if (old_user)
		*old_user = _import_function_callback_user;
	_import_function_callback_user = user;
}

memory_address_t PEExecutable::get_exported_symbol_address(uint16_t ordinal)
{
	if (_export_info) {
		uint32_t rva;
		if (_export_info->find(ordinal, rva))
			return (memory_address_t)(_optional_header.image_base + rva);
	}
	return 0;
}

memory_address_t PEExecutable::get_exported_symbol_address(const char *name)
{
	if (_export_info) {
		uint32_t rva;
		if (_export_info->find(name, rva))
			return (memory_address_t)(_optional_header.image_base + rva);
	}
	return 0;
}


/*
 * Private methods
 */

#ifdef _WIN32
#define _WIN32_LEAN_AND_MEAN
#include <windows.h>

static bool _get_full_path(const char *path, char *full_path,
	size_t full_path_size)
{
	return GetFullPathNameA(path, full_path_size, full_path, NULL) > 0;
}
#else
#error implement for UNIX
#endif
