/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef EXEFMT_PESTRUCT_H
#define EXEFMT_PESTRUCT_H

/*!
 * \file src/exefmt/pestruct.h
 * \brief Functions to unpack PE executable structures
 */

#include "../portab/stdint.h"
#include "../portab/stdbool.h"
#include <stddef.h>


#ifdef __cplusplus
extern "C" {
#endif


/*
 * IMAGE_FILE_HEADER
 */

#define EXEFMT_PE_FILE_HEADER_SIZE 0x14u

struct PEFileHeader
{
    uint16_t machine;
    uint16_t number_of_sections;
    uint32_t time_date_stamp;
    uint32_t pointer_to_symbol_table;
    uint32_t number_of_symbols;
    uint16_t size_of_optional_header;
    uint16_t characteristics;
};

bool pe_file_header_read(const unsigned char *const data,
    const size_t size, struct PEFileHeader *const header);
void pe_file_header_print(const struct PEFileHeader *const header);


/*
 * IMAGE_DATA_DIRECTORY
 */

#define EXEFMT_PE_DATA_DIRECTORY_SIZE 0x8u

struct PEDataDirectory
{
    uint32_t virtual_address;
    uint32_t size;
};

bool pe_data_directory_read(const unsigned char *const data,
    const size_t size, struct PEDataDirectory *const header);
void pe_data_directory_print(const struct PEDataDirectory *const header);


/*
 * IMAGE_OPTIONAL_HEADER{,64} with DataDirectory
 */

#define EXEFMT_PE_OPTIONAL_HEADER32_SIZE \
    (0x60u + 16u * EXEFMT_PE_DATA_DIRECTORY_SIZE)
#define EXEFMT_PE_OPTIONAL_HEADER64_SIZE \
    (0x70u + 16u * EXEFMT_PE_DATA_DIRECTORY_SIZE)
#define EXEFMT_PE_OPTIONAL_HEADER_SIZE \
    EXEFMT_PE_OPTIONAL_HEADER64_SIZE

struct PEOptionalHeader
{
    uint16_t magic;
    uint8_t major_linker_version;
    uint8_t minor_linker_version;
    uint32_t size_of_code;
    uint32_t size_of_initialized_data;
    uint32_t size_of_uninitialized_data;
    uint32_t address_of_entry_point;
    uint32_t base_of_code;
    uint32_t base_of_data;  /* PE32 only */
    uint64_t image_base;
    uint32_t section_alignment;
    uint32_t file_alignment;
    uint16_t major_operating_system_version;
    uint16_t minor_operating_system_version;
    uint16_t major_image_version;
    uint16_t minor_image_version;
    uint16_t major_subsystem_version;
    uint16_t minor_subsystem_version;
    uint32_t win32_version_value;
    uint32_t size_of_image;
    uint32_t size_of_headers;
    uint32_t checksum;
    uint16_t subsystem;
    uint16_t dll_characteristics;
    uint64_t size_of_stack_reserve;
    uint64_t size_of_stack_commit;
    uint64_t size_of_heap_reserve;
    uint64_t size_of_heap_commit;
    uint32_t loader_flags;
    uint32_t number_of_rva_and_sizes;
    struct PEDataDirectory directory_entry_export;
    struct PEDataDirectory directory_entry_import;
    struct PEDataDirectory directory_entry_resource;
    struct PEDataDirectory directory_entry_exception;
    struct PEDataDirectory directory_entry_security;
    struct PEDataDirectory directory_entry_basereloc;
    struct PEDataDirectory directory_entry_debug;
    struct PEDataDirectory directory_entry_architecture;
    struct PEDataDirectory directory_entry_globalptr;
    struct PEDataDirectory directory_entry_tls;
    struct PEDataDirectory directory_entry_load_config;
    struct PEDataDirectory directory_entry_bound_import;
    struct PEDataDirectory directory_entry_iat;
    struct PEDataDirectory directory_entry_delay_import;
    struct PEDataDirectory directory_entry_com_descriptor;
};

bool pe_optional_header_read(const unsigned char *const data,
    const size_t size, struct PEOptionalHeader *const header);
void pe_optional_header_print(const struct PEOptionalHeader *const header);


/*
 * IMAGE_SECTION_HEADER
 */

#define EXEFMT_PE_SECTION_HEADER_SIZE 0x28u

struct PESectionHeader
{
    char name[8];
    uint32_t physical_address;
    uint32_t virtual_size;
    uint32_t virtual_address;
    uint32_t size_of_raw_data;
    uint32_t pointer_to_raw_data;
    uint32_t pointer_to_relocations;
    uint32_t pointer_to_linenumbers;
    uint16_t number_of_relocations;
    uint16_t number_of_linenumbers;
    uint32_t characteristics;
};

bool pe_section_header_read(const unsigned char *const data,
    const size_t size, struct PESectionHeader *const header);
void pe_section_header_print(const struct PESectionHeader *const header);


/*
 * IMAGE_IMPORT_DESCRIPTOR
 */

#define EXEFMT_PE_IMPORT_DESCRIPTOR_SIZE 0x14u

struct PEImportDescriptor
{
    uint32_t characteristics;
    uint32_t original_first_thunk;
    uint32_t time_date_stamp;
    uint32_t forwarder_chain;
    uint32_t name;
    uint32_t first_thunk;
};

bool pe_import_descriptor_read(const unsigned char *const data,
    const size_t size, struct PEImportDescriptor *const header);
void pe_import_descriptor_print(const struct PEImportDescriptor *const header);


/*
 * IMAGE_EXPORT_DIRECTORY
 */

#define EXEFMT_PE_EXPORT_DIRECTORY_SIZE 0x28u

struct PEExportDirectory
{
    uint32_t characteristics;
    uint32_t time_date_stamp;
    uint16_t major_version;
    uint16_t minor_version;
    uint32_t name;
    uint32_t base;
    uint32_t number_of_functions;
    uint32_t number_of_names;
    uint32_t address_of_functions;
    uint32_t address_of_names;
    uint32_t address_of_name_ordinals;
};

bool pe_export_directory_read(const unsigned char *const data,
    const size_t size, struct PEExportDirectory *const header);
void pe_export_directory_print(const struct PEExportDirectory *const header);


#ifdef __cplusplus
}
#endif

#endif
