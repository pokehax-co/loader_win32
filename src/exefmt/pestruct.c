/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "../config.h"
#include "pestruct.h"

#include "../misc/tools.h"

#include <stdio.h>
#include <string.h>


/*
 * Macro to load an IMAGE_DATA_DIRECTORY
 */
#define LOAD_DATA_DIRECTORY(e, o) \
    pe_data_directory_read(data + data_directory + \
        o * EXEFMT_PE_DATA_DIRECTORY_SIZE, EXEFMT_PE_DATA_DIRECTORY_SIZE, \
        &(e))


/*
 * IMAGE_FILE_HEADER
 */

bool pe_file_header_read(const unsigned char *const data,
    const size_t size, struct PEFileHeader *const header)
{
    if (!data || !header || size < EXEFMT_PE_FILE_HEADER_SIZE)
        return false;

    header->machine = read_u16le(data + 0x0);
    header->number_of_sections = read_u16le(data + 0x2);
    header->time_date_stamp = read_u32le(data + 0x4);
    header->pointer_to_symbol_table = read_u32le(data + 0x8);
    header->number_of_symbols = read_u32le(data + 0xC);
    header->size_of_optional_header = read_u16le(data + 0x10);
    header->characteristics = read_u16le(data + 0x12);
    return true;
}

void pe_file_header_print(const struct PEFileHeader *const header)
{
#ifdef DEBUG_PE_PRINT_STRUCTURES
    printf("IMAGE_FILE_HEADER:\n");
    printf(" - Machine: 0x%04X\n", header->machine);
    printf(" - NumberOfSections: %u\n", header->number_of_sections);
    printf(" - TimeDateStamp: %u\n", header->time_date_stamp);
    printf(" - PointerToSymbolTable: 0x%X\n", header->pointer_to_symbol_table);
    printf(" - NumberOfSymbols: %u\n", header->number_of_symbols);
    printf(" - SizeOfOptionalHeader: 0x%X (%u) bytes\n",
        header->size_of_optional_header, header->size_of_optional_header);
    printf(" - Characteristics: 0x%X\n", header->characteristics);
#else
    (void)header;
#endif
}


/*
 * IMAGE_DATA_DIRECTORY
 */

bool pe_data_directory_read(const unsigned char *const data,
    const size_t size, struct PEDataDirectory *const header)
{
    if (!data || !header || size < EXEFMT_PE_DATA_DIRECTORY_SIZE)
        return false;

    header->virtual_address = read_u32le(data + 0x0);
    header->size = read_u32le(data + 0x4);
    return true;
}

void pe_data_directory_print(const struct PEDataDirectory *const header)
{
#ifdef DEBUG_PE_PRINT_STRUCTURES
    printf("IMAGE_DATA_DIRECTORY:\n");
    printf(" - VirtualAddress: 0x%X\n", header->virtual_address);
    printf(" - Size: 0x%X (%u)\n", header->size, header->size);
#else
    (void)header;
#endif
}


/*
 * IMAGE_OPTIONAL_HEADER{,64} with DataDirectory
 */

bool pe_optional_header_read(const unsigned char *const data,
    const size_t size, struct PEOptionalHeader *const header)
{
    bool is_pe32plus;
    unsigned int data_directory;

    if (!data || !header || size < 2)
        return false;

    /* Read magic first */
    is_pe32plus = (header->magic = read_u16le(data + 0x0)) != 0x10B;

    if (is_pe32plus) {
        if (size < EXEFMT_PE_OPTIONAL_HEADER64_SIZE)
            return false;
    } else {
        if (size < EXEFMT_PE_OPTIONAL_HEADER32_SIZE)
            return false;
    }

    header->major_linker_version = data[0x2];
    header->minor_linker_version = data[0x3];
    header->size_of_code = read_u32le(data + 0x4);
    header->size_of_initialized_data = read_u32le(data + 0x8);
    header->size_of_uninitialized_data = read_u32le(data + 0xC);
    header->address_of_entry_point = read_u32le(data + 0x10);
    header->base_of_code = read_u32le(data + 0x14);

    if (is_pe32plus) {
        header->base_of_data = UINT32_MAX;  /* PE32 only */
        header->image_base = read_u64le(data + 0x18);
    } else {
        header->base_of_data = read_u32le(data + 0x18);
        header->image_base = read_u32le(data + 0x1C);
    }

    header->section_alignment = read_u32le(data + 0x20);
    header->file_alignment = read_u32le(data + 0x24);
    header->major_operating_system_version = read_u16le(data + 0x28);
    header->minor_operating_system_version = read_u16le(data + 0x2A);
    header->major_image_version = read_u16le(data + 0x2C);
    header->minor_image_version = read_u16le(data + 0x2E);
    header->major_subsystem_version = read_u16le(data + 0x30);
    header->minor_subsystem_version = read_u16le(data + 0x32);
    header->win32_version_value = read_u32le(data + 0x34);
    header->size_of_image = read_u32le(data + 0x38);
    header->size_of_headers = read_u32le(data + 0x3C);
    header->checksum = read_u32le(data + 0x40);
    header->subsystem = read_u16le(data + 0x44);
    header->dll_characteristics = read_u16le(data + 0x46);

    if (is_pe32plus) {
        header->size_of_stack_reserve = read_u64le(data + 0x48);
        header->size_of_stack_commit = read_u64le(data + 0x50);
        header->size_of_heap_reserve = read_u64le(data + 0x58);
        header->size_of_heap_commit = read_u64le(data + 0x60);
        header->loader_flags = read_u32le(data + 0x68);
        header->number_of_rva_and_sizes = read_u32le(data + 0x6C);
        data_directory = 0x70u;
    } else {
        header->size_of_stack_reserve = read_u32le(data + 0x48);
        header->size_of_stack_commit = read_u32le(data + 0x4C);
        header->size_of_heap_reserve = read_u32le(data + 0x50);
        header->size_of_heap_commit = read_u32le(data + 0x54);
        header->loader_flags = read_u32le(data + 0x58);
        header->number_of_rva_and_sizes = read_u32le(data + 0x5C);
        data_directory = 0x60u;
    }

    /* Load data directories */
    LOAD_DATA_DIRECTORY(header->directory_entry_export, 0);
    LOAD_DATA_DIRECTORY(header->directory_entry_import, 1);
    LOAD_DATA_DIRECTORY(header->directory_entry_resource, 2);
    LOAD_DATA_DIRECTORY(header->directory_entry_exception, 3);
    LOAD_DATA_DIRECTORY(header->directory_entry_security, 4);
    LOAD_DATA_DIRECTORY(header->directory_entry_basereloc, 5);
    LOAD_DATA_DIRECTORY(header->directory_entry_debug, 6);
    LOAD_DATA_DIRECTORY(header->directory_entry_architecture, 7);
    LOAD_DATA_DIRECTORY(header->directory_entry_globalptr, 8);
    LOAD_DATA_DIRECTORY(header->directory_entry_tls, 9);
    LOAD_DATA_DIRECTORY(header->directory_entry_load_config, 10);
    LOAD_DATA_DIRECTORY(header->directory_entry_bound_import, 11);
    LOAD_DATA_DIRECTORY(header->directory_entry_iat, 12);
    LOAD_DATA_DIRECTORY(header->directory_entry_delay_import, 13);
    LOAD_DATA_DIRECTORY(header->directory_entry_com_descriptor, 14);

    return true;
}

void pe_optional_header_print(const struct PEOptionalHeader *const header)
{
#ifdef DEBUG_PE_PRINT_STRUCTURES
    printf("IMAGE_OPTIONAL_HEADER%u:\n", header->magic == 0x10B ? 32 : 64);
    printf(" - Magic: 0x%X\n", header->magic);
    printf(" - MajorLinkerVersion: %u\n", header->major_linker_version);
    printf(" - MinorLinkerVersion: %u\n", header->minor_linker_version);
    printf(" - SizeOfCode: 0x%X (%u)\n", header->size_of_code,
        header->size_of_code);
    printf(" - SizeOfInitializedData: 0x%X (%u)\n",
        header->size_of_initialized_data,
        header->size_of_initialized_data);
    printf(" - SizeOfUninitializedData: 0x%X (%u)\n",
        header->size_of_uninitialized_data,
        header->size_of_uninitialized_data);
    printf(" - AddressOfEntryPoint: 0x%X\n", header->address_of_entry_point);
    printf(" - BaseOfCode: 0x%X\n", header->base_of_code);
    printf(" - BaseOfData: 0x%X\n", header->base_of_data);
    printf(" - ImageBase: 0x%I64X\n", header->image_base);
    printf(" - SectionAlignment: %u\n", header->section_alignment);
    printf(" - FileAlignment: %u\n", header->file_alignment);
    printf(" - MajorOperatingSystemVersion: %u\n",
        header->major_operating_system_version);
    printf(" - MinorOperatingSystemVersion: %u\n",
        header->minor_operating_system_version);
    printf(" - MajorImageVersion: %u\n", header->major_image_version);
    printf(" - MinorImageVersion: %u\n", header->minor_image_version);
    printf(" - MajorSubsystemVersion: %u\n", header->major_subsystem_version);
    printf(" - MinorSubsystemVersion: %u\n", header->minor_subsystem_version);
    printf(" - Win32VersionValue: %u\n", header->win32_version_value);
    printf(" - SizeOfImage: 0x%X (%u)\n", header->size_of_image,
        header->size_of_image);
    printf(" - SizeOfHeaders: 0x%X (%u)\n", header->size_of_headers,
        header->size_of_headers);
    printf(" - CheckSum: 0x%X\n", header->checksum);
    printf(" - Subsystem: %u\n", header->subsystem);
    printf(" - DllCharacteristics: 0x%X\n", header->dll_characteristics);
    printf(" - SizeOfStackReserve: 0x%I64x (%I64u)\n",
        header->size_of_stack_reserve, header->size_of_stack_reserve);
    printf(" - SizeOfStackCommit: 0x%I64x (%I64u)\n",
        header->size_of_stack_commit, header->size_of_stack_commit);
    printf(" - SizeOfHeapReserve: 0x%I64x (%I64u)\n",
        header->size_of_heap_reserve, header->size_of_heap_reserve);
    printf(" - SizeOfHeapCommit: 0x%I64x (%I64u)\n",
        header->size_of_heap_commit, header->size_of_heap_commit);
    printf(" - LoaderFlags: 0x%x\n", header->loader_flags);
    printf(" - NumberOfRvaAndSizes: %u\n", header->number_of_rva_and_sizes);

    printf("IMAGE_directory_entry_export: ");
    pe_data_directory_print(&header->directory_entry_export);

    printf("IMAGE_directory_entry_import: ");
    pe_data_directory_print(&header->directory_entry_import);

    printf("IMAGE_directory_entry_resource: ");
    pe_data_directory_print(&header->directory_entry_resource);

    printf("IMAGE_directory_entry_exception: ");
    pe_data_directory_print(&header->directory_entry_exception);

    printf("IMAGE_directory_entry_security: ");
    pe_data_directory_print(&header->directory_entry_security);

    printf("IMAGE_directory_entry_basereloc: ");
    pe_data_directory_print(&header->directory_entry_basereloc);

    printf("IMAGE_directory_entry_debug: ");
    pe_data_directory_print(&header->directory_entry_debug);

    printf("IMAGE_directory_entry_architecture: ");
    pe_data_directory_print(&header->directory_entry_architecture);

    printf("IMAGE_directory_entry_globalptr: ");
    pe_data_directory_print(&header->directory_entry_globalptr);

    printf("IMAGE_directory_entry_tls: ");
    pe_data_directory_print(&header->directory_entry_tls);

    printf("IMAGE_directory_entry_load_config: ");
    pe_data_directory_print(&header->directory_entry_load_config);

    printf("IMAGE_directory_entry_bound_import: ");
    pe_data_directory_print(&header->directory_entry_bound_import);

    printf("IMAGE_directory_entry_iat: ");
    pe_data_directory_print(&header->directory_entry_iat);

    printf("IMAGE_directory_entry_delay_import: ");
    pe_data_directory_print(&header->directory_entry_delay_import);

    printf("IMAGE_directory_entry_com_descriptor: ");
    pe_data_directory_print(&header->directory_entry_com_descriptor);
#else
    (void)header;
#endif
}


/*
 * IMAGE_SECTION_HEADER
 */

bool pe_section_header_read(const unsigned char *const data,
    const size_t size, struct PESectionHeader *const header)
{
    if (!data || !header || size < EXEFMT_PE_SECTION_HEADER_SIZE)
        return false;

    memcpy(header->name, data, 8);
    header->physical_address = header->virtual_size = read_u32le(data + 0x8);
    header->virtual_address = read_u32le(data + 0xC);
    header->size_of_raw_data = read_u32le(data + 0x10);
    header->pointer_to_raw_data = read_u32le(data + 0x14);
    header->pointer_to_relocations = read_u32le(data + 0x18);
    header->pointer_to_linenumbers = read_u32le(data + 0x1C);
    header->number_of_relocations = read_u16le(data + 0x20);
    header->number_of_linenumbers = read_u16le(data + 0x22);
    header->characteristics = read_u32le(data + 0x24);
    return true;
}

void pe_section_header_print(const struct PESectionHeader *const header)
{
#ifdef DEBUG_PE_PRINT_STRUCTURES
    const uint32_t supported_flags = 0xE2000060u;

    printf("IMAGE_SECTION_HEADER:\n");
    printf(" - Name: '%.8s'\n", header->name);
    printf(" - PhysicalAddress: 0x%X\n", header->physical_address);
    printf(" - VirtualSize: 0x%X (%u)\n", header->virtual_size,
        header->virtual_size);
    printf(" - VirtualAddress: 0x%X\n", header->virtual_address);
    printf(" - SizeOfRawData: 0x%X (%u)\n", header->size_of_raw_data,
        header->size_of_raw_data);
    printf(" - PointerToRawData: 0x%X\n", header->pointer_to_raw_data);
    printf(" - PointerToRelocations: 0x%X\n", header->pointer_to_relocations);
    printf(" - PointerToLinenumbers: 0x%X\n", header->pointer_to_linenumbers);
    printf(" - NumberOfRelocations: %u\n", header->number_of_relocations);
    printf(" - NumberOfLinenumbers: %u\n", header->number_of_linenumbers);
    printf(" - Characteristics: 0x%X\n", header->characteristics);

    if (header->characteristics & 0x20u)    /* IMAGE_SCN_CNT_CODE */
        printf("   * contains code\n");
    if (header->characteristics & 0x40u)    /* IMAGE_SCN_CNT_INITIALIZED_DATA */
        printf("   * contains initialized data\n");
    if (header->characteristics & 0x2000000u)   /* IMAGE_SCN_MEM_DISCARDABLE */
        printf("   * can be discarded\n");
    if (header->characteristics & 0x20000000u)  /* IMAGE_SCN_MEM_EXECUTE */
        printf("   * EXECutable\n");
    if (header->characteristics & 0x40000000u)  /* IMAGE_SCN_MEM_READ */
        printf("   * READable\n");
    if (header->characteristics & 0x80000000u)  /* IMAGE_SCN_MEM_WRITE */
        printf("   * WRITEable\n");

    if (header->characteristics & ~supported_flags) {
        printf("   * UNKNOWN? 0x%08X\n",
            header->characteristics & ~supported_flags);
    }
#else
    (void)header;
#endif
}


/*
 * IMAGE_IMPORT_DESCRIPTOR
 */

bool pe_import_descriptor_read(const unsigned char *const data,
    const size_t size, struct PEImportDescriptor *const header)
{
    if (!data || !header || size < EXEFMT_PE_IMPORT_DESCRIPTOR_SIZE)
        return false;

    header->characteristics = header->original_first_thunk =
        read_u32le(data + 0x0);
    header->time_date_stamp = read_u32le(data + 0x4);
    header->forwarder_chain = read_u32le(data + 0x8);
    header->name = read_u32le(data + 0xC);
    header->first_thunk = read_u32le(data + 0x10);
    return true;
}

void pe_import_descriptor_print(const struct PEImportDescriptor *const header)
{
#ifdef DEBUG_PE_PRINT_STRUCTURES
    printf("IMAGE_IMPORT_DESCRIPTOR:\n");
    printf(" - Characteristics: 0x%X\n", header->characteristics);
    printf(" - OriginalFirstThunk: 0x%X\n", header->original_first_thunk);
    printf(" - TimeDateStamp: 0x%X\n", header->time_date_stamp);
    printf(" - ForwarderChain: 0x%X\n", header->forwarder_chain);
    printf(" - Name: 0x%X\n", header->name);
    printf(" - FirstThunk: 0x%X\n", header->first_thunk);
#else
    (void)header;
#endif
}


/*
 * IMAGE_EXPORT_DIRECTORY
 */

bool pe_export_directory_read(const unsigned char *const data,
    const size_t size, struct PEExportDirectory *const header)
{
    if (!data || !header || size < EXEFMT_PE_EXPORT_DIRECTORY_SIZE)
        return false;

    header->characteristics = read_u32le(data + 0x0);
    header->time_date_stamp = read_u32le(data + 0x4);
    header->major_version = read_u16le(data + 0x8);
    header->minor_version = read_u16le(data + 0xA);
    header->name = read_u32le(data + 0xC);
    header->base = read_u32le(data + 0x10);
    header->number_of_functions = read_u32le(data + 0x14);
    header->number_of_names = read_u32le(data + 0x18);
    header->address_of_functions = read_u32le(data + 0x1C);
    header->address_of_names = read_u32le(data + 0x20);
    header->address_of_name_ordinals = read_u32le(data + 0x24);
    return true;
}

void pe_export_directory_print(const struct PEExportDirectory *const header)
{
#ifdef DEBUG_PE_PRINT_STRUCTURES
    printf("IMAGE_EXPORT_DIRECTORY:\n");
    printf(" - Characteristics: 0x%X\n", header->characteristics);
    printf(" - TimeDateStamp: 0x%X\n", header->time_date_stamp);
    printf(" - MajorVersion: 0x%X\n", header->major_version);
    printf(" - MinorVersion: 0x%X\n", header->minor_version);
    printf(" - Name: 0x%X\n", header->name);
    printf(" - Base: 0x%X\n", header->base);
    printf(" - NumberOfFunctions: 0x%X\n", header->number_of_functions);
    printf(" - NumberOfNames: 0x%X\n", header->number_of_names);
    printf(" - AddressOfFunctions: 0x%X\n", header->address_of_functions);
    printf(" - AddressOfNames: 0x%X\n", header->address_of_names);
    printf(" - AddressOfNameOrdinals: 0x%X\n",
        header->address_of_name_ordinals);
#else
    (void)header;
#endif
}
