/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

/*
 * Entry point of the emulator.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>

#include "misc/log.hpp"
#include "misc/array.hpp"
#include "portab/getopt.h"
#include "exefmt/pe.hpp"
#include "os/win32.hpp"


/* Global instance of the Win32 environment -- there can be only one */
static Win32Environment *_env = NULL;

Win32Environment *get_global_environment(void)
{
	return _env;
}


static int _run(const char *app, int argc, char **argv, char **envp)
{
	/* Probe the executable first */
	if (!PEExecutable::probe(app)) {
		Log::error("%s is not a valid Win32 application.\n", app);
		return 1;
	}

	PEExecutable *pe = new PEExecutable();

	/* Load the executable */
	if (!pe->load(app)) {
		Log::error("Loading %s failed.\n", app);
		delete pe;
		return 1;
	}

	/* Get the headers */
	const PEOptionalHeader *const optional_header = pe->get_optional_header();

	/* Print executable headers */
#ifdef DEBUG_SHOW_EXECUTABLE_HEADERS
	const PEFileHeader *const file_header = pe->get_file_header();
	const PESectionHeader **const sechdrs = pe->get_section_headers();

	pe_file_header_print(file_header);
	pe_optional_header_print(optional_header);
	for (size_t i = 0; i < file_header->number_of_sections; ++i)
		if (sechdrs[i])
			pe_section_header_print(sechdrs[i]);
#endif

	/* Must be a console or a GUI application */
	unsigned short subsystem = optional_header->subsystem;
	if (subsystem != 2 && subsystem != 3) {
		Log::error("%s is not a Windows (console) app\n", argv[0]);
		delete pe;
		return 1;
	}

	/* Create a Win32 environment */
	_env = new Win32Environment(argc, argv, envp);

	/* Load the executable as main process, we'll give up the executable
	   instance */
	if (!_env->load_process(pe)) {
		Log::error("Loading of %s failed\n", argv[0]);
		delete pe;
		return 1;
	}

	/* Run it */
	int exit_code;
	if (!_env->run(exit_code)) {
		Log::error("Running %s failed\n", argv[0]);
		return 1;
	}
	return exit_code;
}

static void usage(const char *const argv0)
{
	fprintf(stderr, "Usage: %s exe [args...]\n", argv0);
}

int main(int argc, char **argv, char **envp)
{
	/* Initialize logging */
	const char *const original_argv0 = argv[0];
	Log::init(original_argv0);

	int option_clean_environment = 0;
	char *option_app = NULL;
	Array<char *> option_environment_vars;

	/* Command-line options */
	static const struct option loader_options[] = {
		{ "help", no_argument, 0, 'h' },
		{ "app", required_argument, 0, 'a' },
		{ "addvar", required_argument, 0, 'e' },
		{ "cleanenv", no_argument, &option_clean_environment, 1 },
		{0, 0, 0, 0}
	};

	for (;;) {
		int option_index = 0;

		/* Parse options */
		int c = getopt_long(argc, argv, "a:e:", loader_options,
			&option_index);
		if (c == -1)
			break;

		switch (c) {
			case 0:
				/* There are no long options without shorthand which do not
				   have a pointer to an option variable */
				break;

			case 'a':
				option_app = optarg;
				break;

			case 'e':
				// FIXME: validate
				option_environment_vars.append(optarg);
				break;

			case 'h':
				usage(original_argv0);
				return 1;

			case '?':
				/* getopt_long() already printed an error message */
				return 1;

			default:
				abort();
		}
	}

	/* If the environment should be inherited, add all environment variables
	   in the envp array. */
	if (!option_clean_environment) {
		for (char **p = envp; *p; ++p)
			option_environment_vars.append(*p);
	}

	/* To act as envp replacement, the vector needs a null pointer */
	option_environment_vars.append(NULL);

	/* No arguments? Depends on --app how we handle this */
	int new_argc = argc - optind;
	char **new_argv = argv + optind;
	if (!new_argc) {
		/* No --app and no non-option arguments -> we're missing something */
		if (!option_app) {
			usage(original_argv0);
			return 1;
		}

		/* No non-option arguments -> use --app as argv */
		else {
			new_argc = 1;
			new_argv = &option_app;
		}
	}

	/*
	 * The executable is located either at the path specified by --app
	 * or by argv[optind] (the first non-option argument).
	 */
	const char *const app_path = option_app ? option_app : argv[optind];

	/* Run so that we always end with a system("pause") for debugging */
	int ret = _run(app_path, new_argc, new_argv, &option_environment_vars[0]);

	if (ret == 0)
		Log::trace("Program terminated successfully.\n");
	else
		Log::error("Program exited with code %d.\n", ret);

	delete _env;
#ifndef NDEBUG
	system("pause");
#endif
	return ret;
}
