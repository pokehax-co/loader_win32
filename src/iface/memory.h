/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef IFACE_MEMORY_H
#define IFACE_MEMORY_H

/*!
 * \file src/iface/memory.h
 * \brief Interface for reading/writing/executing data in memory.
 */

#include "../portab/portab.h"
#include "../portab/stdint.h"

#include <stddef.h>


#ifdef __cplusplus
extern "C" {
#endif


/*!
 * Describes an address in memory. The size of this type determines whether the
 * simulated address space is 32-bit or 64-bit.
 */
#ifdef CONFIG_LOADER_64BIT
typedef uint64_t memory_address_t;
#else
typedef uint32_t memory_address_t;
#endif


/*!
 * printf() format for memory_address_t
 */
#ifdef CONFIG_LOADER_64BIT
#define PRIxMA "I64x"
#define PRIXMA "I64X"
#else
#define PRIxMA "x"
#define PRIXMA "X"
#endif


/*!
 * \enum MemoryStatus
 * \brief Describes the result of a memory operation.
 */
enum MemoryStatus
{
	/*!< Success */
	MEMORY_STATUS_OK = 0,

	/*!< Memory can not be read/written/executed because of protection */
	MEMORY_STATUS_PROTECTION = -1,

	/*!< No memory mapped at location */
	MEMORY_STATUS_NOT_PRESENT = -2,

	/*!< Invalid arguments passed to funcion */
	MEMORY_STATUS_INVALID_ARGUMENT = -3,

	/*!< Out of memory */
	MEMORY_STATUS_NO_MEMORY = -4
};


/*!
 * Use this macro to check whether a memory operation was successful (while the
 * actual type of error on failure is irrelevant).
 */
#define MEMOP_OK(a) ((a) == MEMORY_STATUS_OK)


/*!
 * \typedef MemoryInterfaceRead
 * \brief
 *    Reads 'size' bytes from 'address' into the buffer 'buffer'.
 * \param memory_ctx
 *    Pointer to the context the function operates on
 * \param address
 *    Address to read from
 * \param buffer
 *    Caller-supplied storage to save read data in
 * \param size
 *    Size of the caller-supplied storage
 * \return
 *    A MemoryStatus value
 */
typedef enum MemoryStatus (*MemoryInterfaceRead)(void *memory_ctx,
	memory_address_t address, void *buffer, size_t size);

/*!
 * \typedef MemoryInterfaceExecute
 * \brief
 *    Executes 'size' bytes from 'address' into the buffer 'buffer'.
 * \param memory_ctx
 *    Pointer to the context the function operates on
 * \param address
 *    Address to read from
 * \param buffer
 *    Caller-supplied storage to save read data in
 * \param size
 *    Size of the caller-supplied storage
 * \return
 *    A MemoryStatus value
 */
typedef enum MemoryStatus (*MemoryInterfaceExecute)(void *memory_ctx,
	memory_address_t address, void *buffer, size_t size);

/*!
 * \typedef MemoryInterfaceWrite
 * \brief
 *    Writes 'size' bytes to 'address' from the buffer 'buffer'.
 * \param memory_ctx
 *    Pointer to the context the function operates on
 * \param address
 *    Address to write into
 * \param buffer
 *    Caller-supplied data to write into the address
 * \param size
 *    Size of the caller-supplied data
 * \return
 *    A MemoryStatus value
 */
typedef enum MemoryStatus (*MemoryInterfaceWrite)(void *memory_ctx,
	memory_address_t address, const void *buffer, size_t size);

/*!
 * \struct MemoryInterface
 * \brief
 *    This structure must be provided by implementations of simulated memory.
 * \var read
 *    Pointer to the 'read' function (see MemoryInterfaceRead)
 * \var execute
 *    Pointer to the 'execute' function (see MemoryInterfaceExecute)
 * \var write
 *    Pointer to the 'write' function (see MemoryInterfacewrite)
 */
struct MemoryInterface
{
	MemoryInterfaceRead read;
	MemoryInterfaceExecute execute;
	MemoryInterfaceWrite write;
};


#ifdef __cplusplus
}
#endif

#endif
