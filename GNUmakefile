#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

.SUFFIXES:
.PHONY: all clean check_headers

CFLAGS:=-std=gnu90 -Wall -Wextra -O2
CXXFLAGS:=-std=gnu++98 -Wall -Wextra -O2 -fno-exceptions -fno-rtti
LDFLAGS:=-static -Wl,--image-base=0x3C0000
LIBS:=-lmsvcrt -lntdll

ifeq ($(DEBUG),1)
# Debug builds use the Capstone disassembler
CFLAGS+=-g -D_DEBUG
CXXFLAGS+=-g -D_DEBUG
LDFLAGS+=-g
LIBS:=-lcapstone $(LIBS)
else
CFLAGS+=-s -fomit-frame-pointer -DNDEBUG
CXXFLAGS+=-s -fomit-frame-pointer -DNDEBUG
LDFLAGS+=-s -Wl,-Map,loader_win32.map
endif

OBJS:=\
    src/exefmt/pe.o \
    src/exefmt/pestruct.o \
    src/hw/cpu.o \
    src/hw/powerpc.o \
    src/hw/ppc_fpu.o \
    src/hw/ppc_impl.o \
    src/misc/log.o \
    src/misc/tools.o \
    src/os/advapi32.o \
    src/os/crtdll.o \
    src/os/kernel32.o \
    src/os/ntdll.o \
    src/os/ntppc.o \
    src/os/printf.o \
    src/os/win32.o \
    src/os/win32mod.o \
    src/portab/cxxsup.o \
    src/portab/swprintf.o \
    src/process/virtmem.o \
    src/loader.o

all: loader_win32.exe
clean:
	rm -f loader_win32.exe loader_win32.map $(OBJS) $(OBJS:.o=.d)

check_headers:
	@_curdir="$$(pwd)"; \
	find src/ -type f -name '*.h' -o -name '*.hpp' | \
	while read header; \
	do pushd $$(dirname "$$header") >/dev/null 2>&1; \
	echo -en "#include \"$$_curdir/src/config.h\"\n#include \"$$(basename "$$header")\"\n" | gcc -xc++ -Wall -c - -o /dev/null; \
	popd >/dev/null 2>&1; done

loader_win32.exe: $(OBJS)
	i686-w64-mingw32-g++ $(LDFLAGS) $^ -o $@ $(LIBS)

%.o: %.cpp
	i686-w64-mingw32-g++ $(CXXFLAGS) -MMD -MP -c -o $@ $<

%.o: %.c
	i686-w64-mingw32-gcc $(CFLAGS) -MMD -MP -c -o $@ $<

-include $(OBJS:.o=.d)
