# To-do list

* Move to earlier operating systems and Visual C++ compilers
  - Previously: Windows XP and Visual Studio 2008
  - Currently: Windows 2000 and Visual Studio .NET 2003
  - Future goal: Windows NT 4.0 and Visual C++ 6.0
  - Future goal: Windows NT 4.0 and Visual C++ 4.2
  - Future goal: Windows NT 3.51 and Visual C++ 2.0
* More independence from the CRT (for release builds)
